//---------------------------------------------------------------------------//
//                                                                           //
//  RooFit models for Bs -> Ds h                                             //
//                                                                           //
//  Header file                                                              //
//                                                                           //
//  Authors: Eduardo Rodrigues                                               //
//  Date   : 24 / 05 / 2011                                                  //
//                                                                           //
//---------------------------------------------------------------------------//

#ifndef BS2DSHMODELS_H
#define BS2DSHMODELS_H 1

// STL includes

// ROOT and RooFit includes
#include "RooRealVar.h"
#include "RooStringVar.h"
#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooWorkspace.h"
#include "TString.h"




namespace Bs2DshModels {

  //===============================================================================
  // Background model for Bs->DsPi MDFit.
  //===============================================================================
  RooAbsPdf* build_Bs2DsPi_BKG_MDFitter( RooWorkspace* work,
                                         RooWorkspace* workInt,
                                         std::vector <RooAbsReal*> obs,
                                         std::vector <TString> types,
                                         TString &samplemode,
                                         std::vector<TString> merge,
                                         bool debug);

  //===============================================================================
  // Background mode for Bs->DsK MDFit. .
  //===============================================================================
  RooAbsPdf*  build_Bs2DsK_BKG_MDFitter(RooWorkspace* work, RooWorkspace* workInt,
                                        std::vector <RooAbsReal*> obs,
					std::vector <TString> types,
					TString &samplemode,
					std::vector<TString> merge,
					bool debug = false);


} // end of namespace

//=============================================================================

#endif  // BS2DSHMODELS_H
