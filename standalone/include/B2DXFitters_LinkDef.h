/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#ifdef __CLING__

#pragma link C++ class SharedArrayImp < char> + ;
#pragma link C++ class SharedArrayImp < short> + ;
#pragma link C++ class SharedArrayImp < int> + ;
#pragma link C++ class SharedArrayImp < long> + ;
#pragma link C++ class SharedArrayImp < long long> + ;
#pragma link C++ class SharedArrayImp < unsigned char> + ;
#pragma link C++ class SharedArrayImp < unsigned short> + ;
#pragma link C++ class SharedArrayImp < unsigned int> + ;
#pragma link C++ class SharedArrayImp < unsigned long> + ;
#pragma link C++ class SharedArrayImp < unsigned long long> + ;
#pragma link C++ class SharedArrayImp < float> + ;
#pragma link C++ class SharedArrayImp < double> + ;
#pragma link C++ class SharedArray < char> + ;
#pragma link C++ class SharedArray < short> + ;
#pragma link C++ class SharedArray < int> + ;
#pragma link C++ class SharedArray < long> + ;
#pragma link C++ class SharedArray < long long> + ;
#pragma link C++ class SharedArray < unsigned char> + ;
#pragma link C++ class SharedArray < unsigned short> + ;
#pragma link C++ class SharedArray < unsigned int> + ;
#pragma link C++ class SharedArray < unsigned long> + ;
#pragma link C++ class SharedArray < unsigned long long> + ;
#pragma link C++ class SharedArray < float> + ;
#pragma link C++ class SharedArray < double> + ;

#pragma link C++ namespace Bs2Dsh2011TDAnaModels;
#pragma link C++ function Bs2Dsh2011TDAnaModels::*;
#pragma link C++ namespace GeneralModels;
#pragma link C++ function GeneralModels::*;
#pragma link C++ namespace GeneralUtils;
#pragma link C++ function GeneralUtils::*;

#pragma link C++ class CombBkgPTPdf + ;
#pragma link C++ class CPObservable + ;
#pragma link C++ class DecRateCoeff_Bd + ;
#pragma link C++ class DLLTagCombiner + ;
#pragma link C++ class FitMeTool + ;
#pragma link C++ class HistPID1D + ;
#pragma link C++ class HistPID2D + ;
#pragma link C++ class Inverse + ;
#pragma link C++ class MCBackground + ;
#pragma link C++ class MDFitterSettings + ;
#pragma link C++ class MistagCalibration + ;
#pragma link C++ class MistagDistribution + ;
#pragma link C++ class PIDCalibrationSample + ;
#pragma link C++ class PlotSettings + ;
#pragma link C++ class RangeAcceptance + ;
#pragma link C++ class RooAbsGaussModelEfficiency + ;
#pragma link C++ class RooApollonios + ;
#pragma link C++ class RooBinned1DQuintic + ;
#pragma link C++ class RooBinned1DQuinticPdf + ;
#pragma link C++ class RooBinned2DBicubic + ;
#pragma link C++ class RooBinned2DBicubicPdf + ;
#pragma link C++ class RooBinnedFun + ;
#pragma link C++ class RooBinnedPdf + ;
#pragma link C++ class RooComplementCoef + ;
#pragma link C++ class RooCubicSplineFun + ;
#pragma link C++ class RooCubicSplineKnot + ;
#pragma link C++ class RooCubicSplinePdf + ;
#pragma link C++ class RooEffConvGenContext + ;
#pragma link C++ class RooEffHistProd + ;
#pragma link C++ class RooEffResModel + ;
#pragma link C++ class RooGaussEfficiencyModel + ;
#pragma link C++ class RooHILLdini + ;
#pragma link C++ class RooHORNSdini + ;
#pragma link C++ class RooIpatia2 + ;
#pragma link C++ class RooJohnsonSU + ;
#pragma link C++ class RooKConvGenContext + ;
#pragma link C++ class RooKResModel + ;
#pragma link C++ class RooSimultaneousResModel + ;
#pragma link C++ class RooSwimmingAcceptance + ;
#pragma link C++ class SquaredSum + ;
#pragma link C++ class TagDLLToTagDec + ;
#pragma link C++ class TagDLLToTagEta + ;
#pragma link C++ class TaggingCat + ;

#endif
