// ROOT and RooFit includes
#include "RooFormulaVar.h"
#include "RooAddPdf.h"
#include "RooKeysPdf.h"
#include "RooExtendPdf.h"
#include "RooEffProd.h"
#include "RooGaussian.h"
#include "RooDecay.h"
#include "RooBDecay.h"
#include "RooCBShape.h"
#include "RooWorkspace.h"
#include "RooRealVar.h"
#include "RooExponential.h"
#include "RooProdPdf.h"
#include "TFile.h"
#include "TTree.h"
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooHistPdf.h"
#include <string>
#include <vector>
#include <fstream>


#include "B2DXFitters/Bs2Dsh2011TDAnaModels.h"
#include "B2DXFitters/Lb2XhModels.h"
#include "B2DXFitters/GeneralUtils.h"
#include "B2DXFitters/RooBinned1DQuinticBase.h"
#include "B2DXFitters/BasicMDFitPdf.h"

using namespace std;
using namespace GeneralUtils;
using namespace Bs2Dsh2011TDAnaModels;
using namespace BasicMDFitPdf;

namespace Lb2XhModels {

  //===============================================================================
  // Background 3D model for Bs->DsstPi mass fitter.
  //===============================================================================
  RooAbsPdf* build_Lb2Dsp_BKG( RooWorkspace* work,
			       RooWorkspace* workInt,
			       std::vector <RooAbsReal*> obs,
			       std::vector <TString> types,
			       TString &samplemode,
			       std::vector<TString> merge,
			       bool debug)
  {
    if (debug == true)
      {
        cout<<"------------------------------------------------"<<endl;
	cout<<"[INFO] =====> Build background model Lb2Dsp     "<<endl;
        cout<<"-------------------------------------------------"<<endl;
      }

    RooArgList* list = new RooArgList();
    TString name = "";

    // --------------------------------- Read PDFs from Workspace -------------------------------------------------//
    RooExtendPdf* epdf_Lb2Dsstp = NULL;
    epdf_Lb2Dsstp = buildExtendPdfMDFit( workInt, work, obs, types, samplemode, "Lb2Dsstp", "", merge, debug);
    Double_t valLb2Dsstp = CheckEvts(workInt, samplemode, "Lb2Dsstp",debug);
    list = AddEPDF(list, epdf_Lb2Dsstp, valLb2Dsstp, debug);


    TString nBs2DsDsstKKstPiRhoName = "nBs2DsDsstKKstPiRho_"+samplemode+"_Evts";
    RooRealVar* nBs2DsDsstKKstPiRhoEvts = tryVar(nBs2DsDsstKKstPiRhoName, workInt,debug);
    Double_t valBs2DsDsstKKstPiRho = nBs2DsDsstKKstPiRhoEvts->getValV();

    TString g1_f1_Name = "g1_f1_frac_"+samplemode;
    RooRealVar* g1_f1 = tryVar(g1_f1_Name, workInt,debug);

    TString g1_f2_Name = "g1_f2_frac_"+samplemode;
    RooRealVar* g1_f2 = tryVar(g1_f2_Name, workInt,debug);

    RooProdPdf* pdf_Bs2DsstPi_Tot = NULL;
    RooProdPdf* pdf_Bs2DsRho_Tot = NULL;
    RooProdPdf* pdf_Bs2DsPi_Tot = NULL;
    RooAddPdf* pdf_Bs2DsDsstPiRho_Tot = NULL;

    if ( valBs2DsDsstKKstPiRho != 0.0 )
      {
	pdf_Bs2DsstPi_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsstPi", "", merge, debug);
	pdf_Bs2DsRho_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsRho", "", merge, debug);
	pdf_Bs2DsPi_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsPi", "", merge, debug);

	name="PhysBkgBs2DsDsstPiRhoPdf_m_"+samplemode+"_Tot";
	pdf_Bs2DsDsstPiRho_Tot = new RooAddPdf( name.Data(),
						name.Data(),
						RooArgList(*pdf_Bs2DsPi_Tot, *pdf_Bs2DsstPi_Tot, *pdf_Bs2DsRho_Tot),
						RooArgList(*g1_f1,*g1_f2),
						true
						);

	CheckPDF(pdf_Bs2DsDsstPiRho_Tot, debug);
      }


    TString g2_f1_Name = "g2_f1_frac_"+samplemode;
    RooRealVar* g2_f1 = tryVar(g2_f1_Name, workInt,debug);

    TString g2_f2_Name = "g2_f2_frac_"+samplemode;
    RooRealVar* g2_f2 = tryVar(g2_f2_Name, workInt,debug);

    RooProdPdf* pdf_Bs2DsstK_Tot = NULL;
    RooProdPdf* pdf_Bs2DsKst_Tot = NULL;
    RooProdPdf* pdf_Bs2DsK_Tot = NULL;
    RooAddPdf* pdf_Bs2DsDsstKKst_Tot = NULL;

    if ( valBs2DsDsstKKstPiRho != 0.0 )
      {
	pdf_Bs2DsstK_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsstK", "", merge, debug);
        pdf_Bs2DsKst_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsKst", "", merge, debug);
        pdf_Bs2DsK_Tot = buildProdPdfMDFit(workInt, work, obs, types, samplemode, "Bs2DsK", "", merge, debug);


	name="PhysBkgBs2DsDsstKKstPdf_m_"+samplemode+"_Tot";
        pdf_Bs2DsDsstKKst_Tot = new RooAddPdf( name.Data(),
					       name.Data(),
					       RooArgList(*pdf_Bs2DsK_Tot, *pdf_Bs2DsstK_Tot, *pdf_Bs2DsKst_Tot),
					       RooArgList(*g2_f1,*g2_f2),
					       true
					       );
	CheckPDF(pdf_Bs2DsDsstKKst_Tot, debug);
      }


    RooAddPdf* pdf_Bs2DsDsstKKstPiRho_Tot   = NULL;
    RooExtendPdf* epdf_Bs2DsDsstKKstPiRho   = NULL;


    TString g3_f1_Name = "g3_f1_frac_"+samplemode;
    RooRealVar* g3_f1 = tryVar(g3_f1_Name, workInt,debug);

    if ( valBs2DsDsstKKstPiRho != 0.0 )
      {
	name="PhysBkgBs2DsDsstKKstPiRhoPdf_m_"+samplemode+"_Tot";
        pdf_Bs2DsDsstKKstPiRho_Tot = new RooAddPdf( name.Data(),
						    name.Data(),
						    RooArgList(*pdf_Bs2DsDsstKKst_Tot, *pdf_Bs2DsDsstPiRho_Tot),
						    RooArgList(*g3_f1)
                                               );
	CheckPDF(pdf_Bs2DsDsstKKstPiRho_Tot, debug);

	name = "Bs2DsDsstKKstPiRhoEPDF_m_"+samplemode;
        epdf_Bs2DsDsstKKstPiRho = new RooExtendPdf( name.Data() , pdf_Bs2DsDsstKKstPiRho_Tot-> GetTitle(),
                                                   *pdf_Bs2DsDsstKKstPiRho_Tot  , *nBs2DsDsstKKstPiRhoEvts   );
        CheckPDF( epdf_Bs2DsDsstKKstPiRho, debug );
        list = AddEPDF(list, epdf_Bs2DsDsstKKstPiRho, nBs2DsDsstKKstPiRhoEvts, debug);
      }


    RooAbsPdf* pdf_totBkg = NULL;
    TString nameBkg = "BkgEPDF_m_"+samplemode;
    pdf_totBkg = new RooAddPdf( nameBkg.Data(), nameBkg.Data(), *list);

    return pdf_totBkg;

}


}
