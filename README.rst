B2DXFitters
===========

RooFit-based fitters for ``B -> D X`` that were originally kept in LHCb's Urania project.


Set up
------------

First you need to set up the environment with lb-conda on the latest ROOT version.

.. code-block:: bash

	lb-conda default

Now clone and build the repo

.. code-block:: bash

	git clone https://username@gitlab.cern.ch:/lhcb-b2oc/b2dxfitters.git
	cd b2dxfitters/
	mkdir standalone/build && cd standalone/build/
	cmake .. -DPROJECT_VERSION=0.0.0 -DCMAKE_INSTALL_PREFIX=$PWD/standalone/build -DCMAKE_BUILD_TYPE=Release
	cmake .. && cmake --build . -v && make && make install
	cd ../../
	export PYTHONPATH="${PYTHONPATH}:$PWD/python"
	cd .. && echo "b2dxfitters built!"

You can check it went OK with

.. code-block:: bash

	python
	from B2DXFitters import *

It should report that ``libB2DXFitters`` has been loaded

You can now run your commands as before. You may need to add to the ``PYTHONPATH``



.. code-block::	bash

	python prepareWorkspace.py --configName ../data/EM_Run3/Bs2DsPi/Bs2DsPiConfigForNominalMassFit.py  --MC



Running in the new session
^^^^^^^^^^^^^^^^^^^^^^^^^^

When starting a new session you will need to ``lb-conda default`` and setup the ``PYTHONPATH`` again
