###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from pathlib import Path

from packaging.version import Version
from setuptools import find_namespace_packages
from setuptools_scm import get_version
from skbuild import setup

version = Version(get_version(relative_to=__file__))

setup(
    cmake_args=[
        f"-DPROJECT_VERSION={version.major}.{version.minor}.{version.micro}",
    ],
    cmake_source_dir=Path(__file__).parent / "standalone",
)
