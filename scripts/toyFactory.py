###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
@file toyFactory.py

@brief toy generator for B2OC TD analyses

@author Vincenzo Battista
@date 2016-06-08

Generate a toy sample using a pdf with an arbitrary number of observables
(masses, time, mistag...). For the time part, all the possible effects
are taken into account (asymmetry, resolution, flavour tagging...).
The CP coefficients for the time pdf are built using the
DecRateCoeff_Bd class from Dortmund.
"""
from __future__ import print_function
import ROOT
import argparse
import os
import pprint
import sys
from ROOT import (  # ROOT Basics; RooFit Stuff; B2DXFitters stuff
    BasicMDFitPdf,
    Bd2DhModels,
    GeneralUtils,
    MistagDistribution,
    RooAbsData,
    RooAbsReal,
    RooAddPdf,
    RooArgList,
    RooArgSet,
    RooBinning,
    RooCategory,
    RooConstVar,
    RooDataHist,
    RooDataSet,
    RooExponential,
    RooFit,
    RooHistPdf,
    RooPolynomial,
    RooProdPdf,
    RooRandom,
    RooRealVar,
    RooWorkspace,
    TFile,
    TRandom3,
    TString,
)
from B2DXFitters import acceptanceutils, cpobservables, resmodelutils, timepdfutils_Bd
from B2DXFitters.settings import setEnv
from B2DXFitters.WS import WS

ROOT.PyConfig.IgnoreCommandLineOptions = True  # noqa
setEnv()

ROOT.gROOT.SetBatch()

GENERATION_YIELD_TARGET = {}
WORKSPACE_CACHE = {}
RANDOM_GENERATOR = None
treeStoreFile = None


def getGenerationYield(hypo, year, mode, pol, comp, b2dxConfiguration):
    global GENERATION_YIELD_TARGET, RANDOM_GENERATOR

    key = (hypo, year, mode, pol, comp)
    if key in GENERATION_YIELD_TARGET:
        return GENERATION_YIELD_TARGET[key]

    target = b2dxConfiguration["Components"][comp][hypo][year][mode]
    if pol.lower() == "up":
        target *= b2dxConfiguration.get("FractionsLuminosity", {}).get(year, 1.0)
    elif pol.lower() == "down":
        target *= 1.0 - b2dxConfiguration.get("FractionsLuminosity", {}).get(year, 0.0)
    if target == 0.0:
        return None

    generatedTarget = RANDOM_GENERATOR.Poisson(target)
    GENERATION_YIELD_TARGET[key] = generatedTarget

    return generatedTarget


def getGenerationNumber(configparameter):
    global RANDOM_GENERATOR

    if type(configparameter) != list:
        configparameter = [configparameter]

    p0 = configparameter[0]

    if len(configparameter) == 2:
        offset = RANDOM_GENERATOR.Gaus(0, abs(configparameter[1]))
        if configparameter[-1] < 0:
            p0 -= offset
        else:
            p0 += offset

    return p0


def getGenerationVar(name, configparameter):
    """Build a RooRealVar named "name" with value configparameter[0].
    If the configparameter is a list with 2 entries, the value is randomly
    shifted with a gaus distribution of width configparameter[1].
    """
    p0 = getGenerationNumber(configparameter)
    print("Variable {} generated at point {}".format(name, p0))
    return RooRealVar(name, name, p0)


# ------------------------------------------------------------
def BuildObservables(workspaceIn, myconfigfile, debug):
    obsDict = {}
    for obs in myconfigfile["Observables"].keys():
        print(obs)
        # Take "real" variables (masses, time...)
        if myconfigfile["Observables"][obs]["Type"] == "RooRealVar":
            if debug:
                print("Building " + obs + " of RooRealVar type")
            obsDict[obs] = WS(
                workspaceIn,
                RooRealVar(
                    obs,
                    myconfigfile["Observables"][obs]["Title"],
                    *myconfigfile["Observables"][obs]["Range"]
                ),
            )

        # Take "discrete" variables (tagging decision, final state charge...)
        elif myconfigfile["Observables"][obs]["Type"] == "RooCategory":
            if debug:
                print("Building " + obs + " of RooCategory type")
            cat = RooCategory(obs, myconfigfile["Observables"][obs]["Title"])
            for label, index in list(
                myconfigfile["Observables"][obs]["Categories"].items()
            ):
                cat.defineType(label, index)
                if debug:
                    print("..." + label)
            obsDict[obs] = WS(workspaceIn, cat)
        elif myconfigfile["Observables"][obs]["Type"] == "FromWorkspace":
            if debug:
                print("Take " + obs + " from existing workspace")
            file = TFile.Open(myconfigfile["Observables"][obs]["File"], "READ")
            w = file.Get(myconfigfile["Observables"][obs]["Workspace"])
            newobs = w.obj(myconfigfile["Observables"][obs]["Name"])
            obsDict[obs] = WS(workspaceIn, newobs)
            file.Close()

    if debug:
        print("Observables dictionary:")
        pprint.pprint(obsDict)

    return obsDict


# ------------------------------------------------------------
def BuildTagging(workspaceIn, myconfigfile, obsDict, debug, template_workspace=None):
    tagDict = {}
    # Loop over components
    for comp in myconfigfile["Components"].keys():
        tagDict[comp] = {
            "Calibration": [],
            "MistagPDF": [],
            "GLM": [],
        }
        # Loop over taggers (OS, SS)
        for tagger in myconfigfile["Taggers"][comp].keys():
            # if "Mistag"+tagger in obsDict.keys()
            # and "TagDec"+tagger in obsDict.keys():
            if "TagDec" + tagger in list(obsDict.keys()):
                if (
                    myconfigfile["Taggers"][comp][tagger]["Calibration"].get("Type")
                    == "GLM"
                ):
                    # Create calibration from XML given by EPM
                    if debug:
                        print("Creating calibration function from XML produced by EPM")
                    tagDict[comp]["GLM"].append(
                        *myconfigfile["Taggers"][comp][tagger]["Calibration"]["XML"]
                    )
                    caliblist = []
                    for p in ["tageff", "tagasymm"]:
                        if debug:
                            print(
                                "Create "
                                + p
                                + " parameter for "
                                + tagger
                                + " tagger, "
                                + comp
                                + " component"
                            )
                            caliblist.append(
                                WS(
                                    workspaceIn,
                                    getGenerationVar(
                                        "{}_{}_{}".format(p, tagger, comp),
                                        myconfigfile["Taggers"][comp][tagger][
                                            "Calibration"
                                        ][p],
                                    ),
                                )
                            )
                else:
                    # Create calibration parameters
                    # (p0, p1, dp0, dp1, <eta>, tageff, atageff)
                    if debug:
                        print("Creating linear calibration parameters")
                    tagDict[comp]["GLM"].append(None)
                    caliblist = []
                    for p in [
                        "p0",
                        "p1",
                        "deltap0",
                        "deltap1",
                        "avgeta",
                        "tageff",
                        "tagasymm",
                    ]:
                        if debug:
                            print(
                                "Create "
                                + p
                                + " parameter for "
                                + tagger
                                + " tagger, "
                                + comp
                                + " component"
                            )
                        caliblist.append(
                            WS(
                                workspaceIn,
                                getGenerationVar(
                                    "{}_{}_{}".format(p, tagger, comp),
                                    myconfigfile["Taggers"][comp][tagger][
                                        "Calibration"
                                    ][p],
                                ),
                            )
                        )

                tagDict[comp]["Calibration"].append(caliblist)

                # Create mistag pdf (can be "None" for average mistag)
                if debug:
                    print(
                        "Create mistag pdf for "
                        + tagger
                        + " tagger, "
                        + comp
                        + " component"
                    )
                if myconfigfile["Taggers"][comp][tagger]["MistagPDF"] is None:
                    if debug:
                        print(
                            "No mistag pdf (average mistag) "
                            "for component {}".format(comp)
                        )
                    tagDict[comp]["MistagPDF"] = None
                else:
                    tagDict[comp]["MistagPDF"].append(
                        BuildMistagPDF(
                            workspaceIn,
                            myconfigfile,
                            tagger,
                            comp,
                            obsDict,
                            debug,
                            template_workspace,
                        )
                    )

    if debug:
        print("Tagging dictionary:")
        pprint.pprint(tagDict)

    return tagDict


def printRooAbsDataInfo(data):
    output = "RooDataSet info for {}:\n".format(data.GetName())
    output += "numEntries: {}\n".format(data.numEntries())
    store = data.store()
    output += "store: {}\n".format(store)
    output += "storageType: {}\n".format(store.ClassName())
    if store.ClassName() == "RooTreeDataStore":
        output += "tree: {}\n".format(store.tree())
        output += "path: {}\n".format(store.tree().GetCurrentFile().GetPath())
    output += '.Print("v") output follows'
    print(output)
    data.Print("v")


def importRooHistPDF(
    workspace_in,
    template_workspace,
    variablename,
    pdfname=None,
    bins=100,
    range=None,
    debug=False,
    cut="",
    weightvarname=None,
):
    """
    Create a RooHistPdf based on a dataset from template_workspace add import
    it into workspace_in.

    Args:
        workspace_in: A RooWorkspace in which to import the generated RooHistPDF
        template_workspace: A RooWorkspace that needs to contain a RooDataSet
            with the given variablename.
        variablename: Name of the RooRealVar inside the given template
            workspace for which a RooHistPdf is created.
        pdfname: Name of the generated pdf. Defaults to {variablename}_histpdf.
        dataname: The name of the dataset within the template_workspace.

    Returns:
        A RooHistPdf based on the given variable data inside
        template_workspace.
        The object will already be imported to workspace_in.
    """
    variable = template_workspace.var(variablename)
    variable_argset = RooArgSet(variable)
    datasets = list(template_workspace.allData())
    if cut:
        data = datasets[0].reduce(cut)
    data = datasets[0].reduce(variable_argset)
    for additionalDataset in datasets[1:]:
        if cut:
            additionalDataset = additionalDataset.reduce(cut)
        data.append(additionalDataset.reduce(variable_argset))
    if range is None:
        range = (variable.getMin(), variable.getMax())

    binning = RooBinning(
        bins, range[0], range[1], "{}_datahist_binning".format(variablename)
    )

    variable.setBinning(binning)

    datahist = WS(
        workspace_in,  # need to add underlying datahist to workspace
        RooDataHist(
            "{}_datahist".format(variablename),
            "{}_datahist".format(variablename),
            variable_argset,
            data,
        ),
    )

    if pdfname is None:
        pdfname = "{}_histpdf".format(variablename)

    histpdf = WS(workspace_in, RooHistPdf(pdfname, pdfname, variable_argset, datahist))
    return histpdf


def GetWorkspaceFromFile(filename):
    global WORKSPACE_CACHE
    if filename in WORKSPACE_CACHE:
        print("Found workspace from file {} in cache.".format(filename))
        return WORKSPACE_CACHE[filename]

    print('Loading workspace named "workspace" from file "{}"'.format(filename))
    wsfile = TFile(filename)
    WORKSPACE_CACHE[filename + "_file"] = wsfile
    WORKSPACE_CACHE[filename] = wsfile.Get("workspace")
    return WORKSPACE_CACHE[filename]


def BuildMistagPDF(
    workspaceIn, myconfigfile, tagger, comp, obsDict, debug, template_workspace
):
    tagger_config = myconfigfile["Taggers"][comp][tagger]["MistagPDF"]

    if "Type" in list(tagger_config.keys()):
        if tagger_config["Type"] == "FromWorkspace":
            if debug:
                print(
                    "Take mistag pdf {} from workspace {} in file {}.".format(
                        tagger_config["Name"],
                        tagger_config["Workspace"],
                        tagger_config["File"],
                    )
                )
            pdf = WS(workspaceIn, template_workspace.pdf(tagger_config["Name"]))
        elif tagger_config["Type"] == "Mock":
            if debug:
                print("Build mock mistag pdf")
            pdf = WS(
                workspaceIn,
                BuildMockMistagPDF(
                    workspaceIn, myconfigfile, tagger, comp, obsDict, debug
                ),
            )
        elif tagger_config["Type"] == "FromData":
            # Build and use a RooDataHist and RooHistPdf to generate mistag data
            if debug:
                print("Use Data to build RooHistPdf")
            if "TemplateFile" in tagger_config:
                print("Reading template workspace on demand")
                template_workspace_file = tagger_config["TemplateFile"]
                template_workspace = GetWorkspaceFromFile(template_workspace_file)
            pdf = importRooHistPDF(
                workspaceIn,
                template_workspace,
                "Mistag{}".format(tagger),
                pdfname="MistagPDF_{}_{}".format(tagger, comp),
                cut="TagDec{} != 0".format(tagger),  # only use tagged events
                debug=debug,
                range=(0, 0.5),  # make sure a sensible binning is used!
            )
        else:
            print("ERROR: mistag pdf type " + tagger_config["Type"] + " not supported.")
            exit(-1)
    else:
        print("ERROR: error in mistag PDF building. Please check your config file.")
        exit(-1)

    if debug:
        print("Mistag pdf:")
        pdf.Print("v")

    return pdf


def BuildMockMistagPDF(workspaceIn, myconfigfile, tagger, comp, obsDict, debug):
    mistag = obsDict["Mistag" + tagger]

    eta0 = WS(
        workspaceIn,
        RooRealVar(
            "eta0_" + tagger + "_" + comp,
            "eta0_" + tagger + "_" + comp,
            *myconfigfile["Taggers"][comp][tagger]["MistagPDF"]["eta0"]
        ),
    )
    etaavg = WS(
        workspaceIn,
        RooRealVar(
            "etaavg_" + tagger + "_" + comp,
            "etaavg_" + tagger + "_" + comp,
            *myconfigfile["Taggers"][comp][tagger]["MistagPDF"]["etaavg"]
        ),
    )
    f = WS(
        workspaceIn,
        RooRealVar(
            "f_" + tagger + "_" + comp,
            "f_" + tagger + "_" + comp,
            *myconfigfile["Taggers"][comp][tagger]["MistagPDF"]["f"]
        ),
    )

    pdf = WS(
        workspaceIn,
        MistagDistribution(
            "MistagPDF_" + tagger + "_" + comp,
            "MistagPDF_" + tagger + "_" + comp,
            mistag,
            eta0,
            etaavg,
            f,
        ),
    )

    # the following lines were only for debugging
    # pdf = WS(workspaceIn, RooGaussian("MistagPDF_"+tagger+"_"+comp,
    #                                   "MistagPDF_"+tagger+"_"+comp,
    #                                   mistag, etaavg, f))

    return pdf


# ------------------------------------------------------------
def BuildResolutionAcceptance(workspaceIn, myconfigfile, obsDict, debug):
    resAccDict = {}
    # Loop over components
    for comp in myconfigfile["Components"].keys():
        resAccDict[comp] = {}

        # Build time error pdf
        if debug:
            "Create time error pdf for " + comp
        resAccDict[comp]["TimeErrorPDF"] = BuildTimeErrorPDF(
            workspaceIn, myconfigfile, comp, obsDict, debug
        )

        # Build acceptance
        resAccDict[comp]["Acceptance"] = {}
        acc = None
        accnorm = None
        if myconfigfile["ResolutionAcceptance"][comp]["Acceptance"] is None:
            if debug:
                print("No time acceptance applied for " + comp)
            pass
        elif "Type" in list(
            myconfigfile["ResolutionAcceptance"][comp]["Acceptance"].keys()
        ):
            if (
                myconfigfile["ResolutionAcceptance"][comp]["Acceptance"]["Type"]
                == "Spline"
            ):
                if debug:
                    print("Build spline acceptance for " + comp)
                time = obsDict["BeautyTime"]
                acc, accnorm = acceptanceutils.buildSplineAcceptance(
                    workspaceIn,
                    time,
                    "Acceptance_" + comp,
                    myconfigfile["ResolutionAcceptance"][comp]["Acceptance"][
                        "KnotPositions"
                    ],
                    myconfigfile["ResolutionAcceptance"][comp]["Acceptance"][
                        "KnotCoefficients"
                    ],
                    extrapolate=myconfigfile["ResolutionAcceptance"][comp][
                        "Acceptance"
                    ].get("extrapolate", False),
                    debug=debug,
                )
                # Use normalised acceptance for generation
                acc = accnorm
            else:
                print(
                    "ERROR: acceptance type "
                    + myconfigfile["ResolutionAcceptance"][comp]["Acceptance"]["Type"]
                    + " not supported."
                )
                exit(-1)
        else:
            print(
                "ERROR: error when building acceptance for "
                + comp
                + ". Please check config file."
            )
            exit(-1)

        # Build resolution
        resAccDict[comp]["Resolution"] = {}
        resmodel = None
        if myconfigfile["ResolutionAcceptance"][comp]["Resolution"] is None:
            if debug:
                print(
                    "No time resolution (i.e. perfect resolution) applied for " + comp
                )
                print("")
                print(
                    "WARNING: the usage of RooTruthModel is highly discouraged! "
                    "Use at your own risk!"
                )
                print(
                    "If you want to emulate a perfect resolution, "
                    "a very narrow gaussian is recommended instead."
                )
                print("")
            pass
        elif "Type" in list(
            myconfigfile["ResolutionAcceptance"][comp]["Resolution"].keys()
        ):
            # Need to build a dictionary that "getResolutionModel" understands
            config = {}
            config["AcceptanceFunction"] = myconfigfile["ResolutionAcceptance"][comp][
                "Acceptance"
            ]["Type"]
            if (
                myconfigfile["ResolutionAcceptance"][comp]["Resolution"]["Type"]
                == "GaussianWithPEDTE"
            ):
                if debug:
                    print(
                        "Build gaussian with per-event decay time resolution model for "
                        + comp
                    )
                config["Context"] = "GEN"
                config["DecayTimeResolutionModel"] = "GaussianWithPEDTE"
                config["DecayTimeResolutionBias"] = getGenerationNumber(
                    myconfigfile["ResolutionAcceptance"][comp]["Resolution"]["Bias"]
                )
                config["DecayTimeResolutionScaleFactor"] = getGenerationNumber(
                    myconfigfile["ResolutionAcceptance"][comp]["Resolution"][
                        "ScaleFactor"
                    ]
                )
                time = obsDict["BeautyTime"]
                terr = obsDict["BeautyTimeErr"]
                resmodel, acc = resmodelutils.getResolutionModel(
                    workspaceIn, config, time, terr, acc
                )
            elif (
                myconfigfile["ResolutionAcceptance"][comp]["Resolution"]["Type"]
                == "AverageModel"
            ):
                if debug:
                    print("Build mean time resolution model for " + comp)
                config["Context"] = "GEN"
                config["DecayTimeResolutionModel"] = myconfigfile[
                    "ResolutionAcceptance"
                ][comp]["Resolution"]["Parameters"]
                config["DecayTimeResolutionBias"] = getGenerationNumber(
                    myconfigfile["ResolutionAcceptance"][comp]["Resolution"]["Bias"]
                )
                config["DecayTimeResolutionScaleFactor"] = getGenerationNumber(
                    myconfigfile["ResolutionAcceptance"][comp]["Resolution"][
                        "ScaleFactor"
                    ]
                )
                time = obsDict["BeautyTime"]
                resmodel, acc = resmodelutils.getResolutionModel(
                    workspaceIn, config, time, None, acc
                )
            else:
                print(
                    "ERROR: resolution type "
                    + myconfigfile["ResolutionAcceptance"][comp]["Resolution"]["Type"]
                    + " not supported."
                )
                exit(-1)
        else:
            print(
                "ERROR: error when building resolution for "
                + comp
                + ". Please check config file."
            )
            exit(-1)

        # Put resolution/acceptance in dictionary
        resAccDict[comp]["Acceptance"] = acc
        resAccDict[comp]["Resolution"] = resmodel

    if debug:
        print("Resolution/acceptance dictionary:")
        print(resAccDict)

    return resAccDict


# ------------------------------------------------------------
def BuildTimeErrorPDF(workspaceIn, myconfigfile, comp, obsDict, debug):
    pdf = None
    if myconfigfile["ResolutionAcceptance"][comp]["TimeErrorPDF"] is None:
        if debug:
            print("No time error pdf (mean resolution model)")
        return pdf
    elif "Type" in list(
        myconfigfile["ResolutionAcceptance"][comp]["TimeErrorPDF"].keys()
    ):
        if (
            myconfigfile["ResolutionAcceptance"][comp]["TimeErrorPDF"]["Type"]
            == "FromWorkspace"
        ):
            if debug:
                print(
                    "Take time error pdf "
                    + myconfigfile["ResolutionAcceptance"][comp]["TimeErrorPDF"]["Name"]
                    + " from workspace "
                    + myconfigfile["ResolutionAcceptance"][comp]["TimeErrorPDF"][
                        "Workspace"
                    ]
                    + " in file "
                    + myconfigfile["ResolutionAcceptance"][comp]["TimeErrorPDF"]["File"]
                )
            file = TFile.Open(
                myconfigfile["ResolutionAcceptance"][comp]["TimeErrorPDF"]["File"],
                "READ",
            )
            w = file.Get(
                myconfigfile["ResolutionAcceptance"][comp]["TimeErrorPDF"]["Workspace"]
            )
            pdf = WS(
                workspaceIn,
                w.pdf(
                    myconfigfile["ResolutionAcceptance"][comp]["TimeErrorPDF"]["Name"]
                ),
            )
            file.Close()
        elif (
            myconfigfile["ResolutionAcceptance"][comp]["TimeErrorPDF"]["Type"] == "Mock"
        ):
            if debug:
                print("Build mock time error pdf")
            pdf = BuildMockTimeErrorPDF(workspaceIn, myconfigfile, comp, obsDict, debug)
        elif (
            myconfigfile["ResolutionAcceptance"][comp]["TimeErrorPDF"]["Type"]
            == "FromData"
        ):
            if debug:
                print("Use Data to build TimeErrorPDF")
            template_workspace_file = myconfigfile["ResolutionAcceptance"][comp][
                "TimeErrorPDF"
            ]["TemplateFile"]
            template_workspace = GetWorkspaceFromFile(template_workspace_file)
            pdf = importRooHistPDF(
                workspaceIn,
                template_workspace,
                "BeautyTimeErr",
                "TimeErrorPDF_{}".format(comp),
            )
        else:
            print(
                "ERROR: time error pdf type "
                + myconfigfile["ResolutionAcceptance"][comp]["TimeErrorPDF"]["Type"]
                + " not supported."
            )
            exit(-1)
    else:
        print("ERROR: error in time error pdf building. Please check your config file.")
        exit(-1)

    if debug:
        print("Time error pdf:")
        print(pdf.Print("v"))

    return pdf


# ------------------------------------------------------------
def BuildMockTimeErrorPDF(workspaceIn, myconfigfile, comp, obsDict, debug):
    """Build a mock Time error PDF of the form
    t_err(x) = x^6 * exp(-avg * x)
    and add it to workspaceIn.

    Caution, due to ROOTs normalization, the value of ResolutionAverage has
    nothing to do with the actual position of the average. You need to trial
    and error your correct shape out of this...
    """

    terr = RooRealVar(obsDict["BeautyTimeErr"])
    terr.setMin(0)
    terr.setMax(2)

    avg = WS(
        workspaceIn,
        RooRealVar(
            "terrshape_" + comp,
            "terrshape_" + comp,
            -1
            * myconfigfile["ResolutionAcceptance"][comp]["TimeErrorPDF"][
                "ResolutionAverage"
            ][0],
        ),
    )
    exp = WS(
        workspaceIn, RooExponential("terrexp_" + comp, "terrexp_" + comp, terr, avg)
    )
    zero = workspaceIn.obj("zero")
    one = workspaceIn.obj("one")

    # Something 1 * x^6 * exp(-avg * x) resembles the resolution shape
    # nicely, if avg is of the correct size :P
    poly = WS(
        workspaceIn,
        RooPolynomial(
            "terrpoly_" + comp,
            "terrpoly_" + comp,
            terr,
            RooArgList(
                zero,  # 0
                zero,  # 1
                zero,  # 2
                zero,  # 3
                zero,  # 4
                zero,  # 5
                one,  # 6
            ),
            0,
        ),
    )

    pdf = WS(
        workspaceIn,
        RooProdPdf("TimeErrorPDF_" + comp, "TimeErrorPDF_" + comp, exp, poly),
    )

    return pdf


# ------------------------------------------------------------
def BuildAsymmetries(workspaceIn, myconfigfile, debug):
    asymmDict = {}
    for comp in myconfigfile["Components"].keys():
        asymmDict[comp] = {}
        asymmDict[comp]["Production"] = WS(
            workspaceIn,
            getGenerationVar(
                "AProd_" + comp, myconfigfile["ProductionAsymmetry"][comp]
            ),
        )
        asymmDict[comp]["Detection"] = WS(
            workspaceIn,
            getGenerationVar("ADet_" + comp, myconfigfile["DetectionAsymmetry"][comp]),
        )

    if debug:
        print("Asymmetry dictionary:")
        print(asymmDict)

    return asymmDict


# -----------------------------------------------------------------------------
def BuildTotalPDF(
    workspaceIn,
    myconfigfile,
    obsDict,
    ACPDict,
    tagDict,
    resAccDict,
    asymmDict,
    workTemplate,
    HFAG,
    debug,
    pol="up",
):
    pdfDict = {}

    # Loop over bachelor mass hypotheses
    yieldCount = {}
    for hypo in myconfigfile["Hypothesys"]:
        print("Hypothesys: " + hypo)
        pdfDict[hypo] = {}
        yieldCount[hypo] = {}
        # Loop over years of data taking
        for year in myconfigfile["Years"]:
            print("Year: " + year)
            pdfDict[hypo][year] = {}
            yieldCount[hypo][year] = {}  # add placeholders for "All" DsModes
            # Loop over D decay modes
            for mode in myconfigfile["CharmModes"]:
                print("D decay mode: " + mode)
                pdfDict[hypo][year][mode] = {}
                yieldCount[hypo][year][mode] = {}
                # Loop over components,
                # make sure that signal ist the first component! because of
                # non obvious state dependency of this beast!!!
                active_components = list(myconfigfile["Components"].keys())
                # pop Signal, and create new list with signal at first position
                components = [
                    active_components.pop(active_components.index("Signal"))
                ] + active_components
                for comp in components:
                    print("Component: " + comp)
                    pdfDict[hypo][year][mode][comp] = {}
                    yieldCount[hypo][year][mode][comp] = {}
                    # Loop over observables
                    for obs in myconfigfile["Observables"].keys():
                        # Copy config for CharMode "All"
                        if (
                            obs in ["BeautyMass", "CharmMass", "BacPIDK"]
                            and "All" in myconfigfile["PDFList"][obs][comp][hypo][year]
                        ):
                            myconfigfile["PDFList"][obs][comp][hypo][year][
                                mode
                            ] = myconfigfile["PDFList"][obs][comp][hypo][year]["All"]

                        # Build PDF
                        if (
                            obs in ["BeautyMass", "CharmMass", "BacPIDK"]
                            and myconfigfile["PDFList"][obs][comp][hypo][year][mode][
                                "Type"
                            ]
                            == "None"
                        ):
                            pdfDict[hypo][year][mode][comp][obs] = None
                            continue

                        if (
                            obs == "BeautyTime"
                            and None is not ACPDict
                            and None is not tagDict
                            and None is not resAccDict
                            and None is not asymmDict
                        ):
                            print("Observables: " + obs)
                            pdfDict[hypo][year][mode][comp][obs] = WS(
                                workspaceIn,
                                BuildTimePDF(
                                    workspaceIn,
                                    myconfigfile,
                                    hypo,
                                    year,
                                    comp,
                                    mode,
                                    obsDict,
                                    ACPDict,
                                    tagDict,
                                    resAccDict,
                                    asymmDict,
                                    HFAG,
                                    debug,
                                    pol=pol,
                                ),
                            )
                        elif obs in ["BeautyMass", "CharmMass", "BacPIDK"]:
                            print("Observables: " + obs)
                            pdfDict[hypo][year][mode][comp][obs] = WS(
                                workspaceIn,
                                BuildPDF(
                                    workspaceIn,
                                    myconfigfile,
                                    hypo,
                                    year,
                                    comp,
                                    mode,
                                    obsDict[obs],
                                    workTemplate,
                                    debug,
                                    pol=pol,
                                ),
                            )

                    pdfDict[hypo][year][mode][comp]["Yield"] = WS(
                        workspaceIn,
                        RooRealVar(
                            "nEvts_"
                            + pol
                            + year
                            + "_"
                            + comp
                            + "_"
                            + mode
                            + "_"
                            + hypo
                            + "Hypo",
                            "nEvts_"
                            + pol
                            + year
                            + "_"
                            + comp
                            + "_"
                            + mode
                            + "_"
                            + hypo
                            + "Hypo",
                            myconfigfile["Components"][comp][hypo][year][mode],
                        ),
                    )
                    yieldCount[hypo][year][mode][comp] = myconfigfile["Components"][
                        comp
                    ][hypo][year][mode]

    if debug:
        print("PDF dictionary:")
        pprint.pprint(pdfDict)
        print("Yields:")
        pprint.pprint(yieldCount)

    return {"PDF": pdfDict, "Events": yieldCount}


# -----------------------------------------------------------------------------
def BuildACPDict(workspaceIn, myconfigfile, HFAG, debug):
    ACPDict = {}

    for comp in myconfigfile["Components"].keys():
        ACPDict[comp] = {}

        if (
            "ArgLf" in list(myconfigfile["ACP"][comp].keys())
            and "ArgLbarfbar" in list(myconfigfile["ACP"][comp].keys())
            and "ModLf" in list(myconfigfile["ACP"][comp].keys())
        ):
            if debug:
                print("Building CP coefficients using amplitude values")

            ACPobs = cpobservables.AsymmetryObservables(
                myconfigfile["ACP"][comp]["ArgLf"][0],
                myconfigfile["ACP"][comp]["ArgLbarfbar"][0],
                myconfigfile["ACP"][comp]["ModLf"][0],
                HFAG,
            )
            ACPobs.printtable()

            ACPDict[comp]["C"] = WS(
                workspaceIn, getGenerationVar("C_" + comp, ACPobs.Cf())
            )
            ACPDict[comp]["S"] = WS(
                workspaceIn, getGenerationVar("S_" + comp, ACPobs.Sf())
            )
            ACPDict[comp]["D"] = WS(
                workspaceIn, getGenerationVar("D_" + comp, ACPobs.Df())
            )
            ACPDict[comp]["Sbar"] = WS(
                workspaceIn, getGenerationVar("Sbar_" + comp, ACPobs.Sfbar())
            )
            ACPDict[comp]["Dbar"] = WS(
                workspaceIn, getGenerationVar("Dbar_" + comp, ACPobs.Dfbar())
            )

        else:
            if debug:
                print("Building CP coefficients directly from their values")

            ACPDict[comp]["C"] = WS(
                workspaceIn,
                getGenerationVar("C_" + comp, myconfigfile["ACP"][comp]["C"]),
            )
            ACPDict[comp]["S"] = WS(
                workspaceIn,
                getGenerationVar("S_" + comp, myconfigfile["ACP"][comp]["S"]),
            )
            ACPDict[comp]["D"] = WS(
                workspaceIn,
                getGenerationVar("D_" + comp, myconfigfile["ACP"][comp]["D"]),
            )
            ACPDict[comp]["Sbar"] = WS(
                workspaceIn,
                getGenerationVar("Sbar_" + comp, myconfigfile["ACP"][comp]["Sbar"]),
            )
            ACPDict[comp]["Dbar"] = WS(
                workspaceIn,
                getGenerationVar("Dbar_" + comp, myconfigfile["ACP"][comp]["Dbar"]),
            )

        # Build other decay rate parameters
        ACPDict[comp]["Gamma"] = WS(
            workspaceIn,
            getGenerationVar("Gamma_" + comp, myconfigfile["ACP"][comp]["Gamma"]),
        )
        ACPDict[comp]["DeltaGamma"] = WS(
            workspaceIn,
            getGenerationVar(
                "DeltaGamma_" + comp, myconfigfile["ACP"][comp]["DeltaGamma"]
            ),
        )
        ACPDict[comp]["DeltaM"] = WS(
            workspaceIn,
            getGenerationVar("DeltaM_" + comp, myconfigfile["ACP"][comp]["DeltaM"]),
        )

    if debug:
        print("CP components dictionary:")
        print(ACPDict)

    return ACPDict


# -----------------------------------------------------------------------------
def BuildTimePDF(
    workspaceIn,
    myconfigfile,
    hypo,
    year,
    comp,
    mode,
    obsDict,
    ACPDict,
    tagDict,
    resAccDict,
    asymmDict,
    HFAG,
    debug,
    pol="up",
):
    pdfname = "TimePDF_" + comp
    zero = workspaceIn.obj("zero")
    pdf = workspaceIn.pdf(pdfname)

    if not pdf:
        if debug:
            print(
                "PDF {} not present in workspace {}. Building...".format(
                    pdfname,
                    workspaceIn.GetName(),
                )
            )

        # Retrieve time and final state observables
        time = obsDict["BeautyTime"]
        qf = obsDict["BacCharge"]

        # Retrieve CP coefficients and decay rate parameters
        C = ACPDict[comp]["C"]
        S = ACPDict[comp]["S"]
        D = ACPDict[comp]["D"]
        Sbar = ACPDict[comp]["Sbar"]
        Dbar = ACPDict[comp]["Dbar"]
        Gamma = ACPDict[comp]["Gamma"]
        DeltaGamma = ACPDict[comp]["DeltaGamma"]
        DeltaM = ACPDict[comp]["DeltaM"]

        # Retrieve tagging
        mistagcalib = tagDict[comp]["Calibration"]
        mistagpdf = tagDict[comp]["MistagPDF"]

        GLM = tagDict[comp]["GLM"]

        qt = []
        mistagobs = []
        for tagger in myconfigfile["Taggers"][comp].keys():
            # if "Mistag"+tagger in obsDict.keys()
            # and "TagDec"+tagger in obsDict.keys():
            if "TagDec" + tagger in list(obsDict.keys()):
                qt.append(obsDict["TagDec" + tagger])
            if "Mistag" + tagger in list(obsDict.keys()):
                mistagobs.append(obsDict["Mistag" + tagger])
            else:
                mistagobs.append(zero)

        # Retrieve time error PDF, resolution and acceptance
        terrpdf = resAccDict[comp]["TimeErrorPDF"]
        resmodel = resAccDict[comp]["Resolution"]
        acc = resAccDict[comp]["Acceptance"]
        timeerr = None
        if "BeautyTimeErr" in list(obsDict.keys()):
            timeerr = obsDict["BeautyTimeErr"]

        # Retrieve asymmetries
        aprod = asymmDict[comp]["Production"]
        adet = asymmDict[comp]["Detection"]

        # Build a config dict that buildBDecayTimePdf can understand
        config = {}
        config["Context"] = "GEN"
        config["Debug"] = True if debug else False
        config["ParameteriseIntegral"] = myconfigfile["ACP"][comp][
            "ParameteriseIntegral"
        ]
        config[
            "UseProtoData"
        ] = True  # this is really recommended to speed-up generation
        config["NBinsAcceptance"] = myconfigfile["ACP"][comp]["NBinsAcceptance"]
        if "NBinsProperTimeErr" in list(myconfigfile["ACP"][comp].keys()):
            config["NBinsProperTimeErr"] = myconfigfile["ACP"][comp][
                "NBinsProperTimeErr"
            ]

        # Build time PDF
        pdf = timepdfutils_Bd.buildBDecayTimePdf(
            config,
            "TimePDF_" + pol + year + "_" + comp + "_" + mode + "_" + hypo + "Hypo",
            workspaceIn,
            time,
            timeerr,
            qt,
            qf,
            mistagobs,
            mistagcalib,
            Gamma,
            DeltaGamma,
            DeltaM,
            C,
            D,
            Dbar,
            S,
            Sbar,
            resmodel,
            acc,
            terrpdf,
            mistagpdf,
            aprod,
            adet,
            HFAG,
            GLM,
        )
    elif debug:
        print(
            "PDF {} already present in workspace {}. Won't build!".format(
                pdfname,
                workspaceIn.GetName(),
            )
        )

    return WS(workspaceIn, pdf)


# -----------------------------------------------------------------------------
def BuildPDF(
    workspaceIn,
    myconfigfile,
    hypo,
    year,
    comp,
    mode,
    obs,
    workTemplate,
    debug,
    pol="up",
):
    pdf = None

    if obs.GetName() in ["BeautyMass", "CharmMass"]:
        print(obs.GetName(), comp, hypo, year, mode)
        pprint.pprint(myconfigfile["PDFList"][obs.GetName()][comp][hypo][year])
        mode2 = mode
        if "All" in myconfigfile["PDFList"][obs.GetName()][comp][hypo][year]:
            mode = "All"
        shapeType = myconfigfile["PDFList"][obs.GetName()][comp][hypo][year][mode][
            "Type"
        ]
        if shapeType == "FromWorkspace":
            name = myconfigfile["PDFList"][obs.GetName()][comp][hypo][year][mode][
                "Name"
            ]
            workspaceFileName = myconfigfile["PDFList"][obs.GetName()][comp][hypo][
                year
            ][mode].get("WorkspaceFile")
            workspaceName = myconfigfile["PDFList"][obs.GetName()][comp][hypo][year][
                mode
            ].get("WorkspaceName")
            if workspaceFileName is not None and workspaceName is not None:
                # read individual workspace for this pdf
                f = TFile(workspaceFileName)
                ROOT.SetOwnership(f, True)  # let it be deleted via pythons gc
                ws = f.Get(workspaceName)
                f.Close()
                ROOT.SetOwnership(ws, True)  # let it be deleted via pythons gc
                if debug:
                    print(
                        "Take PDF {} from workspace {} in file {}".format(
                            name, ws, workspaceFileName
                        )
                    )
                templatePdf = ws.pdf(name)
            else:
                if debug:
                    print("Take PDF {} from workspace {}".format(name, workTemplate))
                templatePdf = workTemplate.pdf(name)
            if not templatePdf:
                print(
                    "[ERROR] cannot read pdf {} from workspace {}".format(
                        name, workTemplate.GetName()
                    )
                )
                sys.exit(1)
            pdf = WS(workspaceIn, templatePdf, RooFit.RecycleConflictNodes())
            # name = comp + "_" + obs.GetName()
            # + "_" + mode2 + "_" + year + "_" + hypo + "Hypo"
            # pdf.SetName(name)
            # if pdf has sub-pdfs, these have to be renamed as well
        else:
            pdf = BuildAnalyticalPdf(
                workspaceIn,
                shapeType,
                myconfigfile,
                hypo,
                year,
                comp,
                mode,
                obs,
                debug,
                pol=pol,
            )
            # pdf.SetName(comp+"_"+obs.GetName()+"_"+mode2+"_"+year+"_"+hypo+"Hypo")
            pdf = WS(workspaceIn, pdf)

    elif obs.GetName() in ["BacPIDK"]:
        mode2 = mode
        if "All" in myconfigfile["PDFList"][obs.GetName()][comp][hypo][year]:
            mode = "All"
        shapeType = myconfigfile["PDFList"][obs.GetName()][comp][hypo][year][mode][
            "Type"
        ]
        if shapeType == "FromWorkspace":
            pdfTmp = {}
            name = {}
            for pol in ["Up", "Down"]:
                name[pol] = {}
                myconfigfile["PDFList"][obs.GetName()][comp][hypo][year][mode][
                    "Workspace"
                ]
                myconfigfile["PDFList"][obs.GetName()][comp][hypo][year][mode]["File"]
                if comp != "Combinatorial":
                    name[pol]["Merged"] = myconfigfile["PDFList"][obs.GetName()][comp][
                        hypo
                    ][year][mode]["Name"][pol]
                else:
                    contributions = myconfigfile["PDFList"][obs.GetName()][comp][hypo][
                        year
                    ][mode]["Contributions"]
                    for con in contributions:
                        name[pol][con] = myconfigfile["PDFList"][obs.GetName()][comp][
                            hypo
                        ][year][mode]["Name"][pol][con]
                if debug:
                    for n in name:
                        print(
                            "Take PDF " + n + " from workspace "
                        )  # +work+" inside file "+filename
                # filePDF = TFile.Open(filename,"READ")
                # workTemplate = filePDF.Get(work)
                pdfTmp[pol] = {}
                for n in name:
                    print(n)
                    if comp != "Combinatorial":
                        pdfTmp[pol]["Merged"] = WS(
                            workspaceIn, workTemplate.obj(name[pol]["Merged"])
                        )
                    else:
                        for con in contributions:
                            pdfTmp[pol][con] = WS(
                                workspaceIn, workTemplate.obj(name[pol][con])
                            )
                # filePDF.Close()
            if comp == "Combinatorial":
                sizeContr = len(contributions)
                for pol in ["Up", "Down"]:
                    fracList = RooArgList()
                    for i in range(0, sizeContr - 1):
                        print(
                            myconfigfile["PDFList"][obs.GetName()][comp][hypo][year][
                                mode
                            ]["Name"][pol]["fracPIDK"][i]
                        )
                        fracName = (
                            "fracPIDK"
                            + str(i + 1)
                            + "_"
                            + pol
                            + "_"
                            + mode
                            + "_"
                            + year
                        )
                        fracList.add(
                            WS(
                                workspaceIn,
                                RooRealVar(
                                    fracName,
                                    fracName,
                                    myconfigfile["PDFList"][obs.GetName()][comp][hypo][
                                        year
                                    ][mode]["Name"][pol]["fracPIDK"][i],
                                ),
                            )
                        )
                    pdfList = RooArgList()
                    for con in contributions:
                        pdfList.add(pdfTmp[pol][con])
                    pdfList.Print("v")
                    fracList.Print("v")
                    namePDF = (
                        comp
                        + "_"
                        + obs.GetName()
                        + "_"
                        + mode2
                        + "_"
                        + pol
                        + "_"
                        + year
                        + "_"
                        + hypo
                        + "Hypo"
                    )
                    pdfTmp[pol]["Merged"] = WS(
                        workspaceIn, RooAddPdf(namePDF, namePDF, pdfList, fracList)
                    )

            print(pdfTmp)
            namePDF = comp + "_" + obs.GetName() + "_" + mode2 + "_" + year
            frac = WS(
                workspaceIn,
                RooRealVar(
                    "frac_" + year,
                    "frac_" + year,
                    myconfigfile["FractionsLuminosity"][year],
                ),
            )
            pdf = WS(
                workspaceIn,
                RooAddPdf(
                    namePDF,
                    namePDF,
                    pdfTmp["Up"]["Merged"],
                    pdfTmp["Down"]["Merged"],
                    frac,
                ),
            )
        else:
            pdf = BuildAnalyticalPdf(
                workspaceIn,
                shapeType,
                myconfigfile,
                hypo,
                year,
                comp,
                mode,
                obs,
                debug,
                pol=pol,
            )
            pdf.SetName(
                comp
                + "_"
                + obs.GetName()
                + "_"
                + mode2
                + "_"
                + year
                + "_"
                + hypo
                + "Hypo"
            )
            pdf = WS(workspaceIn, pdf)
    else:
        print("ERROR: pdfs for observables " + obs.GetName() + " not yet implemented.")
        exit(-1)

    pdf.Print("v")
    return pdf


# -----------------------------------------------------------------------------
def BuildAnalyticalPdf(
    workspaceIn, shapeType, myconfigfile, hypo, year, comp, mode, obs, debug, pol="up"
):
    ##########################################
    # Users are encouraged to add other PDFs #
    # here and commit them if required!      #
    ##########################################

    pdf = None

    # Label
    smyh_vec = GeneralUtils.GetSampleModeYearHypo(
        TString(pol),  # Not splitting per magnet pol, at the moment
        TString(mode),
        TString(year),
        TString(hypo),
        TString(""),  # Just need to merge polarities, I guess?
        debug,
    )
    # Should always have a 1-dimensional vector inside this function
    # (one D mode, one year...)
    # In fact, BuildAnalyticalPdf is called for each of these at time
    smyh = smyh_vec[0]

    widthRatio = False
    sameMean = False
    # Build parameters for this PDF into internal workspace
    for param in myconfigfile["PDFList"][obs.GetName()][comp][hypo][year][mode].keys():
        if param != "Type" and param != "widthRatio":
            if debug:
                print(
                    "Building "
                    + comp
                    + "_"
                    + obs.GetName()
                    + "_"
                    + param
                    + "_"
                    + smyh.Data()
                    + " parameter"
                )
            value = myconfigfile["PDFList"][obs.GetName()][comp][hypo][year][mode][
                param
            ]
            # a range could be given if a list of length 3 is given
            if type(value) not in [list, tuple]:
                value = [value]
            par = WS(
                workspaceIn,
                RooRealVar(
                    comp + "_" + obs.GetName() + "_" + param + "_" + smyh.Data(),
                    comp + "_" + obs.GetName() + "_" + param + "_" + smyh.Data(),
                    *value
                ),
            )
            if not par:
                print("ERROR: parameter " + param + " not created. Please check!")
                exit(-1)

        elif param == "widthRatio":
            widthRatio = param

    # Build PDF as requested
    if shapeType == "Ipatia":
        pdf = BasicMDFitPdf.buildIpatiaPDF(
            obs,
            workspaceIn,
            smyh,
            comp,
            False,  # don't shift mean
            False,  # don't rescale tails
            debug,
        )
    elif shapeType == "JohnsonSU":
        pdf = Bd2DhModels.buildJohnsonSUPDF(
            obs, workspaceIn, smyh, comp, False, debug  # don't shift mean
        )
    elif shapeType == "CrystalBall":
        pdf = BasicMDFitPdf.buildCrystalBallPDF(obs, workspaceIn, smyh, comp, debug)
    elif shapeType == "DoubleCrystalBall":
        pdf = BasicMDFitPdf.buildDoubleCrystalBallPDF(
            obs,
            workspaceIn,
            smyh,
            comp,
            widthRatio,
            sameMean,  # don't use "shared mean"
            debug,
        )
    elif shapeType == "Gaussian":
        pdf = BasicMDFitPdf.buildGaussPDF(
            obs, workspaceIn, smyh, comp, False, debug  # don't shift mean
        )
    elif shapeType == "DoubleGaussian":
        pdf = BasicMDFitPdf.buildDoubleGaussPDF(
            obs,
            workspaceIn,
            smyh,
            comp,
            False,  # don't use "width ratio"
            False,  # don't use "same mean"
            False,  # don't shift mean
            debug,
        )
    elif shapeType == "CrystalBallPlusGaussian":
        pdf = Bd2DhModels.buildCrystalBallPlusGaussianPDF(
            obs,
            workspaceIn,
            smyh,
            comp,
            False,  # don't shift mean
            False,  # don't scale widths
            debug,
        )
    elif shapeType == "Exponential":
        pdf = BasicMDFitPdf.buildExponentialPDF(obs, workspaceIn, smyh, comp, debug)
    elif shapeType == "ExponentialPlusSignal":
        pdf = BasicMDFitPdf.buildExponentialPlusSignalPDF(
            obs,
            workspaceIn,
            smyh,
            comp,
            debug,
        )
    elif shapeType == "DoubleExponential":
        pdf = BasicMDFitPdf.buildDoubleExponentialPDF(
            obs, workspaceIn, smyh, comp, debug
        )
    elif shapeType == "ExponentialPlusConstant":
        pdf = Bd2DhModels.buildExponentialPlusConstantPDF(
            obs, workspaceIn, smyh, comp, debug
        )
    elif shapeType == "ExponentialPlusDoubleCrystalBall":
        pdf = BasicMDFitPdf.buildExponentialPlusDoubleCrystalBallPDF(
            obs, workspaceIn, smyh, comp, False, False, debug
        )
    elif shapeType == "MassDiff":
        pdf = BasicMDFitPdf.buildMassDiffPDF(obs, workspaceIn, smyh, comp, debug)
    elif shapeType == "HORNSdini":
        pdf = BasicMDFitPdf.buildHORNSdini(obs, workspaceIn, smyh, comp, debug)
    elif shapeType == "DoubleHORNSdini":
        pdf = BasicMDFitPdf.buildDoubleHORNSdini(obs, workspaceIn, smyh, comp, debug)
    elif shapeType == "HILLdini":
        pdf = BasicMDFitPdf.buildHILLdini(obs, workspaceIn, smyh, comp, debug)
    elif shapeType == "HILLdiniPlusHORNSdini":
        pdf = BasicMDFitPdf.buildHILLdiniPlusHORNSdini(
            obs, workspaceIn, smyh, comp, debug
        )
    elif shapeType.startswith("IpatiaJohnsonSU"):
        pdf = Bd2DhModels.buildIpatiaPlusJohnsonSUPDF(
            obs,
            workspaceIn,
            smyh,
            comp,
            False,  # dont shift mean
            False,  # dont scale tails
            "withwidthratio" in shapeType.lower(),  # activate width ratio
            False,  # dont scale widths individually
            debug,
        )
    elif shapeType.startswith("IpatiaPlusGaussian"):
        pdf = Bd2DhModels.buildIpatiaPlusGaussianPDF(
            obs,
            workspaceIn,
            smyh,
            comp,
            "withwidthratio" in shapeType.lower(),
            "shiftedsignal" in shapeType.lower(),
            False,  # no scaled tails
            debug,
        )

    else:
        print(
            "ERROR: pdf type " + shapeType + " not yet implemented. Please add"
            "your PDF to toyFactory.BuildAnalyticalPdf()."
        )
        exit(-1)

    return pdf


# -----------------------------------------------------------------------------
def BuildProtoData(
    workspaceIn,
    myconfigfile,
    obsDict,
    tagDict,
    resAccDict,
    pdfDict,
    seed,
    debug,
    pol="up",
):
    if "BeautyTime" not in list(obsDict.keys()):
        return None

    # Ok, we have time observable. Let's see if we need per event mistag/time error
    protoDataDict = {}

    for hypo in myconfigfile["Hypothesys"]:
        protoDataDict[hypo] = {}

        for year in myconfigfile["Years"]:
            protoDataDict[hypo][year] = {}

            for mode in myconfigfile["CharmModes"]:
                protoDataDict[hypo][year][mode] = {}

                for comp in myconfigfile["Components"].keys():
                    protoDataDict[hypo][year][mode][comp] = []
                    atLeast = 0

                    # Get random number of events to generate
                    poissonNum = getGenerationYield(
                        hypo, year, mode, pol, comp, myconfigfile
                    )
                    if poissonNum is None:
                        continue

                    # Check tagging
                    tag = 0
                    if tagDict[comp]["MistagPDF"] is not None:
                        for tagger in myconfigfile["Taggers"][comp].keys():
                            if "Mistag" + tagger in list(
                                obsDict.keys()
                            ) and "TagDec" + tagger in list(obsDict.keys()):
                                if debug:
                                    print(
                                        "Generate "
                                        + str(poissonNum)
                                        + " Mistag"
                                        + tagger
                                        + " proto data from "
                                        + tagDict[comp]["MistagPDF"][tag].GetName()
                                    )
                                # make sure that the tree storage
                                # is the current working directory
                                protoDataDict[hypo][year][mode][comp].append(
                                    tagDict[comp]["MistagPDF"][tag].generate(
                                        RooArgSet(obsDict["Mistag" + tagger]),
                                        RooFit.AutoBinned(False),
                                        RooFit.NumEvents(poissonNum),
                                    )
                                )
                                protoDataDict[hypo][year][mode][comp][atLeast].SetName(
                                    protoDataDict[hypo][year][mode][comp][
                                        atLeast
                                    ].GetName()
                                    + pol
                                    + "_"
                                    + year
                                    + "_"
                                    + comp
                                    + "_"
                                    + mode
                                    + "_"
                                    + hypo
                                    + "Hypo"
                                )
                                protoDataDict[hypo][year][mode][comp][atLeast].SetTitle(
                                    protoDataDict[hypo][year][mode][comp][
                                        atLeast
                                    ].GetName()
                                    + pol
                                    + "_"
                                    + year
                                    + "_"
                                    + comp
                                    + "_"
                                    + mode
                                    + "_"
                                    + hypo
                                    + "Hypo"
                                )
                                protoDataDict[hypo][year][mode][comp][atLeast] = WS(
                                    workspaceIn,
                                    protoDataDict[hypo][year][mode][comp][atLeast],
                                )

                                atLeast += 1
                                tag += 1

                    # Check per-event error
                    if resAccDict[comp]["TimeErrorPDF"] is not None:
                        if debug:
                            print(
                                "Generate {} per-event time error proto data "
                                "from {}".format(
                                    poissonNum,
                                    resAccDict[comp]["TimeErrorPDF"].GetName(),
                                )
                            )
                        protoDataDict[hypo][year][mode][comp].append(
                            resAccDict[comp]["TimeErrorPDF"].generate(
                                RooArgSet(obsDict["BeautyTimeErr"]),
                                RooFit.AutoBinned(False),
                                RooFit.NumEvents(poissonNum),
                            )
                        )
                        protoDataDict[hypo][year][mode][comp][atLeast].SetName(
                            protoDataDict[hypo][year][mode][comp][atLeast].GetName()
                            + pol
                            + "_"
                            + year
                            + "_"
                            + comp
                            + "_"
                            + mode
                            + "_"
                            + hypo
                            + "Hypo"
                        )
                        protoDataDict[hypo][year][mode][comp][atLeast].SetTitle(
                            protoDataDict[hypo][year][mode][comp][atLeast].GetName()
                            + pol
                            + "_"
                            + year
                            + "_"
                            + comp
                            + "_"
                            + mode
                            + "_"
                            + hypo
                            + "Hypo"
                        )
                        protoDataDict[hypo][year][mode][comp][atLeast] = WS(
                            workspaceIn, protoDataDict[hypo][year][mode][comp][atLeast]
                        )

                        atLeast = atLeast + 1

    if atLeast == 0 and tag == 0:
        if debug:
            print("No need for proto data")
        return None

    if debug:
        print("Proto data dictionary:")
        pprint.pprint(protoDataDict)

    # Merge datasets for a given hypo, component
    protoDataMerged = {}

    for hypo in myconfigfile["Hypothesys"]:
        protoDataMerged[hypo] = {}

        for year in myconfigfile["Years"]:
            protoDataMerged[hypo][year] = {}

            for mode in myconfigfile["CharmModes"]:
                protoDataMerged[hypo][year][mode] = {}

                for comp in myconfigfile["Components"].keys():
                    if len(protoDataDict[hypo][year][mode][comp]) > 0:
                        protoDataMerged[hypo][year][mode][comp] = {}
                        countGenObs = 0

                        for data in protoDataDict[hypo][year][mode][comp]:
                            if countGenObs == 0:
                                protoDataMerged[hypo][year][mode][comp] = RooDataSet(
                                    protoDataDict[hypo][year][mode][comp][countGenObs]
                                )
                            else:
                                protoDataMerged[hypo][year][mode][comp].merge(
                                    protoDataDict[hypo][year][mode][comp][countGenObs]
                                )
                            countGenObs = countGenObs + 1

                        protoDataMerged[hypo][year][mode][comp].SetName(
                            "ProtoData_"
                            + pol
                            + "_"
                            + year
                            + "_"
                            + comp
                            + "_"
                            + mode
                            + "_"
                            + hypo
                            + "Hypo"
                        )
                        protoDataMerged[hypo][year][mode][comp].SetTitle(
                            "ProtoData_"
                            + pol
                            + "_"
                            + year
                            + "_"
                            + comp
                            + "_"
                            + mode
                            + "_"
                            + hypo
                            + "Hypo"
                        )

                    else:
                        protoDataMerged[hypo][year][mode][comp] = None

    if debug:
        print("Merged proto data dictionary:")
        pprint.pprint(protoDataMerged)

    return protoDataMerged


# -----------------------------------------------------------------------------
def GenerateToys(
    workspaceIn,
    myconfigfile,
    observables,
    pdfDict,
    protoData,
    seed,
    debug,
    outputdir="",
    pol="up",
):
    pdf = pdfDict["PDF"]

    toyDict = {}

    # set default storage type to TTree
    global treeStoreFile
    if treeStoreFile is None:
        treeStoreFile = TFile(
            os.path.join(
                outputdir, "treeStore_{p}_{s}.root".format(p=pol, s=str(seed))
            ),
            "RECREATE",
        )
    else:
        treeStoreFile.cd()

    # Loop over bachelor mass hypotheses
    for hypo in myconfigfile["Hypothesys"]:
        print("Hypothesys: " + hypo)
        toyDict[hypo] = {}
        # Loop over years of data taking
        for year in myconfigfile["Years"]:
            print("Year: " + year)
            toyDict[hypo][year] = {}
            # Loop over D decay modes
            for mode in myconfigfile["CharmModes"]:
                print("D decay mode: " + mode)
                toyDict[hypo][year][mode] = {}
                # Loop over components
                for comp in myconfigfile["Components"].keys():
                    print("Components: " + comp)
                    toyDict[hypo][year][mode][comp] = {}

                    # Get random number of events to generate
                    poissonNum = getGenerationYield(
                        hypo, year, mode, pol, comp, myconfigfile
                    )
                    # Loop over observables
                    for obs in myconfigfile["Observables"].keys():
                        if poissonNum is None:
                            toyDict[hypo][year][mode][comp][obs] = None
                            continue

                        dataName = "ToyData_{}_{}_{}_{}_{}Hypo".format(
                            obs, comp, mode, year, hypo
                        )
                        data = None

                        if obs == "BeautyTime":
                            print("Observable: " + obs)
                            toyDict[hypo][year][mode][comp][obs] = {}
                            genset = RooArgSet(observables.find(obs))

                            # Apparently TagDecs need to be generated from the pdf
                            # to produce proper time + tag distributions
                            # Set conf['ForceProtoForTagging'] = True to turn this off
                            genset.add(observables.find("BacCharge"))
                            if "TagDecOS" in list(
                                myconfigfile["Observables"].keys()
                            ) and not myconfigfile.get("ForceProtoForTagging", False):
                                genset.add(observables.find("TagDecOS"))
                            if "TagDecSS" in list(
                                myconfigfile["Observables"].keys()
                            ) and not myconfigfile.get("ForceProtoForTagging", False):
                                genset.add(observables.find("TagDecSS"))

                            # ProtoData is required for Mistag and TimeError
                            # distributions
                            if debug:
                                print(
                                    "Generate decay time data from "
                                    + pdf[hypo][year][mode][comp][obs].GetName()
                                )
                                # print "Generate "+str(poissonNum)+" decay time data
                                # from "+pdf[hypo][year][mode][comp][obs].GetName()
                            data = pdf[hypo][year][mode][comp][obs].generate(
                                genset,
                                RooFit.ProtoData(
                                    protoData[hypo][year][mode][comp], True
                                ),  # use randomized proto data
                                RooFit.NumEvents(poissonNum),
                            )

                        elif obs in ["BeautyMass", "CharmMass", "BacPIDK"]:
                            print("Observable: " + obs)
                            genset = RooArgSet(observables.find(obs))
                            toyDict[hypo][year][mode][comp][obs] = {}

                            if obs == "BacPIDK":
                                if debug:
                                    print(
                                        "Generate "
                                        + str(poissonNum)
                                        + " BacPIDK data from "
                                        + pdf[hypo][year][mode][comp][obs].GetName()
                                    )
                                data = pdf[hypo][year][mode][comp][obs].generate(
                                    genset,
                                    RooFit.AutoBinned(False),
                                    RooFit.NumEvents(poissonNum),
                                )
                            else:
                                if debug:
                                    print(
                                        "Generate "
                                        + str(poissonNum)
                                        + " "
                                        + obs
                                        + " data from "
                                        + pdf[hypo][year][mode][comp][obs].GetName()
                                    )
                                data = pdf[hypo][year][mode][comp][obs].generate(
                                    genset, RooFit.NumEvents(poissonNum)
                                )

                        if data is not None:
                            data.SetName(dataName)
                            toyDict[hypo][year][mode][comp][obs] = WS(workspaceIn, data)

    if debug:
        print("Toy dictionary:")
        pprint.pprint(toyDict)

    return toyDict


# -----------------------------------------------------------------------------
def BuildTotalDataset(
    workspaceIn, myconfigfile, toyDict, debug, seed, outputdir="", pol="up"
):
    # Build categories to identify sample
    dmodeCat = RooCategory("dmodeCat", "dmodeCat")
    dmodeCatList = []
    dmodeIdx = 0

    componentCat = RooCategory("component", "component")
    for component in list(myconfigfile["Components"].keys()):
        componentCat.defineType("component_" + component)

    # Append all datasets for a given hypothesys, D decay mode
    modesData = []

    for hypo in toyDict.keys():
        print("Hypothesys: " + hypo)

        for year in toyDict[hypo].keys():
            print("Year: " + year)

            for mode in toyDict[hypo][year].keys():
                modeSmall = GeneralUtils.GetModeLower(TString(mode), debug)
                print("D decay mode: " + mode)
                dmodeCatName = "{}_{}_{}_{}Hypo".format(pol, modeSmall, year, hypo)
                dmodeCat.defineType(dmodeCatName, dmodeIdx)
                dmodeCatList.append(dmodeCatName)

                if debug:
                    print(
                        "Defined category {} with index {}".format(
                            dmodeCatName, dmodeIdx
                        )
                    )

                countComp = 0
                obsDataList = []

                for comp in toyDict[hypo][year][mode].keys():
                    print("Component: " + comp)

                    countObs = 0
                    dataObs = None

                    for obs in toyDict[hypo][year][mode][comp].keys():
                        if None is toyDict[hypo][year][mode][comp][obs]:
                            continue

                        print("Observable: " + obs)
                        print(
                            "Entries "
                            + str(toyDict[hypo][year][mode][comp][obs].sumEntries())
                        )
                        if countObs == 0:
                            dataObs = ROOT.RooDataSet(
                                toyDict[hypo][year][mode][comp][obs],
                                "Toy_"
                                + pol
                                + "_"
                                + year
                                + "_"
                                + comp
                                + "_"
                                + mode
                                + "_"
                                + hypo
                                + "Hypo",
                            )
                        else:
                            dataObs.merge(toyDict[hypo][year][mode][comp][obs])

                        countObs = countObs + 1

                    obsDataList.append(dataObs)

                    if None is dataObs or dataObs.numEntries() == 0:
                        continue

                    global treeStoreFile
                    if treeStoreFile is None:
                        treeStoreFile = TFile(
                            os.path.join(
                                outputdir,
                                "treeStore_{p}_{s}.root".format(p=pol, s=str(seed)),
                            ),
                            "RECREATE",
                        )
                    else:
                        treeStoreFile.cd()
                    if countComp == 0:
                        print("Creating data set: {}".format(dataObs.GetName()))
                        dataName = "dataSet{}_{}_{}_{}".format(
                            myconfigfile["Decay"],
                            pol,
                            mode,
                            year,
                        )
                        dataObs.get().Print("v")
                        dataComp = ROOT.RooDataSet(
                            dataName,
                            dataName,
                            dataObs.get(),
                            RooFit.Index(componentCat),
                            RooFit.Import("component_" + comp, dataObs),
                        )
                        dataComp.get().Print("v")
                        print(dataComp.sumEntries())
                    else:
                        print(dataComp.sumEntries())
                        componentDataTmp = ROOT.RooDataSet(
                            "tmp",
                            "tmp",
                            dataObs.get(),
                            RooFit.Index(componentCat),
                            RooFit.Import("component_" + comp, dataObs),
                        )
                        dataComp.append(componentDataTmp)
                        # dataComp.get().Print('v')
                        print(dataComp.sumEntries())

                    countComp = countComp + 1

                dataComp = WS(workspaceIn, dataComp)
                # dataComp.get().Print('v')

                modesData.append(dataComp)

                if dmodeIdx == 0:
                    # Use this dataset to retrieve list of observables in the
                    # first iteration
                    observables = dataComp.get()

                dmodeIdx = dmodeIdx + 1

    observables.Print("v")

    totData = RooDataSet(
        "totData",
        "totData",
        observables,
        RooFit.Index(dmodeCat),
        RooFit.Import(dmodeCatList[0], modesData[0]),
    )
    print(("[INFO] Adding data set: ", modesData[0].GetName()))
    for i in range(1, len(modesData)):
        totDatatmp = RooDataSet(
            "totData",
            "totData",
            observables,
            RooFit.Index(dmodeCat),
            RooFit.Import(dmodeCatList[i], modesData[i]),
        )
        if debug:
            print(("[INFO] Adding data set: ", modesData[i].GetName()))
        totData.append(totDatatmp)

    if debug:
        print("Total dataset:")
        totData.Print("v")
        print("dmodeCat categories:")
        dmodeCat.Print("v")
        print("component categories:")
        componentCat.Print("v")

    return totData, modesData


# -----------------------------------------------------------------------------
def MergeYears(myconfigfile, modesData, pol, debug):
    print("[INFO] Merge years {} of data taking".format(myconfigfile["MergedYears"]))

    dmodeCat = RooCategory("dmodeCat", "dmodeCat")
    modesDataOut = []
    dmodeCatList = []

    for hypo in myconfigfile["Hypothesys"]:
        print("B hypothesis: " + hypo)
        for mode in myconfigfile["CharmModes"]:
            print("D decay mode: " + mode)
            modeSmall = mode.lower()
            joinedName = "".join(myconfigfile["MergedYears"])

            # special cases for B2DXFitter merged naming schema
            if joinedName == "20152016":
                joinedName = "run2"
            elif joinedName == "20112012":
                joinedName = "run1"

            typeName = "{}_{}_{}_{}Hypo".format(pol, modeSmall, joinedName, hypo)
            dataName = "dataSet{}_{}".format(myconfigfile["Decay"], typeName)
            print((typeName, dataName))

            dmodeCat.defineType(typeName, len(dmodeCatList))
            dmodeCatList.append(typeName)

            matchingAdditionalData = []
            additionalDataNames = [
                dataName.replace(joinedName, y) for y in myconfigfile["MergedYears"]
            ]
            for additionalData in modesData:
                # filter 2011, 2012, 2015, 2016 in case of merged years
                if additionalData.GetName() in additionalDataNames:
                    matchingAdditionalData.append(additionalData)

            data0 = matchingAdditionalData[0]
            print(
                "First dataset {} with {} entries".format(
                    data0.GetName(), data0.numEntries()
                )
            )
            data0.SetName(dataName)
            for additionalData in matchingAdditionalData[1:]:
                print(
                    "Adding dataset {} with {} entries".format(
                        additionalData.GetName(), additionalData.numEntries()
                    )
                )
                data0.append(additionalData)

            modesDataOut.append(data0)

            # typeName = TString("up_")+modeSmall+TString("_run1_")
            #           +TString(hypo)+TString("Hypo")
            # type2011 = TString("up_")+modeSmall+TString("_2011_")
            #           +TString(hypo)+TString("Hypo")
            # type2012 = TString("up_")+modeSmall+TString("_2012_")
            #           +TString(hypo)+TString("Hypo")
            # dataName = TString("dataSet")+TString(myconfigfile["Decay"])
            #           + TString("_") + typeName

            # print typeName, dataName
            # sam.defineType(typeName.Data(), idx)
            # sm.append(typeName)

            # for data in modesData:
            #     if TString(data.GetName()).Contains(type2011) == True:
            #         data2011 = data
            #     if TString(data.GetName()).Contains(type2012) == True:
            #         data2012 = data

            # print "Adding: ", data2011.GetName(), data2011.numEntries(),
            # print "  to : ", data2012.GetName(), data2012.numEntries()

            # data2011.append(data2012)
            # data2011.SetName(dataName.Data())
            # modesDataOut.append(data2011)

            # idx  = idx + 1

    totData = RooDataSet(
        "totData",
        "totData",
        modesDataOut[0].get(),
        RooFit.Index(dmodeCat),
        RooFit.Import(dmodeCatList[0], modesDataOut[0]),
    )
    print("[INFO] Adding data set: ", modesDataOut[0].GetName())
    for i in range(1, len(modesDataOut)):
        totDatatmp = RooDataSet(
            "totData",
            "totData",
            modesDataOut[0].get(),
            RooFit.Index(dmodeCat),
            RooFit.Import(dmodeCatList[i], modesDataOut[i]),
        )
        if debug:
            print("[INFO] Adding data set: ", modesDataOut[i].GetName())
        totData.append(totDatatmp)

    if debug:
        print("Total dataset:")
        totData.Print("v")
        print("Sample categories:")
        dmodeCat.Print("v")

    return totData, modesDataOut


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
def toyFactory(
    configName,
    seed,
    workOut,
    workfileOut,
    treeOut,
    treefileOut,
    saveTree,
    HFAG,
    debug,
    outputdir="",
    pol="up",
):
    # Safe settings for numerical integration (if needed)
    RooAbsReal.defaultIntegratorConfig().setEpsAbs(1e-9)
    RooAbsReal.defaultIntegratorConfig().setEpsRel(1e-9)
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooAdaptiveGaussKronrodIntegrator1D"
    ).setCatLabel("method", "15Points")
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooAdaptiveGaussKronrodIntegrator1D"
    ).setRealValue("maxSeg", 1000)
    RooAbsReal.defaultIntegratorConfig().method1D().setLabel(
        "RooAdaptiveGaussKronrodIntegrator1D"
    )
    RooAbsReal.defaultIntegratorConfig().method1DOpen().setLabel(
        "RooAdaptiveGaussKronrodIntegrator1D"
    )

    # Get the configuration file
    myconfigfilegrabber = __import__(configName, fromlist=["getconfig"]).getconfig
    myconfigfile = myconfigfilegrabber()

    print("==========================================================")
    print("GENERATETOYS IS RUNNING WITH THE FOLLOWING CONFIGURATION OPTIONS")
    for option in myconfigfile:
        if option == "constParams":
            for param in myconfigfile[option]:
                print(param, "is constant in the fit")
        else:
            print(option, " = ", myconfigfile[option])
    print("==========================================================")

    workspaceIn = RooWorkspace("workIn", "workIn")
    WS(workspaceIn, RooConstVar("one", "1", 1.0))
    WS(workspaceIn, RooConstVar("zero", "0", 0.0))

    if "WorkspaceToRead" in list(myconfigfile.keys()):
        print("Workspace with templates detected. Opening it...")
        filePDF = TFile.Open(myconfigfile["WorkspaceToRead"]["File"], "READ")
        workTemplate = filePDF.Get(myconfigfile["WorkspaceToRead"]["Workspace"])
        if debug:
            workTemplate.Print("v")
    else:
        workTemplate = None

    print("")
    print("==========================================================")
    print("Set generation seed to " + str(seed))
    print("==========================================================")
    print("")

    ROOT.gInterpreter.ProcessLine("gRandom->SetSeed(" + str(int(seed)) + ")")
    RooRandom.randomGenerator().SetSeed(int(seed))

    print("")
    print("==========================================================")
    print("Build observables")
    print("==========================================================")
    print("")

    obsDict = BuildObservables(workspaceIn, myconfigfile, debug)

    tagDict = None
    resAccDict = None
    asymmDict = None
    ACPDict = None

    if "BeautyTime" in list(obsDict.keys()):
        # Time is one of the observables; take care of:
        # tagging, acceptance, resolution, asymmetries, CP parameters

        print("")
        print("==========================================================")
        print("Setup tagging")
        print("==========================================================")
        print("")

        tagDict = BuildTagging(
            workspaceIn, myconfigfile, obsDict, debug, template_workspace=workTemplate
        )

        print("")
        print("==========================================================")
        print("Setup time resolution and acceptance")
        print("==========================================================")
        print("")

        resAccDict = BuildResolutionAcceptance(
            workspaceIn, myconfigfile, obsDict, debug
        )

        print("")
        print("==========================================================")
        print("Setup time production and detection asymmetries")
        print("==========================================================")
        print("")

        asymmDict = BuildAsymmetries(workspaceIn, myconfigfile, debug)

        print("")
        print("==========================================================")
        print("Setup CP coefficients and parameters")
        print("==========================================================")
        print("")

        ACPDict = BuildACPDict(workspaceIn, myconfigfile, HFAG, debug)

    print("")
    print("==========================================================")
    print("Build PDF for all components/hypotheses")
    print("==========================================================")
    print("")

    pdfDict = BuildTotalPDF(
        workspaceIn,
        myconfigfile,
        obsDict,
        ACPDict,
        tagDict,
        resAccDict,
        asymmDict,
        workTemplate,
        HFAG,
        debug,
        pol=pol,
    )
    if "WorkspaceToRead" in list(myconfigfile.keys()):
        filePDF.Close()

    print("")
    print("==========================================================")
    print("Start toy generation")
    print("==========================================================")
    print("")

    # Include observables
    observables = RooArgSet()
    for obs in list(obsDict.keys()):
        if obs not in ["BeautyTimeErr", "MistagOS", "MistagSS"]:
            # We use "proto data", so we don't include now mistag and time error
            observables.add(obsDict[obs])
    if debug:
        print("Observables for generation:")
        observables.Print("v")

    # Generate "proto data" from time error and mistag PDFs (if needed)
    protoData = BuildProtoData(
        workspaceIn,
        myconfigfile,
        obsDict,
        tagDict,
        resAccDict,
        pdfDict,
        int(seed),
        debug,
        pol=pol,
    )

    # Generate toys
    toyDict = GenerateToys(
        workspaceIn,
        myconfigfile,
        observables,
        pdfDict,
        protoData,
        int(seed),
        debug,
        outputdir=outputdir,
        pol=pol,
    )

    print("")
    print("==========================================================")
    print("Toy generation done. Now merge/append datasets and")
    print("save them to file.")
    print("==========================================================")
    print("")

    totData, modesData = BuildTotalDataset(
        workspaceIn, myconfigfile, toyDict, debug, seed, outputdir=outputdir, pol=pol
    )

    if myconfigfile.get("MergedYears"):
        totData, modesData = MergeYears(myconfigfile, modesData, pol, debug)

    observables = totData.get()
    observables.Print("v")
    workspaceOut = RooWorkspace(workOut, workOut)

    if debug:
        print("Output workspace " + workOut + " content:")
        workspaceOut.Print("v")

    if saveTree:
        print("")
        print("==========================================================")
        print("Save output tree " + treeOut + " to following file:")
        print(os.path.join(outputdir, treefileOut))
        print("==========================================================")
        print("")

        if list(myconfigfile["Components"].keys()).__len__() == 1:
            print("")
            print("==========================================================")
            print("Only one component generated (signal?)")
            print("No need for MD fit.")
            print("Producing tuple with dummy sWeight=1 for all the events")
            print("ready for the sFit.")
            print("==========================================================")
            print("")

            if "2011" in myconfigfile["Years"] and "2012" in myconfigfile["Years"]:
                for hypo in myconfigfile["Hypothesys"]:
                    for mode in myconfigfile["CharmModes"]:
                        modelow = GeneralUtils.GetModeLower(TString(mode), debug)
                        s = (
                            "nSig_{}_".format(pol)
                            + modelow
                            + TString("_run1_")
                            + TString(hypo)
                            + TString("Hypo_Evts_sw")
                        )
                        weight = WS(workspaceOut, RooRealVar(s.Data(), s.Data(), 1.0))
                        observables.add(weight)
                        totData.addColumn(weight)
            elif "2015" in myconfigfile["Years"] and "2016" in myconfigfile["Years"]:
                for hypo in myconfigfile["Hypothesys"]:
                    for mode in myconfigfile["CharmModes"]:
                        s = (
                            "nSig_{}_".format(pol)
                            + mode.lower()
                            + "_run2_"
                            + hypo
                            + "Hypo_Evts_sw"
                        )
                        weight = WS(workspaceOut, RooRealVar(s, s, 1.0))
                        observables.add(weight)
                        totData.addColumn(weight)
            else:
                for hypo in myconfigfile["Hypothesys"]:
                    for year in myconfigfile["Years"]:
                        for mode in myconfigfile["CharmModes"]:
                            modelow = GeneralUtils.GetModeLower(TString(mode), debug)
                            s = (
                                TString("nSig_{}_".format(pol))
                                + modelow
                                + TString("_")
                                + TString(year)
                                + TString("_")
                                + TString(hypo)
                                + TString("Hypo_Evts_sw")
                            )
                            weight = WS(
                                workspaceOut, RooRealVar(s.Data(), s.Data(), 1.0)
                            )
                            observables.add(weight)
                            totData.addColumn(weight)

        fileTree = TFile.Open(os.path.join(outputdir, treefileOut), "RECREATE")
        tree = totData.GetClonedTree()
        tree.SetName(treeOut)
        tree.Write()
        fileTree.Close()

    for data in modesData:
        WS(workspaceOut, data)

    print("")
    print("==========================================================")
    print("Save output workspace " + workOut + " to following file:")
    print(os.path.join(outputdir, workfileOut))
    print("==========================================================")
    print("")
    workspaceOut.Print("v")
    TFile(os.path.join(outputdir, workfileOut), "RECREATE")
    workspaceOut.Write()
    # Wont close workspace file here, since it is used as TreeStorage.
    # Didnt find a way to point treestore to another file


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--configName",
        help="configuration file name",
        required=True,
    )
    parser.add_argument("--seed", default=193627, help="seed for generation")
    parser.add_argument(
        "--outputdir", default="", help="output directory to store workspace/tree"
    )

    parser.add_argument("--workOut", default="workspace", help="output workspace name")

    parser.add_argument(
        "--workfileOut",
        default="toyFactoryWorkFile.root",
        help="output workspace file name",
    )

    parser.add_argument(
        "--saveTree",
        action="store_true",
        default=False,
        help="save tree in addition to workspace",
    )

    parser.add_argument("--treeOut", default="merged", help="output tree name")

    parser.add_argument(
        "--treefileOut", default="toyFactoryTreeFile.root", help="output tree file name"
    )

    parser.add_argument(
        "--HFAG",
        action="store_true",
        default=False,
        help="use HFAG convention for CP coefficients",
    )

    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        default=False,
        help="print debug information while processing",
    )

    parser.add_argument("--pol", type=str, default="up")

    return parser.parse_args()


def initRandomGenerator(seed=20180110):
    seed = int(seed)
    global RANDOM_GENERATOR
    if RANDOM_GENERATOR is None:
        RANDOM_GENERATOR = TRandom3(seed)


if __name__ == "__main__":
    options = parse_args()

    config = options.configName
    last = config.rfind("/")
    directory = config[: last + 1]
    configName = config[last + 1 :]
    p = configName.rfind(".")
    configName = configName[:p]

    sys.path.append(directory)

    print("Config file name: " + configName)
    print("Directory: " + directory)

    RooAbsData.setDefaultStorageType(RooAbsData.Tree)

    initRandomGenerator(options.seed)

    toyFactory(
        configName,
        options.seed,
        options.workOut,
        options.workfileOut,
        options.treeOut,
        options.treefileOut,
        options.saveTree,
        options.HFAG,
        options.debug,
        outputdir=options.outputdir,
        pol=options.pol,
    )
