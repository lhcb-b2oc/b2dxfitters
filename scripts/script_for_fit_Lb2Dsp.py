###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

my_ranges = [1000, 1100, 1200, 1300, 1400]

names = [
    "Nominal",
    "sigmaI_plus",
    "sigmaI_minus",
    "sigmaJ_plus",
    "sigmaJ_minus",
    "10_plus",
    "10_minus",
    "5_plus",
    "5_minus",
    "MassDiff_c_plus",
    "MassDiff_c_minus",
    "MassDiff_m0_plus",
    "MassDiff_m0_minus",
    "MassDiff_A_minus",
    "MassDiff_A_plus",
    "SingleExp",
    "a1_minus",
    "a1_plus",
    "a2_minus",
    "a2_plus",
    "n1_minus",
    "n1_plus",
    "n2_minus",
    "n2_plus",
    "nu_minus",
    "nu_plus",
    "tau_minus",
    "tau_plus",
    "five",
    "fifteen",
    "DCB",
    "Ipatia",
    "GC",
]

my_name = ["GC", "Nominal"]
# one_name = ['Nominal', 'syst', 'notsigsyst', 'notsigLb2Dsstpsyst', 'frachornsfree']
one_name = ["GC"]
# for a in my_ranges:
#     for name in names:
#         f = open("dsp_" + name + "_" + str(a) + "_" + str(a+500) + ".txt", "w")
#         x = int(a)
#         my_files = "snakemake "
#         while x < a+100:
#             my_files += (
#                 "toys_fits_Lb2Dsp_" + name + "/WS_MDFit_Lb2Dsp_KKPi_both_run2_"
#                 + str(x)
#                 + "_"
#                 + str(x + 500)
#                 + ".root "
#             )
#             x += 1
#         my_files += " --cores 9 --latency-wait 900 \n"
#         f.write(my_files)
#         f.close()

my_ranges_2 = [1000, 1100, 1200, 1300, 1400, 2000, 2100, 2200, 2300, 2400]

# for a in my_ranges_2:
#     #for name in my_name_2:
#         f = open("dsp_" + str(a) + "_" + str(a+500) + ".txt", "w")
#         x = int(a)
#         my_files = "snakemake "
#         while x < a+100:
#             my_files += (
#                 "toys_Lb2Dsp_both/toyFactoryWorkFile_Lb2Dsp_KKPi_both_run2_"
#                 + str(x)
#                 + "_"
#                 + str(x + 500)
#                 + ".root "
#             )
#             x += 1
#         my_files += " --cores 9 --latency-wait 900 \n"
#         f.write(my_files)
#         f.close()

#
# my_ranges5 = [1000]
#
for a in my_ranges_2:
    for name in one_name:
        f = open("dsp_" + name + "_" + str(a) + "_" + str(a + 500) + ".txt", "w")
        x = int(a)
        my_files = "snakemake "
        while x < a + 100:
            my_files += (
                "toys_fits_Lb2Dsp_"
                + name
                + "/WS_MDFit_Lb2Dsp_KKPi_both_run2_"
                + str(x)
                + "_"
                + str(x + 500)
                + ".root "
            )
            x += 1
        my_files += " --cores 9 --latency-wait 900 \n"
        f.write(my_files)
        f.close()

my_ranges5 = [1000, 2000]

for a in my_ranges5:
    for name in one_name:
        f = open("dsp5_" + name + "_" + str(a) + "_" + str(a + 500) + ".txt", "w")
        x = int(a)
        my_files = "snakemake "
        while x < a + 500:
            my_files += (
                "toys_fits_Lb2Dsp_"
                + name
                + "/WS_MDFit_Lb2Dsp_KKPi_both_run2_"
                + str(x)
                + "_"
                + str(x + 500)
                + ".root "
            )
            x += 1
        my_files += " --cores 9 --latency-wait 900 \n"
        f.write(my_files)
        f.close()
