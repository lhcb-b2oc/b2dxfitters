###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
import sys
import numpy as np
import logging
import matplotlib.pyplot as plt
from common_utils.plotting import save_with_pickle
from matplotlib.offsetbox import AnchoredText
from plot_pull import BIN_SCALING, plot_pull_axes
from extract_fit_results import get_figures

sys.path.insert(
    0, os.path.join(os.path.abspath(os.path.dirname(__file__)), "../")
)  # noqa


def plot_pulls(
    noms,
    errs,
    generated=0,
    correlation_matrix=None,
    all_variables=None,
    variable="X",
    filename="pulls.pdf",
    n0=None,
):
    print("I entered the plot_pulls!")
    num_bins = int(BIN_SCALING * np.sqrt(noms.shape[0]))
    stats = get_figures(noms, errs, generated, numeric=True)

    plt.figure(figsize=(12, 4))
    ax1 = plt.subplot(131)
    _, bins, _ = plt.hist(noms, bins=num_bins)
    binwidth = bins[1] - bins[0]
    plt.ylabel("N. Toys / {:.2g}".format(binwidth), ha="right", y=1, fontsize="x-small")
    nom_text = "$\\mu = {:L}$\n$\\mu^0 = {}$".format(
        stats["mean"], stats["generated value"]
    )
    nom_at = AnchoredText(
        nom_text,
        loc="lower right",
        bbox_to_anchor=(1, 1),
        bbox_transform=ax1.transAxes,
        borderpad=0.1,
        frameon=False,
    )
    ax1.add_artist(nom_at)
    plt.xlabel("Fitted value")

    ax2 = plt.subplot(132)
    _, bins, _ = plt.hist(errs, bins=num_bins)
    binwidth = bins[1] - bins[0]
    plt.ylabel("N. Toys / {:.2g}".format(binwidth), ha="right", y=1, fontsize="x-small")
    if n0 is not None:
        n0string = "/ {}".format(n0)
    else:
        n0string = ""
    stat_text = "$\\mu = {:L}$\n$N = {}{}$".format(
        stats["stat. uncertainty"], len(noms), n0string
    )
    stat_at = AnchoredText(
        stat_text,
        loc="lower right",
        bbox_to_anchor=(1, 1),
        bbox_transform=ax2.transAxes,
        borderpad=0.1,
        frameon=False,
    )
    ax2.add_artist(stat_at)
    plt.xlabel("Stat. uncertainty")

    pulls = (noms - generated) / errs
    ax3 = plt.subplot(133)

    plot_pull_axes(ax3, pulls, bins=num_bins, variable=variable, stats=stats)
    plt.legend([], [])
    plt.setp(ax3.yaxis.get_label(), fontsize="x-small")

    txt = get_figures(
        noms,
        errs,
        generated,
        correlation_matrix,
        this_name=variable,
        all_names=all_variables,
    )

    infobox = AnchoredText(
        txt,
        loc="upper left",
        bbox_to_anchor=(1, 1),
        bbox_transform=plt.gca().transAxes,
        frameon=False,
        prop=dict(fontsize=6, wrap=True, family="monospace"),
    )
    plt.gca().add_artist(infobox)

    plt.subplots_adjust(0.05, 0.15, 0.8, 0.8, 0.2)
    save_with_pickle(filename)
    plt.close()
    logging.info(txt)
