"""
Common plotting boilerplate code

To setup matplotlib for using latex with some appropriate defaults, one can
import via

  from common_utils.plotting import plt
"""

import os

import matplotlib as mpl

# setup matplotlibrc
os.environ["TEXINPUTS"] = os.path.abspath(os.path.dirname(__file__)) + ":"
mpl.rc_file(os.path.abspath(os.path.join(os.path.dirname(__file__), "matplotlibrc")))
print(os.path.join(os.path.dirname(__file__), "matplotlibrc"))
print("os.environ['TEXINPUTS'] ", os.environ["TEXINPUTS"])
import matplotlib.pyplot as plt  # noqa

plt.style.use("seaborn-colorblind")  # commented that one out
# try:
# plt.style.use('tableau-colorblind10')
# except:
# print("There is no such package! Sorry!")
if os.environ.get("INTERACTIVE_PLOTTING") is not None:
    try:
        mpl.use("MacOSX")
    except ImportError:
        pass
    try:
        mpl.use("qt5agg")
    except ImportError as e:
        raise e
    plt.ion()
else:
    mpl.use("pgf")


def save_with_pickle(filename, tight_layout=False, *args, **kwargs):
    import logging
    import os
    import pickle
    from subprocess import check_call

    import matplotlib.pyplot as plt

    filepart, ext = os.path.splitext(filename)

    # first dump the pickled object
    try:
        with open(filepart + ".pkl", "wb") as f:
            logging.info("Pickled figure saved at {}".format(filepart + ".pkl"))
            pickle.dump(plt.gcf(), f, fix_imports=False)
    except Exception as e:
        logging.warn("Cannot pickle plot. Error: {}".format(e))

    if os.environ.get("INTERACTIVE_PLOTTING") is not None:
        plt.show()
        breakpoint()

    # then save the rendered object
    if tight_layout:
        if type(tight_layout) == dict:
            tight_kwargs = tight_layout
        else:
            tight_kwargs = {}
        plt.tight_layout(**tight_kwargs)

    if os.environ.get("PAPERPLOTS") is not None:
        print("I'm inside this PAPERPLOTS!")
        mpl.rc("savefig", dpi=600)
        for ext in [".pdf", ".ps", ".png"]:
            plt.savefig(filepart + ext, *args, **kwargs)
            logging.info("Plot saved at {}".format(filepart + ext))
            if ext == ".ps":
                check_call(["ps2eps", "-f", filepart + ext])
            elif ext == ".pdf":
                check_call(
                    [
                        "pdfjam",
                        filepart + ext,
                        "--papersize",
                        "{{{}in,{}in}}".format(*(3 * plt.gcf().get_size_inches())),
                        "--outfile",
                        filepart + ext,
                    ]
                )
    # else:
    # print(filename, *args, **kwargs)
    # try:
    plt.savefig(filename, *args, **kwargs)
    logging.info("Plot saved at {}".format(filename))
    # except:
    #    print("Sth is wrong with saving the file!")

    plt.clf()


def pull_y_ticks(small_label_tolerance=10000):
    """Complicated adjustment of yticks for a pull plot. Applied to current
    axes.
    """
    import logging

    from matplotlib.ticker import AutoMinorLocator

    major_ticks = plt.gca().get_yticks()[:-1]
    minor_ticks = plt.gca().get_yticks(minor=True)
    logging.debug(
        "Ylims {}\nMajor ticks {}\nMinor ticks {}".format(
            plt.ylim(), major_ticks, minor_ticks
        )
    )

    if major_ticks[0] == 0:
        major_ticks = major_ticks[1:]

    # update yticks
    plt.gca().set_yticks(major_ticks),
    plt.gca().set_yticklabels(
        [f"{t:.0f}" for t in major_ticks],
        fontsize="x-small" if major_ticks[-1] >= small_label_tolerance else None,
    )
    plt.gca().yaxis.set_minor_locator(AutoMinorLocator())
    logging.debug("New minor ticks {}".format(plt.gca().get_yticks(minor=True)))
