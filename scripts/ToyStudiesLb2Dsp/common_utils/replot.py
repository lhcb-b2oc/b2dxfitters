#! /usr/bin/env python3
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pickle

import IPython
from plotting import save_with_pickle


def parse_args():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("pickled_figure")
    parser.add_argument("output_file", default="build/replotted.pdf")
    parser.add_argument(
        "--ipython", help="""drop into ipython after loading the figure"""
    )
    parser.add_argument("--run", help="""run this after loading the figure""")

    return parser.parse_args()


def main():
    args = parse_args()

    with open(args.pickled_figure, "rb") as f:
        pickle.load(f)

    if args.ipython:
        IPython.embed()
    elif args.run:
        exec(args.run)

    save_with_pickle(args.output_file)


if __name__ == "__main__":
    main()
