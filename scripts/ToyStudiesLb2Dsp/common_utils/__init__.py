###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os


def joinabs(a, *p):
    """Shortcut to get the absolute path of joined path components"""
    path = os.path.join(a, *p)
    return os.path.abspath(path)


def setup_logging(args=None, print_config=True):
    """Setup logging and print args if `print_config` is True."""
    import logging
    import sys
    from pprint import pformat

    loglevel = getattr(logging, getattr(args, "loglevel", "").upper(), logging.INFO)
    logging.basicConfig(
        stream=sys.stdout, level=loglevel, format="%(levelname)s: %(message)s"
    )

    if print_config and args is not None:
        logging.info("Running with config:\n" + pformat(vars(args)))
