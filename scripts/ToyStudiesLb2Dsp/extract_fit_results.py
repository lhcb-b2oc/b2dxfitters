# coding: utf-8
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Script needs to run in Urania environment!
"""

from __future__ import division, print_function
import glob
import os
import sys
import argparse
import imp
import json
import re
import numpy as np
import uncertainties.unumpy as unp
from tqdm import tqdm
from uncertainties import ufloat
import logging
from itertools import product
from pprint import pformat
import pandas as pd
import uproot3 as uproot
from plot_pull import plot_pull_only
from plot_toy_variable import plot_pulls

try:
    import ROOT
except ModuleNotFoundError:
    print("ROOT not found!")
    pass

# import ROOT
# ROOT.PyConfig.IgnoreCommandLineOptions = True  # noqa
# Urania chains us to python2, use importlib.machinery.SourceFileLoader instead
# for python3

sys.path.insert(
    0, os.path.join(os.path.abspath(os.path.dirname(__file__)), "../")
)  # noqa

# import uproot   #changing to the newer version


# setup, prevent bad allocs due to this:
# https://root-forum.cern.ch/t/deleting-tfile-from-python/18417/2
# obsolete since ROOT 6.22 (?)
# ROOT.TFile.Open._creates = True


# naming between generator and fitter is different!
COMPONENT_NAMINGS = {
    "combbkg": "combinatorial",
    "dp0": "deltap0",
    "dp1": "deltap1",
    "p_0_glmcalib": "p0",
    "p_1_glmcalib": "p1",
    "dp_0_glmcalib": "deltap0",
    "dp_1_glmcalib": "deltap1",
    "tageff": "tageff",
    "atageff": "tagasymm",
}


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("workspace", type=str, nargs="+")
    parser.add_argument("-o", "--output", type=str, default="build/")
    parser.add_argument("--object-name", default="workspace")
    parser.add_argument("--toy-factory-config", type=str, nargs="+")
    parser.add_argument("--filetype", type=str, default="pdf")
    parser.add_argument(
        "--exclude",
        type=str,
        nargs="+",
        default=[],
        help="""
                        Define list of excluded seeds. All filenames that
                        contain any of the given arguments are excluded.""",
    )
    parser.add_argument("-a", "--auto-exclude", action="store_true", default=False)
    parser.add_argument(
        "--edm-limit",
        type=float,
        default=1,
        help="""Set limit
                        above which fits are exluded, default to 1.""",
    )
    parser.add_argument(
        "--exclude-via",
        type=str,
        default=None,
        help="""Exclude
                        fit results by scanning logfile. One of ['logfile',
                        'both', 'edm', 'hesse']""",
    )
    parser.add_argument(
        "--exclude-mask",
        type=str,
        help="""Python expression
                        that will be evaluated to exclude individual fits from
                        the sample. Expression must return a numpy mask or None.
                        The following context can be used: var == variablename,
                        nom == nominal values, err == fitted uncertainties,
                        pull == pulls.  An expression might look like
                        `(err > 0.5) if 'p0' in var else None`.
                        """,
    )
    parser.add_argument(
        "-e",
        "--exclude-resultfile",
        type=str,
        help="""Use
                        resultfile from other extraction run to read excluded
                        seeds. Helpful if extracting the simfit results after
                        the mdfit.""",
    )
    parser.add_argument(
        "--loglevel",
        default="info",
        type=str,
        help="""One of
                        logging's log levels""",
    )
    parser.add_argument("--all-correlations", default=False, action="store_true")
    parser.add_argument("--glm-convention", action="store_true", default=False)
    parser.add_argument(
        "--override-year",
        type=str,
        default=None,
        help="""
                        Override year that should be used to access generator
                        config parameters""",
    )
    parser.add_argument("--pull-only", action="store_true", default=False)
    parser.add_argument(
        "--skip-plots",
        action="store_true",
        default=False,
        help="""Dont produce any plot.""",
    )
    parser.add_argument(
        "--variables",
        type=str,
        nargs="+",
        help="""Override
                        set of variables that are extracted from the fit
                        results.""",
    )
    return parser.parse_args()


def get_variable_dict(root_object, exclude_failed=True, edm_limit=1):
    params = []
    const_params = []
    correlationMatrix = []
    none_retval = {
        "variables": None,
        "const_variables": None,
        "correlation": None,
        "edm": None,
    }
    fitresult = None
    if type(root_object) == ROOT.RooFitResult:
        fitresult = root_object
    elif type(root_object) == ROOT.RooWorkspace:
        # try to find a fitresult in a given workspace
        for generic_obj in root_object.allGenericObjects():
            if type(generic_obj) == ROOT.RooFitResult:
                fitresult = generic_obj
                break
    else:
        logging.warn(f"Could not extract fitresult from type {type(root_object)}")
        return none_retval

    if (
        exclude_failed and fitresult.covQual() == 3 and fitresult.edm() < edm_limit
    ) or not exclude_failed:
        for param in fitresult.floatParsFinal():
            params.append(param)
        for param in fitresult.constPars():
            const_params.append(param)
        correlationMatrix = np.array(
            [
                [fitresult.correlationMatrix()(i, j) for j in range(len(params))]
                for i in range(len(params))
            ]
        )
    if params:
        return {
            "variables": {
                v.GetName(): {
                    "obj": v,
                    "nom": v.getVal(),
                    "err": v.getError(),
                    "value": ufloat(v.getVal(), v.getError()),
                    "index": i,
                }
                for i, v in enumerate(params)
            },
            "const_variables": {
                v.GetName(): {
                    "nom": v.getVal(),
                    "index": i,
                }
                for i, v in enumerate(const_params)
            },
            "correlation": correlationMatrix,
            "edm": fitresult.edm(),
        }
    else:
        return none_retval


def get_ws(path, workspacename="workspace"):
    f = ROOT.TFile(path)
    ws = f.Get(workspacename)
    # ws = f[workspacename]
    f.Close()
    ROOT.SetOwnership(ws, True)  # python's gc should handle destruction
    return ws


def get_fitresult_df(filenames, objectname="workspace"):
    variables = {"edm": []}
    for fn in tqdm(filenames):
        ws = get_ws(fn, objectname)
        vdict = get_variable_dict(ws, exclude_failed=False)
        variables["edm"].append(vdict["edm"])
        for varname, value_dict in vdict["variables"].items():
            if f"{varname}_nom" not in variables:
                variables[f"{varname}_nom"] = [value_dict["nom"]]
                variables[f"{varname}_err"] = [value_dict["err"]]
            else:
                variables[f"{varname}_nom"].append(value_dict["nom"])
                variables[f"{varname}_err"].append(value_dict["err"])
        for varname, value_dict in vdict["const_variables"].items():
            if f"{varname}_nom" not in variables:
                variables[f"{varname}_nom"] = [value_dict["nom"]]
            else:
                variables[f"{varname}_nom"].append(value_dict["nom"])
    return pd.DataFrame(variables)


def serialize_root(obj):
    if type(obj) == np.ndarray:
        return obj.tolist()
    return str(obj).strip()


def lower_dict(d):
    """Return dict with all lower keys"""
    if type(d) == dict:
        return {k.lower(): lower_dict(v) for k, v in d.items()}
    else:
        return d


def match_fit_yield(component):
    _, mode, year = re.match(
        # matches polarity_mode_year
        r"{}_(\w+?)_(\w+?)_(\w+?)_evts".format(component),
        variable.lower(),
    ).groups()

    return mode, year


def get_tagging_value(tagparam, tagger, toy_config, glm_convention=False, year=None):
    tagparam = COMPONENT_NAMINGS.get(tagparam, tagparam)
    if year is None:
        value = toy_config["taggers"]["signal"][tagger]["calibration"][tagparam][0]
    else:
        try:
            value = toy_config["taggers"]["signal"][tagger]["calibration"][tagparam][
                year
            ][0]
        except TypeError:  # if same config accross taggers is used for different years
            value = toy_config["taggers"]["signal"][tagger]["calibration"][tagparam][0]
    if glm_convention:
        if tagparam == "p0":
            if year is None:
                value -= toy_config["taggers"]["signal"][tagger]["calibration"][
                    "avgeta"
                ][0]
            else:
                try:
                    value -= toy_config["taggers"]["signal"][tagger]["calibration"][
                        "avgeta"
                    ][year][0]
                except (
                    TypeError
                ):  # if same config accross taggers is used for different years
                    value -= toy_config["taggers"]["signal"][tagger]["calibration"][
                        "avgeta"
                    ][0]
        elif tagparam == "p1":
            value -= 1.0
    return value


backgrounds_dict = {
    "nbd2dsk": "bd2dsk",
    "nbd2dsstk": "bd2dsstk",
    "nbs2dsk": "bs2dsk",
    "nbs2dskst": "bs2dskst",
    "nbs2dspi": "bs2dspi",
    "nbs2dsrho": "bs2dsrho",
    "nbs2dsstk": "bs2dsstk",
    "nbs2dsstkst": "bs2dsstkst",
    "nbs2dsstpi": "bs2dsstpi",
    "nbs2dsstrho": "bs2dsstrho",
    "nlb2dsstp": "lb2dsstp",
    "nlb2lck": "lb2lck",
    "nlb2lcrho": "lb2lcrho",
    "nlb2scpi": "lb2scpi",
    "nlb2dsdelta": "lb2dsdelta",
}


def get_my_variable(var, toy_conf, my_decay_type="lb2dsp"):
    lowercases_whole_name = var.lower()
    lowercases_name = lowercases_whole_name.split("_")[0]
    key_name = backgrounds_dict[lowercases_name]
    if lowercases_name in backgrounds_dict:
        mode, year = match_fit_yield(lowercases_name)
        if year == "20152016":
            # print(toy_conf['components'][key_name]['lb2dsp']['2015'][mode]
            # + toy_conf['components'][key_name]['lb2dsp']['2016'][mode])
            return (
                toy_conf["components"][key_name][my_decay_type]["2015"][mode]
                + toy_conf["components"][key_name][my_decay_type]["2016"][mode]
            )
        elif year == "run2":
            # print("Successfully calculated for variable ", var)
            return (
                toy_conf["components"][key_name][my_decay_type]["2015"][mode]
                + toy_conf["components"][key_name][my_decay_type]["2016"][mode]
                + toy_conf["components"][key_name][my_decay_type]["2017"][mode]
                + toy_conf["components"][key_name][my_decay_type]["2018"][mode]
            )
        else:
            # print("toy_conf['components'][backgrounds_dict[lowercases_name]]"\
            # "['lb2dsp'][year][mode]:", toy_conf['components']\
            # [backgrounds_dict[lowercases_name]]['lb2dsp'][year][mode])
            return toy_conf["components"][backgrounds_dict[lowercases_name]][
                my_decay_type
            ][year][mode]
        # else:
        #    print("ERROR: Sth went wrong with entering variables! " \
        #           "lowercases_name: ", lowercases_name)


def get_generated_value(
    variable,
    toy_config,
    glm_convention=False,
    override_year=None,
    chosen_decay="lb2dsp",
):
    toy_config = lower_dict(toy_config)
    # print('toy_config \n', toy_config)
    if "nsig" in variable.lower():
        mode, year = match_fit_yield("nsig")
        if year == "20152016":
            return (
                toy_config["components"]["signal"][chosen_decay]["2015"][mode]
                + toy_config["components"]["signal"][chosen_decay]["2016"][mode]
            )
        elif year == "run2":
            return (
                toy_config["components"]["signal"][chosen_decay]["2015"][mode]
                + toy_config["components"]["signal"][chosen_decay]["2016"][mode]
                + toy_config["components"]["signal"][chosen_decay]["2017"][mode]
                + toy_config["components"]["signal"][chosen_decay]["2018"][mode]
            )
        else:
            return toy_config["components"]["signal"][chosen_decay][year][mode]

    if "ncombbkg" in variable.lower():
        mode, year = match_fit_yield("ncombbkg")
        if year == "20152016":
            return (
                toy_config["components"]["combinatorial"][chosen_decay]["2015"][mode]
                + toy_config["components"]["combinatorial"][chosen_decay]["2016"][mode]
            )
        elif year == "run2":
            return (
                toy_config["components"]["combinatorial"][chosen_decay]["2015"][mode]
                + toy_config["components"]["combinatorial"][chosen_decay]["2016"][mode]
                + toy_config["components"]["combinatorial"][chosen_decay]["2017"][mode]
                + toy_config["components"]["combinatorial"][chosen_decay]["2018"][mode]
            )
        else:
            return toy_config["components"]["combinatorial"][chosen_decay][year][mode]
    # if 'g1_f1_frac' in variable.lower():
    #     # Depending on the lower edge of the BeautyMass range, this is the
    #     # fraction between Bs2DsstPi and Bd2DsPi contribution...
    #     _, mode, year = variable.lower().replace('g1_f1_frac_', '').split('_')
    #     year = override_year or year
    #
    #     # DANGER hardcoded assumptions ahead
    #     if year in ['run2', '20152016']:
    #         bs2DsstPiYield = toy_config['components']\
    #               ['bs2dsstpi']['bs2dspi']['2015']['phipi']
    #         bd2DsPiYield = toy_config['components']\
    #               ['bd2dspi']['bs2dspi']['2015']['phipi']
    #         if '2016' in toy_config['components']['bs2dsstpi']['bs2dspi']:
    #             bs2DsstPiYield += toy_config['components']\
    #               ['bs2dsstpi']['bs2dspi']['2016']['phipi']
    #             bd2DsPiYield += toy_config['components']\
    #               ['bd2dspi']['bs2dspi']['2016']['phipi']
    #     else:
    #         bs2DsstPiYield = toy_config['components']\
    #               ['bs2dsstpi']['bs2dspi'][year]['phipi']
    #         bd2DsPiYield = toy_config['components']\
    #               ['bd2dspi']['bs2dspi'][year]['phipi']
    #     return float(bs2DsstPiYield) / float(bd2DsPiYield + bs2DsstPiYield)

    # if 'nbs2dsdsstpirho' in variable.lower():
    #     comp, _, mode, year, _ = variable.lower().split('_')
    #     year = override_year or year
    #     if year in ['run2', '20152016']:
    #         return float(
    #             toy_config['components']['bs2dsstpi']['lb2dsp']['2015'][mode]
    #             + toy_config['components']['bs2dsstpi']['lb2dsp']['2016'][mode]
    #             + toy_config['components']['bd2dspi']['lb2dsp']['2015'][mode]
    #             + toy_config['components']['bd2dspi']['lb2dsp']['2016'][mode]
    #         )
    #     else:
    #         return float(
    #             toy_config['components']['bs2dsstpi']['lb2dsp'][year][mode]
    #             + toy_config['components']['bd2dspi']['lb2dsp'][year][mode]
    #         )
    # if ('combbkg' in variable.lower()) and (variable.lower()[0] != 'n'):
    #     (
    #         possible_comp,
    #         possible_observable,
    #         possible_param,
    #         _,
    #         possible_dmode,
    #         possible_year
    #     ) = variable.lower().split('_')
    #     if year == '20152016':
    #         return (
    #             toy_config['components']['combinatorial']\
    #               [chosen_decay]['2015'][mode]
    #             + toy_config['components']['combinatorial']\
    #               [chosen_decay]['2016'][mode]
    #         )
    #     elif year == 'run2':
    #         return (
    #             toy_config['components']['combinatorial']\
    #               [chosen_decay]['2015'][mode]
    #             + toy_config['components']['combinatorial']\
    #               [chosen_decay]['2016'][mode]
    #             + toy_config['components']['combinatorial']\
    #               [chosen_decay]['2017'][mode]
    #             + toy_config['components']['combinatorial']\
    #               [chosen_decay]['2018'][mode]
    #             )
    #     else:
    #         return toy_config['components']['combinatorial']\
    #               [chosen_decay][year][mode]

    if variable.lower()[0] != "n":
        try:
            # first try with mass fit variables
            # variables look somewhat like this
            # Signal_CharmMass_a1_both_nonres_run2
            # Signal_BeautyMass_mean_both_all_run2
            (
                possible_comp,
                possible_observable,
                possible_param,
                _,
                possible_dmode,
                possible_year,
            ) = variable.lower().split("_")
            if possible_comp in COMPONENT_NAMINGS:
                possible_comp = COMPONENT_NAMINGS[possible_comp]

            possible_year = override_year or possible_year
            if possible_year == "20152016":
                possible_year = ["2015", "2016"]
            elif possible_year == "run2":
                possible_year = ["2015", "2016", "2017", "2018"]
            else:
                possible_year = [possible_year]

            if possible_dmode == "all":
                possible_dmode = [m.lower() for m in toy_config["charmmodes"]]
            else:
                possible_dmode = [possible_dmode]

            for comp, observable, param, dmode, year in product(
                [possible_comp],
                [possible_observable],
                [possible_param],
                possible_dmode,
                possible_year,
            ):
                try:
                    return toy_config["pdflist"][observable][comp][chosen_decay][year][
                        dmode
                    ][param][0]
                except TypeError:
                    return toy_config["pdflist"][observable][comp][chosen_decay][year][
                        dmode
                    ][param]
                except KeyError:
                    pass
        except ValueError:
            pass

        try:
            tagparam, tagger, year = variable.lower().split("_")
            return get_tagging_value(tagparam, tagger, toy_config, glm_convention, year)
        except (KeyError, ValueError):
            logging.debug(f"{variable} not matched for per-year tagging")
            pass

        try:
            s0, s1 = variable.lower().split("_")
            # with this format it might still be shared tagging
            if s0 in ["p0", "p1", "dp0", "dp1"]:
                tagparam = s0
                tagger = s1
                return get_tagging_value(tagparam, tagger, toy_config, glm_convention)
            # or an acceptance parameter
            elif "var" in s0:
                acceptance_var = s0
                return toy_config["resolutionacceptance"]["signal"]["acceptance"][
                    "knotcoefficients"
                ][int(acceptance_var.replace("var", "")) - 1]
        except (KeyError, ValueError, IndexError):
            pass

        try:  # now head for the glm convetion...
            par, par_num, glmname, tagger = variable.lower().split("_")
            tagparam = "_".join([par, par_num, glmname])
            return get_tagging_value(tagparam, tagger, toy_config, glm_convention)
        except (KeyError, ValueError):
            pass

        try:  # ... for per-year config
            par, par_num, glmname, tagger, year = variable.lower().split("_")
            year = override_year or year
            tagparam = "_".join([par, par_num, glmname])
            return get_tagging_value(tagparam, tagger, toy_config, glm_convention, year)
        except (KeyError, ValueError):
            pass

        try:
            # now try time fit variables
            if "deltam" in variable.lower():
                return toy_config["acp"]["signal"]["deltam"][0]
        except KeyError:
            pass

        try:
            if variable.lower() == "aprod":
                return toy_config["productionasymmetry"]["signal"][0]
        except KeyError:
            pass

        try:
            if variable.lower() == "adet":
                return toy_config["detectionasymmetry"]["signal"][0]
        except KeyError:
            pass
        logging.warning('Could not parse "{}".'.format(variable))
    if variable.lower()[0] == "n":
        return get_my_variable(variable, toy_config, chosen_decay)


def get_summed_generated_value(
    variable,
    configlist,
    glm_convention=False,
    override_year=None,
    decay_of_choice="lb2dsp",
):
    total_generated = None

    for config in configlist:
        generated = get_generated_value(
            variable,
            config,
            glm_convention,
            override_year=override_year,
            chosen_decay=decay_of_choice,
        )
        if generated is not None and (total_generated is None):
            total_generated = generated
        elif (
            generated is not None
            and variable.startswith("n")
            and variable.lower().endswith("evts")
        ):
            total_generated += generated

    return total_generated


def get_figures(
    noms,
    errs,
    generated,
    correlation_matrix=None,
    this_name=None,
    all_names=None,
    fitted_mean=None,
    fitted_std=None,
    min_correlation=0.15,
    numeric=False,
):
    # make sure we have numpy arrays
    # print("get_figures doesn't break here")
    noms, errs = np.array([noms, errs])

    len_data = noms.shape[0]

    pulls = (noms - generated) / errs

    mean = ufloat(
        np.average(noms, weights=1 / errs**2), np.sqrt(1 / (1 / errs**2).sum())
    )
    unweighted_mean = ufloat(noms.mean(), noms.std(ddof=1) / np.sqrt(len_data))
    pull_mean = ufloat(pulls.mean(), pulls.std(ddof=1) / np.sqrt(len_data))
    pull_width = ufloat(
        pulls.std(ddof=1), pulls.std(ddof=1) / np.sqrt(2 * len_data - 2)
    )
    err_mean = ufloat(errs.mean(), errs.std(ddof=1) / np.sqrt(len_data))
    dev_mean = ufloat(
        (noms - generated).mean(), (noms - generated).std(ddof=1) / np.sqrt(len_data)
    )
    rows = [
        this_name or "",
        r"weighted mean     = {:u}".format(mean),
        r"mean              = {:u}".format(unweighted_mean),
        r"stat. uncertainty = {:u}".format(err_mean),
        r"mean deviation    = {:u}".format(dev_mean),
        r"pull mean         = {:u}".format(pull_mean),
        r"pull width        = {:u}".format(pull_width),
    ]
    if fitted_mean is not None and fitted_std is not None:
        rows.append(r"{:<17} = {:u}".format("fitted mean", fitted_mean))
        rows.append(r"{:<17} = {:u}".format("fitted width", fitted_std))
    rows += [
        r"generated value   = {}".format(generated),
        r"Toys              = {}".format(len_data),
    ]
    txt = "\n".join(rows)

    if numeric:
        return {
            "weighted mean": mean,
            "mean": unweighted_mean,
            "stat. uncertainty": err_mean,
            "mean deviation": dev_mean,
            "pull mean": pull_mean,
            "pull width": pull_width,
            "generated value": generated,
            "Toys": len_data,
        }

    if (
        correlation_matrix is not None
        and all_names is not None
        and this_name is not None
    ):
        correlations = correlation_matrix[all_names.index(this_name)]
        indices = np.arange(len(correlations))[np.abs(correlations) > min_correlation]
        max_width = np.max([len(all_names[i]) for i in indices])
        if len(indices):
            txt += "\n\nCorrelated > {:.0f}% with:".format(100 * min_correlation)
            for i in indices:
                name = all_names[i]
                txt += "\n{{:<{}}} {{}}".format(max_width + 1).format(
                    name + ":", correlations[i]
                )
    elif (
        correlation_matrix is not None or all_names is not None or this_name is not None
    ):
        logging.warning(
            "Warning: requesting correlation_matrix without variable names."
            " Wont print."
        )
    return txt


def edm_from_fitresult_ok(ws: ROOT.RooFitResult, edm_limit=1):
    return ws.edm() < edm_limit


def check_logfile_excluded(filename, check_edm=True, check_hesse=True, limit=100):
    try:
        with open(filename) as f:
            lines = f.readlines()[-3000:]

        # result_idx = lines.index('[INFO]  Printing results\n')
    except ValueError:
        logging.debug("Results line not found in {}".format(filename))
        return False
    except IOError:
        logging.warn("Skipping {} due to missing logfile".format(filename))
        return False

    hesse_valid = False
    edm_valid = False
    for line in lines:  # [result_idx - 10:result_idx + 10]:
        if (
            "covariance matrix quality: Full"
            in line  # general for no sweight correction
            or "Hesse is valid" in line  # for Minuit2
        ):
            logging.debug(
                "Hesse state determined valide from file {} via line:\n{}".format(
                    filename, line
                )
            )
            hesse_valid = True
        if "estimated distance to minimum" in line:
            edm = re.search("estimated distance to minimum: (.*)$", line).group(1)
            logging.debug("EDM = {} determined from line:\n{}".format(edm, line))
            if float(edm) < limit:
                edm_valid = True

    logging.debug(
        "logfile {}: edm_valid: {} hesse_valid: {}".format(
            filename, edm_valid, hesse_valid
        )
    )

    return (not check_edm or edm_valid) and (not check_hesse or hesse_valid)


if __name__ == "__main__":
    args = parse_args()
    loglevel = getattr(logging, args.loglevel.upper(), None)
    logging.basicConfig(
        level=loglevel, format="%(levelname)s: %(message)s", stream=sys.stdout
    )
    logging.info("Running with args:\n{}".format(pformat(vars(args))))
    filenames = []
    for f in args.workspace:
        filenames += glob.glob(f)
    for excluded in args.exclude:
        filenames = [f for f in filenames if excluded not in f]
    filenames.sort()

    # here is the infrastructure for extracting the type of decay!
    # - to add more decays, enter the name in possible_decays
    print("Using filenames[0] for the decay type recognition: ", filenames[0], "\n")

    used_decay_name = ""
    possible_decays = ["lb2dsp", "lb2lcpi"]
    for decay_type in possible_decays:
        if decay_type in filenames[0].lower():
            print("In ", filenames[0].lower(), " the decay ", decay_type, " was found.")
            used_decay_name = decay_type
    if used_decay_name == "":
        raise ValueError(
            "The decay type was not recognised! "
            "Maybe add the decay type to the 'possible_decays' dictionary "
            "or change the name of the folder you store your fitresults in, "
            "so the decay type can be recognised from there? \n"
        )

    with uproot.recreate(
        os.path.join(args.output, "results.root")
    ) as f:  # here it is changed by me
        # with uproot.create(os.path.join(args.output, 'results.root')) as f:
        df = get_fitresult_df(filenames, args.object_name)
        f["results"] = uproot.newtree(df.dtypes)
        f["results"].extend({**df})
    results = {"data": {}, "correlations": {}, "seeds": []}

    # sanity check
    if args.auto_exclude and args.exclude_via:
        logging.warning(
            "Option `--auto-exclude` given but ignored due to" " `--exclude-via`."
        )
    if args.exclude_via:
        if args.exclude_via.lower() in ["logfile", "both"]:
            check_hesse = check_edm = True
        elif args.exclude_via.lower() == "hesse":
            check_hesse = True
            check_edm = False
        elif args.exclude_via.lower() == "edm":
            check_hesse = False
            check_edm = True
        else:
            logging.fatal(
                "Logfile exclude option {} not implemented.".format(args.exclude_via)
            )
            check_hesse = check_edm = False
    else:
        check_hesse = check_edm = False
    for fn in tqdm(filenames):
        ws = get_ws(fn, args.object_name)
        if args.exclude_via:
            logfilename = (
                fn.replace("_fitresult", "")
                .replace(".root", ".log")
                .replace("WS_MDFit", "sWeights_MDFit")
            )

            hesse_ok = (not check_hesse) or check_logfile_excluded(
                logfilename, check_hesse=True, check_edm=False
            )

            edm_ok = (not check_edm) or edm_from_fitresult_ok(
                ws, edm_limit=args.edm_limit
            )

            if hesse_ok and edm_ok:
                vdict = get_variable_dict(ws, False)
            else:
                vdict = dict(variables=None, correlation=None)
        else:
            vdict = get_variable_dict(
                ws,
                exclude_failed=args.auto_exclude,
                edm_limit=args.edm_limit,
            )
        results["data"][fn] = vdict["variables"]
        results["correlations"][fn] = vdict["correlation"]

    if args.variables:
        monitored_vars = args.variables
    else:
        monitored_vars = None
        for fn in filenames:
            if results["data"][fn]:
                # need to use floatParsFinal to ensure order of parameters
                params = results["data"][fn]
                monitored_vars = len(params) * [None]
                for key, value in params.items():
                    monitored_vars[value["index"]] = key
                break
            else:
                logging.debug("No results found in {}".format(fn))

    logging.info("Monitored variables: {}".format(monitored_vars))
    if monitored_vars is None:
        logging.critical("Did not find any variables in fitresults!")
        sys.exit(1)

    results["variables"] = {
        n: {"nom": [], "err": [], "pull": []} for n in monitored_vars
    }

    configs = []
    for configfile in args.toy_factory_config:
        path, fname = os.path.split(configfile)
        module_name = os.path.splitext(fname)[0]
        logging.debug("Loading config from {}.{}".format(path, module_name))
        configs.append(imp.load_source(module_name, configfile).getconfig())

    # fill generated values first
    for variable in sorted(monitored_vars):
        generated = get_summed_generated_value(
            variable,
            configs,
            args.glm_convention,
            args.override_year,
            decay_of_choice=used_decay_name,
        )
        # it is wrong in here!
        results["variables"][variable]["gen"] = generated

    if args.exclude_resultfile is not None:
        with open(args.exclude_resultfile) as f:
            previously_accepted = json.load(f)["seeds"]
    else:
        previously_accepted = None

    # now transform list of per-toy results into list of per-variable results
    # and test for excluded toys
    n_hess_excluded = 0
    n_eval_excluded = 0
    n_err_excluded = 0
    n_prev_excluded = 0
    for toy, result in results["data"].items():
        # toydir = os.path.split(toy)[0]
        # print("toydir: ", toydir)
        # seed = os.path.split(toydir)[1] #I'm changing this line
        seed = ""
        # Adding a different type of selector for seed number
        # whether the decay type name is longer
        if len(used_decay_name) == 6:
            seed = os.path.split(toy)[1][31:35]
        elif len(used_decay_name) == 7:
            seed = os.path.split(toy)[1][32:36]
        print("Reading file with seed: ", seed)

        if previously_accepted is not None and int(seed) not in previously_accepted:
            n_prev_excluded += 1
            continue
        if result is not None:
            skip_eval, skip_err = False, False
            noms = {}
            errs = {}
            pulls = {}
            for var in monitored_vars:
                try:
                    n = result[var]["nom"]
                    e = result[var]["err"]
                    g = results["variables"][var]["gen"]
                    if g is not None:
                        p = (n - g) / e
                    else:
                        n = e = p = np.nan
                except KeyError as e:
                    logging.fatal("Unable to find {} in {}".format(var, toy))
                    raise e
                if e == 0:
                    logging.debug("Excluding {} due to 0 err".format(toy))
                    skip_err = True
                noms[var] = n
                errs[var] = e
                pulls[var] = p
            if not skip_err:
                results["seeds"].append(int(seed))
                for var in monitored_vars:
                    results["variables"][var]["nom"].append(noms[var])
                    results["variables"][var]["err"].append(errs[var])
                    results["variables"][var]["pull"].append(pulls[var])
            else:
                n_err_excluded += 1
        else:
            n_hess_excluded += 1

    n0 = len(results["data"])
    masks = {}
    for var in monitored_vars:
        results["variables"][var]["nom"] = np.array(results["variables"][var]["nom"])
        results["variables"][var]["err"] = np.array(results["variables"][var]["err"])
        results["variables"][var]["pull"] = np.array(results["variables"][var]["pull"])
        if args.exclude_mask is not None:
            nom = results["variables"][var]["nom"]
            err = results["variables"][var]["err"]
            pull = results["variables"][var]["pull"]
            ones = np.ones_like(nom).astype(bool)
            mask = eval(args.exclude_mask)
            if mask is None:
                mask = ones
            masks[var] = mask
        if (
            len(results["variables"][var]["nom"]) == 0
            or len(results["variables"][var]["err"]) == 0
        ):
            logging.error(
                (
                    "No values extracted for variable {}. Nominal value "
                    "or error list is empty."
                ).format(var)
            )

    if args.exclude_mask is not None:
        total_mask = np.prod(list(masks.values()), axis=0).astype(bool)
        logging.debug("total exclude mask: {}".format(pformat(total_mask)))
        for var in monitored_vars:
            results["variables"][var]["nom"] = results["variables"][var]["nom"][
                total_mask
            ]
            results["variables"][var]["err"] = results["variables"][var]["err"][
                total_mask
            ]
            results["variables"][var]["pull"] = results["variables"][var]["pull"][
                total_mask
            ]
        n_eval_excluded = (~total_mask).sum()
    else:
        n_eval_excluded = 0

    excluded = n_eval_excluded + n_hess_excluded + n_err_excluded + n_prev_excluded
    logging.info(
        (
            "Excluded {ex} ({rel:.2f}%) out of {tot} toys.\n"
            "Due to bad Hess status: {hess}\n"
            "Due to exclude evaluation function: {eval}\n"
            "Due to zero error estimate: {err}\n"
            "Due to previously excluded: {prev}\n"
        ).format(
            ex=excluded,
            rel=excluded / len(results["data"]) * 100,
            tot=len(results["data"]),
            hess=n_hess_excluded,
            eval=n_eval_excluded,
            err=n_err_excluded,
            prev=n_prev_excluded,
        )
    )

    if not os.path.isdir(args.output):
        os.mkdir(args.output)

    correlations = [r for r in results["correlations"].values() if r is not None]
    correlation_matrix = unp.uarray(
        np.mean(correlations, axis=0),
        np.std(correlations, axis=0, ddof=1) / np.sqrt(len(correlations)),
    )

    # in addition to the fitted correlations, calculate the toy correlations
    toy_correlation_matrix = np.corrcoef(
        [results["variables"][v]["nom"] for v in monitored_vars],
        [results["variables"][v]["nom"] for v in monitored_vars],
    )
    for var in sorted(monitored_vars):
        i = monitored_vars.index(var)
        if args.all_correlations:
            logging.info('Correlation of var {}: "{}" with:'.format(i, var))
        results["variables"][var]["correlations"] = {}
        results["variables"][var]["toy_correlations"] = {}
        for corr in sorted(monitored_vars):
            j = monitored_vars.index(corr)
            results["variables"][var]["correlations"][corr] = (
                correlation_matrix[i, j].n,
                correlation_matrix[i, j].s,
            )
            results["variables"][var]["toy_correlations"][
                corr
            ] = toy_correlation_matrix[i, j]
            if args.all_correlations:
                logging.info(
                    "\t{: >3} {: <20} {: <16S} {:.3g}".format(
                        j, corr, correlation_matrix[i, j], toy_correlation_matrix[i, j]
                    )
                )

    with open(os.path.join(args.output, "results.json"), "w") as f:
        json.dump(
            dict(
                variables=results["variables"],
                seeds=results["seeds"],
                excluded=excluded,
            ),
            f,
            default=serialize_root,
            indent=2,
        )

    if len(args.toy_factory_config):
        for variable in sorted(monitored_vars):
            if results["variables"][variable]["gen"] is not None:
                if not args.skip_plots:
                    logging.info("\n")
                    pulldir = os.path.join(args.output, "pullonly")
                    if not os.path.isdir(pulldir):
                        os.mkdir(pulldir)
                    plot_pull_only(
                        results["variables"][variable]["nom"],
                        results["variables"][variable]["err"],
                        results["variables"][variable]["gen"],
                        variable=variable,
                        filename=os.path.join(
                            pulldir,
                            "{}{}_pullonly.{}".format(
                                variable,
                                "_" + args.override_year if args.override_year else "",
                                args.filetype.lower(),
                            ),
                        ),
                    )
                    if not args.pull_only and variable != "nSig_both_kkpi_run2_Evts":
                        # print("results['variables'][variable]['nom'] \n",
                        #       results['variables'][variable]['nom'])
                        # print("results['variables'][variable]['err'] \n",
                        #       results['variables'][variable]['err'])
                        # print("results['variables'][variable]['gen'] \n",
                        #       results['variables'][variable]['gen'])
                        plot_pulls(
                            results["variables"][variable]["nom"],
                            results["variables"][variable]["err"],
                            results["variables"][variable]["gen"],
                            variable=variable,
                            filename=os.path.join(
                                args.output,
                                "{}{}.{}".format(
                                    variable,
                                    (
                                        "_" + args.override_year
                                        if args.override_year
                                        else ""
                                    ),
                                    args.filetype.lower(),
                                ),
                            ),
                            correlation_matrix=correlation_matrix,
                            all_variables=monitored_vars,
                            n0=n0,
                        )

                    if not args.pull_only and variable == "nSig_both_kkpi_run2_Evts":
                        # print("results['variables'][variable]['nom'] \n",
                        #       results['variables'][variable]['nom'])
                        # print("results['variables'][variable]['err'] \n",
                        #       results['variables'][variable]['err'])
                        # print("results['variables'][variable]['gen'] \n",
                        #       results['variables'][variable]['gen'])
                        my_nom = []
                        my_err = []
                        my_gen = results["variables"][variable]["gen"]

                        for i in range(len(results["variables"][variable]["nom"])):
                            # print("results['variables'][variable]['nom'][i] \n",
                            # results['variables'][variable]['nom'][i])
                            my_pull = 0.0
                            my_pull = (
                                results["variables"][variable]["nom"][i] - my_gen
                            ) / results["variables"][variable]["err"][i]
                            if abs(my_pull) < 4.0:
                                my_nom.append(results["variables"][variable]["nom"][i])
                                my_err.append(results["variables"][variable]["err"][i])
                        print(my_nom)
                        print(my_err)
                        plot_pulls(
                            np.array(my_nom),
                            np.array(my_err),
                            results["variables"][variable]["gen"],
                            variable=variable,
                            filename=os.path.join(
                                args.output,
                                "{}{}.{}".format(
                                    variable,
                                    (
                                        "_" + args.override_year
                                        if args.override_year
                                        else ""
                                    ),
                                    args.filetype.lower(),
                                ),
                            ),
                            correlation_matrix=correlation_matrix,
                            all_variables=monitored_vars,
                            n0=n0,
                        )
