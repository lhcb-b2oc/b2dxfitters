#! /usr/bin/env python3

import json
import logging
import os
import sys
from argparse import ArgumentParser
from pprint import pformat
import numpy as np
from scipy.stats import norm, poisson
from uncertainties import ufloat
from common_utils.plotting import plt, save_with_pickle
from matplotlib.offsetbox import AnchoredText
from extract_fit_results import get_figures

sys.path.insert(
    0, os.path.join(os.path.abspath(os.path.dirname(__file__)), "../")
)  # noqa

BIN_SCALING = 2

TITLE_DICT = {
    # mdfit
    # 'CombBkg_BeautyMass_cB1': r'$c_{B_s^0}^1$',
    # 'CombBkg_BeautyMass_frac': r'$f_{B_s^0}$',
    # 'CombBkg_CharmMass_cD': r'$c_{D_s^-}$',
    # 'CombBkg_CharmMass_fracD': r'$f_{D_s^-}$',
    # 'g1_f1_frac': r'$f_\mathup{lowMass}$',
    # 'nBs2DsDsstPiRho': r'$N_\mathup{lowMass}$',
    # 'nCombBkg': r'$N_\mathup{Comb}$',
    # 'nSig': r'$N_{D_s^- \pi^+}$',
    # 'Signal_BeautyMass_mean': r'$\mu_{B_s^0}$',
    # 'Signal_BeautyMass_sigmaJ': r'$\sigma_{B_s^0}^J$',
    # 'Signal_CharmMass_mean': r'$\mu_{D_s^-}$',
    # 'Signal_CharmMass_sigmaG': r'$\sigma_{D_s^-}^G$',
    # 'Signal_CharmMass_sigmaI': r'$\sigma_{D_s^-}^H$',
    # 'Signal_CharmMass_sigmaJ': r'$\sigma_{D_s^-}^J$',
    "nSig_both_kkpi_run2_Evts": r"$N_{D_s^- p}$",
    "nSig_both_pkpi_run2_Evts": r"$N_{\bar{\Lambda}_c^- \pi^+}$",
    "nLb2Dsstp_both_kkpi_run2_Evts": r"$N_{\Lambda_b^0 \to D_s^{*-} p}$",
    "nCombBkg_both_kkpi_run2_Evts": r"$N_{Comb}$",
    "nBs2DsstRho_both_kkpi_run2_Evts": r"$N_{B_s^0 \to D_s^{*-} \rho^{+}}$",
    "nBs2DsstPi_both_kkpi_run2_Evts": r"$N_{B_s^0 \to D_s^{*-} \pi^{+}}$",
    "nBs2DsstKst_both_kkpi_run2_Evts": r"$N_{B_s^0 \to D_s^{*\mp} K^{*\pm}}$",
    "nBs2DsstK_both_kkpi_run2_Evts": r"$N_{B_s^0 \to D_s^{*\mp} K^{\pm}}$",
    "nBs2DsRho_both_kkpi_run2_Evts": r"$N_{B_s^0 \to D_s^{-} \rho^{+}}$",
    "nBs2DsPi_both_kkpi_run2_Evts": r"$N_{B_s^0 \to D_s^{-} \pi^{+}}$",
    "nBs2DsKst_both_kkpi_run2_Evts": r"$N_{B_s^0 \to D_s^{\mp} K^{*\pm}}$",
    "nBs2DsK_both_kkpi_run2_Evts": r"$N_{B_s^0 \to D_s^{\mp} K^{\pm}}$",
    "nBd2DsstK_both_kkpi_run2_Evts": r"$N_{B^0 \to D_s^{*-} K^{+}}$",
    "nBd2DsK_both_kkpi_run2_Evts": r"$N_{B^0 \to D_s^{-} K^{+}}$",
    "nLb2ScPi_both_pkpi_run2_Evts": r"$N_{\bar{\Lambda}_b^0 \to "
    "\bar{\Sigma}_c^{-} \pi^{+}}$",
    "nLb2LcRho_both_pkpi_run2_Evts": r"$N_{\bar{\Lambda}_b^0 \to "
    "\bar{\Lambda}_c^- \rho^{+}}$",
    "nLb2LcK_both_pkpi_run2_Evts": r"$N_{\bar{\Lambda}_b^0 \to "
    "\bar{\Lambda}_c^- K^{+}}$",
    "nCombBkg_both_pkpi_run2_Evts": r"$N_{Comb}$",
    "Signal_BeautyMass_sigmaJ_both_all_run2": r"$\sigma_{Sig}^{J}$",
    "Signal_BeautyMass_sigmaI_both_all_run2": r"$\sigma_{Sig}^{H}$",
    "Signal_BeautyMass_mean_both_all_run2": r"$\mu_{Sig}$",
    "CombBkg_BeautyMass_a_both_all_run2": r"$A_{Comb}$",
    "CombBkg_BeautyMass_cB_both_all_run2": r"$c_B$",
    "nLb2DsDelta_both_kkpi_run2_Evts": r"$N_{\Lambda_b^0 \to D_s^{-} \Delta^{+}}$",
    # sfit
    # 'adet': r'$a_\text{det}$',
    # 'aprod': r'$a_\text{prod}$',
    # 'DeltaMs_Bs2DsPi': r'$\Delta m_s$',
}


class ConfigurationError(Exception):
    pass


def get_variable_tex(var):
    if var in TITLE_DICT:
        return TITLE_DICT[var]

    param = var.split("_")[0]
    if "var" in param:
        rval = "$c_{" + param[3:] + "}$"
        return rval

    if "tageff" in var.lower() or "p0" in var.lower() or "p1" in var.lower():
        rval = ""
        if var.startswith("a"):
            rval = r"$\Delta\varepsilon_\text{tag}"
        elif var.startswith("tag"):
            rval = r"$\varepsilon_\text{tag}"
        elif var.startswith("dp"):
            rval = r"$\Delta p_" + var[2]
        elif var.startswith("p"):
            rval = r"$p_" + var[1]
        if "OS" in var:
            rval += r"^\text{OS}"
        elif "SS" in var:
            rval += r"^\text{SS}"
        rval += "$"
        return rval

    # try to find variable name by cutting away last parts
    part_var = ""
    for part in var.split("_"):
        part_var += part
        try:
            return TITLE_DICT[part_var]
        except KeyError:
            part_var += "_"

    return var


def plot_pull_axes(
    ax, pulls, bins, variable="X", stats=None, top_loc="lower", toylabel=None
):
    if stats is None:
        stats = {
            "pull mean": ufloat(pulls.mean(), pulls.std(ddof=1) / np.sqrt(len(pulls))),
            "pull width": ufloat(
                pulls.std(ddof=1), pulls.std(ddof=1) / np.sqrt(2 * len(pulls) - 2)
            ),
        }

    plt.sca(ax)
    entries, bins = np.histogram(pulls, bins=bins)
    binwidth = bins[1] - bins[0]
    xs = np.linspace(bins[0], bins[-1], 300)
    int_low, int_high = poisson.interval(0.682, entries)
    low_err = entries - int_low
    high_err = int_high - entries
    if toylabel is None:
        toylabel = "{} Toys".format(len(pulls))
    plt.errorbar(
        bins[:-1] + binwidth / 2,
        entries,
        xerr=binwidth / 2,
        yerr=(low_err, high_err),
        fmt=".",
        label=toylabel,
        zorder=0,
    )
    plt.plot(
        xs,
        norm.pdf(xs) * entries.sum() * binwidth,
        "--",
        alpha=0.8,
        label="Normal Dist.",
        zorder=1,
    )

    plt.xlabel(r"Pull of {}".format(get_variable_tex(variable)), ha="right", x=1)
    plt.ylabel("N. Toys / {:.2g}".format(binwidth), ha="right", y=1)
    at = AnchoredText(
        "$\\mu = {:L}$\n$\\sigma = {:L}$".format(
            stats["pull mean"], stats["pull width"]
        ),
        loc="{} right".format(top_loc),
        bbox_to_anchor=(1, 1),
        bbox_transform=plt.gca().transAxes,
        borderpad=0.1,
        frameon=False,
    )
    ax.add_artist(at)
    plt.legend(
        loc="{} left".format(top_loc),
        frameon=False,
        bbox_to_anchor=(0, 1),
        borderpad=0.1,
        handlelength=1.1,
    )


def plot_pull_only(noms, errs, generated=0, variable="X", filename="pulls.pdf"):
    num_bins = int(BIN_SCALING * np.sqrt(noms.shape[0]))

    pulls = (noms - generated) / errs
    stats = get_figures(noms, errs, generated, numeric=True, this_name=variable)
    plt.rcParams["axes.formatter.use_mathtext"] = True

    plt.figure(figsize=(5.8, 3.5))
    _ = plt.subplot(111)
    plot_pull_axes(plt.gca(), pulls, bins=num_bins, stats=stats, variable=variable)

    plt.subplots_adjust(0.17, 0.23, 0.97, 0.76)
    save_with_pickle(filename)
    plt.close()


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("--results", type=str, nargs="+", required=True)
    parser.add_argument("--vars", type=str, nargs="+", required=True)
    parser.add_argument("--output", type=str, required=True)
    parser.add_argument("--xaxis-title", type=str)
    parser.add_argument("--xlim", type=float, nargs=2, default=[None, None])
    parser.add_argument("--xhigh", type=float, help="""Selection""")
    parser.add_argument("--xlow", type=float, help="""Selection""")
    parser.add_argument("--toylabel")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    print("I entered the file!")
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    logging.info(f"Running with config:\n{pformat(vars(args))}")
    pulls = []
    if len(args.results) != len(args.vars):
        if len(args.results) != 1 and len(args.vars) != 1:
            results = []
            vars_ = []
            for r in args.results:
                results += len(args.vars) * [r]
            for v in args.vars:
                vars_ += len(args.results) * [v]
            args.vars = vars_
            args.results = results
            logging.info(
                "Expanding variables and result files:\n"
                f"results: {args.results}\nvars: {args.vars}"
            )
        if len(args.results) == 1:
            logging.info(f"Using {args.results} for all variables.")
            args.results = len(args.vars) * args.results
        if len(args.vars) == 1:
            logging.info(f"Using {args.vars} for all results.")
            args.vars = len(args.results) * args.vars

    for result, var in zip(args.results, args.vars):
        with open(result, "r") as f:
            results = json.load(f)
            pulls.append(results["variables"][var]["pull"])
    pulls = np.concatenate(pulls)
    if args.xhigh is not None:
        pulls = pulls[pulls <= args.xhigh]
    if args.xlow is not None:
        pulls = pulls[pulls >= args.xlow]
    if args.xlim[0] is None:
        args.xlim[0] = pulls.min()
    if args.xlim[1] is None:
        args.xlim[1] = pulls.max()
    plt.figure(figsize=(5.4, 3.5))
    ax = plt.subplot(111)
    if args.xaxis_title is None:
        args.xaxis_title = args.vars[0]
    plot_pull_axes(
        ax,
        pulls,
        bins=np.linspace(*args.xlim, BIN_SCALING * int(np.sqrt(len(pulls)))),
        variable=args.xaxis_title,
        top_loc="upper",
        toylabel=args.toylabel,
    )
    plt.subplots_adjust(0.16, 0.21, 0.98, 0.98)
    save_with_pickle(args.output)
