#!/bin/sh
# -*- mode: python; coding: utf-8 -*-
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# vim: ft=python:sw=4:tw=78:expandtab
# ---------------------------------------------------------------------------
# @file fitShapes.py
#
# @brief Obtain shapes
#
# @author Vincenzo Battista
# @date 2016-03-31
#
# ---------------------------------------------------------------------------
# This file is used as both a shell script and as a Python script.
""":"
# This part is run by the shell. It does some setup which is convenient to save
# work in common use cases.

# make sure the environment is set up properly
if test -n "$CMTCONFIG" \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersDict.so \
     -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersLib.so; then
    # all ok, software environment set up correctly, so don't need to do
    # anything
    true
else
    if test -n "$CMTCONFIG"; then
    # clean up incomplete LHCb software environment so we can run
    # standalone
        echo Cleaning up incomplete LHCb software environment.
        PYTHONPATH=`echo $PYTHONPATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
        export PYTHONPATH
        LD_LIBRARY_PATH=`echo $LD_LIBRARY_PATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
        export LD_LIBRARY_PATH
        exec env -u CMTCONFIG -u B2DXFITTERSROOT "$0" "$@"
    fi
    # automatic set up in standalone build mode
    if test -z "$B2DXFITTERSROOT"; then
        cwd="$(pwd)"
        # try to find from where script is executed, use current directory as
        # fallback
        tmp="$(dirname $0)"
        tmp=${tmp:-"$cwd"}
        # convert to absolute path
        tmp=`readlink -f "$tmp"`
        # move up until standalone/setup.sh found, or root reached
        while test \( \! -d "$tmp"/standalone \) -a -n "$tmp" -a "$tmp"\!="/"; do
            tmp=`dirname "$tmp"`
        done
        if test -d "$tmp"/standalone; then
            cd "$tmp"/standalone
            . ./setup.sh
        else
            echo `basename $0`: Unable to locate standalone/setup.sh
            exit 1
        fi
        cd "$cwd"
        unset tmp
        unset cwd
    fi
fi

# figure out which custom allocators are available
# prefer jemalloc over tcmalloc
for i in libjemalloc libtcmalloc; do
    for j in `echo "$LD_LIBRARY_PATH" | tr ':' ' '` \
        /usr/local/lib /usr/lib /lib; do
        for k in `find "$j" -name "$i"'*.so.?' | sort -r`; do
            if test \! -e "$k"; then
            continue
        fi
        echo adding $k to LD_PRELOAD
        if test -z "$LD_PRELOAD"; then
            export LD_PRELOAD="$k"
            break 3
        else
            export LD_PRELOAD="$LD_PRELOAD":"$k"
            break 3
        fi
    done
    done
done

# set batch scheduling (if schedtool is available)
schedtool="`which schedtool 2>/dev/zero`"
if test -n "$schedtool" -a -x "$schedtool"; then
    echo "enabling batch scheduling for this job (schedtool -B)"
    schedtool="$schedtool -B -e"
else
    schedtool=""
fi

# set ulimit to protect against bugs which crash the machine: 3G vmem max,
# no more then 8M stack
ulimit -v $((3072 * 1024))
ulimit -s $((   8 * 1024))

# trampoline into python
exec $schedtool /usr/bin/time -v env python -O "$0" - "$@"
"""
# -----------------------------------------------------------------------------
# Load necessary libraries
# -----------------------------------------------------------------------------
# "
from __future__ import print_function

__doc__ = """ real docstring """
import sys
import argparse
from ROOT import (
    TH1F,
    GeneralUtils,
    RooAbsData,
    RooAbsReal,
    RooArgList,
    RooCategory,
    RooDataHist,
    RooFit,
    RooKeysPdf,
    RooRealVar,
    RooWorkspace,
    TCanvas,
    TFile,
    TLatex,
    TString,
    gROOT,
    gStyle,
    kGreen,
    kMagenta,
    kOrange,
    kRed,
    kWhite,
    string,
)
from B2DXFitters import BasicMDFitPdf, Bd2DhModels, Bs2Dsh2011TDAnaModels
from B2DXFitters.WS import WS
from B2DXFitters import FitResultGrabberUtils
from B2DXFitters.MDFitSettingTranslator import Translator
from B2DXFitters.mdfitutils import checkMerge as checkMerge
from B2DXFitters.mdfitutils import getBinCut as getBinCut
from B2DXFitters.mdfitutils import getBins as getBins
from B2DXFitters.mdfitutils import getBkgTypes as getBkgTypes
from B2DXFitters.mdfitutils import getExpectedValue as getExpectedValue
from B2DXFitters.mdfitutils import getExpectedYield as getExpectedYield
from B2DXFitters.mdfitutils import getObservables as getObservables
from B2DXFitters.mdfitutils import getPDFNameFromConfig as getPDFNameFromConfig
from B2DXFitters.mdfitutils import getPIDKComponents as getPIDKComponents
from B2DXFitters.mdfitutils import getSampleModeYear as getSampleModeYear
from B2DXFitters.mdfitutils import getSampleYear as getSampleYear
from B2DXFitters.mdfitutils import getShapeTypes as getShapeTypes
from B2DXFitters.mdfitutils import getSigOrCombPDF as getSigOrCombPDF
from B2DXFitters.mdfitutils import getStdVector as getStdVector
from B2DXFitters.mdfitutils import getType as getType
from B2DXFitters.mdfitutils import readVariables as readVariables
from B2DXFitters.mdfitutils import readVariablesForShapes as readVariablesForShapes
from B2DXFitters.mdfitutils import getTopLevelSMY as getTopLevelSMY
from B2DXFitters.mdfitutils import (
    setConstantIfSoConfigured as setConstantIfSoConfigured,
)

gROOT.SetBatch()


# ------------------------------------------------------------
def makeCanvas(
    dataset,
    model,
    var,
    result,
    min,
    max,
    bins,
    text,
    title,
    Xtitle,
    weighted,
    logScale,
    noChi2,
    minY,
    save,
):
    # Create canvas
    canv = TCanvas(
        "canv_" + dataset.GetName(), "Canvas of " + dataset.GetTitle(), 1200, 1000
    )
    canv.Divide(1, 2)

    # Setup upper pad
    canv.cd(1)
    pad1 = canv.GetPad(1)
    pad1.cd()
    pad1.SetPad(0.005, 0.05, 1.0, 1.0)
    pad1.SetBorderMode(0)
    pad1.SetBorderSize(-1)
    pad1.SetFillStyle(0)
    pad1.SetBottomMargin(0.30)
    pad1.SetLeftMargin(0.17)
    pad1.SetTopMargin(0.08)
    pad1.SetRightMargin(0.05)
    if var == "lab0_MassFitConsD_M" or var == "BeautyMass":
        pad1.SetRightMargin(0.08)
    pad1.SetFillStyle(0)
    pad1.SetTickx(0)
    frame_top = makeTopFrame(var, title, min, max, bins)

    if logScale:
        pad1.SetLogy()

    dataset.plotOn(frame_top)
    model.plotOn(frame_top, RooFit.LineStyle(1), RooFit.LineColor(4))

    # Build pull histogram and get chi2/ndof
    pullHist = frame_top.pullHist()

    if not weighted:
        if None is not result:
            # Fitted parameters: compute ndof properly
            chi2ndof = frame_top.chiSquare(result.floatParsFinal().getSize())
        else:
            # No fit: ndof only given by binning
            chi2ndof = frame_top.chiSquare(0)
    else:
        if None is not result:
            obs = result.floatParsFinal()
            TString(var.GetName())
            pdfName = (
                "pdf_Norm["
                + var.GetName()
                + "]_Range[fit_nll_pdf_combData]_NormRange[fit_nll_pdf_combData]"
            )
            print(pdfName)
            chi2ndof = frame_top.chiSquare(pdfName, "h_combData", obs.getSize())
        else:
            # No fit: ndof only given by binning
            chi2ndof = frame_top.chiSquare(0)
    chi2ndof = round(chi2ndof, 2)

    # Plot other pdf components (if any)
    if model.InheritsFrom("RooAddPdf"):
        pdflist = model.pdfList()
        print("List of PFDs to plot:")
        pdflist.Print("v")
        print("Plotting component:")
        color = [kRed, kOrange, kMagenta + 2, kGreen + 3, kMagenta]
        for pdf in range(0, pdflist.getSize()):
            print(pdflist[pdf].GetName(), color[pdf])
            model.plotOn(
                frame_top,
                RooFit.Components(pdflist[pdf].GetName()),
                RooFit.LineStyle(2),
                RooFit.LineColor(color[pdf]),
            )

    frame_top.GetYaxis().SetRangeUser(0.0, frame_top.GetMaximum() * 1.1)
    if logScale:
        frame_top.GetYaxis().SetRangeUser(float(minY), frame_top.GetMaximum() * 1.5)
    unit = "MeV/c^{2}"
    frame_top.GetYaxis().SetTitle(
        (
            TString.Format(
                "#font[132]{Candidates / ( "
                + "{0:0.2f}".format(frame_top.GetXaxis().GetBinWidth(1))
                + " "
                + unit
                + ")}"
            )
        ).Data()
    )

    frame_top.Draw()

    # Add some text
    lhcbtext = makeText(0.06)
    lhcbtext.DrawTextNDC(0.93, 0.88, text)
    chi2text = makeText(0.06)
    if not noChi2:
        chi2text.DrawLatexNDC(0.70, 0.80, "#chi^{2}/ndof=" + str(chi2ndof))
    print(chi2ndof)
    pad1.Update()
    pad1.Draw()

    # Setup lower pad
    canv.cd(2)
    pad2 = canv.GetPad(2)
    pad2.cd()
    pad2.SetPad(0.005, 0.005, 1.0, 0.37)
    pad2.cd()
    pad2.SetBorderMode(0)
    pad2.SetBorderSize(-1)
    pad2.SetFillStyle(0)
    pad2.SetBottomMargin(0.40)
    pad2.SetLeftMargin(0.17)
    pad2.SetRightMargin(0.05)
    pad2.SetTickx(0)
    pad2.SetGridx()
    pad2.SetGridy()
    pad2.SetLogy(0)

    gStyle.SetOptLogy(0)

    frame_bot = makeBottomFrame(var, Xtitle, min, max, bins)
    pullHist.SetLineColor(0)  # errorbar->you shall not pass!
    frame_bot.addPlotable(pullHist, "B")  # P
    pullHist.SetFillColor(1)
    frame_bot.GetYaxis().SetRangeUser(-5, 5)

    frame_bot.Draw()

    pad2.Update()
    pad2.Draw()
    canv.Update()

    canv.SaveAs(save + ".pdf")
    canv.SaveAs(save + ".png")
    canv.SaveAs(save + ".C")
    canv.SaveAs(save + ".root")


# ------------------------------------------------------------
def makeTopFrame(var, title, min, max, bins):
    frame_top = var.frame(min, max, bins)
    frame_top.SetTitle("")
    frame_top.GetXaxis().SetLabelSize(0.065)
    frame_top.GetYaxis().SetLabelSize(0.065)
    frame_top.GetXaxis().SetLabelFont(132)
    frame_top.GetYaxis().SetLabelFont(132)
    frame_top.GetXaxis().SetLabelOffset(0.006)
    frame_top.GetYaxis().SetLabelOffset(0.006)
    frame_top.GetXaxis().SetLabelColor(kWhite)
    frame_top.GetXaxis().SetTitleSize(0.065)
    frame_top.GetYaxis().SetTitleSize(0.065)
    frame_top.GetYaxis().SetTitleOffset(0.02)
    frame_top.GetYaxis().SetNdivisions(512)
    frame_top.GetXaxis().SetTitleOffset(1.00)
    frame_top.GetYaxis().SetTitleOffset(1.40)

    return frame_top


# ------------------------------------------------------------
def makeBottomFrame(var, Xtitle, min, max, bins):
    frame_bot = var.frame(min, max, bins)
    frame_bot.SetTitle("")
    frame_bot.GetYaxis().SetTitle("")
    frame_bot.GetXaxis().SetTitle(Xtitle)
    frame_bot.GetYaxis().SetTitleSize(0.09)
    frame_bot.GetYaxis().SetTitleOffset(0.26)
    frame_bot.GetYaxis().SetTitleFont(62)
    frame_bot.GetYaxis().SetNdivisions(106)
    frame_bot.GetYaxis().SetLabelSize(0.12)
    frame_bot.GetYaxis().SetLabelOffset(0.006)
    frame_bot.GetXaxis().SetLabelOffset(0.06)
    frame_bot.GetXaxis().SetTitleSize(0.16)
    frame_bot.GetXaxis().SetTitleFont(132)
    frame_bot.GetXaxis().SetTitleOffset(1.2)
    frame_bot.GetXaxis().SetNdivisions(5)
    frame_bot.GetYaxis().SetNdivisions(5)
    frame_bot.GetYaxis().SetRangeUser(-5, 5)
    frame_bot.GetXaxis().SetLabelSize(0.16)
    frame_bot.GetXaxis().SetLabelFont(132)
    frame_bot.GetYaxis().SetLabelFont(132)

    return frame_bot


# ------------------------------------------------------------
def makeText(size):
    text = TLatex()
    text.SetTextFont(132)
    text.SetTextColor(1)
    text.SetTextSize(size)
    text.SetTextAlign(132)

    return text


# ------------------------------------------------------------
def BuildExponentialPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "E"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_cB_" + samplemode,
            typemode + "_" + varName + "_cB_" + samplemode,
            *pdfDict["cB"]
        ),
    )

    # Build PDF
    pdf = BasicMDFitPdf.buildExponentialPDF(obs, workOut, samplemode, typemode, debug)

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildDoubleExponentialPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "doubleExpo"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_cB1_" + samplemode,
            typemode + "_" + varName + "_cB1_" + samplemode,
            *pdfDict["cB1"]
        ),
    )

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_cB2_" + samplemode,
            typemode + "_" + varName + "_cB2_" + samplemode,
            *pdfDict["cB2"]
        ),
    )

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frac_" + samplemode,
            typemode + "_" + varName + "_frac_" + samplemode,
            *pdfDict["frac"]
        ),
    )

    # Build PDF
    pdf = BasicMDFitPdf.buildDoubleExponentialPDF(
        obs, workOut, samplemode, typemode, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildExponentialPlusConstantPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "EplusC"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_cB_" + samplemode,
            typemode + "_" + varName + "_cB_" + samplemode,
            *pdfDict["cB"]
        ),
    )

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_fracExpo_" + samplemode,
            typemode + "_" + varName + "_fracExpo_" + samplemode,
            *pdfDict["fracExpo"]
        ),
    )

    # Build PDF
    pdf = Bd2DhModels.buildExponentialPlusConstantPDF(
        obs, workOut, samplemode, typemode, debug
    )

    return pdf


# ------------------------------------------------------------
def BuildGaussPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "G"
    samplemode = nickname
    shiftMean = pdfDict["shiftMean"]

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_mean_" + samplemode,
            typemode + "_" + varName + "_mean_" + samplemode,
            *pdfDict["mean"]
        ),
    )

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma_" + samplemode,
            typemode + "_" + varName + "_sigma_" + samplemode,
            *pdfDict["sigma"]
        ),
    )

    # Build PDF
    pdf = BasicMDFitPdf.buildGaussPDF(
        obs, workOut, samplemode, typemode, shiftMean, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildDoubleGaussPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "DG"
    samplemode = nickname
    sameMean = pdfDict["sameMean"]

    WS(
        workOut,
        RooRealVar(
            (
                "Signal_" + varName + "_mean_" + samplemode
                if sameMean
                else typemode + "_" + varName + "_mean_" + samplemode
            ),
            (
                "Signal_" + varName + "_mean_" + samplemode
                if sameMean
                else typemode + "_" + varName + "_mean_" + samplemode
            ),
            *pdfDict["mean"]
        ),
    )

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma1_" + samplemode,
            typemode + "_" + varName + "_sigma1_" + samplemode,
            *pdfDict["sigma1"]
        ),
    )

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma2_" + samplemode,
            typemode + "_" + varName + "_sigma2_" + samplemode,
            *pdfDict["sigma2"]
        ),
    )

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frac_" + samplemode,
            typemode + "_" + varName + "_frac_" + samplemode,
            *pdfDict["frac"]
        ),
    )

    # Build PDF
    pdf = BasicMDFitPdf.buildDoubleGaussPDF(
        obs, workOut, samplemode, typemode, False, sameMean, False, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildCrystalBallPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "CB"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_mean_" + samplemode,
            typemode + "_" + varName + "_mean_" + samplemode,
            *pdfDict["mean"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_alpha_" + samplemode,
            typemode + "_" + varName + "_alpha_" + samplemode,
            *pdfDict["alpha"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n_" + samplemode,
            typemode + "_" + varName + "_n_" + samplemode,
            *pdfDict["n"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma_" + samplemode,
            typemode + "_" + varName + "_sigma_" + samplemode,
            *pdfDict["sigma"]
        ),
    )

    # Build PDF
    pdf = BasicMDFitPdf.buildCrystalBallPDF(obs, workOut, samplemode, typemode, debug)

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildCrystalBallPlusExponentialPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "CBplusE"
    samplemode = nickname
    shiftMean = pdfDict["shiftMean"]

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_mean_" + samplemode,
            typemode + "_" + varName + "_mean_" + samplemode,
            *pdfDict["mean"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_alpha_" + samplemode,
            typemode + "_" + varName + "_alpha_" + samplemode,
            *pdfDict["alpha"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n_" + samplemode,
            typemode + "_" + varName + "_n_" + samplemode,
            *pdfDict["n"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaCB_" + samplemode,
            typemode + "_" + varName + "_sigmaCB_" + samplemode,
            *pdfDict["sigmaCB"]
        ),
    )
    if shiftMean:
        WS(
            workOut,
            RooRealVar(
                typemode + "_" + varName + "_shift_" + samplemode,
                typemode + "_" + varName + "_shift_" + samplemode,
                *pdfDict["shift"]
            ),
        )

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_cB_" + samplemode,
            typemode + "_" + varName + "_cB_" + samplemode,
            *pdfDict["cB"]
        ),
    )

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_fracExpo_" + samplemode,
            typemode + "_" + varName + "_fracExpo_" + samplemode,
            *pdfDict["fracExpo"]
        ),
    )

    # Build PDF
    pdf = Bd2DhModels.buildCrystalBallPlusExponentialPDF(
        obs, workOut, samplemode, typemode, shiftMean, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildCrystalBallPlusGaussianPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "CBplusG"
    samplemode = nickname
    shiftMean = pdfDict["shiftMean"]

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_mean_" + samplemode,
            typemode + "_" + varName + "_mean_" + samplemode,
            *pdfDict["mean"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_alpha_" + samplemode,
            typemode + "_" + varName + "_alpha_" + samplemode,
            *pdfDict["alpha"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n_" + samplemode,
            typemode + "_" + varName + "_n_" + samplemode,
            *pdfDict["n"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaCB_" + samplemode,
            typemode + "_" + varName + "_sigmaCB_" + samplemode,
            *pdfDict["sigmaCB"]
        ),
    )
    if shiftMean:
        WS(
            workOut,
            RooRealVar(
                typemode + "_" + varName + "_shift_" + samplemode,
                typemode + "_" + varName + "_shift_" + samplemode,
                *pdfDict["shift"]
            ),
        )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaG_" + samplemode,
            typemode + "_" + varName + "_sigmaG_" + samplemode,
            *pdfDict["sigmaG"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_fracG_" + samplemode,
            typemode + "_" + varName + "_fracG_" + samplemode,
            *pdfDict["fracG"]
        ),
    )

    # Build PDF
    pdf = Bd2DhModels.buildCrystalBallPlusGaussianPDF(
        obs, workOut, samplemode, typemode, shiftMean, False, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildDoubleCrystalBallPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "DCB"
    samplemode = nickname
    sameMean = pdfDict["sameMean"]

    WS(
        workOut,
        RooRealVar(
            (
                "Signal_" + varName + "_mean_" + samplemode
                if sameMean
                else typemode + "_" + varName + "_mean_" + samplemode
            ),
            (
                "Signal_" + varName + "_mean_" + samplemode
                if sameMean
                else typemode + "_" + varName + "_mean_" + samplemode
            ),
            *pdfDict["mean"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_alpha1_" + samplemode,
            typemode + "_" + varName + "_alpha1_" + samplemode,
            *pdfDict["alpha1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_alpha2_" + samplemode,
            typemode + "_" + varName + "_alpha2_" + samplemode,
            *pdfDict["alpha2"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n1_" + samplemode,
            typemode + "_" + varName + "_n1_" + samplemode,
            *pdfDict["n1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n2_" + samplemode,
            typemode + "_" + varName + "_n2_" + samplemode,
            *pdfDict["n2"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma1_" + samplemode,
            typemode + "_" + varName + "_sigma1_" + samplemode,
            *pdfDict["sigma1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma2_" + samplemode,
            typemode + "_" + varName + "_sigma2_" + samplemode,
            *pdfDict["sigma2"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frac_" + samplemode,
            typemode + "_" + varName + "_frac_" + samplemode,
            *pdfDict["frac"]
        ),
    )

    # Build PDF
    pdf = BasicMDFitPdf.buildDoubleCrystalBallPDF(
        obs, workOut, samplemode, typemode, False, sameMean, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildJohnsonSUPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "J"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_mean_" + samplemode,
            typemode + "_" + varName + "_mean_" + samplemode,
            *pdfDict["mean"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma_" + samplemode,
            typemode + "_" + varName + "_sigma_" + samplemode,
            *pdfDict["sigma"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_nu_" + samplemode,
            typemode + "_" + varName + "_nu_" + samplemode,
            *pdfDict["nu"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_tau_" + samplemode,
            typemode + "_" + varName + "_tau_" + samplemode,
            *pdfDict["tau"]
        ),
    )

    # Build PDF
    pdf = Bd2DhModels.buildJohnsonSUPDF(
        obs, workOut, samplemode, typemode, False, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildJohnsonSUPlusGaussianPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "JplusG"
    samplemode = nickname
    sameMean = pdfDict["sameMean"]

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_meanJ_" + samplemode,
            typemode + "_" + varName + "_meanJ_" + samplemode,
            *pdfDict["meanJ"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaJ_" + samplemode,
            typemode + "_" + varName + "_sigmaJ_" + samplemode,
            *pdfDict["sigmaJ"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_nuJ_" + samplemode,
            typemode + "_" + varName + "_nuJ_" + samplemode,
            *pdfDict["nuJ"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_tauJ_" + samplemode,
            typemode + "_" + varName + "_tauJ_" + samplemode,
            *pdfDict["tauJ"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_meanGshift_" + samplemode,
            typemode + "_" + varName + "_meanGshift_" + samplemode,
            *pdfDict["meanGshift"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaG_" + samplemode,
            typemode + "_" + varName + "_sigmaG_" + samplemode,
            *pdfDict["sigmaG"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frac_" + samplemode,
            typemode + "_" + varName + "_frac_" + samplemode,
            *pdfDict["frac"]
        ),
    )

    # Build PDF
    pdf = Bd2DhModels.buildJohnsonSUPlusGaussianPDF(
        obs, workOut, samplemode, typemode, sameMean, False, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildJohnsonSUPlusGaussianPlusExponentialPDF(
    workOut, obs, nickname, pdfDict, debug
):
    # Build parameters
    varName = obs.GetName()
    typemode = "JplusGplusExpo"
    samplemode = nickname
    sameMean = pdfDict["sameMean"]

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_meanJ_" + samplemode,
            typemode + "_" + varName + "_meanJ_" + samplemode,
            *pdfDict["meanJ"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaJ_" + samplemode,
            typemode + "_" + varName + "_sigmaJ_" + samplemode,
            *pdfDict["sigmaJ"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_nuJ_" + samplemode,
            typemode + "_" + varName + "_nuJ_" + samplemode,
            *pdfDict["nuJ"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_tauJ_" + samplemode,
            typemode + "_" + varName + "_tauJ_" + samplemode,
            *pdfDict["tauJ"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_meanGshift_" + samplemode,
            typemode + "_" + varName + "_meanGshift_" + samplemode,
            *pdfDict["meanGshift"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaG_" + samplemode,
            typemode + "_" + varName + "_sigmaG_" + samplemode,
            *pdfDict["sigmaG"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_cB_" + samplemode,
            typemode + "_" + varName + "_cB_" + samplemode,
            *pdfDict["cB"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_relFracSignal_" + samplemode,
            typemode + "_" + varName + "_relFracSignal_" + samplemode,
            *pdfDict["relFracSignal"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_fracExpo_" + samplemode,
            typemode + "_" + varName + "_fracExpo_" + samplemode,
            *pdfDict["fracExponential"]
        ),
    )

    # Build PDF
    pdf = Bd2DhModels.buildJohnsonSUPlusGaussianPlusExponentialPDF(
        obs, workOut, samplemode, typemode, sameMean, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildJohnsonSUPlus2GaussianPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "Jplus2G"
    samplemode = nickname
    sameMean = pdfDict["sameMean"]

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_meanJ_" + samplemode,
            typemode + "_" + varName + "_meanJ_" + samplemode,
            *pdfDict["meanJ"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaJ_" + samplemode,
            typemode + "_" + varName + "_sigmaJ_" + samplemode,
            *pdfDict["sigmaJ"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_nuJ_" + samplemode,
            typemode + "_" + varName + "_nuJ_" + samplemode,
            *pdfDict["nuJ"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_tauJ_" + samplemode,
            typemode + "_" + varName + "_tauJ_" + samplemode,
            *pdfDict["tauJ"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_meanG1shift_" + samplemode,
            typemode + "_" + varName + "_meanG1shift_" + samplemode,
            *pdfDict["meanG1shift"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_meanG2shift_" + samplemode,
            typemode + "_" + varName + "_meanG2shift_" + samplemode,
            *pdfDict["meanG2shift"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma1G_" + samplemode,
            typemode + "_" + varName + "_sigma1G_" + samplemode,
            *pdfDict["sigma1G"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma2G_" + samplemode,
            typemode + "_" + varName + "_sigma2G_" + samplemode,
            *pdfDict["sigma2G"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frac1G_" + samplemode,
            typemode + "_" + varName + "_frac1G_" + samplemode,
            *pdfDict["frac1G"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frac2G_" + samplemode,
            typemode + "_" + varName + "_frac2G_" + samplemode,
            *pdfDict["frac2G"]
        ),
    )

    # Build PDF
    pdf = Bd2DhModels.buildJohnsonSUPlus2GaussianPDF(
        obs, workOut, samplemode, typemode, sameMean, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildIpatiaPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "Ipatia"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_l_" + samplemode,
            typemode + "_" + varName + "_l_" + samplemode,
            *pdfDict["l"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_zeta_" + samplemode,
            typemode + "_" + varName + "_zeta_" + samplemode,
            *pdfDict["zeta"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_fb_" + samplemode,
            typemode + "_" + varName + "_fb_" + samplemode,
            *pdfDict["fb"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_mean_" + samplemode,
            typemode + "_" + varName + "_mean_" + samplemode,
            *pdfDict["mean"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma_" + samplemode,
            typemode + "_" + varName + "_sigma_" + samplemode,
            *pdfDict["sigma"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a1_" + samplemode,
            typemode + "_" + varName + "_a1_" + samplemode,
            *pdfDict["a1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n1_" + samplemode,
            typemode + "_" + varName + "_n1_" + samplemode,
            *pdfDict["n1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a2_" + samplemode,
            typemode + "_" + varName + "_a2_" + samplemode,
            *pdfDict["a2"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n2_" + samplemode,
            typemode + "_" + varName + "_n2_" + samplemode,
            *pdfDict["n2"]
        ),
    )

    # Build PDF
    pdf = BasicMDFitPdf.buildIpatiaPDF(
        obs, workOut, samplemode, typemode, False, False, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildIpatiaPlusExponentialPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "IpatiaPlusExponential"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_l_" + samplemode,
            typemode + "_" + varName + "_l_" + samplemode,
            *pdfDict["l"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_zeta_" + samplemode,
            typemode + "_" + varName + "_zeta_" + samplemode,
            *pdfDict["zeta"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_fb_" + samplemode,
            typemode + "_" + varName + "_fb_" + samplemode,
            *pdfDict["fb"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_mean_" + samplemode,
            typemode + "_" + varName + "_mean_" + samplemode,
            *pdfDict["mean"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma_" + samplemode,
            typemode + "_" + varName + "_sigma_" + samplemode,
            *pdfDict["sigma"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a1_" + samplemode,
            typemode + "_" + varName + "_a1_" + samplemode,
            *pdfDict["a1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n1_" + samplemode,
            typemode + "_" + varName + "_n1_" + samplemode,
            *pdfDict["n1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a2_" + samplemode,
            typemode + "_" + varName + "_a2_" + samplemode,
            *pdfDict["a2"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n2_" + samplemode,
            typemode + "_" + varName + "_n2_" + samplemode,
            *pdfDict["n2"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_cB_" + samplemode,
            typemode + "_" + varName + "_cB_" + samplemode,
            *pdfDict["cB"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frac_" + samplemode,
            typemode + "_" + varName + "_frac_" + samplemode,
            *pdfDict["frac"]
        ),
    )

    # Build PDF
    pdf = Bd2DhModels.buildIpatiaPlusExponentialPDF(
        obs, workOut, samplemode, typemode, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildIpatiaPlusGaussianPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "IpatiaPlusGaussian"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_l_" + samplemode,
            typemode + "_" + varName + "_l_" + samplemode,
            *pdfDict["l"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_zeta_" + samplemode,
            typemode + "_" + varName + "_zeta_" + samplemode,
            *pdfDict["zeta"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_fb_" + samplemode,
            typemode + "_" + varName + "_fb_" + samplemode,
            *pdfDict["fb"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_mean_" + samplemode,
            typemode + "_" + varName + "_mean_" + samplemode,
            *pdfDict["mean"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaI_" + samplemode,
            typemode + "_" + varName + "_sigmaI_" + samplemode,
            *pdfDict["sigmaI"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaG_" + samplemode,
            typemode + "_" + varName + "_sigmaG_" + samplemode,
            *pdfDict["sigmaG"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a1_" + samplemode,
            typemode + "_" + varName + "_a1_" + samplemode,
            *pdfDict["a1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n1_" + samplemode,
            typemode + "_" + varName + "_n1_" + samplemode,
            *pdfDict["n1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a2_" + samplemode,
            typemode + "_" + varName + "_a2_" + samplemode,
            *pdfDict["a2"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n2_" + samplemode,
            typemode + "_" + varName + "_n2_" + samplemode,
            *pdfDict["n2"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_fracI_" + samplemode,
            typemode + "_" + varName + "_fracI_" + samplemode,
            *pdfDict["fracI"]
        ),
    )

    # Build PDF
    pdf = Bd2DhModels.buildIpatiaPlusGaussianPDF(
        obs, workOut, samplemode, typemode, False, False, False, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildIpatiaPlus2GaussianPDF(workOut, obs, nickname, pdfDict, debug):
    varName = obs.GetName()
    typemode = "IpatiaPlus2Gaussian"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_l_" + samplemode,
            typemode + "_" + varName + "_l_" + samplemode,
            *pdfDict["l"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_zeta_" + samplemode,
            typemode + "_" + varName + "_zeta_" + samplemode,
            *pdfDict["zeta"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_fb_" + samplemode,
            typemode + "_" + varName + "_fb_" + samplemode,
            *pdfDict["fb"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_mean_" + samplemode,
            typemode + "_" + varName + "_mean_" + samplemode,
            *pdfDict["mean"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaI_" + samplemode,
            typemode + "_" + varName + "_sigmaI_" + samplemode,
            *pdfDict["sigmaI"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaG_" + samplemode,
            typemode + "_" + varName + "_sigmaG_" + samplemode,
            *pdfDict["sigmaG"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma2G_" + samplemode,
            typemode + "_" + varName + "_sigma2G_" + samplemode,
            *pdfDict["sigma2G"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a1_" + samplemode,
            typemode + "_" + varName + "_a1_" + samplemode,
            *pdfDict["a1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n1_" + samplemode,
            typemode + "_" + varName + "_n1_" + samplemode,
            *pdfDict["n1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a2_" + samplemode,
            typemode + "_" + varName + "_a2_" + samplemode,
            *pdfDict["a2"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n2_" + samplemode,
            typemode + "_" + varName + "_n2_" + samplemode,
            *pdfDict["n2"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frac1_" + samplemode,
            typemode + "_" + varName + "_frac1_" + samplemode,
            *pdfDict["frac1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frac2_" + samplemode,
            typemode + "_" + varName + "_frac2_" + samplemode,
            *pdfDict["frac2"]
        ),
    )

    # Build PDF
    pdf = Bd2DhModels.buildIpatiaPlus2GaussianPDF(
        obs, workOut, samplemode, typemode, False, False, False, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildIpatiaPlusJohnsonSUPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "IpatiaPlusJohnsonSU"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_l_" + samplemode,
            typemode + "_" + varName + "_l_" + samplemode,
            *pdfDict["l"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_zeta_" + samplemode,
            typemode + "_" + varName + "_zeta_" + samplemode,
            *pdfDict["zeta"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_fb_" + samplemode,
            typemode + "_" + varName + "_fb_" + samplemode,
            *pdfDict["fb"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_mean_" + samplemode,
            typemode + "_" + varName + "_mean_" + samplemode,
            *pdfDict["mean"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaI_" + samplemode,
            typemode + "_" + varName + "_sigmaI_" + samplemode,
            *pdfDict["sigmaI"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaJ_" + samplemode,
            typemode + "_" + varName + "_sigmaJ_" + samplemode,
            *pdfDict["sigmaJ"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a1_" + samplemode,
            typemode + "_" + varName + "_a1_" + samplemode,
            *pdfDict["a1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n1_" + samplemode,
            typemode + "_" + varName + "_n1_" + samplemode,
            *pdfDict["n1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a2_" + samplemode,
            typemode + "_" + varName + "_a2_" + samplemode,
            *pdfDict["a2"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n2_" + samplemode,
            typemode + "_" + varName + "_n2_" + samplemode,
            *pdfDict["n2"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_nu_" + samplemode,
            typemode + "_" + varName + "_nu_" + samplemode,
            *pdfDict["nu"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_tau_" + samplemode,
            typemode + "_" + varName + "_tau_" + samplemode,
            *pdfDict["tau"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_fracI_" + samplemode,
            typemode + "_" + varName + "_fracI_" + samplemode,
            *pdfDict["fracI"]
        ),
    )

    # Build PDF
    pdf = Bd2DhModels.buildIpatiaPlusJohnsonSUPDF(
        obs, workOut, samplemode, typemode, False, False, False, False, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def buildHILLdiniPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "HILLdini"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a_" + samplemode,
            typemode + "_" + varName + "_a_" + samplemode,
            *pdfDict["a"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_b_" + samplemode,
            typemode + "_" + varName + "_b_" + samplemode,
            *pdfDict["b"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_csi_" + samplemode,
            typemode + "_" + varName + "_csi_" + samplemode,
            *pdfDict["csi"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_shift_" + samplemode,
            typemode + "_" + varName + "_shift_" + samplemode,
            *pdfDict["shift"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma_" + samplemode,
            typemode + "_" + varName + "_sigma_" + samplemode,
            *pdfDict["sigma"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_R_" + samplemode,
            typemode + "_" + varName + "_R_" + samplemode,
            *pdfDict["R"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frac_" + samplemode,
            typemode + "_" + varName + "_frac_" + samplemode,
            *pdfDict["frac"]
        ),
    )

    # Build PDF
    pdf = BasicMDFitPdf.buildHILLdini(obs, workOut, samplemode, typemode, debug)

    return WS(workOut, pdf)


# ------------------------------------------------------------
def buildHORNSdiniPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "HORNSdini"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a_" + samplemode,
            typemode + "_" + varName + "_a_" + samplemode,
            *pdfDict["a"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_b_" + samplemode,
            typemode + "_" + varName + "_b_" + samplemode,
            *pdfDict["b"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_csi_" + samplemode,
            typemode + "_" + varName + "_csi_" + samplemode,
            *pdfDict["csi"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_shift_" + samplemode,
            typemode + "_" + varName + "_shift_" + samplemode,
            *pdfDict["shift"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma_" + samplemode,
            typemode + "_" + varName + "_sigma_" + samplemode,
            *pdfDict["sigma"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_R_" + samplemode,
            typemode + "_" + varName + "_R_" + samplemode,
            *pdfDict["R"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frac_" + samplemode,
            typemode + "_" + varName + "_frac_" + samplemode,
            *pdfDict["frac"]
        ),
    )

    # Build PDF
    pdf = BasicMDFitPdf.buildHORNSdini(obs, workOut, samplemode, typemode, debug)

    return WS(workOut, pdf)


# ------------------------------------------------------------
def buildHILLdiniPlusHORNSdiniPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "HILLHORNSdini"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_ahill_" + samplemode,
            typemode + "_" + varName + "_ahill_" + samplemode,
            *pdfDict["ahill"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_bhill_" + samplemode,
            typemode + "_" + varName + "_bhill_" + samplemode,
            *pdfDict["bhill"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_csihill_" + samplemode,
            typemode + "_" + varName + "_csihill_" + samplemode,
            *pdfDict["csihill"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma_" + samplemode,
            typemode + "_" + varName + "_sigma_" + samplemode,
            *pdfDict["sigma"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_ahorns_" + samplemode,
            typemode + "_" + varName + "_ahorns_" + samplemode,
            *pdfDict["ahorns"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_bhorns_" + samplemode,
            typemode + "_" + varName + "_bhorns_" + samplemode,
            *pdfDict["bhorns"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_csihorns_" + samplemode,
            typemode + "_" + varName + "_csihorns_" + samplemode,
            *pdfDict["csihorns"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmahorns_" + samplemode,
            typemode + "_" + varName + "_sigmahorns_" + samplemode,
            *pdfDict["sigmahorns"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_shift_" + samplemode,
            typemode + "_" + varName + "_shift_" + samplemode,
            *pdfDict["shift"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_R_" + samplemode,
            typemode + "_" + varName + "_R_" + samplemode,
            *pdfDict["R"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frac_" + samplemode,
            typemode + "_" + varName + "_frac_" + samplemode,
            *pdfDict["frac"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frachorns_" + samplemode,
            typemode + "_" + varName + "_frachorns_" + samplemode,
            *pdfDict["frachorns"]
        ),
    )

    # Build PDF
    pdf = BasicMDFitPdf.buildHILLdiniPlusHORNSdini(
        obs, workOut, samplemode, typemode, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def buildDoubleHORNSdiniPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "DoubleHORNSdini"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a_" + samplemode,
            typemode + "_" + varName + "_a_" + samplemode,
            *pdfDict["a"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_b_" + samplemode,
            typemode + "_" + varName + "_b_" + samplemode,
            *pdfDict["b"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_csi_" + samplemode,
            typemode + "_" + varName + "_csi_" + samplemode,
            *pdfDict["csi"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigma_" + samplemode,
            typemode + "_" + varName + "_sigma_" + samplemode,
            *pdfDict["sigma"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmacorr_" + samplemode,
            typemode + "_" + varName + "_sigmacorr_" + samplemode,
            *pdfDict["sigmacorr"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_shift_" + samplemode,
            typemode + "_" + varName + "_shift_" + samplemode,
            *pdfDict["shift"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_shiftcorr_" + samplemode,
            typemode + "_" + varName + "_shiftcorr_" + samplemode,
            *pdfDict["shiftcorr"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_R_" + samplemode,
            typemode + "_" + varName + "_R_" + samplemode,
            *pdfDict["R"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_frac_" + samplemode,
            typemode + "_" + varName + "_frac_" + samplemode,
            *pdfDict["frac"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_fraccorr_" + samplemode,
            typemode + "_" + varName + "_fraccorr_" + samplemode,
            *pdfDict["fraccorr"]
        ),
    )

    # Build PDF
    pdf = Bs2Dsh2011TDAnaModels.buildDoubleHORNSdini(
        obs, workOut, samplemode, typemode, False, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildIpatiaGaussConvPDF(workOut, obs, nickname, pdfDict, debug):
    # Build parameters
    varName = obs.GetName()
    typemode = "IpatiaGaussConv"
    samplemode = nickname

    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_l_" + samplemode,
            typemode + "_" + varName + "_l_" + samplemode,
            *pdfDict["l"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_zeta_" + samplemode,
            typemode + "_" + varName + "_zeta_" + samplemode,
            *pdfDict["zeta"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_fb_" + samplemode,
            typemode + "_" + varName + "_fb_" + samplemode,
            *pdfDict["fb"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_mean_" + samplemode,
            typemode + "_" + varName + "_mean_" + samplemode,
            *pdfDict["mean"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaI_" + samplemode,
            typemode + "_" + varName + "_sigmaI_" + samplemode,
            *pdfDict["sigmaI"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_sigmaG_" + samplemode,
            typemode + "_" + varName + "_sigmaG_" + samplemode,
            *pdfDict["sigmaG"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a1_" + samplemode,
            typemode + "_" + varName + "_a1_" + samplemode,
            *pdfDict["a1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n1_" + samplemode,
            typemode + "_" + varName + "_n1_" + samplemode,
            *pdfDict["n1"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_a2_" + samplemode,
            typemode + "_" + varName + "_a2_" + samplemode,
            *pdfDict["a2"]
        ),
    )
    WS(
        workOut,
        RooRealVar(
            typemode + "_" + varName + "_n2_" + samplemode,
            typemode + "_" + varName + "_n2_" + samplemode,
            *pdfDict["n2"]
        ),
    )

    # Build PDF
    pdf = Bd2DhModels.buildIpatiaGaussConvPDF(
        obs, workOut, samplemode, typemode, False, False, False, debug
    )

    return WS(workOut, pdf)


# ------------------------------------------------------------
def BuildRooKeysPdf(workOut, obs, nickname, pdfDict, data, debug):
    # Build parameters
    rho = pdfDict["Rho"]
    mirror = pdfDict["Mirror"]

    if mirror == "MirrorBoth":
        pdf = RooKeysPdf(
            nickname + "_" + str(obs.GetName()) + "_RooKeysPdf_all",
            nickname + "_" + str(obs.GetName()) + "_RooKeysPdf_all",
            obs,
            data,
            RooKeysPdf.MirrorBoth,
            rho,
        )
    elif mirror == "NoMirror":
        pdf = RooKeysPdf(
            nickname + "_" + str(obs.GetName()) + "_RooKeysPdf_all",
            nickname + "_" + str(obs.GetName()) + "_RooKeysPdf_all",
            obs,
            data,
            RooKeysPdf.NoMirror,
            rho,
        )
    else:
        print("ERROR: mirror mode " + str(mirror) + " not currently handled")
        exit(-1)

    return WS(workOut, pdf)


# ------------------------------------------------------------
def TakeInputPdf(workOut, pdfDict):
    name = pdfDict["InputPdf"]
    pdf = workOut.pdf(name)
    return WS(workOut, pdf)


# ------------------------------------------------------------
# ------------------------------------------------------------
# ------------------------------------------------------------
def fitShapes(
    debug,
    configName,
    inputFile,
    outputFile,
    workData,
    workOut,
    save,
    initial,
    variable,
    component,
    mode,
    sample,
    year,
    merge,
    hypo,
    pdfType,
    binned,
    weighted,
    superimpose,
    logScale,
    noChi2,
    minY,
    jobs,
    outputplotdir,
    reduction,
    cut,
    bins,
):
    # Tune integrator configuration
    RooAbsReal.defaultIntegratorConfig().setEpsAbs(1e-7)
    RooAbsReal.defaultIntegratorConfig().setEpsRel(1e-7)
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooIntegrator1D"
    ).setCatLabel("extrapolation", "WynnEpsilon")
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooIntegrator1D"
    ).setCatLabel("maxSteps", "1000")
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooIntegrator1D"
    ).setCatLabel("minSteps", "0")
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooAdaptiveGaussKronrodIntegrator1D"
    ).setCatLabel("method", "21Points")
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooAdaptiveGaussKronrodIntegrator1D"
    ).setRealValue("maxSeg", 1000)
    # Since we have finite ranges, the RooIntegrator1D is best suited to the job
    RooAbsReal.defaultIntegratorConfig().method1D().setLabel("RooIntegrator1D")

    # Get the configuration file
    print(configName)
    myconfigfilegrabber = __import__(configName, fromlist=["getconfig"]).getconfig
    myconfigfile = myconfigfilegrabber()

    print("==========================================================")
    print("FITSHAPES IS RUNNING WITH THE FOLLOWING CONFIGURATION OPTIONS")
    for option in myconfigfile:
        if option == "constParams":
            for param in myconfigfile[option]:
                print(param, "is constant in the fit")
        else:
            print(option, " = ", myconfigfile[option])
    print("==========================================================")

    # RooAbsData.setDefaultStorageType(RooAbsData.Tree)
    RooAbsData.setDefaultStorageType(RooAbsData.Vector)
    mdt = Translator(myconfigfile, "MDSettings", False)

    MDSettings = mdt.getConfig()
    MDSettings.Print("v")

    print("")
    print("=========================================")
    print("Get input workspace " + str(workData) + " from:")
    print(str(inputFile))
    print("=========================================")
    print("")
    workspace = GeneralUtils.LoadWorkspace(TString(inputFile), TString(workData), debug)
    if workspace is None:
        raise SystemExit("No workspace found")

    print("")
    print("=========================================")
    print("Get output workspace " + str(workOut) + " from:")
    print(str(outputFile))
    print("=========================================")
    print("")
    if initial != "":
        workspaceOut = GeneralUtils.LoadWorkspace(
            TString(initial), TString(workOut), debug
        )
    else:
        workspaceOut = RooWorkspace(workOut, workOut)

    print("")
    print("=========================================")
    print("Get input observables from:")
    print(str(workData))
    print("=========================================")
    print("")
    observables = getObservables(MDSettings, workspace, debug)
    if "Beauty" in variable:
        obs = observables.find(MDSettings.GetMassBVarOutName().Data())
        obs.setRange(*myconfigfile["BasicVariables"]["BeautyMass"]["Range"])
        print(variable, hypo)
        obs.SetTitle(myconfigfile["AxisTitle"][variable][hypo])
        obs = WS(workspaceOut, obs)
    elif "Charm" in variable:
        obs = observables.find(MDSettings.GetMassDVarOutName().Data())
        obs.setRange(*myconfigfile["BasicVariables"]["CharmMass"]["Range"])
        obs.SetTitle(myconfigfile["AxisTitle"][variable][hypo])
        obs = WS(workspaceOut, obs)
    else:
        print("ERROR: " + variable + " is not currently handled")
        exit(-1)

    print("")
    print("=========================================")
    print("Get dataset from:")
    print(str(workData))
    print("=========================================")
    print("")

    TString("_")
    s, m, y = checkMerge(mode, sample, year, merge)
    mod, sam, yr = getTopLevelSMY(mode, sample, year, merge)

    if debug:
        print("-----------------------------")
        print("[INFO] Initial samples: ")
        print("-----------------------------")
    if "Signal" in component or "Comb" in component:
        sm_init = getSampleModeYear(mode, sample, year, debug)
    else:
        sm_init = getSampleYear(sample, year, debug)
    if debug:
        print("-----------------------------")
        print("[INFO] Target samples: ")
        print("-----------------------------")
    if "Signal" in component or "Comb" in component:
        sm = getSampleModeYear(m, s, y, debug)
    else:
        sm = getSampleYear(s, y, debug)

    if debug:
        print("-----------------------------")
        print("[INFO] Merging options: ")
        print("-----------------------------")

    if merge != "":
        my = merge + year
        mergeList = getStdVector(my, debug)
    else:
        mergeList = getStdVector(["none"], debug)
    sampleList = getStdVector(sample, False)
    modeList = getStdVector(mode, False)
    yearList = getStdVector(year, False)

    print("s,m,y: ", s, m, y)
    print("mergeList:", my)
    print("sampleList:", sample)
    print("modeList: ", mode)
    print("yearList:", year)

    TString(mode[0])
    sampleTS = TString(sample[0])
    yearTS = TString("".join(year))
    TString(hypo)

    datasetTS = TString(myconfigfile["dataSetPrefix"][component])
    if "Comb" in component:
        datasetTS = datasetTS + TString(variable) + TString("_")

    print("Component:")
    print(component)
    print("Dataset name prefix:")
    print(datasetTS.Data())

    if "pol" in merge:
        sampleTS = TString("both")

    sam = WS(workspaceOut, RooCategory("sample", "sample"))

    if "Signal" in component or "Comb" in component:
        if weighted:
            dataSet = GeneralUtils.GetDataSet_weighted(
                workspace,
                sam,
                datasetTS,
                sampleList,
                modeList,
                yearList,
                sm_init,
                sm,
                mergeList,
                debug,
            )
        else:
            dataSet = GeneralUtils.GetDataSet(
                workspace,
                observables,
                sam,
                datasetTS,
                sampleList,
                modeList,
                yearList,
                sm_init,
                sm,
                mergeList,
                debug,
            )
        dataSet.SetName(
            "combData"
        )  # _"+component+"_"+sampleTS.Data()+"_"+modeTS.Data()+"_"+yearTS.Data())
    else:
        if weighted:
            dataSet = GeneralUtils.GetDataSet_weighted(
                workspace,
                sam,
                datasetTS,
                sampleList,
                modeList,
                yearList,
                sm_init,
                sm,
                mergeList,
                debug,
            )
        else:
            dataSet = GeneralUtils.GetDataSet(
                workspace,
                observables,
                sam,
                datasetTS,
                sampleList,
                modeList,
                yearList,
                sm_init,
                sm,
                mergeList,
                debug,
            )
        dataSet.SetName(
            "combData"
        )  # _"+component+"_"+sampleTS.Data()+"_"+yearTS.Data())

    if reduction:
        myconfigfile = getBins(myconfigfile, bins, debug)
        if cut != "" and bins[0] != -1:
            print("[ERROR] both --cut and --bin options specified")
            print("[ERROR] --cut: ", cut)
            print("[ERROR] --bin: ", bins)
            exit(-1)
        if cut != "":
            print(string(cut))
            dataSet = dataSet.reduce(cut)
        else:
            bin_cut = getBinCut(myconfigfile, bins, debug)
            dataSet = dataSet.reduce(bin_cut)
    else:
        if type(bins) == list:
            if bins[0] != -1:
                print("[ERROR] --bin specified, but --reduction not used")
                exit(-1)

    if debug:
        print("Dataset number of entries:")
        print(dataSet.numEntries())
        print("Dataset sum of entries:")
        print(dataSet.sumEntries())
        print("Sample categories")
        sam.Print("v")
        dataSet.Print("v")

    nbins = myconfigfile["pdfList"][component][hypo][variable][pdfType]["Bins"]
    minFit = myconfigfile["pdfList"][component][hypo][variable][pdfType]["Min"]
    maxFit = myconfigfile["pdfList"][component][hypo][variable][pdfType]["Max"]
    obs.setRange(minFit, maxFit)
    obs.setUnit(myconfigfile["pdfList"][component][hypo][variable][pdfType]["Unit"])
    if binned:
        obs.setBins(nbins)
        dataHist = TH1F("dataHist", "dataHist", nbins, minFit, maxFit)
        dataSet.fillHistogram(dataHist, RooArgList(obs))
        dataSet = RooDataHist(
            dataSet.GetName(), dataSet.GetTitle(), RooArgList(obs), dataHist, 1.0
        )

    if None is workspaceOut.data(dataSet.GetName()):
        dataSet = WS(workspaceOut, dataSet)

    print("")
    print("=========================================")
    print("Build PDF to fit dataset:")
    print(str(dataSet.GetName()))
    print("=========================================")
    print("")

    nickname = dataSet.GetName()
    doFit = True
    pdfDict = myconfigfile["pdfList"][component][hypo][variable][pdfType]
    title = myconfigfile["pdfList"][component][hypo][variable][pdfType]["Title"]
    Xtitle = myconfigfile["AxisTitle"][variable][hypo]

    if "JohnsonSUPlus2Gaussian" in pdfType:
        pdf = BuildJohnsonSUPlus2GaussianPDF(
            workspaceOut, obs, nickname, pdfDict, debug
        )
    elif "JohnsonSUPlusGaussianPlusExponential" in pdfType:
        pdf = BuildJohnsonSUPlusGaussianPlusExponentialPDF(
            workspaceOut, obs, nickname, pdfDict, debug
        )
    elif "JohnsonSUPlusGaussian" in pdfType:
        pdf = BuildJohnsonSUPlusGaussianPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "IpatiaGaussConv" in pdfType:
        pdf = BuildIpatiaGaussConvPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "IpatiaPlusExponential" in pdfType:
        pdf = BuildIpatiaPlusExponentialPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "IpatiaPlusGaussian" in pdfType:
        pdf = BuildIpatiaPlusGaussianPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "IpatiaPlus2Gaussian" in pdfType:
        pdf = BuildIpatiaPlus2GaussianPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "IpatiaPlusJohnsonSU" in pdfType:
        pdf = BuildIpatiaPlusJohnsonSUPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "Ipatia" in pdfType:
        pdf = BuildIpatiaPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "CrystalBallPlusExponential" in pdfType:
        pdf = BuildCrystalBallPlusExponentialPDF(
            workspaceOut, obs, nickname, pdfDict, debug
        )
    elif "CrystalBallPlusGaussian" in pdfType:
        pdf = BuildCrystalBallPlusGaussianPDF(
            workspaceOut, obs, nickname, pdfDict, debug
        )
    elif "DoubleGaussian" in pdfType:
        pdf = BuildDoubleGaussPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "Gaussian" in pdfType:
        pdf = BuildGaussPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "DoubleCrystalBall" in pdfType:
        pdf = BuildDoubleCrystalBallPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "CrystalBall" in pdfType:
        pdf = BuildCrystalBallPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "ExponentialPlusConstant" in pdfType:
        pdf = BuildExponentialPlusConstantPDF(
            workspaceOut, obs, nickname, pdfDict, debug
        )
    elif "DoubleExponential" in pdfType:
        pdf = BuildDoubleExponentialPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "Exponential" in pdfType:
        pdf = BuildExponentialPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "JohnsonSU" in pdfType:
        pdf = BuildJohnsonSUPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "HILLdiniPlusHORNSdini" in pdfType:
        pdf = buildHILLdiniPlusHORNSdiniPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "DoubleHORNSdini" in pdfType:
        pdf = buildDoubleHORNSdiniPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "HILLdini" in pdfType:
        pdf = buildHILLdiniPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "HORNSdini" in pdfType:
        pdf = buildHORNSdiniPDF(workspaceOut, obs, nickname, pdfDict, debug)
    elif "RooKeysPdf" in pdfType:
        pdf = BuildRooKeysPdf(workspaceOut, obs, nickname, pdfDict, dataSet, debug)
        doFit = False
    elif "TakeInputPdf" in pdfType:
        pdf = TakeInputPdf(workspaceOut, pdfDict)
    else:
        print("ERROR: " + pdfType + " pdf not currently handled")
        exit(-1)
    pdf.SetName("pdf")

    if doFit and not superimpose:
        print("")
        print("=========================================")
        print("Fit PDF:")
        print(str(pdf.GetName()))
        print("to dataset:")
        print(str(dataSet.GetName()))
        print("=========================================")
        print("")

        if weighted:
            fitResult = WS(
                workspaceOut,
                pdf.fitTo(
                    dataSet,
                    RooFit.Range(minFit, maxFit),
                    RooFit.Save(1),
                    RooFit.Optimize(2),
                    RooFit.Strategy(2),
                    # RooFit.Verbose(False),
                    RooFit.Minimizer("Minuit2", "migrad"),
                    RooFit.Offset(True),
                    RooFit.SumW2Error(True),
                    RooFit.NumCPU(int(jobs)),
                ),
            )
        else:
            fitResult = WS(
                workspaceOut,
                pdf.fitTo(
                    dataSet,
                    RooFit.Range(minFit, maxFit),
                    RooFit.Save(1),
                    RooFit.Optimize(2),
                    RooFit.Strategy(2),
                    # RooFit.Verbose(False),
                    RooFit.Timer(True),
                    RooFit.Offset(True),
                    RooFit.SumW2Error(True),
                    RooFit.NumCPU(int(jobs)),
                ),
            )

        fitResult.SetName("FitTo_" + dataSet.GetName())
        fitResult.SetTitle("Fit to " + dataSet.GetTitle())

        print("")
        print("=========================================")
        print("Fit done. Summarizing results:")
        print("=========================================")
        print("")

        fitResult.Print("v")
        fitResult.covarianceMatrix().Print("v")
        fitResult.correlationMatrix().Print("v")
    else:
        fitResult = None

    print("")
    print("=========================================")
    print("Saving plot")
    print("=========================================")
    print("")

    text = "LHCb simulation"
    if "Comb" in component:
        text = "LHCb"
    print(pdfType)

    modeSave = ""
    for m in mode:
        modeSave += m + "_"
    namefile = (
        outputplotdir
        + "Template_"
        + component
        + "_"
        + hypo
        + "_"
        + str(variable)
        + "_"
        + pdfType
        + "_"
        + sampleTS.Data()
        + "_"
        + modeSave
        + yearTS.Data()
    )
    if binned:
        namefile += "_binned"
    if weighted:
        namefile += "_weighted"
    if superimpose:
        namefile += "_noFit"
    if reduction:
        namefile += "_bins"
        if cut:
            namefile += "_with_cut"
        else:
            for b in bins:
                if b != -1:
                    namefile += "_{i}".format(i=b)

    makeCanvas(
        dataSet,
        pdf,
        obs,
        fitResult,
        minFit,
        maxFit,
        nbins,
        text,
        title,
        Xtitle,
        weighted,
        False,
        noChi2,
        minY,
        namefile,
    )

    if logScale:
        namefile += "_logScale"

    makeCanvas(
        dataSet,
        pdf,
        obs,
        fitResult,
        minFit,
        maxFit,
        nbins,
        text,
        title,
        Xtitle,
        weighted,
        logScale,
        noChi2,
        minY,
        namefile,
    )

    if save:
        print("")
        print("=========================================")
        print("Saving output workspace")
        print("=========================================")
        print("")

        if debug:
            workspaceOut.Print()

        workspaceOut.SaveAs(outputFile)

    resultfile = TFile(outputFile.replace(".root", "_fitresult.root"), "RECREATE")
    fitResult.Write()
    resultfile.Close()

    print("")
    print("=========================================")
    print("Pretty-print fit results")
    print("=========================================")
    print("")

    # fitResult.Print("v")

    if None is not fitResult:
        FitResultGrabberUtils.PrintLatexTable(fitResult)


parser = argparse.ArgumentParser()
parser.add_argument(
    "-d",
    "--debug",
    action="store_true",
    dest="debug",
    default=False,
    help="print debug information while processing",
)
parser.add_argument(
    "--save",
    action="store_true",
    dest="save",
    default=False,
    help="save output workspace to file",
)
parser.add_argument(
    "--configName",
    dest="configName",
    default="MyConfigFile",
    help="configuration file name",
)
parser.add_argument(
    "--inputFile",
    dest="inputFile",
    default="MyInputFile.root",
    help="input file containing workspace with MC/data samples to fit",
)
parser.add_argument(
    "--workData",
    dest="workData",
    default="workspace",
    help="workspace containing MC/data samples to fit",
)
parser.add_argument(
    "--workOut",
    dest="workOut",
    default="workOut",
    help="workspace to store fit results, pdfs, observables",
)
parser.add_argument(
    "--variable", dest="variable", default="BeautyMass", help="observable name"
)
parser.add_argument(
    "--component", dest="component", default="Bd2DPi", help="component name to fit"
)
parser.add_argument(
    "-m",
    "--mode",
    dest="mode",
    nargs="+",
    default="kkpi",
    help="Mode can be: all, kkpi, kpipi, pipipi, nonres, kstk, phipi",
)
parser.add_argument(
    "-p",
    "--pol",
    "--polarity",
    dest="pol",
    nargs="+",
    default="down",
    help="Polarity can be: up, down, both",
)
parser.add_argument(
    "--year",
    nargs="+",
    dest="year",
    default="",
    help="year of data taking can be: 2011, 2012, 2015, 2016, 2017, 2018",
)
parser.add_argument(
    "--merge",
    nargs="+",
    dest="merge",
    default="both",
    help="merge can be: pol, run1, run2, runs",
)
parser.add_argument(
    "--hypo", dest="hypo", default="Bd2DPi", help="bachelor mass hypothesys"
)

parser.add_argument("--pdf", dest="pdf", default="Ipatia", help="PDF used for fitting")
parser.add_argument(
    "--binned",
    action="store_true",
    dest="binned",
    default=False,
    help="performed binned maximum likelihood fit (faster)",
)
parser.add_argument(
    "--weighted",
    action="store_true",
    dest="weighted",
    default=False,
    help="performed maximum likelihood fit to weighted MC",
)
parser.add_argument(
    "--superimpose",
    action="store_true",
    dest="superimpose",
    default=False,
    help="plot PDF on data without fitting (use initial parameters)",
)
parser.add_argument(
    "--logScale",
    action="store_true",
    dest="logScale",
    default=False,
    help="use log scale for vertical axis",
)
parser.add_argument(
    "--noChi2",
    action="store_true",
    dest="noChi2",
    default=False,
    help="Do not display a chi2/ndof",
)
parser.add_argument(
    "--reduce",
    action="store_true",
    dest="reduction",
    default=False,
    help="Reduce dataSet",
)
parser.add_argument("--cut", dest="cut", default="", help="Cut dataSet")
parser.add_argument(
    "--bin",
    "--bins",
    dest="bins",
    type=int,
    default=-1,
    nargs="+",
    help="Set bin reading from config file",
)
parser.add_argument(
    "--outputFile",
    dest="outputFile",
    default="MyOutputFile.root",
    help="output file to store fit results",
)
parser.add_argument(
    "--outputplotdir",
    dest="outputplotdir",
    default="MyOutputDir",
    help="output directory to store plots",
)
parser.add_argument(
    "--initial",
    dest="initial",
    default="",
    help="file to take the output workspace from (if it exists)",
)
parser.add_argument(
    "--minY",
    dest="minY",
    default="0.1",
    help="In case of log fit this gives the miniminum Y",
)
parser.add_argument(
    "--jobs", dest="jobs", default="4", help="The number of parrallel jobs"
)

# ------------------------------------------------------------
if __name__ == "__main__":
    try:
        args = parser.parse_args()
    except Exception:
        parser.print_help()
        sys.exit(-1)

    if args.pol[0] == "both":
        args.pol = ["up", "down"]

    if args.year[0] == "run1":
        args.year = ["2011", "2012"]
    if args.year[0] == "run2":
        args.year = ["2015", "2016", "2017", "2018"]
    if args.year[0] == "all":
        args.year = ["2011", "2012", "2015", "2016", "2017", "2018"]

    print(vars(args))

    config = args.configName
    last = config.rfind("/")
    directory = config[: last + 1]
    configName = config[last + 1 :]
    p = configName.rfind(".")
    configName = configName[:p]

    sys.path.append(directory)

    print("Config file name: " + configName)
    print("Directory: " + directory)
    print("Weighted: ", args.weighted)
    print("Binned: ", args.binned)

    fitShapes(
        args.debug,
        configName,
        args.inputFile,
        args.outputFile,
        args.workData,
        args.workOut,
        args.save,
        args.initial,
        args.variable,
        args.component,
        args.mode,
        args.pol,
        args.year,
        args.merge,
        args.hypo,
        args.pdf,
        args.binned,
        args.weighted,
        args.superimpose,
        args.logScale,
        args.noChi2,
        args.minY,
        args.jobs,
        args.outputplotdir,
        args.reduction,
        args.cut,
        args.bins,
    )
