#!/usr/bin/env python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# --------------------------------------------------------------------------- #
#                                                                             #
#   Python script to run a toy MC fit for the CP asymmetry observables        #
#   in Bs -> Ds K                                                             #
#   with the FitMeTool fitter                                                 #
#                                                                             #
#   Example usage:                                                            #
#      python runBs2DsKCPAsymmObsFitterOnData.py [-d -s]                      #
#                                                                             #
#   Author: Eduardo Rodrigues                                                 #
#   Date  : 14 / 06 / 2011                                                    #
#   Author: Manuel Schiller                                                   #
#   Author: Agnieszka Dziurda                                                 #
#   Author: Vladimir Vava Gligorov                                            #
#   Author: Ulrich Eitschberger
# --------------------------------------------------------------------------- #

from __future__ import print_function
import ROOT
import argparse
import itertools
import sys
from array import array
from pprint import pprint
from ROOT import (
    GeneralUtils,
    Inverse,
    RooAbsReal,
    RooAddition,
    RooArgList,
    RooArgSet,
    RooBDecay,
    RooBinning,
    RooCategory,
    RooConstVar,
    RooCubicSplineFun,
    RooDataHist,
    RooDataSet,
    RooFit,
    RooFormulaVar,
    RooGaussEfficiencyModel,
    RooLinkedList,
    RooMsgService,
    RooMultiVarGaussian,
    RooNLLVar,
    RooProdPdf,
    RooProfileLL,
    RooRealVar,
    RooSimultaneous,
    RooUnblindUniform,
    RooWorkspace,
    TCanvas,
    TFile,
    TMatrixDSym,
    TRandom3,
    TString,
    gROOT,
    kRed,
)

from B2DXFitters import (
    DecRateCoeff_Bd,
    PTResModels,
    SFitUtils,
)
from B2DXFitters.mdfitutils import (
    checkMerge,
    getCombinedData,
    getSampleModeYear,
)

# from B2DXFitters import *
from B2DXFitters.WS import WS

ROOT.PyConfig.IgnoreCommandLineOptions = True  # noqa
gROOT.SetBatch()

# storageFile = None  # global file to hold TreeStorage
# simDataStorageFile = None
RANDOM_GENERATOR = None
NP_RANDOM_GENERATOR = None


class ConfigurationError(Exception):
    pass


# MISCELLANEOUS
bName = "B_{s}"


# ------------------------------------------------------------------------------
def printLine(bold):
    if bold:
        print(
            "=================================================================="
            "============================================================="
        )
    else:
        print(
            "------------------------------------------------------------------"
            "-------------------------------------------------------------"
        )


# ------------------------------------------------------------------------------
def printMessage(message, bold=True):
    printLine(bold)
    print("[INFO] ", message)
    printLine(bold)


# ------------------------------------------------------------------------------
def setConstantIfSoConfigured(var, myconfigfile):
    defName = var.GetName() + "_default"
    if (var.GetName() or defName) in myconfigfile["constParams"]:
        var.setConstant()
        print(
            "[INFO] Parameter: %s set to be constant with value %lf"
            % (var.GetName(), var.getValV())
        )
    else:
        print("[INFO] Parameter: %s floats in the fit" % (var.GetName()))
        print("[INFO]   ", var.Print())


# ------------------------------------------------------------------------------
def getCPparameters(decay, myconfigfile):
    from B2DXFitters import cpobservables

    if decay.Contains("K"):
        argLf = myconfigfile["StrongPhase"] - myconfigfile["WeakPhase"]
        argLbarfbar = myconfigfile["StrongPhase"] + myconfigfile["WeakPhase"]
        modLf = myconfigfile["ModLf"]
        ACPobs = cpobservables.AsymmetryObservables(argLf, argLbarfbar, modLf, True)
        ACPobs.printtable()
        Cf = ACPobs.Cf()
        Sf = ACPobs.Sf()
        Df = ACPobs.Df()
        Sfbar = ACPobs.Sfbar()
        Dfbar = ACPobs.Dfbar()
    else:
        Cf = 1.0
        Sf = 0.0
        Df = 0.0
        Sfbar = 0.0
        Dfbar = 0.0

    limit = [-3.0, 3.0]
    if "CPlimit" in myconfigfile:
        limit[0] = myconfigfile["CPlimit"]["lower"]
        limit[1] = myconfigfile["CPlimit"]["upper"]

    sigC = RooRealVar("C_%s" % (decay.Data()), "C coeff.", Cf, limit[0], limit[1])
    sigS = RooRealVar("S_%s" % (decay.Data()), "S coeff.", Sf, limit[0], limit[1])
    sigD = RooRealVar("D_%s" % (decay.Data()), "D coeff.", Df, limit[0], limit[1])
    sigCbar = RooFormulaVar(
        "Cbar_%s" % (decay.Data()), "Cbar coeff.", "-1*@0", RooArgList(sigC)
    )
    #    sigCbar = RooRealVar('Cbar_%s'%(decay.Data()), 'Cbar coeff.',
    #                           Cf, limit[0], limit[1])
    sigSbar = RooRealVar(
        "Sbar_%s" % (decay.Data()), "Sbar coeff.", Sfbar, limit[0], limit[1]
    )
    sigDbar = RooRealVar(
        "Dbar_%s" % (decay.Data()), "Dbar coeff.", Dfbar, limit[0], limit[1]
    )
    setConstantIfSoConfigured(sigC, myconfigfile)
    setConstantIfSoConfigured(sigS, myconfigfile)
    setConstantIfSoConfigured(sigD, myconfigfile)
    #    setConstantIfSoConfigured(sigCbar,myconfigfile)
    setConstantIfSoConfigured(sigSbar, myconfigfile)
    setConstantIfSoConfigured(sigDbar, myconfigfile)

    return sigC, sigS, sigD, sigCbar, sigSbar, sigDbar


def initRandomGenerator(seed):
    import numpy as np

    seed = int(seed)
    global RANDOM_GENERATOR, NP_RANDOM_GENERATOR
    if RANDOM_GENERATOR is None:
        RANDOM_GENERATOR = TRandom3(seed)
    np.random.seed(seed)


# ------------------------------------------------------------------------------
def getTagEff(myconfig, ws, mdSet, par, lab):
    numTag = mdSet.GetNumTagVar()
    tagList = []
    for i in range(0, numTag):
        if mdSet.CheckUseTag(i) is True:
            tagList.append(str(mdSet.GetTagMatch(i)))

    from B2DXFitters.WS import WS as WS

    tagEffList = RooArgList()
    tagEffSig = []
    tagEff = 0.0

    if tagList.__len__() == 1:
        if tagList[0] in myconfig["TaggingCalibration"]:
            if par in myconfig["TaggingCalibration"][tagList[0]]:
                tagEff = myconfig["TaggingCalibration"][tagList[0]][par]
                varName = par + "_" + tagList[0]
            elif lab in myconfig["TaggingCalibration"][tagList[0]]:
                tagEff = myconfig["TaggingCalibration"][tagList[0]][lab][par]
                varName = par + "_" + tagList[0] + "_" + str(lab)
            else:
                print(
                    "[ERROR] Tagging efficiency is unknown. "
                    "Please check the config file."
                )
                exit(0)
            tagEffSig.append(
                WS(
                    ws,
                    RooRealVar(
                        varName,
                        "Signal tagging efficiency",
                        tagEff,
                        0.0 if par.lower().startswith("tag") else -1.0,
                        1.0,
                    ),
                )
            )
            setConstantIfSoConfigured(tagEffSig[0], myconfig)
            tagEffList.add(tagEffSig[0])

        else:
            print(
                "[ERROR] Tagging efficiency is unknown. Please check the config file."
            )
            exit(0)
    elif tagList.__len__() == 2:
        if lab in myconfig["TaggingCalibration"][tagList[0]]:
            tagEff0 = myconfig["TaggingCalibration"][tagList[0]][lab][par]
            tagEff1 = myconfig["TaggingCalibration"][tagList[1]][lab][par]
            varName1 = par + "_" + tagList[0] + "_" + str(lab)
            varName2 = par + "_" + tagList[1] + "_" + str(lab)
        else:
            tagEff0 = myconfig["TaggingCalibration"][tagList[0]][par]
            tagEff1 = myconfig["TaggingCalibration"][tagList[1]][par]
            varName1 = par + "_" + tagList[0]
            varName2 = par + "_" + tagList[1]

        tagValue = [tagEff0, tagEff1]
        varName = [
            varName1,
            varName2,
        ]  # [tagEff0 - tagEff0*tagEff1,  tagEff1 - tagEff0*tagEff1, tagEff0*tagEff1]
        i = 0
        for tag in tagList:
            tagEffSig.append(
                WS(
                    ws,
                    RooRealVar(
                        varName[i],
                        "Signal tagging efficiency",
                        tagValue[i],
                        0.0 if par.lower().startswith("tag") else -1.0,
                        1.0,
                    ),
                )
            )
            s = tagEffSig.__len__()
            setConstantIfSoConfigured(tagEffSig[s - 1], myconfig)
            tagEffList.add(tagEffSig[s - 1])
            i = i + 1
    else:
        print("[ERROR] More than two taggers are not supported")
        exit(0)
    return tagEffList, ws


def getCalibrationParameter(
    myconfigfile,
    ws,
    sample_name,
    tagger_name,
    parameter_name,
    rangeDown,
    rangeUp,
    mean=False,
):
    from B2DXFitters.WS import WS

    sample_config = myconfigfile["TaggingCalibration"][tagger_name].get(sample_name)
    if sample_config is not None:
        varName = "{}_{}_{}".format(parameter_name, tagger_name, sample_name)
        if mean:
            varName = "{}_mean_{}_{}".format(parameter_name, tagger_name, sample_name)

        parameter_value = sample_config[parameter_name]
    else:
        varName = "{}_{}".format(parameter_name, tagger_name)
        if mean:
            varName = "{}_mean_{}".format(parameter_name, tagger_name)
        parameter_value = myconfigfile["TaggingCalibration"][tagger_name][
            parameter_name
        ]

    var = WS(ws, RooRealVar(varName, varName, parameter_value, rangeDown, rangeUp))
    setConstantIfSoConfigured(var, myconfigfile)
    return var, ws


def cleanup_glm_from_workspace(configfile, workspace):
    """De-serialization of GLM models currently causes segfaults.
    This method removes the full GLM models and only stores the tagging parameters.
    """
    if configfile["TaggingCalibration"].get("Type") == "GLM":
        print("TaggingCalibration type is GLM, cleaning workspace before write")
    else:
        return

    all_functions = list(workspace.allFunctions())
    for function_obj in all_functions:
        if type(function_obj) == ROOT.Espresso.RooGLMFunction:
            print("Removing {} from workspace".format(function_obj))
            workspace.RecursiveRemove(function_obj)
            # insert back plain variables
            for var in function_obj.getVariables():
                if not workspace.var(var.GetName()):
                    print("Adding back {} to workspace".format(var))
                    getattr(workspace, "import")(var)

    print("Cleaned workspace:")
    workspace.Print("v")


def getScaledTerr(myconfigfile, ws, name):
    from B2DXFitters.WS import WS as WS

    varNames = ["p0", "p1", "p2"]
    values = {}

    for varName in varNames:
        values[varName] = {}
        values[varName]["val"] = {}
        values[varName]["shared"] = {}

        if name in myconfigfile["Resolution"]:
            values[varName]["val"] = myconfigfile["Resolution"][name]["scaleFactor"][
                varName
            ]
            values[varName]["shared"] = False
        else:
            values[varName]["val"] = myconfigfile["Resolution"]["scaleFactor"][varName]
            values[varName]["shared"] = True

    trm_scale = {}

    for varName in varNames:
        trm_scale[varName] = {}
        if not values[varName]["shared"]:
            trm_scale[varName] = WS(
                ws,
                RooRealVar(
                    "trm_scale_" + varName + name,
                    "Gaussian resolution model mean " + varName + name,
                    values[varName]["val"],
                    "ps",
                ),
            )
        else:
            trm_scale[varName] = WS(
                ws,
                RooRealVar(
                    "trm_scale_" + varName,
                    "Gaussian resolution model mean " + varName,
                    values[varName]["val"],
                    "ps",
                ),
            )

    print("--------------------------------------------------------------")
    if "UsedResolution" in myconfigfile:
        print("[INFO] Used resolution: ", myconfigfile["UsedResolution"])
    if name != "":
        print("[INFO]    for the label: ", name)
    print("[INFO] s0: ", trm_scale["p0"].GetName(), trm_scale["p0"].getValV())
    print("[INFO] s1: ", trm_scale["p1"].GetName(), trm_scale["p1"].getValV())
    print("[INFO] s2: ", trm_scale["p2"].GetName(), trm_scale["p2"].getValV())

    return trm_scale, ws


def getFixedParam(value, idx=0):
    """
    Read fixed parameter and possibly vary it with a gaussian distribution
    if value is a list and len(value) == 2
    """
    import numpy as np

    global RANDOM_GENERATOR

    if type(value) in [list, tuple]:
        if len(value) <= 0 or len(value) >= 3:
            raise ConfigurationError()

        if len(value) == 2 and type(value[1]) == float:
            std = RANDOM_GENERATOR.Gaus(0, value[1])
        elif len(value) == 2 and type(value[0]) == list:
            return np.random.multivariate_normal(*value)[idx]
        else:
            std = 0

        value = value[0]
        value += std

    return value


def getTrmMean(myconfigfile, ws, name):
    from B2DXFitters.WS import WS

    value = getFixedParam(myconfigfile["Resolution"][name]["meanBias"])

    if name in myconfigfile["Resolution"]:
        m = WS(
            ws,
            RooRealVar(
                "trm_mean" + str(name), "Gaussian resolution model mean", value, "ps"
            ),
        )
    else:
        m = WS(
            ws, RooRealVar("trm_mean", "Gaussian resolution model mean", value, "ps")
        )
    print("--------------------------------------------------------------")
    print("[INFO] mean: ", m.GetName(), m.getValV())

    return m, ws


def getTrmMeanScaleFactor(myconfig, ws, name):
    """Read myconfig["Resolution"][name]['meanScale'] if available and
    provide RooRealVar meant for resolution mean scale factor.
    Defaults to 1.0 if the configuration is not available
    """
    from B2DXFitters.WS import WS

    if name in myconfig["Resolution"]:
        scaleFactorValue = myconfig["Resolution"][name].get("meanScale", 1.0)
        titleAppendix = "_" + name
    else:
        scaleFactorValue = myconfig["Resolution"].get("meanScale", 1.0)
        titleAppendix = ""
    scaleFactor = WS(
        ws,
        RooRealVar(
            "trm_mean_scale" + titleAppendix,
            "Gaussian resolution model mean scale factor",
            scaleFactorValue,
        ),
    )

    print(
        "[INFO] mean scale factor: {} {}".format(
            scaleFactor.GetName(), scaleFactor.getValV()
        )
    )

    return scaleFactor, ws


def getSplinesVariables(myconfigfile, ws, name, i):
    from B2DXFitters.WS import WS as WS

    lab = "_" + name
    if name in myconfigfile["Acceptance"]:
        var = WS(
            ws,
            RooRealVar(
                "var" + str(i + 1) + str(lab),
                "var" + str(i + 1) + str(lab),
                myconfigfile["Acceptance"][name]["values"][i],
                0.0,
                10.0,
            ),
        )
    else:
        var = WS(
            ws,
            RooRealVar(
                "var" + str(i + 1),
                "var" + str(i + 1),
                myconfigfile["Acceptance"]["values"][i],
                0.0,
                10.0,
            ),
        )

    # print "[INFO]  ",var.GetName(), var.getValV()
    setConstantIfSoConfigured(var, myconfigfile)

    return var, ws


def getAsymmetryVariable(
    myconfigfile,
    workspace,
    dataset_name,
    asymmetry="Production",
    value=0,
    limits=(-1, 1),
):
    """Initialize adet/aprod variable from configuration in
    myconfigfile['Asymmetries']
    """
    if asymmetry == "Production":
        variable_name = "aprod"
    elif asymmetry == "Detection":
        variable_name = "adet"
    else:
        raise ConfigurationError(
            (
                "Asymmetry key {} not supported. Possible "
                "values are ['Production', 'Detection']."
            ).format(asymmetry)
        )

    if "Asymmetries" in myconfigfile:
        if dataset_name in myconfigfile["Asymmetries"]:
            value = myconfigfile["Asymmetries"][dataset_name][asymmetry]
            variable_name += "_" + dataset_name
        else:
            value = myconfigfile["Asymmetries"][asymmetry]

    variable = WS(workspace, RooRealVar(variable_name, variable_name, value, *limits))
    setConstantIfSoConfigured(variable, myconfigfile)
    print("[INFO] {} initialized to {}".format(variable_name, value))

    return variable, workspace


def decayTimeCalibrationActive(config, year):
    year_config = config.get("TimeCalibration", {}).get(year)

    return year_config is not None


def calibrateDecayTime(
    dataset,
    rooObs,
    config,
    year,
    timeVarName="BeautyTime",
    terrVarName="BeautyTimeErr",
    newTimeVarName="BeautyTimeCalib",
    range=None,
):
    """Calibrate observable with name `timeVarName` with a function of form
    `time * ({a} + {b} * timeErr) + {c}`.
    The calibrated observable will be added to the dataest with name
    `newTimeVarName`.

    Args:
        dataset (RooDataSet): The dataset containing `timeVarName`
        rooObs (RooRealVar): The RooRealVar observable that is used to describe
            the data points. Min/Max will be adjusted
        config (dict): The B2DXFitter sfit config object
        year (str): The year label for which the config should be used
        timeVarName (str): name of the observable to calibrate
        terrVarName (str): name of the decay time error observable
        newTimeVarName (str): name of the new observable that will be added to
            the dataset
        range (tuple): range for the new time observable. If `None` the range
            from `timeVarName` will be used.

    Returns:
        The same dataset, with an additional observable `newTimeVarName`.
    """
    year_config = config.get("TimeCalibration", {}).get(year)

    if not decayTimeCalibrationActive(config, year):
        print(
            "[INFO] No time calibration applied for dataset '{}', year '{}'".format(
                dataset, year
            )
        )
        return dataset

    existingBranches = dataset.get()
    timeObs = existingBranches.find(timeVarName)
    timeErrObs = existingBranches.find(terrVarName)

    a, b, c = year_config.get("params", (1.0, 0.0, 0.0))
    formula = RooFormulaVar(
        newTimeVarName,
        newTimeVarName,
        "{timeVarName} * ({a} + {b} * {terrVarName}) + {c}".format(
            timeVarName=timeVarName, terrVarName=timeVarName, a=a, b=b, c=c
        ),
        RooArgList(timeObs, timeErrObs),
    )

    min, max = range or (a * timeObs.getMin() + c, a * timeObs.getMax() + c)
    dataset.addColumn(formula)
    dataset.get().find(newTimeVarName).setMin(min)
    dataset.get().find(newTimeVarName).setMax(max)
    rooObs.setMin(min)
    rooObs.setMax(max)
    print(
        "[INFO] Added calibrated decay time '{}' "
        "with a={}, b={}, c={}, min={}, max={}.".format(
            newTimeVarName, a, b, c, min, max
        )
    )
    return dataset


def prepareFit(
    debug,
    wsname,
    pereventmistag,
    pereventterr,
    toys,
    pathName,
    treeName,
    workName,
    configName,
    scan,
    binned,
    plotsWeights,
    sample,
    mode,
    year,
    merge,
    unblind,
    mc,
    sim,
    jobs,
    storageFileName,
    hack_sweight_names=False,
    max_evts_per_sample=None,
):
    # global storageFile

    # Get the configuration file
    myconfigfilegrabber = __import__(configName, fromlist=["getconfig"]).getconfig
    myconfigfile = myconfigfilegrabber()

    print("==========================================================")
    print("FITTER IS RUNNING WITH THE FOLLOWING CONFIGURATION OPTIONS")
    for option in myconfigfile:
        if option == "constParams":
            for param in myconfigfile[option]:
                print(param, "is constant in the fit")
        else:
            print(option, " = ", myconfigfile[option])
    print("==========================================================")

    RooAbsReal.defaultIntegratorConfig().setEpsAbs(1e-7)
    RooAbsReal.defaultIntegratorConfig().setEpsRel(1e-7)
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooIntegrator1D"
    ).setCatLabel("extrapolation", "WynnEpsilon")
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooIntegrator1D"
    ).setCatLabel("maxSteps", "1000")
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooIntegrator1D"
    ).setCatLabel("minSteps", "0")
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooAdaptiveGaussKronrodIntegrator1D"
    ).setCatLabel("method", "21Points")
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooAdaptiveGaussKronrodIntegrator1D"
    ).setRealValue("maxSeg", 1000)

    if sim:
        if ("Labels" in myconfigfile) is False:
            print(
                '[ERROR] For simultanous fit you need "Labels" '
                "in your config file, an example: "
                'configdict["Labels"] = ["20152016","2017"]'
            )
            sys.exit(-1)

    # Reading data set
    # -----------------------

    from B2DXFitters.MDFitSettingTranslator import Translator

    mdt = Translator(myconfigfile, "MDSettings", False)
    MDSettings = mdt.getConfig()
    MDSettings.Print("v")

    from B2DXFitters.WS import WS as WS

    ws = RooWorkspace("intWork", "intWork")

    decay = TString(myconfigfile["Decay"])
    hypo = myconfigfile.get("BachelorHypo", "")

    print("[INFO] hypo: ", hypo)
    #    Constant
    # ---------------------
    zero = RooConstVar("zero", "0", 0.0)
    one = RooConstVar("one", "1", 1.0)
    minusone = RooConstVar("minusone", "-1", -1.0)
    two = RooConstVar("two", "2", 2.0)
    label = []
    smy = {}
    path = {}

    #   Sample Mode Year Labels
    # -------------------------------

    config_labels = myconfigfile.get("Labels", ["default"])
    for mode_part, sample_part, year_part, merge_part, label_part, path_part in zip(
        mode, sample, year, merge, config_labels, pathName
    ):
        print(
            (
                "checking merge",
                mode_part,
                sample_part,
                year_part,
                merge_part,
                label_part,
                path_part,
            )
        )
        s, m, y = checkMerge(mode_part, sample_part, year_part, merge_part)
        if debug:
            printMessage("File to open: " + str(path_part), False)
            printMessage("Initial samples: ", False)
        sm_init = getSampleModeYear(mode_part, sample_part, year_part, debug)
        if debug:
            printMessage("Target samples: ", False)

        # a hack to replace "run2" with "20152016" in the sweight branch names...
        sm = getSampleModeYear(m, s, y, debug)
        if hack_sweight_names:
            for i in range(sm.size()):
                sm[i] = sm[i].Data().replace("run2", "20152016")
            print("Vector updated: {}".format(list(sm)))
        label.append(label_part)
        path[label_part] = path_part
        smy[label_part] = sm

    #    Reading Data Sets from Files
    # --------------------------------------------
    printMessage("Reading Data Sets from Files ")

    # change to storage file before reading DataSets
    # storageFile.cd()

    # test sweightcorrection setting before using it
    if myconfigfile.get("SWeightCorrection") not in [
        None,
        "Read",
        "AsymptoticallyCorrect",
        "Fit",
    ]:
        raise ConfigurationError(
            "SWeightCorrection option '{}' unknown.".format(
                myconfigfile.get("SWeightCorrection")
            )
        )

    if not mc:
        for lab in label:
            ws = SFitUtils.ReadDataFromSWeights(
                TString(path[lab]),
                TString(treeName),
                MDSettings,
                smy[lab],
                hypo,
                False,  # False -> unweighted data
                toys,
                False,
                False,  # don't apply sWeight correction factor
                ws,
                lab,
                debug,
            )  # unweighted data
            ws = SFitUtils.ReadDataFromSWeights(
                TString(path[lab]),
                TString(treeName),
                MDSettings,
                smy[lab],
                hypo,
                True,  # True -> weighted data
                toys,
                False,
                myconfigfile.get("SWeightCorrection")
                == "Read",  # apply sWeight correction factor?
                ws,
                lab,
                debug,
            )  # weighted data
        ws.Print()
    else:
        # always only use first argument here since all are the same
        ws = GeneralUtils.LoadWorkspace(TString(pathName[0]), TString(workName), debug)

    #    DataSet Reading from Workspace
    # --------------------------------------------
    printMessage("Reading data from Workspaces")

    data = {}  # unweighted data
    dataW = {}  # weighted data

    for lab in label:
        data[lab] = {}
        dataW[lab] = {}

    if not mc:
        for lab in label:
            nameData = TString("dataSet_time_") + TString(lab)
            nameDataWA = TString("dataSet_time_weighted_") + TString(lab)
            data[lab] = GeneralUtils.GetDataSet(ws, nameData, debug)
            # copy sWeight column to keep it in the TreeStorage
            sWeightVar = data[lab].get().find("sWeights")
            if sWeightVar:
                data[lab].addColumn(
                    RooFormulaVar("sWeights_", "sWeights", RooArgList(sWeightVar))
                )
            dataW[lab] = GeneralUtils.GetDataSet(ws, nameDataWA, debug)
    else:
        for mode_part, pol_part, years_part, lab in zip(mode, sample, year, label):
            # hack the sample merging
            data[lab] = getCombinedData(
                ws, myconfigfile["Decay"], mc, mode_part, pol_part, years_part, debug
            )
            if max_evts_per_sample is not None:
                data[lab] = data[lab].reduce(
                    ROOT.RooFit.EventRange(0, max_evts_per_sample)
                )
            dataW[lab] = data[lab]

    nEntries = []
    for lab in label:
        nEntries.append(dataW[lab].numEntries())
        dataW[lab].Print("v")

    # Reading observables from data
    # --------------------------------------------
    printMessage("Reading observables from data")

    obs = dataW[label[0]].get()
    timeVarName = MDSettings.GetTimeVarOutName().Data()
    if any([decayTimeCalibrationActive(myconfigfile, y) for y in label]):
        newTimeVarName = timeVarName + "Calib"
        time = RooRealVar(obs.find(timeVarName), newTimeVarName)
        time.SetTitle(newTimeVarName)
        print((time.getMin(), time.getMax()))
    else:
        newTimeVarName = None
        time = obs.find(timeVarName)

    terr = obs.find(MDSettings.GetTerrVarOutName().Data())
    id = obs.find(MDSettings.GetIDVarOutName().Data())

    # Get Tagger List
    numTag = MDSettings.CheckNumUsedTag()
    tagList = []
    for i in range(0, 2):
        if MDSettings.CheckUseTag(i) is True:
            tagList.append(str(MDSettings.GetTagMatch(i)))

    tag = []
    mistag = []
    observables = RooArgSet(time, id)

    for i in range(numTag):
        tag.append(obs.find("TagDec" + tagList[i]))
        mistag.append(obs.find("Mistag" + tagList[i]))
        mistag[i].setRange(0, 0.5)
        observables.add(tag[i])

    if debug:
        observables.Print("v")

    # Plotting sWeights if needed
    # --------------------------------------------
    if plotsWeights:  # and not mc:
        name = TString("sfit")
        obs2 = data.get()
        weight2 = obs2.find("sWeights")
        swpdf = GeneralUtils.CreateHistPDF(data, weight2, name, 100, debug)
        GeneralUtils.SaveTemplate(data, swpdf, weight2, name)
        exit(0)

    # Physical parameters
    # -----------------------
    printMessage("Physical parameters ")

    gammas = RooRealVar(
        "Gammas_%s" % decay.Data(),
        "%s average lifetime" % bName,
        getFixedParam(myconfigfile["Gammas"], 0),
        0.0,
        5.0,
        "ps^{-1}",
    )
    setConstantIfSoConfigured(gammas, myconfigfile)
    deltaGammas = RooRealVar(
        "deltaGammas_%s" % decay.Data(),
        "Lifetime difference",
        getFixedParam(myconfigfile["DeltaGammas"], 1),
        -1.0,
        1.0,
        "ps^{-1}",
    )
    setConstantIfSoConfigured(deltaGammas, myconfigfile)

    if toys or unblind:
        deltaMs = RooRealVar(
            "DeltaMs_{}".format(decay),
            "#Delta m_{s}",
            myconfigfile["DeltaMs"],
            10.0,
            25.0,
            "ps^{-1}",
        )
    else:
        deltaMs = RooRealVar(
            "DeltaMs_{}".format(decay),
            "#Delta m_{s} blinded",
            myconfigfile["DeltaMs"],
            10.0,
            25.0,
            "ps^{-1}",
        )
        deltaMsBlinding = RooUnblindUniform(
            "DeltaMs_{}_blinding".format(decay),
            "#Delta m_{s} blindig",
            "Pandabaerhatsnichtschwer",
            5 * 0.021,
            deltaMs,
        )

    setConstantIfSoConfigured(deltaMs, myconfigfile)

    # Decay time acceptance model
    # ---------------------------
    printMessage("Acceptance model: splines")

    tMax = time.getMax()
    tMin = time.getMin()
    binName = TString("splineBinning")
    numKnots = myconfigfile["Acceptance"]["knots"].__len__()
    TimeBin = RooBinning(tMin, tMax, binName.Data())
    for i in range(0, numKnots):
        print(
            "[INFO]   knot %s in place %s "
            % (str(i), str(myconfigfile["Acceptance"]["knots"][i]))
        )
        TimeBin.addBoundary(myconfigfile["Acceptance"]["knots"][i])

    TimeBin.removeBoundary(tMin)
    TimeBin.removeBoundary(tMax)
    TimeBin.Print("v")
    time.setBinning(TimeBin, binName.Data())
    time.setRange(tMin, tMax)
    listCoeff = GeneralUtils.GetCoeffFromBinning(TimeBin, time, debug)
    printLine(False)

    tacc_list = {}
    tacc_var = {}
    spl = {}

    for lab in label:
        tacc_list[lab] = {}
        tacc_var[lab] = {}
        tacc_list[lab] = RooArgList()
        tacc_var[lab] = []

        for i in range(0, numKnots):
            tacc, ws = getSplinesVariables(myconfigfile, ws, lab, i)
            tacc_var[lab].append(tacc)
            tacc_list[lab].add(tacc_var[lab][i])

        if myconfigfile["Acceptance"].get("FloatAll", False):
            for i in range(numKnots, numKnots + 2):
                tacc, ws = getSplinesVariables(myconfigfile, ws, lab, i)
                tacc_var[lab].append(tacc)
                tacc_list[lab].add(tacc_var[lab][i])
        else:
            if lab in myconfigfile["Acceptance"]:
                tacc_var[lab].append(
                    WS(
                        ws,
                        RooRealVar(
                            "var" + str(numKnots + 1) + "_" + str(lab),
                            "var" + str(numKnots + 1) + "_" + str(lab),
                            1,
                        ),
                    )
                )
            else:
                tacc_var[lab].append(
                    WS(
                        ws,
                        RooRealVar(
                            "var" + str(numKnots + 1), "var" + str(numKnots + 1), 1
                        ),
                    )
                )

            lenght = tacc_var[lab].__len__()
            tacc_list[lab].add(tacc_var[lab][lenght - 1])
            print(
                "[INFO]   n-2: ",
                tacc_var[lab][lenght - 2].GetName(),
                tacc_var[lab][lenght - 2].getValV(),
            )
            print(
                "[INFO]   n-1: ",
                tacc_var[lab][lenght - 1].GetName(),
                tacc_var[lab][lenght - 1].getValV(),
            )
            if lab in myconfigfile["Acceptance"]:
                name = "var" + str(numKnots + 2) + "_" + str(lab)
            else:
                name = "var" + str(numKnots + 2)
            if myconfigfile["Acceptance"].get("FixLast1", False):
                tacc_var[lab].append(WS(ws, RooRealVar(name, name, 1)))
            else:
                tacc_var[lab].append(
                    WS(
                        ws,
                        RooAddition(
                            name,
                            name,
                            RooArgList(
                                tacc_var[lab][lenght - 2], tacc_var[lab][lenght - 1]
                            ),
                            listCoeff,
                        ),
                    )
                )
            tacc_list[lab].add(tacc_var[lab][lenght])
            print(
                "[INFO]   n: ",
                tacc_var[lab][lenght].GetName(),
                tacc_var[lab][lenght].getValV(),
            )

        spl[lab] = RooCubicSplineFun(
            "splinePdf_" + str(lab),
            "splinePdf_" + str(lab),
            time,
            binName.Data(),
            tacc_list[lab],
        )
        printLine(False)

    # Decay time resolution model
    # ---------------------------
    trm = []
    terrpdf = []

    if not pereventterr:
        print("[INFO] Resolution model: Triple gaussian model")
        trm = PTResModels.tripleGausEffModel(
            time,
            spl,
            myconfigfile["Resolution"]["scaleFactor"],
            myconfigfile["Resolution"]["meanBias"],
            myconfigfile["Resolution"]["shape"]["sigma1"],
            myconfigfile["Resolution"]["shape"]["sigma2"],
            myconfigfile["Resolution"]["shape"]["sigma3"],
            myconfigfile["Resolution"]["shape"]["frac1"],
            myconfigfile["Resolution"]["shape"]["frac2"],
            debug,
        )

        terrpdf = None
    else:
        # the decay time error is an extra observable !
        printMessage("Resolution model: Gaussian with per-event observable")

        observables.add(terr)
        trm_scale = RooRealVar(
            "trm_scale", "Gaussian resolution model scale factor", 1.0
        )

        trm_mean = {}
        trm_mean_scale_factor = {}
        scale = {}
        terr_scaled = {}
        trm = {}
        terrpdf = {}

        for lab in label:
            trm_mean[lab], ws = getTrmMean(myconfigfile, ws, lab)
            scale[lab], ws = getScaledTerr(myconfigfile, ws, lab)
            terr_scaled[lab] = RooFormulaVar(
                "trm_scaled_terr_" + str(lab),
                "scale",
                "@0+@1*@3+@2*@3*@3",
                RooArgList(scale[lab]["p0"], scale[lab]["p1"], scale[lab]["p2"], terr),
            )
            trm_mean_scale_factor[lab], ws = getTrmMeanScaleFactor(
                myconfigfile, ws, lab
            )
            trm[lab] = RooGaussEfficiencyModel(
                "resmodel_" + lab,
                "resmodel_" + lab,
                time,
                spl[lab],
                trm_mean[lab],
                terr_scaled[lab],
                trm_mean_scale_factor[lab],
                trm_scale,
            )

            terrpdfName = TString("terrpdf_") + TString(lab)
            terrpdf[lab] = GeneralUtils.CreateHistPDF(
                dataW[lab], terr, terrpdfName, 80, debug
            )
            GeneralUtils.SaveTemplate(dataW[lab], terrpdf[lab], terr, terrpdfName)

        if debug:
            print(trm_mean)
            print(scale)
            print(terr_scaled)
            print(trm)
            print(terrpdf)

    # read out if constraints for the tagging calibration parameters should be used
    if "ConstrainsForTaggingCalib" in myconfigfile:
        constraints_for_tagging_calib = myconfigfile["ConstrainsForTaggingCalib"]
    else:
        constraints_for_tagging_calib = False

    # Per-event mistag
    # ---------------------------
    printMessage("Flavour Tagging")
    printMessage(
        "Flavour Tagging Constrains: " + str(constraints_for_tagging_calib), False
    )

    if pereventmistag:
        print("[INFO] Mistag model: per-event observable")

        p0 = {}
        dp0 = {}
        p1 = {}
        dp1 = {}

        p0_mean = {}
        dp0_mean = {}
        p1_mean = {}
        dp1_mean = {}

        taggingMultiVarGaussSet = {}  # RooArgSet()

        avetacalib = {}  # []

        mistagCalibList = {}  # RooArgList()
        constList = {}  # RooArgSet()

        for i in range(0, numTag):
            observables.add(mistag[i])

        if myconfigfile["TaggingCalibration"].get("Type") in ["GLM", "Logit"]:
            # mistag functino will be added to additional parameters for
            # DecRateCoeff_Bd later on
            omegas = {
                lab: {tagger_idx: None for tagger_idx in range(numTag)} for lab in label
            }
            omegabars = {
                lab: {tagger_idx: [] for tagger_idx in range(numTag)} for lab in label
            }
        if myconfigfile["TaggingCalibration"].get("Type") == "GLM":
            # if we use shard tagging, we should only build one calibration
            # function per tagger
            glm = [None] * numTag
            for lab, tagger_idx in itertools.product(label, list(range(numTag))):
                tagger_name = tagList[tagger_idx]
                if lab in myconfigfile["TaggingCalibration"][tagger_name]:
                    glm_name = "GLMCalib_{}_{}".format(tagger_name, lab)
                    xml = myconfigfile["TaggingCalibration"][tagger_name][lab]["xml"]
                    shared_tagging = False
                else:
                    glm_name = "GLMCalib_{}".format(tagger_name)
                    xml = myconfigfile["TaggingCalibration"][tagger_name]["xml"]
                    shared_tagging = True

                # inject existing glm model in case of shared tagging
                if shared_tagging and glm[tagger_idx] is not None:
                    omegas[lab][tagger_idx] = WS(ws, glm[tagger_idx].b_mistag())
                    omegabars[lab][tagger_idx] = WS(ws, glm[tagger_idx].bbar_mistag())
                    continue

                glm[tagger_idx] = ROOT.Espresso.GLMBuilder(
                    glm_name,
                    glm_name,
                    mistag[tagger_idx],
                    glm_name,
                    xml,
                )

                # set parameters constant if needed
                for coeff in glm[tagger_idx].coefficients():
                    if coeff.GetName() in myconfigfile["constParams"]:
                        print("Setting parameter {} constant".format(coeff.GetName()))
                        coeff.setConstant(True)
                    else:
                        print("Setting parameter {} floating".format(coeff.GetName()))
                        coeff.setConstant(False)
                for coeff in glm[tagger_idx].delta_coefficients():
                    if coeff.GetName() in myconfigfile["constParams"]:
                        print("Setting parameter {} constant".format(coeff.GetName()))
                        coeff.setConstant(True)
                    else:
                        print("Setting parameter {} floating".format(coeff.GetName()))
                        coeff.setConstant(False)
                # add calibration function to workspace, and fill in the handle
                # for later
                omegas[lab][tagger_idx] = WS(ws, glm[tagger_idx].b_mistag())
                omegabars[lab][tagger_idx] = WS(ws, glm[tagger_idx].bbar_mistag())
        elif myconfigfile["TaggingCalibration"].get("Type") == "Logit":
            for lab, tagger_idx in itertools.product(label, list(range(numTag))):
                tagger_name = tagList[tagger_idx]
                p0, ws = getCalibrationParameter(
                    myconfigfile, ws, lab, tagger_name, "p0", -1, 1
                )
                p1, ws = getCalibrationParameter(
                    myconfigfile, ws, lab, tagger_name, "p1", -1, 1
                )
                dp0, ws = getCalibrationParameter(
                    myconfigfile, ws, lab, tagger_name, "dp0", -0.3, 0.3
                )
                dp1, ws = getCalibrationParameter(
                    myconfigfile, ws, lab, tagger_name, "dp1", -0.3, 0.3
                )
                omega_name = "mistag_{}_{}".format(tagger_name, lab)
                omega = RooFormulaVar(
                    omega_name,
                    omega_name,
                    (
                        "1 / (1 + exp("
                        "-(log(@0 / (1 - @0))"
                        "+ sqrt(1 / 2) * (@1 + @3 / 2)"
                        "+ sqrt(3 / 2) * (@2 + @4 / 2) * log(@0 / (1 - @0)))))"
                    ),
                    RooArgList(mistag[tagger_idx], p0, p1, dp0, dp1),
                )
                # add calibration function to workspace, and fill in the handle
                # for later
                omegas[lab][tagger_idx] = WS(ws, omega)
                omegabar_name = "mistagbar_{}_{}".format(tagger_name, lab)
                omegabar = RooFormulaVar(
                    omegabar_name,
                    omegabar_name,
                    (
                        "1 / (1 + exp("
                        "-(log(@0 / (1 - @0))"
                        "+ sqrt(1 / 2) * (@1 - @3 / 2)"
                        "+ sqrt(3 / 2) * (@2 - @4 / 2) * log(@0 / (1 - @0)))))"
                    ),
                    RooArgList(mistag[tagger_idx], p0, p1, dp0, dp1),
                )
                omegabars[lab][tagger_idx] = WS(
                    ws, omegabar
                )  # implicitly setting delta parameters to 0
        else:
            print(numTag, tagList.__len__())
            for lab in label:
                p0[lab] = []
                dp0[lab] = []
                p1[lab] = []
                dp1[lab] = []
                avetacalib[lab] = []
                for i in range(0, numTag):
                    # get mean p0 and p1 as well as delta p0 and delta p1 values
                    par_p0, ws = getCalibrationParameter(
                        myconfigfile, ws, lab, tagList[i], "p0", -1.0, 1
                    )
                    p0[lab].append(par_p0)
                    par_dp0, ws = getCalibrationParameter(
                        myconfigfile, ws, lab, tagList[i], "dp0", -0.3, 0.3
                    )
                    dp0[lab].append(par_dp0)
                    par_p1, ws = getCalibrationParameter(
                        myconfigfile, ws, lab, tagList[i], "p1", 0.0, 2.0
                    )
                    p1[lab].append(par_p1)
                    par_dp1, ws = getCalibrationParameter(
                        myconfigfile, ws, lab, tagList[i], "dp1", -0.3, 0.3
                    )
                    dp1[lab].append(par_dp1)
                    par_av, ws = getCalibrationParameter(
                        myconfigfile, ws, lab, tagList[i], "average", 0.0, 1.0
                    )
                    avetacalib[lab].append(par_av)

                    if constraints_for_tagging_calib:
                        p0_mean[lab] = []
                        dp0_mean[lab] = []
                        p1_mean[lab] = []
                        dp1_mean[lab] = []
                        p0_mean[lab].append(
                            getCalibrationParameter(
                                myconfigfile, ws, lab, tagList[i], "p0", 0.0, 0.5, True
                            )
                        )
                        dp0_mean[lab].append(
                            getCalibrationParameter(
                                myconfigfile,
                                ws,
                                lab,
                                tagList[i],
                                "dp0",
                                -1.0,
                                1.0,
                                True,
                            )
                        )
                        p1_mean[lab].append(
                            getCalibrationParameter(
                                myconfigfile, ws, lab, tagList[i], "p1", 0.5, 1.5, True
                            )
                        )
                        dp1_mean[lab].append(
                            getCalibrationParameter(
                                myconfigfile,
                                ws,
                                lab,
                                tagList[i],
                                "dp1",
                                -1.0,
                                1.0,
                                True,
                            )
                        )

            if debug:
                print(p0)
                print(p1)
                print(dp0)
                print(dp1)
                print(avetacalib)

            if constraints_for_tagging_calib:
                elem = {}
                for lab in label:
                    print("[INFO] Tagging parameters are constrained in the fit")
                    if "cov" not in myconfigfile["TaggingCalibration"][tagList[i]]:
                        if (
                            "cov"
                            not in myconfigfile["TaggingCalibration"][tagList[i]][lab]
                        ):
                            print(
                                "[ERROR] "
                                "Covariance matrix of at least one tagger not set. "
                                "Please check the config file."
                            )
                            exit(0)
                    elem[lab] = 4 * 4 * [0.0]

                    for j in range(0, 4):
                        for k in range(0, 4):
                            if lab in myconfigfile["TaggingCalibration"][tagList[i]]:
                                val = myconfigfile["TaggingCalibration"][tagList[i]][
                                    lab
                                ]["cov"][j][k]
                            else:
                                val = myconfigfile["TaggingCalibration"][tagList[i]][
                                    "cov"
                                ][j][k]
                            elem[lab][j * 4 + k] = val

                    getattr(ws, "import")(
                        TMatrixDSym(4), "cov_matrix_" + str(name) + "_" + str(lab)
                    )

                    ws.obj("cov_matrix_" + str(name) + "_" + str(lab)).SetMatrixArray(
                        array("d", elem[lab])
                    )
                    print("[INFO] Not yet fully supported")
                    exit(0)
                    taggingMultiVarGaussSet[lab] = {}
                    taggingMultiVarGaussSet[lab] = RooArgSet()
                    taggingMultiVarGaussSet[lab].add(
                        WS(
                            ws,
                            RooMultiVarGaussian(
                                "tag_cons_multivg_" + str(name) + "_" + str(lab),
                                "tag_cons_multivg_" + str(name) + "_" + str(lab),
                                RooArgList(
                                    p0[lab][i], dp0[lab][i], p1[lab][i], dp1[lab][i]
                                ),
                                RooArgList(
                                    p0_mean[lab][i],
                                    dp0_mean[lab][i],
                                    p1_mean[lab][i],
                                    dp1_mean[lab][i],
                                ),
                                ws.obj("cov_matrix_" + str(name) + "_" + str(lab)),
                            ),
                        )
                    )

        mistagPDFList = {}
        for lab in label:
            mistagPDFList[lab] = SFitUtils.CreateDifferentMistagTemplates(
                dataW[lab], MDSettings, 200, True, lab, debug
            )

    else:
        print("[INFO] Mistag model: average mistag")
        mistagHistPdf = None
        mistagCalibrated = mistag

    # CP observables
    # --------------------------
    one1 = RooConstVar("one1", "1", 1.0)
    one2 = RooConstVar("one2", "1", 1.0)

    # Tagging
    # -------
    printMessage("Tagging efficiency", False)
    tagEffSigList = {}
    for lab in label:
        tagEffSigList[lab] = {}
        tagEffSigList[lab], ws = getTagEff(myconfigfile, ws, MDSettings, "tagEff", lab)
    if debug:
        print(tagEffSigList)

    # Production, detector and tagging asymmetries
    # --------------------------------------------
    printMessage("Tagging assymetries", False)
    aTagEffSigList = {}
    for lab in label:
        aTagEffSigList[lab] = {}
        aTagEffSigList[lab], ws = getTagEff(
            myconfigfile, ws, MDSettings, "aTagEff", lab
        )
    if debug:
        pprint(aTagEffSigList)

    printMessage("Production and detection assymetries", False)
    aProd = {}
    aDet = {}
    for lab in label:
        aProd[lab] = {}
        aDet[lab] = {}
        aProd[lab], ws = getAsymmetryVariable(
            myconfigfile, ws, lab, asymmetry="Production"
        )
        aDet[lab], ws = getAsymmetryVariable(
            myconfigfile, ws, lab, asymmetry="Detection"
        )

    if debug:
        print((aProd, aDet))

    # Coefficient in front of sin, cos, sinh, cosh
    # --------------------------------------------
    printMessage("Coefficient in front of sin, cos, sinh, cosh")
    C, S, D, Cbar, Sbar, Dbar = getCPparameters(decay, myconfigfile)
    flag = 0

    otherargs = {}
    for lab in label:
        otherargs[lab] = []
        if pereventmistag:
            for tagger_idx in range(numTag):
                otherargs[lab].append(tag[tagger_idx])

                if myconfigfile["TaggingCalibration"].get("Type") in ["GLM", "Logit"]:
                    otherargs[lab].append(omegas[lab][tagger_idx])
                    otherargs[lab].append(omegabars[lab][tagger_idx])
                else:
                    otherargs[lab].append(mistag[tagger_idx])
                    otherargs[lab].append(p0[lab][tagger_idx])
                    otherargs[lab].append(p1[lab][tagger_idx])
                    otherargs[lab].append(dp0[lab][tagger_idx])
                    otherargs[lab].append(dp1[lab][tagger_idx])
                    otherargs[lab].append(avetacalib[lab][tagger_idx])

                otherargs[lab].append(tagEffSigList[lab][tagger_idx])
                otherargs[lab].append(aTagEffSigList[lab][tagger_idx])
        else:
            print("No per-event mistag not supported! Will certainly crash now...")
            sys.exit(-1)

        otherargs[lab].append(aProd[lab])
        otherargs[lab].append(aDet[lab])

    for lab in label:
        for i in range(0, numTag):
            print(mistagPDFList[lab][i])

    cosh = {}
    sinh = {}
    cos = {}
    sin = {}

    print("Initializing CP coefficients with otherargs:")
    pprint(otherargs)

    for lab in label:
        cosh[lab] = DecRateCoeff_Bd(
            "signal_cosh_" + str(lab),
            "signal_cosh",
            DecRateCoeff_Bd.kCosh,
            id,
            one1,
            one2,
            *otherargs[lab]
        )
        sinh[lab] = DecRateCoeff_Bd(
            "signal_sinh_" + str(lab),
            "signal_sinh",
            DecRateCoeff_Bd.kSinh,
            id,
            D,
            Dbar,
            *otherargs[lab]
        )
        cos[lab] = DecRateCoeff_Bd(
            "signal_cos_" + str(lab),
            "signal_cos",
            DecRateCoeff_Bd.kCos,
            id,
            C,
            Cbar,
            *otherargs[lab]
        )
        sin[lab] = DecRateCoeff_Bd(
            "signal_sin_" + str(lab),
            "signal_sin",
            DecRateCoeff_Bd.kSin,
            id,
            S,
            Sbar,
            *otherargs[lab]
        )

    if debug:
        print(cosh)
        print(sinh)
        print(cos)
        print(sin)

    # Dec Rates to check significance!

    #    Sbar_minus = RooFormulaVar( 'Sbar_minus',"Sbar_minus",
    #               "-1*@0",RooArgList(Sbar))
    #
    #    cosh = DecRateCoeff_Bd('signal_cosh', 'signal_cosh',
    #           DecRateCoeff_Bd.kCosh, id, one1, one2, *otherargs)
    #    sinh = DecRateCoeff_Bd('signal_sinh', 'signal_sinh',
    #           DecRateCoeff_Bd.kSinh, id, D, D, *otherargs)
    #    cos =  DecRateCoeff_Bd('signal_cos' , 'signal_cos' ,
    #           DecRateCoeff_Bd.kCos,  id, C, C, *otherargs)
    #    sin =  DecRateCoeff_Bd('signal_sin' , 'signal_sin' ,
    #           DecRateCoeff_Bd.kSin,  id, Sbar_minus, Sbar, *otherargs)

    # if debug:
    #    print "[INFO] sin, cos, sinh, cosh created"
    #    cosh.Print("v")
    #    sinh.Print("v")
    #    cos.Print("v")
    #    sin.Print("v")
    #    exit(0)

    # Time PDF
    # ---------------------------
    printMessage("Time PDF")

    tauinv = Inverse("tauinv", "tauinv", gammas)

    timePDF = {}
    for lab in label:
        timePDF[lab] = {}
        name_time = TString("time_signal") + lab
        timePDF[lab] = RooBDecay(
            name_time.Data(),
            name_time.Data(),
            time,
            tauinv,
            deltaGammas,
            cosh[lab],
            sinh[lab],
            cos[lab],
            sin[lab],
            deltaMs,
            trm[lab],
            RooBDecay.SingleSided,
        )

    if debug:
        for lab in label:
            timePDF[lab].printComponentTree()
            printLine(False)

    # Conditional PDF gor per-event mistag or per-event terr
    # --------------------------------------------------------
    printMessage("Conditional PDF")
    condPDF = {}

    if pereventterr and pereventmistag:
        for lab in label:
            noncondset = RooArgSet(time, id)
            condpdfset = RooArgSet(terrpdf[lab])
            condPDF[lab] = {}
            for i in range(0, numTag):
                noncondset.add(tag[i])
                condpdfset.add(mistagPDFList[lab][i])
            name_timeterr = TString("signal_TimeTimeerrPdf_") + lab
            # default way with conditional pdf and extra pdfs for terr and all mistags
            condPDF[lab] = RooProdPdf(
                name_timeterr.Data(),
                name_timeterr.Data(),
                condpdfset,
                RooFit.Conditional(RooArgSet(timePDF[lab]), noncondset),
            )
            # new idea to use only terr as external PDF
            # (maybe even this is not necessary)
            # => need to use RooConditional in FitTo later
            #       totPDF = RooProdPdf(name_timeterr.Data(), name_timeterr.Data(),
            #       RooArgSet(terrpdf), RooFit.Conditional(RooArgSet(timePDF),
            #       RooArgSet(time, id, tag[0], tag[1], mistag[0], mistag[1])))

    else:
        condPDF = timePDF

    if debug:
        for lab in label:
            condPDF[lab].printComponentTree()
            printLine(False)

    # Binning data set if needed
    # --------------------------------------------------------
    dataWA = {}
    if binned:
        printMessage("Binning dats set: --binned = " + str(binned))
        time.setBins(150)
        terr.setBins(10)
        if pereventmistag:
            for i in range(0, numTag):
                mistag[i].setBins(10)
        dataWA_binned = {}

        for lab in label:
            dataW[lab].Print("v")
            printLine(False)
            observables.Print("v")
            printLine(False)
            dataWA[lab] = {}
            dataWA[lab] = RooDataHist(
                "dataWA_binned" + str(lab),
                "dataWA_binned" + str(lab),
                observables,
                dataW[lab],
            )
            dataWA[lab].Print("v")
    else:
        dataWA = dataW

    # Getting the Total PDF
    # --------------------------------------------------------
    printMessage("Total PDF")
    if sim:
        if binned:
            printMessage("Binned version not supported yet for Simultaneous fit")
            exit(0)

        printMessage("Simultaneous PDF")

        cat = RooCategory("category", "category")
        for lab in label:
            cat.defineType(lab)
        cat = WS(ws, cat)
        if debug:
            cat.Print("v")
            printLine(False)

        totPDF = RooSimultaneous("time_signal", "time_signal", cat)
        for lab in label:
            totPDF.addPdf(condPDF[lab], lab)

        # change ROOT wd to storageFile before creating RooDataSets
        # global simDataStorageFile
        # simDataStorageFile = TFile("/ceph/users/kheinicke/b2oc/" \
        # "dsk-run2/toys/highstat-signalonly/1003/simDataStorage.root", "RECREATE")

        printMessage("Creating combData", False)
        nameData = TString("combData")
        if not mc:
            weight_args = [
                RooFit.Cut("sWeights != 0"),
                RooFit.WeightVar("sWeights", True),
            ]
        else:
            weight_args = []
        combData = RooDataSet(
            nameData.Data(),
            nameData.Data(),
            data[label[0]].get(),
            RooFit.Index(cat),
            RooFit.Import(label[0], data[label[0]]),
            *weight_args
        )
        combData = calibrateDecayTime(
            combData, time, myconfigfile, year=label[0], newTimeVarName=newTimeVarName
        )
        combData.Print("v")
        for lab in label:
            if lab == label[0]:
                continue
            tmpDataName = "combData_" + data[lab].GetName()
            tmpData = RooDataSet(
                tmpDataName,
                tmpDataName,
                data[lab].get(),
                RooFit.Index(cat),
                RooFit.Import(lab, data[lab]),
                *weight_args
            )
            if debug:
                printMessage("Adding data set to combData", False)
                tmpData.Print("v")

            tmpData = calibrateDecayTime(
                tmpData, time, myconfigfile, year=lab, newTimeVarName=newTimeVarName
            )
            combData.append(tmpData)

        printMessage("Final combData", False)

        combData.Print("v")
        print("[INFO] Weighted data set: ", combData.isWeighted())
        printLine(False)
    else:
        totPDF = condPDF[label[0]]
        combData = RooDataSet(
            "combData",
            "combData",
            dataWA[label[0]].get(),
            RooFit.Import(dataWA[label[0]]),
            RooFit.Cut("sWeights != 0"),
            RooFit.WeightVar("sWeights", True),
        )
        combData = calibrateDecayTime(
            combData, time, myconfigfile, year=label[0], newTimeVarName=newTimeVarName
        )
        totPDF.SetName("time_signal")

    print("Final dataset contains {} entries".format(combData.numEntries()))

    if debug:
        totPDF.printComponentTree()
        printLine(False)

    # Delta Ms likelihood scan
    # ---------------------------
    if scan:
        RooMsgService.instance().Print("v")
        RooMsgService.instance().deleteStream(1002)
        if debug:
            print("Likelihood scan performing")
        nll = RooNLLVar(
            "nll",
            "-log(sig)",
            totPDF[0],
            dataWA,
            RooFit.NumCPU(int(jobs)),
            RooFit.Silence(True),
        )
        pll = RooProfileLL("pll", "", nll, RooArgSet(deltaMs))
        h = pll.createHistogram("h", deltaMs, RooFit.Binning(200))
        h.SetLineColor(kRed)
        h.SetLineWidth(2)
        h.SetTitle("Likelihood Function - Delta Ms")
        like = TCanvas("like", "like", 1200, 800)
        like.cd(True)
        h.Draw()
        like.Update()
        like.SaveAs("likelihood_Delta_Ms.pdf")
        exit(0)

    return (
        totPDF,
        myconfigfile,
        constraints_for_tagging_calib,
        taggingMultiVarGaussSet,
        combData,
        locals(),
    )


def runSFit(
    debug,
    wsname,
    pereventmistag,
    pereventterr,
    toys,
    pathName,
    treeName,
    workName,
    configName,
    scan,
    binned,
    plotsWeights,
    sample,
    mode,
    year,
    merge,
    unblind,
    mc,
    sim,
    jobs,
    storageFileName,
    hack_sweight_names=False,
):
    (
        totPDF,
        myconfigfile,
        constraints_for_tagging_calib,
        taggingMultiVarGaussSet,
        combData,
        otherParameters,  # need to keep them around so python wont delete them
    ) = prepareFit(
        debug,
        wsname,
        pereventmistag,
        pereventterr,
        toys,
        pathName,
        treeName,
        workName,
        configName,
        scan,
        binned,
        plotsWeights,
        sample,
        mode,
        year,
        merge,
        unblind,
        mc,
        sim,
        jobs,
        storageFileName,
        hack_sweight_names=hack_sweight_names,
        max_evts_per_sample=args.max_events_per_sample,
    )

    # Fitting options
    # -----------------------------
    fitOptsTmp = [
        RooFit.Save(True),
        RooFit.Optimize(2),
        RooFit.Strategy(2),
        # should not be used. See:
        # https://indico.cern.ch/event/859068/contributions/3653700/
        # ...but might be ok as long as the pulls are fine
        RooFit.SumW2Error(myconfigfile.get("SWeightCorrection") == "Fit"),
        RooFit.AsymptoticError(
            myconfigfile.get("SWeightCorrection") == "AsymptoticallyCorrect"
        ),
        RooFit.Extended(False),
        RooFit.NumCPU(int(jobs)),
        RooFit.Minimizer("Minuit"),
        RooFit.Offset(True),
        RooFit.Hesse(True),
    ]
    if unblind or toys:
        fitOptsTmp.append(RooFit.Verbose(True))
    else:
        fitOptsTmp.append(RooFit.PrintLevel(-1))
        fitOptsTmp.append(RooFit.Warnings(False))
        fitOptsTmp.append(RooFit.PrintEvalErrors(-1))

    if constraints_for_tagging_calib:
        fitOptsTmp.append(RooFit.ExternalConstraints(taggingMultiVarGaussSet))

    fitOpts = RooLinkedList()
    for cmd in fitOptsTmp:
        fitOpts.Add(cmd)

    # Fitting
    # -----------------------------
    printMessage(
        (
            "Fitting --unblind = {} "
            "--binned = {} "
            "--toys = {} "
            "tagging constraints = {}"
        ).format(unblind, binned, toys, constraints_for_tagging_calib)
    )
    fitOpts.Print("v")
    printLine(False)

    # simDataStorageFile.cd()

    # TODO: Remove this workaround if https://sft.its.cern.ch/jira/browse/ROOT-10668
    # is fixed
    for param in totPDF.getParameters(combData):
        if not param.isConstant() and param.GetName() != param.GetTitle():
            print(
                "[INFO] Resetting title of {} (was '{}')".format(
                    param.GetName(), param.GetTitle()
                )
            )
            param.SetTitle(param.GetName())

    myfitresult = totPDF.fitTo(combData, fitOpts)

    # Printing results
    # -----------------------------
    printMessage("Printing results")

    if unblind or toys:
        myfitresult.Print("v")
        myfitresult.correlationMatrix().Print()
        myfitresult.covarianceMatrix().Print()
    else:
        print("[INFO Result] Matrix quality is", myfitresult.covQual())
        par = myfitresult.floatParsFinal()
        const = myfitresult.constPars()
        print("[INFO Result] Status: ", myfitresult.status())
        print("[INFO Result] -------------- Constant parameters ------------- ")
        for i in range(0, const.getSize()):
            print(
                "[INFO Result] parameter %s  set to be  %0.4lf"
                % (const[i].GetName(), const[i].getValV())
            )

        print("[INFO Result] -------------- Floated parameters ------------- ")

        for i in range(0, par.getSize()):
            name = TString(par[i].GetName())
            if name.Contains(myconfigfile["Decay"]):
                if name == "DeltaMs_{}".format(myconfigfile["Decay"]):
                    # deltams is blinded so we *could* print the valuek
                    print(
                        "[INFO Result] parameter {} (blinded) = (X +/- {:0.4f})".format(
                            par[i].GetName(),
                            par[i].getError(),
                        )
                    )
                else:
                    print(
                        "[INFO Result] parameter {} = (XXX +/- {:.4f})".format(
                            par[i].GetName(),
                            par[i].getError(),
                        )
                    )
            else:
                print(
                    "[INFO Result] parameter {} = ({:.4f} +/- {:.4f})".format(
                        par[i].GetName(), par[i].getValV(), par[i].getError()
                    )
                )

    workout = RooWorkspace("workspace", "workspace")
    # getattr(workout, "import")(combData)
    # wont serialize RooDataSet since it is buggy
    # this will hopefully be fixed with https://sft.its.cern.ch/jira/browse/ROOT-10475
    getattr(workout, "import")(totPDF)
    getattr(workout, "import")(myfitresult)

    wsFile = TFile(wsname, "RECREATE")
    workout.Write()
    wsFile.Close()

    fitresultFile = TFile(wsname.replace(".root", "_fitresult.root"), "RECREATE")
    myfitresult.Write("fitresult")
    fitresultFile.Close()

    treeFile = TFile(wsname.replace(".root", "_tree.root"), "RECREATE")
    clonedTree = (
        combData.GetClonedTree()
    )  # using GetClonedTree seems to be a valid choice to serialize RooDataSet
    clonedTree.Write("combData")
    treeFile.Close()


def initArgParser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d",
        "--debug",
        dest="debug",
        default=False,
        action="store_true",
        help="print debug information while processing",
    )
    parser.add_argument(
        "-s",
        "--save",
        dest="wsname",
        metavar="WSNAME",
        default="WS_Time_DsPi.root",
        help='save the model PDF and generated dataset to file "WS_WSNAME.root"',
    )

    parser.add_argument(
        "--pereventmistag",
        dest="pereventmistag",
        default=False,
        action="store_true",
        help="Use the per-event mistag?",
    )

    parser.add_argument(
        "--pereventterr",
        dest="pereventterr",
        default=False,
        action="store_true",
        help="Use the per-event time errors?",
    )

    parser.add_argument(
        "-t",
        "--toys",
        dest="toys",
        action="store_true",
        default=False,
        help="are we working with toys?",
    )

    parser.add_argument(
        "--f", "--fileName", dest="fileName", nargs="+", help="name of the inputfiles"
    )

    parser.add_argument(
        "--treeName", dest="treeName", default="merged", help="name of the workspace"
    )
    parser.add_argument(
        "--workName", dest="workName", default="workspace", help="name of the workspace"
    )
    parser.add_argument("--scan", dest="scan", default=False, action="store_true")

    parser.add_argument("--cat", dest="cat", default=False, action="store_true")

    parser.add_argument(
        "--configName", dest="configName", default="Bs2DsPiConfigForNominalDMSFit"
    )

    parser.add_argument(
        "--plotsWeights", dest="plotsWeights", default=False, action="store_true"
    )

    parser.add_argument("--sim", dest="sim", default=False, action="store_true")

    parser.add_argument(
        "-p",
        "--pol",
        "--polarity",
        dest="pol",
        nargs="+",
        default="down",
        help="Polarity can be: up, down, both ",
    )
    parser.add_argument(
        "-m",
        "--mode",
        dest="mode",
        nargs="+",
        default="kkpi",
        help="Mode can be: all, kkpi, kpipi, pipipi, nonres, kstk, phipi",
    )

    parser.add_argument(
        "--merge",
        nargs="+",
        dest="merge",
        default="",
        help="merge can be: pol, run1, run2, runs",
    )
    parser.add_argument(
        "--year",
        nargs="+",
        dest="year",
        default="",
        help="year of data taking can be: 2011, 2012, 2015, 2016, 2017",
    )

    parser.add_argument(
        "--binned",
        dest="binned",
        default=False,
        action="store_true",
        help="binned data Set",
    )
    parser.add_argument(
        "--unblind",
        dest="unblind",
        default=False,
        action="store_true",
        help="unblind results",
    )
    parser.add_argument(
        "--MC", dest="mc", default=False, action="store_true", help="fit MC samples"
    )
    parser.add_argument(
        "-j",
        "--jobs",
        default=1,
        help="""Specify the number of
                        cores used by RooFit""",
    )
    parser.add_argument(
        "--storageFile",
        type=str,
        default="storage.root",
        help="""
                        ROOT file to be used as RooTreeDataStore backend.""",
    )
    parser.add_argument(
        "--combined-data-hack",
        default=False,
        action="store_true",
        help=""" Set this to true if the provided dataset is
                        already combined and contains sweight branch names
                        ending with e.g. 20152016.  """,
    )
    parser.add_argument(
        "--max-events-per-sample",
        type=int,
        help="""Cut away
                        events > this number""",
    )
    parser.add_argument(
        "--random-seed",
        type=int,
        help="""Initialise
                        random generator with this seed. Useful for fitting
                        toys with fixed, varied fit parameters.""",
    )
    return parser


def parse_args():
    return initArgParser().parse_args()


def translateMergingArgs(args):
    # check length of list args argument
    if (
        not args.mc
        and len(args.year) != len(args.fileName)
        and not args.combined_data_hack
    ):
        raise ConfigurationError(
            "If not using --combined-data-hack, Number of "
            "filenames provided must match number of configured years"
        )

    # translate merged years
    tmp_years = []
    tmp_merge = []
    tmp_mode = []

    tmp_pol = []
    if args.pol[0] == "both":
        pol = ["up", "down"]
    else:
        pol = args.pol

    for i, y in enumerate(args.year):
        if y == "20152016":
            y = ["2015", "2016"]
            if len(pol) > 1:
                tmp_merge.append(["pol", "run2"])  # always merge pol and runs for now
            else:
                tmp_merge.append(["run2"])
        else:
            y = [y]
            tmp_merge.append(["pol" if len(pol) > 1 else ""])
        tmp_years.append(y)
        tmp_pol.append(pol)
        tmp_mode.append(args.mode)

    args.year = tmp_years
    args.pol = tmp_pol
    args.merge = tmp_merge
    args.mode = tmp_mode

    if args.combined_data_hack or args.mc:
        # if the data already is combined, read all years from the same file
        args.fileName = len(args.year) * args.fileName

    return args


if __name__ == "__main__":
    args = parse_args()

    print("==========================================================")
    print("FITTER IS RUNNING WITH THE FOLLOWING CONFIGURATION OPTIONS")
    print(vars(args))
    print("==========================================================")

    args = translateMergingArgs(args)

    config = args.configName
    last = config.rfind("/")
    directory = config[: last + 1]
    configName = config[last + 1 :]
    p = configName.rfind(".")
    configName = configName[:p]

    # import sys
    sys.path.append(directory)

    if args.random_seed:
        initRandomGenerator(args.random_seed)
        print(
            "Random generator {} initialized with seed {}.".format(
                RANDOM_GENERATOR, args.random_seed
            )
        )

    # storageFile = TFile(args.storageFile, "RECREATE")
    # RooAbsData.setDefaultStorageType(RooAbsData.Tree)

    runSFit(
        debug=args.debug,
        wsname=args.wsname,
        pereventmistag=args.pereventmistag,
        pereventterr=args.pereventterr,
        toys=args.toys,
        pathName=args.fileName,
        treeName=args.treeName,
        workName=args.workName,
        configName=configName,
        scan=args.scan,
        binned=args.binned,
        plotsWeights=args.plotsWeights,
        sample=args.pol,
        mode=args.mode,
        year=args.year,
        merge=args.merge,
        unblind=args.unblind,
        mc=args.mc,
        sim=args.sim,
        jobs=args.jobs,
        storageFileName=args.storageFile,
        hack_sweight_names=args.combined_data_hack,
    )
