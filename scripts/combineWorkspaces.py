#! /usr/bin/env python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys

import ROOT

ROOT.PyConfig.IgnoreCommandLineOptions = True

if __name__ == "__main__":
    upfilename, downfilename, outputfilename = sys.argv[1:]

    print(
        "Adding datasets to {} from {}, saving to {}".format(
            upfilename, downfilename, outputfilename
        )
    )

    upfile = ROOT.TFile(upfilename)
    downfile = ROOT.TFile(downfilename)

    ws = upfile.Get("workspace")

    for data in downfile.Get("workspace").allData():
        name = data.GetName().replace("up", "down")
        data.SetName(name)
        getattr(ws, "import")(data)

    outfile = ROOT.TFile(outputfilename, "RECREATE")
    ws.Write("workspace")

    outfile.Close()
