###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function

import os
import sys

from plotSFit import plotDataSet, plotFitModel, prepareFrames, tuneAndDraw
from ROOT import RooSimultaneous, TCanvas, TFile
from runSFit_new import initArgParser, prepareFit, translateMergingArgs


class ConfigurationError(BaseException):
    pass


def parse_args():
    parser = initArgParser()
    parser.add_argument("--fitresult", type=str, required=True)
    parser.add_argument("--fitresult-object", type=str, default="fitresult")
    parser.add_argument("--output-directory", type=str, default="build")
    parser.add_argument("--log-scale", default=False, action="store_true")
    parser.add_argument("--binning", default=150, type=int)
    args = parser.parse_args()
    # store initial years argument before translateMergingArgs function
    args.plotting_years = args.year
    return args


if __name__ == "__main__":
    args = parse_args()

    args = translateMergingArgs(args)

    configDir = os.path.dirname(args.configName)
    sys.path.insert(0, configDir)
    configName = os.path.splitext(os.path.split(args.configName)[1])[0]

    (
        totPDF,
        myconfigfile,
        constraints_for_tagging_calib,
        taggingMultiVarGaussSet,
        combData,
        otherParameters,  # need to keep them around so python wont delete them
    ) = prepareFit(
        debug=args.debug,
        wsname=args.wsname,
        pereventmistag=args.pereventmistag,
        pereventterr=args.pereventterr,
        toys=args.toys,
        pathName=args.fileName,
        treeName=args.treeName,
        workName=args.workName,
        configName=configName,
        scan=args.scan,
        binned=args.binned,
        plotsWeights=args.plotsWeights,
        sample=args.pol,
        mode=args.mode,
        year=args.year,
        merge=args.merge,
        unblind=args.unblind,
        mc=args.mc,
        sim=args.sim,
        jobs=args.jobs,
        storageFileName=args.storageFile,
    )

    fitresultFile = TFile(args.fitresult)
    fitresult = fitresultFile.Get(args.fitresult_object)

    # set the pdf parameters to the fitresult
    for fittedParam in fitresult.floatParsFinal():
        pdfParam = totPDF.getParameters(combData).find(fittedParam.GetName())
        try:
            pdfParam.setVal(fittedParam.getVal())
        except AttributeError as e:
            print(e)
            print(
                "Cannot set variable {} to fitted value".format(fittedParam.GetName())
            )

    if not args.sim:
        raise ConfigurationError("Non-simultaneous fit is not supported")

    category = totPDF.getObservables(combData).find("category")
    time = totPDF.getObservables(combData).find("BeautyTime")

    canvas = TCanvas("canvas", "canvas", 1200, 1000)
    canvas.cd()

    additionalCuts = myconfigfile.get("AdditionalCuts", {}).get("All", {}).get("Data")
    if additionalCuts is not None:
        print("[INFO] applying additional data cut:\n\t{}".format(additionalCuts))
        before = combData.numEntries()
        combData = combData.reduce(additionalCuts)
        after = combData.numEntries()
        print("[INFO] {} -> {} entries".format(before, after))
    else:
        print("[INFO] No additional cuts applied")

    timeFrame, pullFrame = prepareFrames(
        time, decay=myconfigfile["Decay"], entries=combData.numEntries()
    )

    pdfNameDict = {
        cat: "time_signal" + cat
        for cat in args.plotting_years
        if totPDF.getComponents().find("time_signal" + cat)
    }

    plotPDF = RooSimultaneous("time_signal", "time_signal", category)
    for category_label, pdfName in list(pdfNameDict.items()):
        plotPDF.addPdf(totPDF.getComponents().find(pdfName), category_label)

    plottedData = plotDataSet(
        combData, timeFrame, args.binning, args.sim, args.plotting_years
    )
    plotFitModel(
        plotPDF,
        timeFrame,
        myconfigfile,
        args.log_scale,
        myconfigfile["Decay"],
        args.debug,
        pdfNameDict,
        args.sim,
        category,
        args.plotting_years,
        plottedData,
        time,
    )

    if args.toys:
        data_name = "Toy Data"
    else:
        data_name = "Data"
    _, _, canvas, _ = tuneAndDraw(
        timeFrame,
        pullFrame,
        canvas,
        range=(time.getMin(), time.getMax()),
        config=myconfigfile,
        data_name=data_name,
    )

    for ext in ["png", "pdf", "C"]:
        canvas.Print(
            os.path.join(
                args.output_directory,
                "{}_{}.{}".format(time.GetName(), myconfigfile["Decay"], ext),
            )
        )
