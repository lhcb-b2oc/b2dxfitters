###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# --------------------------------------------------------------------------- #
#                                                                             #
#   Python script to compare datas or pdfs                                    #
#                                                                             #
#   Example usage:                                                            #
#      python comparePDF.py                                                   #
#                                                                             #
#   Author: Agnieszka Dziurda                                                 #
#   Date  : 28 / 06 / 2012                                                    #
#                                                                             #
# --------------------------------------------------------------------------- #

# -----------------------------------------------------------------------------
# Load necessary libraries
# -----------------------------------------------------------------------------
""":"
# This part is run by the shell. It does some setup which is convenient to save
# work in common use cases.

# make sure the environment is set up properly
if test -n "$CMTCONFIG" \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersDict.so \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersLib.so; then
    # all ok, software environment set up correctly, so don't need to do
    # anything
    true
else
    if test -n "$CMTCONFIG"; then
        # clean up incomplete LHCb software environment so we can run
        # standalone
        echo Cleaning up incomplete LHCb software environment.
        PYTHONPATH=`echo $PYTHONPATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
        export PYTHONPATH
        LD_LIBRARY_PATH=`echo $LD_LIBRARY_PATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
	export LD_LIBRARY_PATH
        exec env -u CMTCONFIG -u B2DXFITTERSROOT "$0" "$@"
    fi
    # automatic set up in standalone build mode
    if test -z "$B2DXFITTERSROOT"; then
        cwd="$(pwd)"
        # try to find from where script is executed, use current directory as
        # fallback
        tmp="$(dirname $0)"
        tmp=${tmp:-"$cwd"}
        # convert to absolute path
        tmp=`readlink -f "$tmp"`
        # move up until standalone/setup.sh found, or root reached
        while test \( \! -d "$tmp"/standalone \) -a -n "$tmp" -a "$tmp"\!="/"; do
            tmp=`dirname "$tmp"`
        done
        if test -d "$tmp"/standalone; then
            cd "$tmp"/standalone
            . ./setup.sh
        else
            echo `basename $0`: Unable to locate standalone/setup.sh
            exit 1
        fi
            cd "$cwd"
        unset tmp
        unset cwd
    fi
fi
# figure out which custom allocators are available
# prefer jemalloc over tcmalloc
for i in libjemalloc libtcmalloc; do
    for j in `echo "$LD_LIBRARY_PATH" | tr ':' ' '` \
            /usr/local/lib /usr/lib /lib; do
        for k in `find "$j" -name "$i"'*.so.?' | sort -r`; do
            if test \! -e "$k"; then
                continue
            fi
            echo adding $k to LD_PRELOAD
            if test -z "$LD_PRELOAD"; then
                export LD_PRELOAD="$k"
                break 3
            else
                export LD_PRELOAD="$LD_PRELOAD":"$k"
                break 3
            fi
        done
    done
done
# set batch scheduling (if schedtool is available)
schedtool="`which schedtool 2>/dev/zero`"
if test -n "$schedtool" -a -x "$schedtool"; then
    echo "enabling batch scheduling for this job (schedtool -B)"
    schedtool="$schedtool -B -e"
else
    schedtool=""
fi

# set ulimit to protect against bugs which crash the machine: 2G vmem max,
# no more then 8M stack
ulimit -v $((2048 * 1024))
ulimit -s $((   8 * 1024))

# trampoline into python
exec $schedtool /usr/bin/time -v env python -O -- "$0" "$@"
"""
# -----------------------------------------------------------------------------
# Load necessary libraries
# -----------------------------------------------------------------------------
from __future__ import division, print_function
from past.utils import old_div

__doc__ = """ real docstring """
import os
import sys
import argparse
from ROOT import (
    PlotSettings,
    RooAbsData,
    RooAbsReal,
    RooAddition,
    RooArgList,
    RooArgSet,
    RooBDecay,
    RooBinning,
    RooCategory,
    RooCubicSplineFun,
    RooDataHist,
    RooDataSet,
    RooFit,
    RooFormulaVar,
    RooGaussEfficiencyModel,
    RooProdPdf,
    RooRealConstant,
    RooRealVar,
    RooWorkspace,
    TCanvas,
    TGraph,
    TLatex,
    TLegend,
    TLine,
    TPad,
    TString,
    gROOT,
    gStyle,
    kBlue,
    kRed,
    kSolid,
    kWhite,
)
from B2DXFitters import GeneralUtils, MassFitUtils
from B2DXFitters.MDFitSettingTranslator import Translator
from B2DXFitters.WS import WS as WS
from B2DXFitters.mdfitutils import checkDuplications as checkDuplications
from B2DXFitters.mdfitutils import checkMerge as checkMerge
from B2DXFitters.mdfitutils import getObservables as getObservables
from B2DXFitters.mdfitutils import getSampleModeYear as getSampleModeYear
from B2DXFitters.mdfitutils import getStdVector as getStdVector

gROOT.SetBatch()


def printMessage(message, bold=True):
    printLine(bold)
    print("[INFO] ", message)
    printLine(bold)


# ------------------------------------------------------------------------------
def printLine(bold):
    if bold:
        print(
            "==================================================================="
            "============================================================"
        )
    else:
        print(
            "-------------------------------------------------------------------"
            "------------------------------------------------------------"
        )


# ------------------------------------------------------------------------------
# Get signature of signal obtained from MC for PIDK variable
# -----------------------------------------------------------------------------
def getSignalNames(myconfig):
    decay = myconfig["Decay"]
    Dmodes = myconfig["CharmModes"]
    year = myconfig["YearOfDataTaking"]

    hypo = ""
    if "Hypothesis" in list(myconfig.keys()):
        hypo = " " + myconfig["Hypothesis"] + "Hypo"

    signalNames = []
    for y in year:
        for dmode in Dmodes:
            name = "#Signal " + decay + " " + dmode + " " + y + hypo
            signalNames.append(TString(name))

    return signalNames


# ------------------------------------------------------------------------------
def checkWeights(workspace, datasetTS, smyhs, debug):
    dataCheck = []
    i = 0
    nEn = []

    sum2011 = 0
    sum2012 = 0
    sumRun1 = 0
    for smyh in smyhs:
        name = datasetTS + smyh
        dataCheck.append(GeneralUtils.GetDataSet(workspace, name, debug))
        if i != 0:
            nEn.append(dataCheck[i].sumEntries())
            if smyh.Contains("2011"):
                sum2011 = sum2011 + dataCheck[i].sumEntries()
            if smyh.Contains("2012"):
                sum2012 = sum2012 + dataCheck[i].sumEntries()
            sumRun1 = sumRun1 + dataCheck[i].sumEntries()
        else:
            nEn.append(dataCheck[0].sumEntries())
            sumRun1 = sumRun1 + dataCheck[0].sumEntries()
            if smyh.Contains("2011"):
                sum2011 = sum2011 + dataCheck[0].sumEntries()
            if smyh.Contains("2012"):
                sum2012 = sum2012 + dataCheck[0].sumEntries()
        i = i + 1

    if debug:
        print(nEn)
        print("Sum of 2011: ", sum2011)
        print("Sum of 2012: ", sum2012)
        print("Sum of Run1: ", sumRun1)
        j = 0
        print("--------------------------------------------")
        print("[INFO] Percentage with respect to year")
        print("--------------------------------------------")
        for smyh in smyhs:
            if smyh.Contains("2011"):
                print(
                    "Sample: ",
                    smyh,
                    " percent: ",
                    old_div(nEn[j], sum2011) * 100,
                    " [%]",
                )
            if smyh.Contains("2012"):
                print(
                    "Sample: ",
                    smyh,
                    " percent: ",
                    old_div(nEn[j], sum2012) * 100,
                    " [%]",
                )
            j = j + 1

    print("--------------------------------------------")
    print("[INFO] Percentage with respect to all data")
    print("--------------------------------------------")

    j = 0
    for smyh in smyhs:
        print("Sample: ", smyh, " percent: ", old_div(nEn[j], sumRun1) * 100, " [%]")
        j = j + 1


# ------------------------------------------------------------------------------
def getSplinesVariables(myconfigfile, ws, name, i):
    lab = "_" + name
    if name in myconfigfile["Acceptance"]:
        var = WS(
            ws,
            RooRealVar(
                "var" + str(i + 1) + str(lab),
                "var" + str(i + 1) + str(lab),
                myconfigfile["Acceptance"][name]["values"][i],
                0.0,
                10.0,
            ),
        )
    else:
        var = WS(
            ws,
            RooRealVar(
                "var" + str(i + 1),
                "var" + str(i + 1),
                myconfigfile["Acceptance"]["values"][i],
                0.0,
                10.0,
            ),
        )

    setConstantIfSoConfigured(var, myconfigfile)
    return var, ws


# ------------------------------------------------------------------------------
def getScaledTerr(myconfigfile, ws, name):
    varNames = ["p0", "p1", "p2"]
    values = {}

    for varName in varNames:
        values[varName] = {}
        values[varName]["val"] = {}
        values[varName]["shared"] = {}

        if name in myconfigfile["Resolution"]:
            values[varName]["val"] = myconfigfile["Resolution"][name]["scaleFactor"][
                varName
            ]
            values[varName]["shared"] = False
        else:
            values[varName]["val"] = myconfigfile["Resolution"]["scaleFactor"][varName]
            values[varName]["shared"] = True

    trm_scale = {}
    for varName in varNames:
        trm_scale[varName] = {}
        if not values[varName]["shared"]:
            trm_scale[varName] = WS(
                ws,
                RooRealVar(
                    "trm_scale_" + varName + name,
                    "Gaussian resolution model mean " + varName + name,
                    values[varName]["val"],
                    "ps",
                ),
            )
        else:
            trm_scale[varName] = WS(
                ws,
                RooRealVar(
                    "trm_scale_" + varName,
                    "Gaussian resolution model mean " + varName,
                    values[varName]["val"],
                    "ps",
                ),
            )

    print("--------------------------------------------------------------")
    if "UsedResolution" in myconfigfile:
        print("[INFO] Used resolution: ", myconfigfile["UsedResolution"])
    if name != "":
        print("[INFO]    for the label: ", name)
    print("[INFO] s0: ", trm_scale["p0"].GetName(), trm_scale["p0"].getValV())
    print("[INFO] s1: ", trm_scale["p1"].GetName(), trm_scale["p1"].getValV())
    print("[INFO] s2: ", trm_scale["p2"].GetName(), trm_scale["p2"].getValV())

    return trm_scale, ws


# ------------------------------------------------------------------------------
def getTrmMean(myconfigfile, ws, name):
    value = myconfigfile["Resolution"][name]["meanBias"]

    if name in myconfigfile["Resolution"]:
        m = WS(
            ws,
            RooRealVar(
                "trm_mean" + str(name), "Gaussian resolution model mean", value, "ps"
            ),
        )
    else:
        m = WS(
            ws, RooRealVar("trm_mean", "Gaussian resolution model mean", value, "ps")
        )
    print("--------------------------------------------------------------")
    print("[INFO] mean: ", m.GetName(), m.getValV())

    return m, ws


# ------------------------------------------------------------------------------
def getTrmMeanScaleFactor(myconfig, ws, name):
    """Read myconfig["Resolution"][name]['meanScale'] if available and
    provide RooRealVar meant for resolution mean scale factor.
    Defaults to 1.0 if the configuration is not available
    """
    if name in myconfig["Resolution"]:
        scaleFactorValue = myconfig["Resolution"][name].get("meanScale", 1.0)
        titleAppendix = "_" + name
    else:
        scaleFactorValue = myconfig["Resolution"].get("meanScale", 1.0)
        titleAppendix = ""
    scaleFactor = WS(
        ws,
        RooRealVar(
            "trm_mean_scale" + titleAppendix,
            "Gaussian resolution model mean scale factor",
            scaleFactorValue,
        ),
    )

    print(
        "[INFO] mean scale factor: {} {}".format(
            scaleFactor.GetName(), scaleFactor.getValV()
        )
    )

    return scaleFactor, ws


# ------------------------------------------------------------------------------
def setConstantIfSoConfigured(var, myconfigfile):
    defName = var.GetName() + "_default"
    if (var.GetName() or defName) in myconfigfile["constParams"]:
        var.setConstant()
        print(
            "[INFO] Parameter: %s set to be constant with value %lf"
            % (var.GetName(), var.getValV())
        )
    else:
        print("[INFO] Parameter: %s floats in the fit" % (var.GetName()))
        print("[INFO]   ", var.Print())


# ------------------------------------------------------------------------------
def runFitSplineAcc(
    debug,
    configName,
    read,
    fileNameIn,
    wsname,
    workName,
    sample,
    mode,
    merge,
    year,
    binned,
    rel,
    log,
    plot,
    sufix,
    checkW,
    plot_prefix,
    bar,
):
    RooAbsData.setDefaultStorageType(RooAbsData.Tree)
    ws = RooWorkspace("intWork", "intWork")

    myconfigfilegrabber = __import__(configName, fromlist=["getconfig"]).getconfig
    myconfigfile = myconfigfilegrabber()

    print("==========================================================")
    print("RUN MD FITTER IS RUNNING WITH THE FOLLOWING CONFIGURATION OPTIONS")
    for option in myconfigfile:
        if option == "constParams":
            for param in myconfigfile[option]:
                print(param, "is constant in the fit")
        else:
            print(option, " = ", myconfigfile[option])
    print("==========================================================")

    RooAbsData.setDefaultStorageType(RooAbsData.Tree)

    TString("../data/") + TString(configName) + TString(".py")

    mdt = Translator(myconfigfile, "MDSettings", False)

    MDSettings = mdt.getConfig()
    MDSettings.Print("v")

    dirPlot = "Plot"
    extPlot = "pdf"
    if "ControlPlots" in myconfigfile:
        if "Directory" in myconfigfile["ControlPlots"]:
            dirPlot = myconfigfile["ControlPlots"]["Directory"]
            if not os.path.exists(dirPlot):
                os.makedirs(dirPlot)
        if "Extension" in myconfigfile["ControlPlots"]:
            extPlot = myconfigfile["ControlPlots"]["Extension"]

    plotSettings = PlotSettings(
        "plotSettings",
        "plotSettings",
        TString(dirPlot),
        extPlot,
        100,
        True,
        False,
        True,
    )
    plotSettings.Print("v")

    workspace = RooWorkspace("workspace", "workspace")
    decayTS = myconfigfile["Decay"]

    if not read:
        workspace = RooWorkspace("workspace", "workspace")
        signalNames = getSignalNames(myconfigfile)
        for i in range(0, signalNames.__len__()):
            print(signalNames[i])
            year = GeneralUtils.CheckDataYear(signalNames[i])
            workspace = MassFitUtils.ObtainSignal(
                TString(myconfigfile["dataName"]),
                signalNames[i],
                MDSettings,
                decayTS,
                False,
                False,
                workspace,
                False,
                1.0,
                1.0,
                plotSettings,
                debug,
            )

        workspace.Print()
        GeneralUtils.SaveWorkspace(workspace, TString(workName), debug)
    else:
        workspace = GeneralUtils.LoadWorkspace(
            TString(fileNameIn), TString(workName), debug
        )

    t = TString("_")
    datasetTS = TString("dataSetMC_") + decayTS + t
    if checkDuplications(year, merge):
        print(
            "------------------------------------------------------"
            "----------------------------------"
        )
        print(
            "[ERROR]: You try to merge twice the same data sample, "
            "please check --merge options: ",
            merge,
        )
        print(
            "------------------------------------------------------"
            "----------------------------------"
        )
        exit(0)
    s, m, y = checkMerge(mode, sample, year, merge)

    if debug:
        print("-----------------------------")
        print("[INFO] Initial samples: ")
        print("-----------------------------")
    sm_init = getSampleModeYear(mode, sample, year, debug)
    if debug:
        print("-----------------------------")
        print("[INFO] Target samples: ")
        print("-----------------------------")
    sm = getSampleModeYear(m, s, y, debug)

    if debug:
        print("-----------------------------")
        print("[INFO] Merging options: ")
        print("-----------------------------")

    my = ""
    if merge != "":
        my = merge + year
        mergeList = getStdVector(my, debug)
    else:
        my = year
        mergeList = getStdVector(["none"], debug)
    sampleList = getStdVector(sample, False)
    modeList = getStdVector(mode, False)
    yearList = getStdVector(year, False)

    sam = RooCategory("sample", "sample")

    getObservables(MDSettings, workspace, debug)
    combData = GeneralUtils.GetDataSet_weighted(
        workspace,
        sam,
        datasetTS,
        sampleList,
        modeList,
        yearList,
        sm_init,
        sm,
        mergeList,
        debug,
    )

    if debug:
        combData.Print("v")

    obs = combData.get()
    time = obs.find(MDSettings.GetTimeVarOutName().Data())
    terr = obs.find(MDSettings.GetTerrVarOutName().Data())

    time.setBins(int(myconfigfile["Bins"]))
    dataF = RooDataSet(
        "dataSet_time", "dataSet_time", combData.get(), RooFit.Import(combData)
    )
    dataF_binned = RooDataHist(
        "data_fit_binned", "data_fit_binned", RooArgSet(time, terr), dataF
    )

    print("-------------------- Data set info --------------------")
    print(
        "[INFO] Data set unbinned: ",
        dataF.GetName(),
        dataF.numEntries(),
        dataF.sumEntries(),
    )
    print(
        "[INFO] Data set binned: ",
        dataF_binned.GetName(),
        dataF_binned.numEntries(),
        dataF.sumEntries(),
    )
    print("-------------------------------------------------------")
    dataF.Print("v")
    print("-------------------------------------------------------")
    dataF_binned.Print("v")
    print("-------------------------------------------------------")

    printMessage("Acceptance model: splines")

    tMax = time.getMax()
    tMin = time.getMin()
    binName = TString("splineBinning")
    numKnots = myconfigfile["Acceptance"]["knots"].__len__()
    TimeBin = RooBinning(tMin, tMax, binName.Data())
    for i in range(0, numKnots):
        print(
            "[INFO]   knot %s in place %s "
            % (str(i), str(myconfigfile["Acceptance"]["knots"][i]))
        )
        TimeBin.addBoundary(myconfigfile["Acceptance"]["knots"][i])

    TimeBin.removeBoundary(tMin)
    TimeBin.removeBoundary(tMax)
    TimeBin.Print("v")
    time.setBinning(TimeBin, binName.Data())
    time.setRange(tMin, tMax)
    listCoeff = GeneralUtils.GetCoeffFromBinning(TimeBin, time, debug)
    printLine(False)

    tacc_list = {}
    tacc_var = {}
    spl = {}

    label = y
    print(y)

    for lab in label:
        tacc_list[lab] = {}
        tacc_var[lab] = {}
        tacc_list[lab] = RooArgList()
        tacc_var[lab] = []

        for i in range(0, numKnots):
            tacc, ws = getSplinesVariables(myconfigfile, ws, lab, i)
            tacc_var[lab].append(tacc)
            tacc_list[lab].add(tacc_var[lab][i])

        if myconfigfile["Acceptance"].get("FloatAll", False):
            for i in range(numKnots, numKnots + 2):
                tacc, ws = getSplinesVariables(myconfigfile, ws, lab, i)
                tacc_var[lab].append(tacc)
                tacc_list[lab].add(tacc_var[lab][i])
        else:
            if lab in myconfigfile["Acceptance"]:
                tacc_var[lab].append(
                    WS(
                        ws,
                        RooRealVar(
                            "var" + str(numKnots + 1) + "_" + str(lab),
                            "var" + str(numKnots + 1) + "_" + str(lab),
                            1,
                        ),
                    )
                )
            else:
                tacc_var[lab].append(
                    WS(
                        ws,
                        RooRealVar(
                            "var" + str(numKnots + 1), "var" + str(numKnots + 1), 1
                        ),
                    )
                )

            lenght = tacc_var[lab].__len__()
            tacc_list[lab].add(tacc_var[lab][lenght - 1])
            print(
                "[INFO]   n-2: ",
                tacc_var[lab][lenght - 2].GetName(),
                tacc_var[lab][lenght - 2].getValV(),
            )
            print(
                "[INFO]   n-1: ",
                tacc_var[lab][lenght - 1].GetName(),
                tacc_var[lab][lenght - 1].getValV(),
            )
            if lab in myconfigfile["Acceptance"]:
                name = "var" + str(numKnots + 2) + "_" + str(lab)
            else:
                name = "var" + str(numKnots + 2)
            if myconfigfile["Acceptance"].get("FixLast1", False):
                tacc_var[lab].append(WS(ws, RooRealVar(name, name, 1)))
            else:
                tacc_var[lab].append(
                    WS(
                        ws,
                        RooAddition(
                            name,
                            name,
                            RooArgList(
                                tacc_var[lab][lenght - 2], tacc_var[lab][lenght - 1]
                            ),
                            listCoeff,
                        ),
                    )
                )
            tacc_list[lab].add(tacc_var[lab][lenght])
            print(
                "[INFO]   n: ",
                tacc_var[lab][lenght].GetName(),
                tacc_var[lab][lenght].getValV(),
            )

        spl[lab] = RooCubicSplineFun(
            "splinePdf_" + str(lab),
            "splinePdf_" + str(lab),
            time,
            binName.Data(),
            tacc_list[lab],
        )
        printLine(False)

    trm = []
    terrpdf = []

    # the decay time error is an extra observable !
    printMessage("Resolution model: Gaussian with per-event observable")

    trm_scale = RooRealVar("trm_scale", "Gaussian resolution model scale factor", 1.0)

    trm_mean = {}
    trm_mean_scale_factor = {}
    scale = {}
    terr_scaled = {}
    trm = {}
    terrpdf = {}

    for lab in label:
        trm_mean[lab], ws = getTrmMean(myconfigfile, ws, lab)
        scale[lab], ws = getScaledTerr(myconfigfile, ws, lab)
        terr_scaled[lab] = RooFormulaVar(
            "trm_scaled_terr_" + str(lab),
            "scale",
            "@0+@1*@3+@2*@3*@3",
            RooArgList(scale[lab]["p0"], scale[lab]["p1"], scale[lab]["p2"], terr),
        )
        trm_mean_scale_factor[lab], ws = getTrmMeanScaleFactor(myconfigfile, ws, lab)
        trm[lab] = RooGaussEfficiencyModel(
            "resmodel_" + lab,
            "resmodel_" + lab,
            time,
            spl[lab],
            trm_mean[lab],
            terr_scaled[lab],
            trm_mean_scale_factor[lab],
            trm_scale,
        )

        terrpdfName = TString("terrpdf_") + TString(lab)
        terrpdf[lab] = GeneralUtils.CreateHistPDF(dataF, terr, terrpdfName, 80, debug)
        GeneralUtils.SaveTemplate(dataF, terrpdf[lab], terr, terrpdfName)

    print(" -----------------------------------")
    print("[INFO] Fixed parameters in RooBDecay")
    print("------------------------------------")
    print("[INFO] Delta Gammas : ", myconfigfile["DeltaGammas"])
    print("[INFO] Gammas: ", myconfigfile["Gammas"])
    print("[INFO] Tau: ", myconfigfile["Tau"])
    print("[INFO] 1/Tau: ", 1.0 / myconfigfile["Tau"])
    print("[INFO] DeltaMs: ", myconfigfile["DeltaMs"])
    print("[INFO] cosh - 1.0")
    print("[INFO] D*sinh - ", myconfigfile["sinh"])
    print("[INFO] C*cos - ", myconfigfile["cos"])
    print("[INFO] S*sin - ", myconfigfile["sin"])
    print("------------------------------------")

    timePDF = RooBDecay(
        "Bdecay",
        "Bdecay",
        time,
        RooRealConstant.value(myconfigfile["Tau"]),
        RooRealConstant.value(myconfigfile["DeltaGammas"]),
        RooRealConstant.value(1.0),
        RooRealConstant.value(myconfigfile["sinh"]),
        RooRealConstant.value(myconfigfile["cos"]),
        RooRealConstant.value(myconfigfile["sin"]),
        RooRealConstant.value(myconfigfile["DeltaMs"]),
        trm[y[0]],
        RooBDecay.SingleSided,
    )

    noncondset = RooArgSet(time)

    name_timeterr = TString("time_signal")
    totPDF = RooProdPdf(
        name_timeterr.Data(),
        name_timeterr.Data(),
        RooArgSet(terrpdf[y[0]]),
        RooFit.Conditional(RooArgSet(timePDF), noncondset),
    )

    myfitresult = totPDF.fitTo(
        dataF_binned,
        RooFit.Save(1),
        RooFit.Optimize(2),
        RooFit.Strategy(2),
        RooFit.Verbose(False),
        RooFit.SumW2Error(True),
        RooFit.Extended(False),
    )
    # RooFit.Offset(True))

    myfitresult.Print("v")

    workout = RooWorkspace("workspace", "workspace")
    getattr(workout, "import")(dataF)
    getattr(workout, "import")(totPDF)
    getattr(workout, "import")(myfitresult)
    workout.writeToFile(wsname)

    if plot:
        range_dw = time.getMin()
        range_up = time.getMax()

        lhcbtext = TLatex()
        lhcbtext.SetTextFont(132)
        lhcbtext.SetTextColor(1)
        lhcbtext.SetTextSize(0.07)
        lhcbtext.SetTextAlign(12)

        legend = TLegend(0.62, 0.70, 0.88, 0.88)

        legend.SetTextSize(0.05)
        legend.SetTextFont(12)
        legend.SetFillColor(4000)
        legend.SetShadowColor(0)
        legend.SetBorderSize(0)
        legend.SetTextFont(132)

        l1 = TLine()
        l1.SetLineColor(kBlue + 3)
        l1.SetLineWidth(4)
        l1.SetLineStyle(kSolid)
        legend.AddEntry(l1, "decay", "L")

        l2 = TLine()
        l2.SetLineColor(kRed)
        l2.SetLineWidth(4)
        l2.SetLineStyle(kSolid)
        legend.AddEntry(l2, "acceptance", "L")

        frame_m = time.frame()
        frame_m.SetTitle("")

        frame_m.GetXaxis().SetLabelSize(0.06)
        frame_m.GetYaxis().SetLabelSize(0.06)
        frame_m.GetXaxis().SetLabelFont(132)
        frame_m.GetYaxis().SetLabelFont(132)
        frame_m.GetXaxis().SetLabelOffset(0.006)
        frame_m.GetYaxis().SetLabelOffset(0.006)
        frame_m.GetXaxis().SetLabelColor(kWhite)

        frame_m.GetXaxis().SetTitleSize(0.06)
        frame_m.GetYaxis().SetTitleSize(0.06)
        frame_m.GetYaxis().SetNdivisions(512)
        unit = "[ps]"
        frame_m.GetXaxis().SetTitleOffset(1.00)
        frame_m.GetYaxis().SetTitleOffset(1.00)
        frame_m.GetYaxis().SetTitle(
            (
                TString.Format(
                    "#font[132]{Candidates / ( "
                    + str(time.getBinWidth(1))
                    + " "
                    + unit
                    + ")}"
                )
            ).Data()
        )

        bin = 148
        dataF.plotOn(frame_m, RooFit.Binning(bin), RooFit.Name("dataSetCut"))
        totPDF.plotOn(frame_m, RooFit.LineColor(kBlue + 3), RooFit.Name("FullPdf"))

        spl[y[0]].plotOn(
            frame_m,
            RooFit.LineColor(kRed),
            RooFit.Normalization(frame_m.GetMaximum() * 0.25, RooAbsReal.Relative),
        )

        canvas = TCanvas("canvas", "canvas", 1200, 800)
        canvas.cd()
        canvas.SetLeftMargin(0.01)
        canvas.SetRightMargin(0.01)
        canvas.SetTopMargin(0.05)
        canvas.SetBottomMargin(0.05)
        pad1 = TPad("upperPad", "upperPad", 0.050, 0.22, 1.0, 1.0)
        pad1.SetBorderMode(0)
        pad1.SetBorderSize(-1)
        pad1.SetFillStyle(0)
        pad1.SetTickx(0)
        pad1.SetLeftMargin(0.115)
        pad1.SetRightMargin(0.05)
        pad1.Draw()
        pad1.cd()
        frame_m.GetYaxis().SetRangeUser(0.1, frame_m.GetMaximum() * 1.0)
        # frame_m.Draw()
        if log:
            gStyle.SetOptLogy(1)
            frame_m.GetYaxis().SetTitleOffset(1.10)
            frame_m.GetYaxis().SetRangeUser(1.5, frame_m.GetMaximum() * 1.5)
        frame_m.Draw()
        legend.Draw("same")
        lhcbtext.DrawTextNDC(0.50, 0.85, "LHCb")

        canvas.cd()
        pad2 = TPad("lowerPad", "lowerPad", 0.050, 0.005, 1.0, 0.3275)
        pad2.SetBorderMode(0)
        pad2.SetBorderSize(-1)
        pad2.SetFillStyle(0)
        pad2.SetBottomMargin(0.35)
        pad2.SetLeftMargin(0.115)
        pad2.SetRightMargin(0.05)
        pad2.SetTickx(0)
        pad2.Draw()
        pad2.SetLogy(0)
        pad2.cd()

        gStyle.SetOptLogy(0)
        frame_m.Print("v")
        frame_p = time.frame(RooFit.Title("pull_frame"))
        frame_p.Print("v")
        frame_p.SetTitle("")
        frame_p.GetYaxis().SetTitle("")
        frame_p.GetYaxis().SetTitleSize(0.09)
        frame_p.GetYaxis().SetTitleOffset(0.26)
        frame_p.GetYaxis().SetTitleFont(62)
        frame_p.GetYaxis().SetNdivisions(106)
        frame_p.GetYaxis().SetLabelSize(0.12)
        frame_p.GetYaxis().SetLabelOffset(0.006)
        frame_p.GetXaxis().SetTitleSize(0.15)
        frame_p.GetXaxis().SetTitleFont(132)
        frame_p.GetXaxis().SetTitleOffset(0.85)
        frame_p.GetXaxis().SetNdivisions(515)
        frame_p.GetYaxis().SetNdivisions(5)
        frame_p.GetXaxis().SetLabelSize(0.12)
        frame_p.GetXaxis().SetLabelFont(132)
        frame_p.GetYaxis().SetLabelFont(132)
        frame_p.GetXaxis().SetTitle("#font[132]{#tau(B_{s}) [ps]}")

        TString(time.GetName())
        pullnameTS = TString("FullPdf")
        pullname2TS = TString("dataSetCut")
        pullHist = frame_m.pullHist(pullname2TS.Data(), pullnameTS.Data())
        if args.bar:
            pullHist.SetLineColor(0)  # remove errorbar
            frame_p.addPlotable(pullHist, "B")
            pullHist.SetFillColor(1)
        else:
            frame_p.addPlotable(pullHist, "P")

        chi2 = frame_m.chiSquare()
        chi22 = frame_m.chiSquare(pullnameTS.Data(), pullname2TS.Data())

        print("chi2: %f" % (chi2))
        print("chi22: %f" % (chi22))

        axisX = pullHist.GetXaxis()
        Bin = RooBinning(range_dw, range_up, "P")
        Bin.addUniform(bin, range_dw, range_up)
        axisX.Set(Bin.numBins(), Bin.array())

        axisY = pullHist.GetYaxis()
        max = 5.0  # axisY.GetXmax()
        min = -5.0  # axisY.GetXmin()
        axisY.SetLabelSize(0.12)
        axisY.SetNdivisions(5)
        axisX.SetLabelSize(0.12)

        rangeX = max - min
        zero = old_div(max, rangeX)
        print("max: %s, min: %s, range: %s, zero:%s" % (max, min, rangeX, zero))

        graph = TGraph(2)
        graph.SetMaximum(max)
        graph.SetMinimum(min)
        graph.SetPoint(1, range_dw, 0)
        graph.SetPoint(2, range_up, 0)

        graph2 = TGraph(2)
        graph2.SetMaximum(max)
        graph2.SetMinimum(min)
        graph2.SetPoint(1, range_dw, -3)
        graph2.SetPoint(2, range_up, -3)
        graph2.SetLineColor(kRed)

        graph3 = TGraph(2)
        graph3.SetMaximum(max)
        graph3.SetMinimum(min)
        graph3.SetPoint(1, range_dw, 3)
        graph3.SetPoint(2, range_up, 3)
        graph3.SetLineColor(kRed)

        pullHist.GetXaxis().SetLabelFont(132)
        pullHist.GetYaxis().SetLabelFont(132)
        pullHist.SetTitle("")

        frame_p.GetYaxis().SetRangeUser(-5.0, 5.0)
        frame_p.Draw()

        graph.Draw("same")
        graph2.Draw("same")
        graph3.Draw("same")
        # tex.DrawLatex(0.50,0.30,"m(B_{s} #rightarrow D_{s}#pi) [MeV/c^{2}]")

        pad2.Update()
        canvas.Update()

        saveName = "Plot_Acceptance_" + str(decayTS) + "_" + "".join(year)

        if sufix != "":
            saveName = saveName + "_" + sufix
        canName = os.path.join(plot_prefix, saveName + ".pdf")
        canNamePng = os.path.join(plot_prefix, saveName + ".png")
        canNameRoot = os.path.join(plot_prefix, saveName + ".root")
        canNameC = os.path.join(plot_prefix, saveName + ".C")
        canNameEps = os.path.join(plot_prefix, saveName + ".eps")

        canvas.SaveAs(canName)
        canvas.SaveAs(canNamePng)
        canvas.SaveAs(canNameEps)
        canvas.SaveAs(canNameC)
        canvas.SaveAs(canNameRoot)


# ------------------------------------------------------------------------------

parser = argparse.ArgumentParser()

parser.add_argument(
    "-d",
    "--debug",
    action="store_true",
    dest="debug",
    default=False,
    help="print debug information while processing",
)

parser.add_argument(
    "-p",
    "--pol",
    "--polarity",
    dest="pol",
    nargs="+",
    default="down",
    help="Polarity can be: up, down, both ",
)

parser.add_argument(
    "-m",
    "--mode",
    dest="mode",
    nargs="+",
    default="kkpi",
    help="Mode can be: all, kkpi, kpipi, pipipi, nonres, kstk, phipi",
)

parser.add_argument(
    "--merge",
    nargs="+",
    dest="merge",
    default="",
    help="merge can be: pol, run1, run2, runs",
)

parser.add_argument(
    "--year",
    nargs="+",
    dest="year",
    default="",
    help="year of data taking can be: 2011, 2012, 2015, 2016, 2017, 2018",
)

parser.add_argument(
    "--checkWeights",
    dest="checkWeights",
    default=False,
    action="store_true",
    help="check weights for subsamples",
)

parser.add_argument(
    "--binned",
    dest="binned",
    default=False,
    action="store_true",
    help="binned data Set",
)

parser.add_argument(
    "--read",
    dest="read",
    action="store_true",
    default=False,
)

parser.add_argument(
    "--fileName",
    dest="fileIn",
    default="/afs/cern.ch/work/g/gligorov//public/Bs2DsKPlotsForPaper/NominalFit/",
    help="set observable ",
)

parser.add_argument("-s", "--save", dest="save", default="WS_Spline_Bs2DsPi.root")

parser.add_argument(
    "--workName", dest="work", default="workspace", help="set observable "
)
parser.add_argument(
    "--configName", dest="configName", default="Bs2DsPiConfigForNominalDMSFit"
)
parser.add_argument(
    "--logscale",
    "--log",
    dest="log",
    action="store_true",
    default=False,
    help="log scale of plot",
)
parser.add_argument(
    "--plot", dest="plot", action="store_true", default=False, help="plot fit result"
)

parser.add_argument("--rel", dest="rel", default=1000)
parser.add_argument(
    "--sufix", dest="sufix", metavar="SUFIX", default="", help="Add sufix to output"
)
parser.add_argument(
    "--plot-prefix",
    type=str,
    default="",
    help="""Prepend plot
                    paths with this prefix to adjust the output directory.
                    Defaults to an empty string""",
)
parser.add_argument(
    "--bar",
    dest="bar",
    action="store_true",
    default=False,
    help="Different style of pull histogram with black bars",
)

# -----------------------------------------------------------------------------

if __name__ == "__main__":
    try:
        args = parser.parse_args()
    except Exception:
        parser.print_help()
        sys.exit(-1)

    if args.pol[0] == "both":
        args.pol = ["up", "down"]

    if args.year[0] == "run1":
        args.year = ["2011", "2012"]
    if args.year[0] == "run2":
        args.year = ["2015", "2016", "2017", "2018"]
    if args.year[0] == "all":
        args.year = ["2011", "2012", "2015", "2016", "2017", "2018"]

    print(vars(args))

    config = args.configName
    last = config.rfind("/")
    directory = config[: last + 1]
    configName = config[last + 1 :]
    p = configName.rfind(".")
    configName = configName[:p]

    sys.path.append(directory)

    runFitSplineAcc(
        args.debug,
        configName,
        args.read,
        args.fileIn,
        args.save,
        args.work,
        args.pol,
        args.mode,
        args.merge,
        args.year,
        args.binned,
        args.rel,
        args.log,
        args.plot,
        args.sufix,
        args.checkWeights,
        args.plot_prefix,
        args.bar,
    )
# -----------------------------------------------------------------------------
