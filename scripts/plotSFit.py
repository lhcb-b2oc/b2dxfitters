#!/usr/bin/env python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# --------------------------------------------------------------------------- #
#                                                                             #
#   Python script to plot the Bd -> D pi time models                          #
#                                                                             #
#   Example usage:                                                            #
#      python plotBs2DsPiTimeModelsOnData.py WS_Time_DsPi.root                #
#                                                                             #
#   Author: Eduardo Rodrigues                                                 #
#   Date  : 01 / 06 / 2011                                                    #
#   Author: Agnieszka Dziurda                                                 #
#   Author: Vladimir Vava Gligorov                                            #
#                                                                             #
# --------------------------------------------------------------------------- #

# -----------------------------------------------------------------------------
# settings for running without GaudiPython
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Load necessary libraries
# -----------------------------------------------------------------------------
# "
from __future__ import division, print_function

import math
import os
import sys
import argparse
from past.utils import old_div
from ROOT import (
    RooAbsData,
    RooAbsReal,
    RooAddition,
    RooArgList,
    RooArgSet,
    RooBinning,
    RooConstVar,
    RooCubicSplineFun,
    RooDataSet,
    RooFit,
    RooRealVar,
    RooSimultaneous,
    SetOwnership,
    TCanvas,
    TFile,
    TGraph,
    TGraphErrors,
    TLegend,
    TLine,
    TPad,
    TString,
    gROOT,
    gStyle,
    kBlack,
    kBlue,
    kGreen,
    kOrange,
    kRed,
    kWhite,
)

from B2DXFitters import GeneralUtils, utils

# from ROOT import *


gROOT.SetBatch()
# gROOT.ProcessLine(".x ../root/.rootlogon.C")

# MODELS
signalModelOnly = True


# ------------------------------------------------------------------------------
def getDataCut(smy):
    c = []
    for s in smy:
        c.append("category==category::%s" % (s))

    cut = c[0]
    for i in range(1, c.__len__()):
        cut = cut + " || " + c[i]

    print("Total cut on data: %s" % (cut))

    return cut


# ------------------------------------------------------------------------------
def plotDataSet(dataset, frame, bin, sim, cat):
    if sim:
        datacut = getDataCut(cat)
        args = [
            frame,
            RooFit.Cut(datacut),
            RooFit.Binning(bin),
            RooFit.Name("dataSetCut"),
        ]
        if dataset.isWeighted():
            args.append(RooFit.DataError(RooAbsData.SumW2))
        dataset.plotOn(*args)
        dataout = dataset.reduce(datacut)
        dataout.Print("v")
        print(dataout.numEntries(), dataout.sumEntries())
        return dataout

    else:
        dataset.plotOn(
            frame,
            RooFit.Binning(bin),
            RooFit.DataError(RooAbsData.SumW2),
            RooFit.Name("dataSetCut"),
        )
        return dataset


# ------------------------------------------------------------------------------
def plotFitModel(
    model,
    frame,
    myconfigfile,
    logscale,
    decay,
    debug,
    namePDF,
    sim,
    category,
    cat,
    data,
    time,
    plot_acceptance=False,
):
    if debug:
        model.Print("t")
        frame.Print("v")

    t = "_"
    if "Acceptance" in list(myconfigfile.keys()):
        var = {}
        tacc_list = {}
        numKnots = myconfigfile["Acceptance"]["knots"].__len__()

        tMax = time.getMax()
        tMin = time.getMin()
        binName = TString("splineBinning")
        numKnots = myconfigfile["Acceptance"]["knots"].__len__()
        TimeBin = RooBinning(tMin, tMax, binName.Data())
        for i in range(0, numKnots):
            print(
                "[INFO]   knot %s in place %s "
                % (str(i), str(myconfigfile["Acceptance"]["knots"][i]))
            )
            TimeBin.addBoundary(myconfigfile["Acceptance"]["knots"][i])

        TimeBin.removeBoundary(tMin)
        TimeBin.removeBoundary(tMax)
        TimeBin.removeBoundary(tMin)
        TimeBin.removeBoundary(tMax)
        TimeBin.Print("v")
        time.setBinning(TimeBin, binName.Data())
        time.setRange(tMin, tMax)

        if sim:
            for c in cat:
                var[c] = []
                tacc_list[c] = RooArgList()
                for i in range(0, numKnots + 1):
                    varName = "var%d" % (int(i + 1))
                    vatmp = model.getParameters(data).find(varName)
                    if not vatmp:
                        varName = varName + t + c
                        vatmp = model.getParameters(data).find(varName)
                    var[c].append(vatmp)

                    print(
                        "[INFO] Load %s with value %0.3lf"
                        % (var[c][i].GetName(), var[c][i].getValV())
                    )
                    tacc_list[c].add(var[c][i])
                    SetOwnership(var[c][i], False)
                varAdd = model.getParameters(data).find("var%d" % (numKnots + 2))
                if not varAdd:
                    varAdd = model.getParameters(data).find(
                        "var%d" % (numKnots + 2) + t + c
                    )
                if not varAdd:  # coefficient needs to be recalculated
                    coeffList = GeneralUtils.GetCoeffFromBinning(TimeBin, time)
                    name = "var{}_{}".format(numKnots + 2, c)
                    varAdd = RooAddition(
                        name, name, RooArgList(var[c][-2], var[c][-1]), coeffList
                    )
                print(
                    "[INFO] Load %s with value %0.3lf"
                    % (varAdd.GetName(), varAdd.getValV())
                )
                tacc_list[c].add(varAdd)
                SetOwnership(varAdd, False)
        else:
            tacc_list["def"] = {}
            tacc_list["def"] = RooArgList()
            var["def"] = []

            # check if knot variables have cat slug
            if model.getParameters(data).find("var1"):
                knot_slug = ""
            else:
                knot_slug = "_" + cat[0]

            for i in range(0, numKnots + 1):
                varName = "var{}{}".format(i + 1, knot_slug)
                vatmp = model.getParameters(data).find(varName)
                if not vatmp:
                    print(
                        "Error while loading knot variable. "
                        "This is the workspace variable content:"
                    )
                    model.getParameters(data).Print("v")
                    raise Exception(
                        "Knot variables {} not present in workspace.".format((varName))
                    )
                var["def"].append(vatmp)

                print(
                    "[INFO] Load %s with value %0.3lf"
                    % (var["def"][i].GetName(), var["def"][i].getValV())
                )
                tacc_list["def"].add(var["def"][i])

            varAdd = RooAddition(
                model.getParameters(data).find(
                    "var{}{}".format(numKnots + 2, knot_slug)
                )
            )
            print(
                "[INFO] Load %s with value %0.3lf"
                % (varAdd.GetName(), varAdd.getValV())
            )

            tacc_list["def"].add(varAdd)

    elif "ResolutionAcceptance" in list(myconfigfile.keys()):
        # Create acceptance
        var = []
        tacc_list = RooArgList()
        numKnots = myconfigfile["ResolutionAcceptance"]["Signal"]["Acceptance"][
            "KnotPositions"
        ].__len__()
        print("[INFO] Number of knots: " + str(numKnots))
        if decay == "Bd2DPi":
            v = 9
        else:
            v = 6

        for i in range(0, numKnots + 1):
            if i != v:
                varName = "Acceptance_SplineAccCoeff%d" % (int(i))
                var.append(model.getParameters(data).find(varName))
                print(
                    "[INFO] Load %s with value %0.3lf"
                    % (var[i].GetName(), var[i].getValV())
                )
            else:
                var.append(RooConstVar("one", "one", 1.0))
                print("[INFO] Load one as coefficient no. %d" % (v))

            tacc_list.add(var[i])

        varName = "Acceptance_SplineAccCoeff%d" % (int(numKnots + 1))
        var.append(model.getParameters(data).find(varName))
        print(
            "[INFO] Load %s with value %0.3lf"
            % (var[numKnots + 1].GetName(), var[numKnots + 1].getValV())
        )
        tacc_list.add(var[numKnots + 1])

        # Create binning
        binning = RooBinning(time.getMin(), time.getMax(), "splineBinning")
        for kn in myconfigfile["ResolutionAcceptance"]["Signal"]["Acceptance"][
            "KnotPositions"
        ]:
            binning.addBoundary(kn)
        binning.removeBoundary(time.getMin())
        binning.removeBoundary(time.getMax())
        binning.removeBoundary(time.getMin())
        binning.removeBoundary(time.getMax())
        oldBinning, lo, hi = time.getBinning(), time.getMin(), time.getMax()
        time.setBinning(binning, "splineBinning")
        time.setBinning(oldBinning)
        time.setRange(lo, hi)

    spl = {}
    if sim:
        for c in cat:
            print((tacc_list, tacc_list[c]))
            tacc_list[c].Print("v")
            spl[c] = {}
            spl[c] = RooCubicSplineFun(
                "splinePdf" + str(c),
                "splinePdf" + str(c),
                time,
                "splineBinning",
                tacc_list[c],
            )
    else:
        spl = RooCubicSplineFun(
            "splinePdf", "splinePdf", time, "splineBinning", tacc_list["def"]
        )

    if logscale:
        # from math import log
        rel = math.log(frame.GetMaximum()) * 4.0
    else:
        rel = frame.GetMaximum() / 4.0

    if sim:
        components = namePDF[cat[0]]
        comp = cat[0]
        for c in cat:
            if c != cat[0]:
                components = components + "," + namePDF[c]
                comp = comp + "," + c

        color = {
            "2015": kRed,
            "2016": kBlue,
            "20152016": kRed,
            "2017": kGreen + 3,
            "2018": kOrange,
        }
        if plot_acceptance:
            for i, c in enumerate(cat):
                fr = spl[c].plotOn(
                    frame,
                    RooFit.LineColor(color.get(c, kBlue)),
                    RooFit.Normalization(rel, RooAbsReal.Relative),
                    RooFit.Name("sPline" + c),
                )

        sampleSet = RooArgSet(category)
        if len(cat) == 1:
            fr = model.plotOn(
                frame,
                RooFit.ProjWData(sampleSet, data),
                RooFit.Slice(category, comp),
                RooFit.LineColor(kBlue + 3),
                RooFit.Name("FullPdf"),
            )
        else:
            fr = model.plotOn(
                frame,
                RooFit.ProjWData(sampleSet, data),
                RooFit.LineColor(kBlue + 3),
                RooFit.Name("FullPdf"),
            )

    else:
        if plot_acceptance:
            fr = spl.plotOn(
                frame,
                RooFit.LineColor(kRed),
                RooFit.Normalization(rel, RooAbsReal.Relative),
                RooFit.Name("sPline"),
            )
        fr = model.plotOn(frame, RooFit.LineColor(kBlue + 3), RooFit.Name("FullPdf"))

    return locals()


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------


def getDescription(decay):
    happystar = (
        "#lower[-0.95]{#scale[0.5]{(}}#lower[-0.8]"
        "{#scale[0.5]{*}}#lower[-0.95]{#scale[0.5]{)}}"
    )
    happypm = "#lower[-0.95]{#scale[0.6]{#pm}}"
    happymp = "#lower[-0.95]{#scale[0.6]{#mp}}"
    happyplus = "#lower[-0.95]{#scale[0.6]{+}}"
    happymin = "#lower[-1.15]{#scale[0.7]{-}}"
    happy0 = "#lower[-0.85]{#scale[0.6]{0}}"

    if decay == "Bs2DsPi":
        desc = (
            "B_{s}#kern[-0.7]{"
            + happy0
            + "}#rightarrow D_{s}#kern[-0.3]{"
            + happymin
            + "}#kern[0.1]{#pi"
            + happyplus
            + "}"
        )
    elif decay == "Bs2DsK":
        desc = (
            "B_{s}#kern[-0.7]{"
            + happy0
            + "} #rightarrow D_{s}#kern[-0.3]{"
            + happymp
            + "}#kern[0.1]{K"
            + happypm
            + "}"
        )
    elif decay == "Bs2DsstPi":
        desc = (
            "B_{s}#kern[-0.7]{"
            + happy0
            + "}#rightarrow D_{s}#kern[-0.3]{"
            + happystar
            + happymin
            + "}#kern[0.1]{#pi"
            + happyplus
            + "}"
        )
    elif decay == "Bs2DsstK":
        desc = (
            "B_{s}#kern[-0.7]{"
            + happy0
            + "} #rightarrow D_{s}#kern[-0.3]{"
            + happystar
            + happymp
            + "}#kern[0.1]{K"
            + happypm
            + "}"
        )
    elif decay == "Bd2DPi":
        desc = (
            "B_{d}#kern[-0.7]{"
            + happy0
            + "} #rightarrow D#kern[-0.3]{"
            + happymp
            + "}#kern[0.1]{#pi"
            + happypm
            + "}"
        )
    else:
        desc = "Signal"

    return desc


# ------------------------------------------------------------------------------
def legends(model, frame):
    stat = frame.findObject("data_statBox")
    prefix = "Sig" if signalModelOnly else "Tot"
    if not stat:
        stat = frame.findObject("%sEPDF_tData_statBox" % prefix)
    if stat:
        stat.SetTextSize(0.025)
    pt = frame.findObject("%sEPDF_t_paramBox" % prefix)
    if pt:
        pt.SetTextSize(0.02)
    # Legend of EPDF components
    leg = TLegend(0.56, 0.42, 0.87, 0.62)
    leg.SetFillColor(0)
    leg.SetTextSize(0.02)
    comps = model.getComponents()

    if signalModelOnly:
        pdfName = "SigEPDF_t"
        pdf = comps.find(pdfName)
        curve = frame.findObject(pdfName + "_Norm[time]")
        if curve:
            leg.AddEntry(curve, pdf.GetTitle(), "l")
        return leg, curve
    else:
        pdf1 = comps.find("SigEPDF_t")
        pdfName = "TotEPDF_t_Norm[time]_Comp[%s]" % "SigEPDF_t"
        curve1 = frame.findObject(pdfName)
        if curve1:
            leg.AddEntry(curve1, pdf1.GetTitle(), "l")
        pdf = comps.find("Bd2DKEPDF_t")
        pdfName = "TotEPDF_t_Norm[time]_Comp[%s]" % "Bd2DKEPDF_t"
        curve2 = frame.findObject(pdfName)
        if curve2:
            leg.AddEntry(curve2, pdf.GetTitle(), "l")
        pdf = comps.find("CombBkgEPDF_t")
        pdfName = "TotEPDF_t_Norm[time]_Comp[%s]" % "CombBkgEPDF_t"
        curve3 = frame.findObject(pdfName)
        if curve3:
            leg.AddEntry(curve3, pdf.GetTitle(), "l")
        pdfName = "TotEPDF_t_Norm[time]_Comp[%s]" % "CombBkgEPDF_t,Bd2DKEPDF_t"
        curve4 = frame.findObject(pdfName)
        if curve4:
            leg.AddEntry(curve4, "All but %s" % pdf1.GetTitle(), "f")
            curve4.SetLineColor(0)
        pdfName = "TotEPDF_t_Norm[time]"
        pdf = comps.find("TotEPDF_t")
        curve5 = frame.findObject(pdfName)
        # if curve5 : leg.AddEntry(curve5, pdf.GetTitle(), 'l')
        if curve5:
            leg.AddEntry(curve5, "Model (signal & background) EPDF", "l")
        return leg, curve4


def prepareFrames(timeObs, decay="Bs2DsPi", entries=-1, log=False):
    frame_t = timeObs.frame()
    frame_t.SetTitle("")

    frame_t.GetXaxis().SetLabelFont(132)
    frame_t.GetYaxis().SetLabelFont(132)
    frame_t.GetXaxis().SetLabelOffset(0.006)
    frame_t.GetYaxis().SetLabelOffset(0.006)
    frame_t.GetXaxis().SetLabelColor(kWhite)

    frame_t.GetXaxis().SetTitleSize(0.06)
    frame_t.GetYaxis().SetTitleSize(0.06)
    frame_t.GetYaxis().SetNdivisions(512)

    if decay != "Bd2DPi":
        frame_t.GetXaxis().SetTitleOffset(1.00)
        frame_t.GetYaxis().SetTitleOffset(1.00)
        frame_t.GetXaxis().SetLabelSize(0.06)
        frame_t.GetYaxis().SetLabelSize(0.06)
    else:
        frame_t.GetXaxis().SetTitleOffset(1.25)
        frame_t.GetYaxis().SetTitleOffset(1.2)
        frame_t.GetXaxis().SetLabelSize(0.03)
        frame_t.GetYaxis().SetLabelSize(0.05)

    unit = "ps"
    frame_t.GetYaxis().SetTitle(
        "#font[132]{{Candidates / ({:0.2f} {})}}".format(timeObs.getBinWidth(1), unit)
    )

    if not log and entries > 10000 or decay == "Bd2DPi":
        frame_t.GetYaxis().SetNdivisions(508)
        frame_t.GetYaxis().SetTitleOffset(1.35)
        if decay == "Bd2DPi":
            frame_t.GetYaxis().SetTitleOffset(1.2)
        frame_t.GetYaxis().SetRangeUser(1.5, frame_t.GetMaximum() * 1.1)

    return frame_t


def tuneAndDraw(
    frame_t,
    frame_p,
    canvas,
    range,
    config,
    pulls=True,
    log=False,
    plotLabel="",
    draw_legend=True,
    data_name="Data",
    plot_acceptance=False,
):
    if log:
        gStyle.SetOptLogy(1)
        frame_t.GetYaxis().SetTitleOffset(1.10)
        frame_t.GetYaxis().SetRangeUser(1.5, frame_t.GetMaximum() * 1.5)

    if config["Decay"] == "Bd2DPi":
        legend = TLegend(0.52, 0.65, 0.75, 0.93)
    else:
        legend = TLegend(0.60, 0.65, 0.80, 0.93)

    legend.SetTextSize(0.06)
    legend.SetTextFont(12)
    legend.SetFillColor(0)
    legend.SetShadowColor(0)
    legend.SetBorderSize(0)
    legend.SetTextFont(132)
    legend.SetHeader(plotLabel)

    gr = TGraphErrors(1)
    gr.SetName("gr")
    gr.SetLineColor(kBlack)
    gr.SetLineWidth(2)
    gr.SetMarkerStyle(20)
    gr.SetMarkerSize(1.3)
    gr.SetMarkerColor(kBlack)
    # gr.Draw("P");
    legend.AddEntry(gr, data_name, "lep")

    descTS = TString(getDescription(config["Decay"]))
    l1 = TLine()
    l1.SetLineColor(kBlue + 3)
    l1.SetLineWidth(4)
    legend.AddEntry(l1, descTS.Data(), "L")
    if plot_acceptance:
        l2 = TLine()
        l2.SetLineColor(kRed)
        l2.SetLineWidth(4)
        legend.AddEntry(l2, "Acceptance", "L")

    pad1 = TPad("upperPad", "upperPad", 0.005, 0.05, 1.0, 1.0)
    pad1.SetBorderMode(0)
    pad1.SetBorderSize(-1)
    pad1.SetFillStyle(0)
    pad1.SetTickx(0)
    pad1.SetBottomMargin(0.30)
    pad1.SetLeftMargin(0.17)
    pad1.SetTopMargin(0.05)
    pad1.SetRightMargin(0.05)
    pad1.Draw()
    pad1.cd()

    frame_t.Draw()
    if draw_legend:
        legend.Draw("same")

    pad1.Update()

    canvas.cd()
    pad2 = TPad("lowerPad", "lowerPad", 0.005, 0.005, 1.0, 0.37)
    pad2.SetBorderMode(0)
    pad2.SetBorderSize(-1)
    pad2.SetFillStyle(0)
    pad2.SetBottomMargin(0.35)
    pad2.SetBottomMargin(0.40)
    pad2.SetLeftMargin(0.17)
    pad2.SetRightMargin(0.05)

    pad2.SetTickx(0)
    pad2.Draw()
    pad2.SetLogy(0)
    pad2.cd()

    gStyle.SetOptLogy(0)

    frame_p.Print("v")
    frame_p.SetTitle("")
    frame_p.GetYaxis().SetTitle("")
    frame_p.GetYaxis().SetTitleSize(0.08)
    frame_p.GetYaxis().SetTitleOffset(0.26)
    frame_p.GetYaxis().SetTitleFont(132)
    frame_p.GetYaxis().SetNdivisions(106)
    if config["Decay"] != "Bd2DPi":
        frame_p.GetYaxis().SetLabelSize(0.18)
        frame_p.GetXaxis().SetLabelSize(0.18)
    else:
        frame_p.GetYaxis().SetLabelSize(0.12)
        frame_p.GetXaxis().SetLabelSize(0.12)

    frame_p.GetYaxis().SetLabelOffset(0.006)
    frame_p.GetXaxis().SetTitleSize(0.20)
    frame_p.GetXaxis().SetTitleFont(132)
    frame_p.GetXaxis().SetTitleOffset(0.85)
    frame_p.GetXaxis().SetNdivisions(5)
    frame_p.GetYaxis().SetNdivisions(5)
    frame_p.GetXaxis().SetLabelFont(132)
    frame_p.GetYaxis().SetLabelFont(132)

    frame_p.GetYaxis().SetRangeUser(-3.0, 3.0)
    frame_p.GetXaxis().SetTitle("#font[132]{#tau(" + descTS.Data() + ") [ps]}")

    frame_p.Draw()

    timeDown, timeUp = range

    if pulls:
        pullHist = frame_t.pullHist()
        pullHist.SetName("pullHist")
        pullHist.setYAxisLimits(-3.0, 3.0)

        frame_p.addPlotable(pullHist, "P")

        axisX = pullHist.GetXaxis()
        axisY = pullHist.GetYaxis()

        axisX.Set(100, timeDown, timeUp)
        axisX.SetTitle("#font[132]{#tau(" + descTS.Data() + ") [ps]}")
        axisX.SetTitleSize(0.150)
        axisX.SetTitleFont(132)
        if decay == "Bd2DPi":
            axisX.SetLabelSize(0.130)
        else:
            axisX.SetLabelSize(0.150)
        axisX.SetLabelFont(132)
        maxX = axisX.GetXmax()
        minX = axisX.GetXmin()

        max = axisY.GetXmax()
        min = axisY.GetXmin()
        axisY.SetLabelSize(0.150)
        axisY.SetLabelFont(132)
        axisY.SetNdivisions(5)

        max = 5.0
        min = -5.0
        axisY.SetRangeUser(min, max)
        axisX.SetRangeUser(timeDown, timeUp)

        range = max - min
        zero = old_div(max, range)
        print("max: %s, min: %s, range: %s, zero:%s" % (max, min, range, zero))
        print("maxX: %s, minX: %s" % (maxX, minX))
        print("time range: (%lf, %lf)" % (timeDown, timeUp))

        graph = TGraph(2)
        graph.SetMaximum(max)
        graph.SetMinimum(min)
        graph.SetName("graph1")
        graph.SetTitle("")
        graph.SetPoint(1, timeDown, 0)
        graph.SetPoint(2, timeUp, 0)

        graph2 = TGraph(2)
        graph2.SetMaximum(max)
        graph2.SetMinimum(min)
        graph2.SetName("graph2")
        graph2.SetTitle("")
        graph2.SetPoint(1, timeDown, -3)
        graph2.SetPoint(2, timeUp, -3)
        graph2.SetLineColor(kRed)

        graph3 = TGraph(2)
        graph3.SetMaximum(max)
        graph3.SetMinimum(min)
        graph3.SetName("graph3")
        graph3.SetTitle("")
        graph3.SetPoint(1, timeDown, 3)
        graph3.SetPoint(2, timeUp, 3)
        graph3.SetLineColor(kRed)

        pullHist.SetTitle("")
        pullHist.Draw("AP")
        pullHist.GetYaxis().SetRangeUser(min, max)
        graph.Draw("SAME")
        graph2.Draw("SAME")
        graph3.Draw("SAME")

    pad2.Update()
    canvas.Update()

    if pulls:
        chi2 = frame_t.chiSquare()
        chi22 = frame_t.chiSquare(1)
        print("chi2: %f" % (chi2))
        print("chi22: %f" % (chi22))

    return frame_t, frame_p, canvas, locals()


parser = argparse.ArgumentParser()

parser.add_argument("FILENAME", help="Input Filename")

parser.add_argument(
    "-w",
    "--workspace",
    dest="wsname",
    metavar="WSNAME",
    default="workspace",
    help="RooWorkspace name as stored in ROOT file",
)

parser.add_argument(
    "-s",
    "--suffix",
    dest="sufix",
    metavar="SUFIX",
    default="",
    help="Add sufix to output",
)

parser.add_argument(
    "--plotLabel",
    dest="plotLabel",
    metavar="PLOTLABEL",
    default="LHCb",
    help="Plot label",
)

parser.add_argument(
    "--configName", dest="configName", default="Bs2DsPiConfigForNominalDMSFit"
)

parser.add_argument("--bin", dest="bin", default=148, help="set number of bins")

parser.add_argument(
    "--logscale",
    "--log",
    dest="log",
    action="store_true",
    default=False,
    help="log scale of plot",
)
parser.add_argument(
    "--legend",
    dest="legend",
    action="store_true",
    default=False,
    help="plot legend on the plot",
)

parser.add_argument(
    "-v",
    "--variable",
    "--var",
    dest="var",
    default="BeautyTime",
    help="set observable ",
)

parser.add_argument(
    "--outdir", dest="outdir", default="", help="directory to save plots"
)

parser.add_argument(
    "--dataSetToPlot",
    dest="dataSetToPlot",
    default="combData",
    help="name of RooDataSet to plot",
)

parser.add_argument(
    "--pdfToPlot",
    dest="pdfToPlot",
    default="time_signaldefault",
    help="name of pdf to plot",
)

parser.add_argument("--debug", dest="debug", default=False, help="verbose output")

parser.add_argument(
    "--sim",
    "--simfit",
    dest="sim",
    action="store_true",
    default=False,
    help="simultaneous fit",
)

parser.add_argument(
    "--cat",
    dest="cat",
    nargs="+",
    default="",
    help="category to plot, for example 20152016 2017",
)
parser.add_argument("--acceptance", default=False, action="store_true")
parser.add_argument("--data-name", default="Data")

if __name__ == "__main__":
    args = parser.parse_args()

    if not os.path.exists(args.FILENAME):
        parser.error('ROOT file "%s" not found! Nothing plotted.' % args.FILENAME)
        parser.print_help()

    print("==========================================================")
    print("USING  THE FOLLOWING CONFIGURATION args")
    print(vars(args))
    print("==========================================================")

    RooAbsReal.defaultIntegratorConfig().setEpsAbs(1e-13)
    RooAbsReal.defaultIntegratorConfig().setEpsRel(1e-13)
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooAdaptiveGaussKronrodIntegrator1D"
    ).setCatLabel("method", "21Points")
    RooAbsReal.defaultIntegratorConfig().getConfigSection(
        "RooAdaptiveGaussKronrodIntegrator1D"
    ).setRealValue("maxSeg", 1000)
    # since we have finite ranges, the RooIntegrator1D is best suited to the job
    RooAbsReal.defaultIntegratorConfig().method1D().setLabel(
        "RooAdaptiveGaussKronrodIntegrator1D"
    )

    gROOT.SetStyle("Plain")
    # gROOT.SetBatch(False)

    bin = int(args.bin)
    log = args.log
    leg = args.legend
    plotLabel = args.plotLabel
    v = args.var
    varTS = TString(v)

    config = args.configName
    last = config.rfind("/")
    directory = config[: last + 1]
    configName = config[last + 1 :]
    p = configName.rfind(".")
    configName = configName[:p]

    sys.path.append(directory)

    myconfigfilegrabber = __import__(configName, fromlist=["getconfig"]).getconfig
    myconfigfile = myconfigfilegrabber()

    print("==========================================================")
    print("PREPARING WORKSPACE IS RUNNING WITH THE FOLLOWING CONFIGURATION args")
    for option in myconfigfile:
        if option == "constParams":
            for param in myconfigfile[option]:
                print(param, "is constant in the fit")
        else:
            print(option, " = ", myconfigfile[option])
    print("==========================================================")

    decay = myconfigfile["Decay"]
    decayTS = TString(decay)

    sfx = args.sufix
    outputdir = args.outdir

    sufixTS = TString(sfx)
    filename = args.FILENAME

    f = TFile(filename)

    w = f.Get(args.wsname)

    if not w:
        parser.error(
            'Workspace "%s" not found in file "%s"! Nothing plotted.'
            % (args.wsname, filename)
        )
    else:
        print("Workspace content:")
        w.Print("v")

    f.Close()
    time = w.var(varTS.Data())
    time.setBins(bin)
    # time.setRange(0.4,15)

    cat = args.cat
    category = w.obj("category")

    # time.setRange(timeDown,timeUp)
    if args.sim:
        print(category.Print("v"))
        namePDF = {}
        modelPDF = RooSimultaneous("time_signal", "time_signal", category)
        for c in ["2015", "2016", "20152016", "2017", "2018"]:
            namePDF[c] = {}
            namePDF[c] = "time_signal" + c
            tmpPDF = w.obj(namePDF[c])
            if not tmpPDF:
                continue
            modelPDF.addPdf(tmpPDF, c)
    else:
        modelPDF = w.obj(args.pdfToPlot)
        namePDF = modelPDF.GetName()

    if modelPDF:
        print(modelPDF.GetName())

    # @NOTE Since serialization of rooworkspace is not robust if using
    # RooTreeDataStore backend, the underlying TTree is stored and needs to be
    # re-inserted into a RooDataSet
    treeFile = TFile(filename.replace(".root", "_tree.root"))
    tree = treeFile.Get("combData")
    datasetArgs = RooArgSet(time)
    datasetArgs.add(w.allCats())
    # Need to workaround ROOT 6.20 changes... again
    utils.addRooCategoryIdxSlug(datasetArgs)
    importArgs = [RooFit.Import(tree)]
    if tree.FindBranch("sWeights_"):
        sWeightVar = RooRealVar("sWeights_", "sWeights_", 1)
        datasetArgs.add(sWeightVar)
        importArgs.append(RooFit.WeightVar("sWeights_"))
    # use the pre-filled categories as input vars, to automatically create the
    # RooDataSet categories for plotting
    dataset = RooDataSet("tmp", "tmp", datasetArgs, *importArgs)

    # rename after workaround
    utils.removeRooCategoryIdxSlug(datasetArgs)
    utils.removeRooCategoryIdxSlug(dataset.get())

    additionalCuts = myconfigfile.get("AdditionalCuts", {}).get("All", {}).get("Data")
    if additionalCuts is not None:
        print("[INFO] applying additional data cut:\n\t{}".format(additionalCuts))
        before = dataset.numEntries()
        dataset = dataset.reduce(additionalCuts)
        after = dataset.numEntries()
        print("[INFO] {} -> {} entries".format(before, after))
    else:
        print("[INFO] No additional cuts applied")

    # dataset  = w.data(args.dataSetToPlot)
    # if not dataset:
    #     dataset = w.data("dataSet_time_weighted_default")
    # if dataset:
    #     print dataset.GetName()

    if not (modelPDF and dataset):
        w.Print("v")
        exit(1)

    canvas = TCanvas("canvas", "canvas", 1200, 1000)
    canvas.cd()

    time = w.var(varTS.Data())

    frame_t = prepareFrames(
        time, decay=myconfigfile["Decay"], entries=dataset.numEntries(), log=log
    )

    datacut = plotDataSet(dataset, frame_t, bin, args.sim, args.cat)

    print("##### modelPDF is")
    print(modelPDF)
    others = plotFitModel(
        modelPDF,
        frame_t,
        myconfigfile,
        log,
        decay,
        args.debug,
        namePDF,
        args.sim,
        category,
        cat,
        dataset,
        time,
        args.acceptance,
    )

    frame_t = others["frame"]
    time = others["time"]
    myconfigfile = others["myconfigfile"]

    frame_t, frame_p, canvas, o = tuneAndDraw(
        frame_t,
        time.frame(),
        canvas,
        range=(time.getMin(), time.getMax()),
        config=myconfigfile,
        plot_acceptance=args.acceptance,
        plotLabel=plotLabel,
        data_name=args.data_name,
    )

    others.update(o)

    if args.sim:
        print("-----------------------------------------------")
        print("[WARNING] Simfit plotting is a work in progress")
        print("-----------------------------------------------")

    sufixTS = TString(sfx)
    if sufixTS != "":
        sufixTS = TString("_") + sufixTS

    head = TString("time_")
    if log:
        head = head + TString("log_")

    # for some reason, if I dont import os here again, os is not defined...
    import os

    exten = [".pdf", ".png", ".root", ".C", ".eps"]
    for ext in exten:
        nameSave = head + decayTS + sufixTS + TString(ext)
        canvas.Print(os.path.join(outputdir, nameSave.Data()))
