#!/usr/bin/env python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# --------------------------------------------------------------------------- #
#                                                                             #
#   Python script to plot MDFitter results                                    #
#                                                                             #
#   Example usage:                                                            #
#      python -i plotBs2DsKMassModels.py                                      #
#                                                                             #
#   Author: Agnieszka Dziurda                                                 #
#   Date  : 21 / 06 / 2015                                                    #
#                                                                             #
# --------------------------------------------------------------------------- #

# -----------------------------------------------------------------------------
# settings for running without GaudiPython
# -----------------------------------------------------------------------------
""":"
# This part is run by the shell. It does some setup which is convenient to save
# work in common use cases.

# make sure the environment is set up properly
if test -n "$CMTCONFIG" \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersDict.so \
         -a -f $B2DXFITTERSROOT/$CMTCONFIG/libB2DXFittersLib.so; then
    # all ok, software environment set up correctly, so don't need to do
    # anything
    true
else
    if test -n "$CMTCONFIG"; then
        # clean up incomplete LHCb software environment so we can run
        # standalone
        echo Cleaning up incomplete LHCb software environment.
        PYTHONPATH=`echo $PYTHONPATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
        export PYTHONPATH
        LD_LIBRARY_PATH=`echo $LD_LIBRARY_PATH | tr ':' '\n' | \
            egrep -v "^($User_release_area|$MYSITEROOT/lhcb)" | \
            tr '\n' ':' | sed -e 's/:$//'`
	export LD_LIBRARY_PATH
        exec env -u CMTCONFIG -u B2DXFITTERSROOT "$0" "$@"
    fi
    # automatic set up in standalone build mode
    if test -z "$B2DXFITTERSROOT"; then
        cwd="$(pwd)"
        if test -z "$(dirname $0)"; then
            # have to guess location of setup.sh
            cd ../standalone
            . ./setup.sh
            cd "$cwd"
        else
            # know where to look for setup.sh
            cd "$(dirname $0)"/../standalone
            . ./setup.sh
            cd "$cwd"
        fi
        unset cwd
    fi
fi
# figure out which custom allocators are available
# prefer jemalloc over tcmalloc
for i in libjemalloc libtcmalloc; do
    for j in `echo "$LD_LIBRARY_PATH" | tr ':' ' '` \
            /usr/local/lib /usr/lib /lib; do
        for k in `find "$j" -name "$i"'*.so.?' | sort -r`; do
            if test \! -e "$k"; then
                continue
            fi
            echo adding $k to LD_PRELOAD
            if test -z "$LD_PRELOAD"; then
                export LD_PRELOAD="$k"
                break 3
            else
                export LD_PRELOAD="$LD_PRELOAD":"$k"
                break 3
            fi
        done
    done
done
# set batch scheduling (if schedtool is available)
schedtool="`which schedtool 2>/dev/zero`"
if test -n "$schedtool" -a -x "$schedtool"; then
    echo "enabling batch scheduling for this job (schedtool -B)"
    schedtool="$schedtool -B -e"
else
    schedtool=""
fi

# set ulimit to protect against bugs which crash the machine: 2G vmem max,
# no more then 8M stack
ulimit -v $((2048 * 1024))
ulimit -s $((   8 * 1024))

# trampoline into python
exec $schedtool /usr/bin/time -v env python -O -- "$0" "$@"
"""
# -----------------------------------------------------------------------------
# Load necessary libraries
# -----------------------------------------------------------------------------
from __future__ import division, print_function

__doc__ = """ real docstring """
import importlib
import os
import sys
import argparse
import ROOT
from os.path import exists
from ROOT import (
    TH1F,
    Form,
    RooAbsReal,
    RooBinning,
    RooFit,
    TCanvas,
    TColor,
    TFile,
    TLatex,
    TLegend,
    TPad,
    TString,
    gROOT,
    gStyle,
    kSolid,
    kWhite,
)
from B2DXFitters import GeneralUtils
from B2DXFitters.WS import WS as WS

ROOT.PyConfig.IgnoreCommandLineOptions = True  # noqa
gROOT.SetBatch()
gROOT.ProcessLine(".x ../root/.rootlogon.C")

# -----------------------------------------------------------------------------
# Configuration settings
# -----------------------------------------------------------------------------

# PLOTTING CONFIGURATION
plotData = True
plotModel = True

# MISCELLANEOUS
bName = "B_{s}"

bin = 120
# ------------------------------------------------------------------------------

parser = argparse.ArgumentParser()

parser.add_argument(
    "FILENAME",
    help="Input Filename",
    type=str,
)
parser.add_argument(
    "--fileData", help="""Re-read dataset from data workspace""", default=None
)
parser.add_argument("--data-workspace-name", default="workspace")
parser.add_argument(
    "--mdfit-tuple",
    help="""MDFit TTree output tuple. Provide this if the
                    plotting script crashes while reading the RooDataSet from
                    the MDFit workspace.""",
    type=str,
)
parser.add_argument(
    "--mdfit-tuple-treename",
    help="""Treename of mdfit tuple.""",
    type=str,
    default="data_sweight",
)
parser.add_argument(
    "-w",
    "--workspace",
    dest="wsname",
    metavar="WSNAME",
    default="FitMeToolWS",
    help="RooWorkspace name as stored in ROOT file",
)

parser.add_argument(
    "-p",
    "--pol",
    "--polarity",
    dest="pol",
    nargs="+",
    default="down",
    help="Sample: choose up, down or both",
)

parser.add_argument(
    "-m",
    "--mode",
    dest="modeDs",
    nargs="+",
    default="kkpi",
    help="Mode: choose all, nonres, kstk, kkpi, kpipi or pipipi",
)

parser.add_argument(
    "--year",
    dest="year",
    nargs="+",
    default="",
    help="year of data taking can be: 2011, 2012, run1",
)

parser.add_argument(
    "-t",
    "--toy",
    dest="toy",
    #                   metavar = 'TOY',
    action="store_true",
    default=False,
    help="if ToyMC choose yes.",
)

parser.add_argument(
    "-v",
    "--variable",
    "--var",
    dest="var",
    default="BeautyMass",
    help="set observable ",
)

parser.add_argument(
    "-s", "--suffix", metavar="SUFIX", default="", help="Add sufix to output"
)
parser.add_argument(
    "--merge",
    dest="merge",
    default="",
    nargs="+",
    help="for merging magnet polarities use: --merge pol, "
    "for merging years of data taking use: --merge year, "
    "for merging both use: --merge both",
)
parser.add_argument(
    "--logscale",
    "--log",
    dest="log",
    action="store_true",
    default=False,
    help="log scale of plot",
)
parser.add_argument(
    "--bin", "--bins", dest="bin", default=100, help="set number of bins"
)

parser.add_argument("--min", dest="ymin", default=0.1, help="minimal value on y axis")

parser.add_argument(
    "--legend",
    dest="legend",
    action="store_true",
    default=False,
    help="plot legend on the plot",
)

parser.add_argument("--dim", dest="dim", default=1)

parser.add_argument(
    "-d",
    "--debug",
    action="store_true",
    dest="debug",
    default=False,
    help="print debug information while processing",
)

parser.add_argument(
    "--bar",
    dest="bar",
    action="store_true",
    default=False,
    help="Different style of pull histogram with black bars",
)

parser.add_argument(
    "--configName", dest="configName", default="Bs2DsstKConfigForNominalMassFit"
)
parser.add_argument(
    "--plot-prefix",
    type=str,
    default="",
    help="""Prepend plot
                    paths with this prefix to adjust the output directory.
                    Defaults to an empty string""",
)


def getTotPDF(w, smy, comp, debug):
    c = []
    n = []
    print("[INFO] Get Total PDF: ")
    #    hypo = TString("")
    #    smy = GeneralUtils.GetSampleModeYearHypo(TString(sam),
    #           TString(mod), TString(year), hypo, merge, debug )
    for p in comp:
        for s in smy:
            s = "%s" % (s)
            var = w.var("n%s_%s_Evts" % (p, s))
            if var:
                if p == "Sig" or p == "CombBkg":
                    c.append("n%s_%s_Evts*%sEPDF_%s" % (comp[0], s, p, s))
                else:
                    c.append("n%s_%s_Evts*%sEPDF_m_%s" % (comp[0], s, p, s))
                n.append("n%s_%s_Evts" % (p, s))
                print("...........n%s_%s_Evts" % (p, s))
            else:
                c.append("")
                n.append("")

    print(c, end=" ")
    print(n, end=" ")
    pdfcomp = c[0]
    # if n.__len__() < 20 or merge == True:
    for i in range(1, c.__len__()):
        pdfcomp = pdfcomp + "," + c[i]
    if debug:
        print("Total PDF to print: %s" % (pdfcomp))
    w.factory("SUM:FullPdf(%s)" % (pdfcomp))

    totName = TString("FullPdf")
    modelPDF = w.pdf(totName.Data())

    return modelPDF


# ------------------------------------------------------------------------------
def plotFitModel(modelPDF, frame, var, smy, decay, comp, color):
    # if debug :

    c = []
    for p in comp:
        for s in smy:
            if p == "Sig" or p == "CombBkg":
                continue
                # c.append("%sEPDF_%s" % (p, s))
            elif (p == "Lb2DsDsstP" or p == "Bs2DsDsstPiRho") and decay == "Bs2DsK":
                c.append("PhysBkg%sPdf_m_%s_Tot" % (p, s))
            elif decay == "Lb2Dsp" and (p == "Bs2DsDsstPiRho" or p == "Bs2DsDsstKKst"):
                c.append("PhysBkg%sPdf_m_%s_Tot" % (p, s))
            else:
                c.append("%sEPDF_m_%s" % (p, s))

    comp.__len__()
    numCom = c.__len__()
    numSM = smy.__len__()
    print("================")
    print(comp)
    print("================")
    print(c)
    print("================")

    pdfcomp = []

    n = 0
    for j in range(0, numCom):
        for i in range(0, numSM):
            print(c[n])
            print(i + j * numCom)
            if i == 0:
                pdfcomp.append(c[n])
            else:
                pdfcomp[j] = pdfcomp[j] + "," + c[n]
            n = n + 1

    for n in pdfcomp:
        print("PDF to plot: %s" % (n))

    for i in range(0, numCom):
        print(i)
        modelPDF.plotOn(
            frame,
            RooFit.Components(pdfcomp[i]),
            RooFit.DrawOption("L"),
            RooFit.LineStyle(kSolid),
            RooFit.LineColor(color[2 + i]),
            RooFit.FillColor(color[2 + i]),
            RooFit.Normalization(1.0, RooAbsReal.RelativeExpected),
            RooFit.Name(Form("PDF%d" % (i))),
        )


# ------------------------------------------------------------------------------
def getDescription(comp, decay, low):
    happystar = (
        "#lower[-0.95]{#scale[0.5]{(}}"
        "#lower[-0.8]{#scale[0.5]{*}}#lower[-0.95]{#scale[0.5]{)}}"
    )
    happystar2 = "#lower[-0.65]{#scale[0.6]{*}}"
    happypm = "#lower[-0.95]{#scale[0.6]{#pm}}"
    happymp = "#lower[-0.95]{#scale[0.6]{#mp}}"
    happyplus = "#lower[-0.95]{#scale[0.6]{+}}"
    happymin = "#lower[-1.15]{#scale[0.7]{-}}"
    happy0 = "#lower[-0.85]{#scale[0.6]{0}}"

    from B2DXFitters import TLatexUtils

    desc = []
    for c in comp:
        if c == "Sig" or c == "Signal":
            if decay == "Bs2DsPi":
                desc.append(
                    "Signal B_{s}#kern[-0.7]{"
                    + happy0
                    + "}#rightarrow D_{s}#kern[-0.3]{"
                    + happymin
                    + "}#kern[0.1]{#pi"
                    + happyplus
                    + "}"
                )
            elif decay == "Bs2DsK":
                desc.append(
                    "Signal B_{s}#kern[-0.7]{"
                    + happy0
                    + "} #rightarrow D_{s}#kern[-0.3]{"
                    + happymp
                    + "}#kern[0.1]{K"
                    + happypm
                    + "}"
                )
            elif decay == "Bs2DsstPi":
                desc.append(
                    "Signal B_{s}#kern[-0.7]{"
                    + happy0
                    + "}#rightarrow D_{s}#kern[-0.3]{"
                    + happystar
                    + happymin
                    + "}#kern[0.1]{#pi"
                    + happyplus
                    + "}"
                )
            elif decay == "Bs2DsstK":
                desc.append(
                    "Signal B_{s}#kern[-0.7]{"
                    + happy0
                    + "} #rightarrow D_{s}#kern[-0.3]{"
                    + happystar
                    + happymp
                    + "}#kern[0.1]{K"
                    + happypm
                    + "}"
                )
            elif decay == "Lb2LcPi":
                desc.append(
                    "Signal #bar{#Lambda}_{b}#kern[-0.7]{"
                    + happy0
                    + "} #rightarrow #bar{#Lambda}_{c}#kern[-0.3]{"
                    + happymin
                    + "}#kern[0.1]{#pi"
                    + happyplus
                    + "}"
                )
            elif decay == "Lb2Dsp":
                desc.append(
                    "Signal #Lambda_{b}#kern[-0.7]{"
                    + happy0
                    + "}#rightarrow D_{s}#kern[-0.3]{"
                    + happymin
                    + "}#kern[0.1]{p}"
                )
            else:
                desc.append("Signal")
        elif c == "Bs2DsDsstPiRho" and decay == "Bs2DsPi":
            if low > 5250.0:
                desc.append(
                    "B_{(d,s)}#kern[-3.7]{"
                    + happy0
                    + "} #rightarrow D_{s}#kern[-0.3]{"
                    + happymin
                    + happystar
                    + "}#kern[0.1]{#pi"
                    + happyplus
                    + "}"
                )
            else:
                desc.append(
                    "B_{s}#kern[-0.7]{"
                    + happy0
                    + "} #rightarrow D_{s}#kern[-0.3]{"
                    + happymin
                    + happystar
                    + "}#kern[0.1]{(#pi"
                    + happyplus
                    + ",#kern[0.1]{#rho"
                    + happyplus
                    + "})}"
                )
        elif c == "Bs2DsDsstPiRho" and (decay == "Bs2DsK" or decay == "Lb2Dsp"):
            desc.append(
                "B_{s}#kern[-0.7]{"
                + happy0
                + "} #rightarrow D_{s}#kern[-0.3]{"
                + happymin
                + happystar
                + "}#kern[0.1]{(#pi"
                + happyplus
                + ",#kern[0.1]{#rho"
                + happyplus
                + "})}"
            )

        elif c == "Lb2DsDsstP" and decay == "Bs2DsK":
            desc.append(
                "#Lambda_{b}#kern[-1.2]{"
                + happy0
                + "} #rightarrow D_{s}#kern[-0.3]{"
                + happymin
                + happystar
                + "}#kern[0.1]{p}"
            )

        elif c == "Bs2DsDsstKKst" and decay == "Bs2DsK":
            desc.append(
                "B_{(d,s)}#kern[-3.7]{"
                + happy0
                + "} #kern[+0.3]{#rightarrow}D_{s}#kern[-0.3]{"
                + happymp
                + happystar
                + "}#kern[0.1]{K"
                + happypm
                + happystar
                + "}"
            )
        elif c == "Bd2DsPi" and decay == "Bs2DsPi":
            desc.append(
                "B_{d}#kern[-0.7]{"
                + happy0
                + "}#rightarrow D_{s}#kern[-0.3]{"
                + happymin
                + "}#kern[0.1]{#pi"
                + happyplus
                + "}"
            )
        elif c == "Bd2DRho":
            desc.append(
                "B_{d}#kern[-0.7]{"
                + happy0
                + "}#rightarrow D#kern[+0.2]{"
                + happymin
                + "}#kern[0.2]{#rho"
                + happyplus
                + "}"
            )
        elif c == "Bd2DstPi":
            desc.append(
                "B_{d}#kern[-0.7]{"
                + happy0
                + "}#rightarrow D#kern[+0.2]{"
                + happymin
                + happystar2
                + "}#kern[0.2]{#pi"
                + happyplus
                + "}"
            )
        elif c == "Bd2DsstPi":
            desc.append(
                "B_{d}#kern[-0.7]{"
                + happy0
                + "}#rightarrow D_{s}#kern[-0.3]{"
                + happymin
                + happystar2
                + "}#kern[0.2]{#pi"
                + happyplus
                + "}"
            )
        elif c == "Bd2DsK":
            desc.append(
                "B_{d}#kern[-0.7]{"
                + happy0
                + "}#rightarrow D_{s}#kern[-0.3]{"
                + happymin
                + "}#kern[0.1]{K"
                + happyplus
                + "}"
            )
        elif c == "Bd2DsstK":
            desc.append(
                "B_{d}#kern[-0.7]{"
                + happy0
                + "}#rightarrow D_{s}#kern[-0.3]{"
                + happymin
                + happystar2
                + "}#kern[0.2]{K"
                + happyplus
                + "}"
            )
        elif c == "Bs2DsDsstRho" and decay == "Bs2DsstK":
            desc.append(
                "B_{s}#kern[-0.7]{"
                + happy0
                + "} #rightarrow D_{s}#kern[-0.3]{"
                + happymin
                + happystar
                + "}#kern[0.1]{(#pi"
                + happyplus
                + ",#kern[0.1]{#rho"
                + happyplus
                + "})}"
            )
        elif c == "BsBd2DsstKst":
            desc.append(
                "B_{(d,s)}#kern[-3.7]{"
                + happy0
                + "} #rightarrow D_{s}#kern[-0.3]{"
                + happymp
                + happystar
                + "}#kern[0.1]{K"
                + happypm
                + happystar
                + "}"
            )
        elif c == "Bd2DKst":
            desc.append(
                "B_{d}#kern[-0.7]{"
                + happy0
                + "}#rightarrow D#kern[+0.2]{"
                + happymin
                + "}#kern[0.2]{K"
                + happystar2
                + happyplus
                + "}"
            )
        elif c == "Lb2LcRho":
            desc.append(
                "#bar{#Lambda}_{b}#kern[-1.2]{"
                + happy0
                + "} #rightarrow #bar{#Lambda}_{c}#kern[-1.2]{"
                + happymin
                + "}#kern[0.1]{#rho"
                + happyplus
                + "}"
            )
        elif c == "Lb2ScPi":
            desc.append(
                "#bar{#Lambda}_{b}#kern[-1.2]{"
                + happy0
                + "} #rightarrow #bar{#Sigma}_{c}#kern[-1.2]{"
                + happymin
                + "}#kern[0.1]{#pi"
                + happyplus
                + "}"
            )
        elif c == "Lb2LcPi":
            desc.append(
                "#bar{#Lambda}_{b}#kern[-1.2]{"
                + happy0
                + "} #rightarrow #bar{#Lambda}_{c}#kern[-1.2]{"
                + happymin
                + "}#kern[0.1]{#pi"
                + happyplus
                + "}"
            )
        elif c == "Lb2LcK":
            desc.append(
                "#bar{#Lambda}_{b}#kern[-1.2]{"
                + happy0
                + "} #rightarrow #bar{#Lambda}_{c}#kern[-1.2]{"
                + happymin
                + "}#kern[0.1]{K"
                + happyplus
                + "}"
            )
        elif c == "Lb2Dsstp":
            desc.append(
                "#Lambda_{b}#kern[-1.2]{"
                + happy0
                + "} #rightarrow D_{s}#kern[-0.3]{"
                + happymin
                + happystar2
                + "}#kern[0.1]{p}"
            )
        elif c == "Lb2Dsp":
            desc.append(
                "#Lambda_{b}#kern[-1.2]{"
                + happy0
                + "} #rightarrow D_{s}#kern[-0.3]{"
                + happymin
                + "}#kern[0.1]{p}"
            )
        elif c == "Bs2DsstPi":
            desc.append(
                "B_{s}#kern[-0.7]{"
                + happy0
                + "} #rightarrow D_{s}#kern[-0.3]{"
                + happymin
                + happystar2
                + "}#kern[0.1]{#pi"
                + happyplus
                + "}"
            )
        elif c == "Bs2DsRho":
            desc.append(
                "B_{s}#kern[-0.7]{"
                + happy0
                + "} #rightarrow D_{s}#kern[-0.3]{"
                + happymin
                + "}#kern[0.1]{#rho"
                + happyplus
                + "}"
            )
        elif c == "Bs2DsstRho":
            desc.append(
                "B_{s}#kern[-0.7]{"
                + happy0
                + "} #rightarrow D_{s}#kern[-0.3]{"
                + happymin
                + happystar2
                + "}#kern[0.1]{#rho"
                + happyplus
                + "}"
            )
        elif c == "Bs2DsstK":
            desc.append(
                "B_{s}#kern[-0.7]{"
                + happy0
                + "} #rightarrow D_{s}#kern[-0.3]{"
                + happymp
                + happystar2
                + "}#kern[0.1]{K"
                + happypm
                + "}"
            )
        elif c == "Bs2DsKst":
            desc.append(
                "B_{s}#kern[-0.7]{"
                + happy0
                + "} #rightarrow D_{s}#kern[-0.3]{"
                + happymp
                + "}#kern[0.1]{K"
                + happypm
                + happystar2
                + "}"
            )
        elif c == "Bs2DsstKst":
            desc.append(
                "B_{s}#kern[-0.7]{"
                + happy0
                + "} #rightarrow D_{s}#kern[-0.3]{"
                + happymp
                + happystar2
                + "}#kern[0.1]{K"
                + happypm
                + happystar2
                + "}"
            )
        elif c == "Lb2DsDelta":
            desc.append(
                "#Lambda_{b}#kern[-1.2]{"
                + happy0
                + "} #rightarrow D_{s}#kern[-0.3]{"
                + happymin
                + "}#kern[0.1]{#Delta"
                + happyplus
                + "}"
            )

        else:
            desc.append(str(TLatexUtils.DecDescrToTLatex(c)))
    return desc


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

if __name__ == "__main__":
    args = parser.parse_args()

    configDirectory = os.path.dirname(os.path.abspath(args.configName))
    configModule = os.path.splitext(os.path.split(args.configName)[1])[0]

    initial_path = sys.path
    sys.path.insert(0, configDirectory)

    myconfigfile = importlib.import_module(configModule).getconfig()
    sys.path = initial_path

    if args.modeDs[0] == "all":
        args.modeDs = myconfigfile["CharmModes"]

    if not exists(args.FILENAME):
        parser.error('ROOT file "%s" not found! Nothing plotted.' % args.FILENAME)
        parser.print_help()

    print("==========================================================")
    print("PREPARING WORKSPACE IS RUNNING WITH THE FOLLOWING CONFIGURATION OPTIONS")
    for option in myconfigfile:
        if option == "constParams":
            for param in myconfigfile[option]:
                print(param, "is constant in the fit")
        else:
            print(option, " = ", myconfigfile[option])
    print("==========================================================")

    from B2DXFitters.MDFitSettingTranslator import Translator

    mdt = Translator(myconfigfile, "MDSettings", False)
    MDSettings = mdt.getConfig()
    MDSettings.Print("v")

    f = TFile(args.FILENAME)

    w = f.Get(args.wsname)
    if not w:
        parser.error(
            'Workspace "%s" not found in file "%s"! Nothing plotted.'
            % (args.wsname, args.FILENAME)
        )

    dim = int(args.dim)
    bin = int(args.bin)
    mVarTS = TString(args.var)
    mass = w.var(mVarTS.Data())
    mass.Print("v")
    log = args.log
    leg = args.legend
    debug = args.debug

    from B2DXFitters.mdfitutils import getTopLevelSMY as getTopLevelSMY

    mod, sam, yr = getTopLevelSMY(args.modeDs, args.pol, args.year, args.merge)

    from B2DXFitters.mdfitutils import checkMerge as checkMerge
    from B2DXFitters.mdfitutils import getSampleModeYear as getSampleModeYear
    from B2DXFitters.mdfitutils import getStdVector as getStdVector

    if args.pol[0] == "both":
        args.pol = ["up", "down"]

    if args.year[0] == "run1":
        args.year = ["2011", "2012"]
    if args.year[0] == "run2":
        args.year = ["2015", "2016"]
    if args.year[0] == "all":
        args.year = ["2011", "2012", "2015", "2016"]

    s, m, y = checkMerge(args.modeDs, args.pol, args.year, args.merge)
    smy = getSampleModeYear(m, s, y, debug)

    sufixTS = TString(args.suffix)
    if sufixTS != "":
        sufixTS = TString("_") + sufixTS

    from B2DXFitters.mdfitutils import getExpectedValue as getExpectedValue
    from B2DXFitters.mdfitutils import getExpectedYield as getExpectedYield
    from B2DXFitters.mdfitutils import getObservables as getObservables
    from B2DXFitters.mdfitutils import getPDFNameFromConfig as getPDFNameFromConfig
    from B2DXFitters.mdfitutils import getPIDKComponents as getPIDKComponents
    from B2DXFitters.mdfitutils import getSigOrCombPDF as getSigOrCombPDF
    from B2DXFitters.mdfitutils import getType as getType
    from B2DXFitters.mdfitutils import readVariables as readVariables
    from B2DXFitters.mdfitutils import (
        setConstantIfSoConfigured as setConstantIfSoConfigured,
    )

    ch = TString(myconfigfile["Decay"])

    range_dw = mass.getMin()
    range_up = mass.getMax()

    if mVarTS.Contains("PIDK") is False:
        unit = "MeV/#font[12]{c}^{2}"
    else:
        unit = ""

    Bin = RooBinning(range_dw, range_up, "P")
    Bin.addUniform(bin, range_dw, range_up)

    mass.setBinning(Bin)

    ty = TString("ToyNo")
    if args.toy:
        ty = TString("ToyYes")
    w.Print("v")

    if "PlotSettings" in myconfigfile:
        print(type(myconfigfile["PlotSettings"]["components"]))
        if type(myconfigfile["PlotSettings"]["components"]) == dict:
            compEPDF = myconfigfile["PlotSettings"]["components"]["EPDF"]
            compPDF = myconfigfile["PlotSettings"]["components"]["PDF"]
            compLEG = myconfigfile["PlotSettings"]["components"]["Legend"]
            colorPDF = myconfigfile["PlotSettings"]["colors"]["PDF"]
            colorLEG = myconfigfile["PlotSettings"]["colors"]["Legend"]
        else:
            compEPDF = myconfigfile["PlotSettings"]["components"]
            compPDF = compEPDF
            compLEG = compEPDF
            colorPDF = myconfigfile["PlotSettings"]["colors"]
            colorLEG = colorPDF
    else:
        print("[ERROR] PlotSettings missed in the config file.")
        exit(0)

    colorPDFHex = []
    for color in colorPDF:
        if type(color) == str:
            colorPDFHex.append(TColor.GetColor(color))
        else:
            colorPDFHex.append(color)

    colorLEGHex = []
    for color in colorLEG:
        colorLEGHex.append(TColor.GetColor(color))

    desc = getDescription(compLEG, ch, range_dw)

    frame_m = mass.frame(range_dw, range_up + 0.00000001 * range_up)
    frame_m.SetTitle("")

    frame_m.GetXaxis().SetLabelSize(0.065)
    frame_m.GetYaxis().SetLabelSize(0.065)
    frame_m.GetXaxis().SetLabelFont(132)
    frame_m.GetYaxis().SetLabelFont(132)
    frame_m.GetXaxis().SetLabelOffset(0.006)
    frame_m.GetYaxis().SetLabelOffset(0.006)
    frame_m.GetYaxis().SetLabelColor(kWhite)

    frame_m.GetXaxis().SetTitleFont(132)
    frame_m.GetYaxis().SetTitleFont(132)
    frame_m.GetXaxis().SetTitleSize(0.065)
    frame_m.GetYaxis().SetTitleSize(0.065)
    frame_m.GetYaxis().SetNdivisions(512)
    frame_m.GetXaxis().SetNdivisions(508)

    frame_m.GetXaxis().SetTitleOffset(1.00)
    frame_m.GetYaxis().SetTitleOffset(0.5)
    frame_m.GetYaxis().SetTitle("Arbitrary Units")

    modelPDF = getTotPDF(w, smy, compEPDF, debug)
    plotFitModel(modelPDF, frame_m, mVarTS, smy, ch, compPDF, colorPDFHex)

    if log:
        gStyle.SetOptLogy(1)
        frame_m.GetYaxis().SetRangeUser(1.5, frame_m.GetMaximum() * 1.5)

    labelX = GeneralUtils.GetXLabel(ch, mVarTS, TString(mod), debug)
    frame_m.GetXaxis().SetTitle(labelX.Data())

    canvas = TCanvas("canvas", "canvas", 1200, 800)
    canvas.cd()
    pad1 = TPad("upperPad", "upperPad", 0.005, 0.005, 1.0, 1.0)
    pad1.SetBorderMode(0)
    pad1.SetBorderSize(-1)
    pad1.SetFillStyle(0)
    pad1.SetBottomMargin(0.15)
    pad1.SetLeftMargin(0.10)
    pad1.SetTopMargin(0.05)
    pad1.SetRightMargin(0.05)
    if mVarTS == "lab0_MassFitConsD_M" or mVarTS == "BeautyMass":
        pad1.SetRightMargin(0.08)
    pad1.SetFillStyle(0)
    pad1.SetTickx(0)
    pad1.Draw()
    pad1.cd()

    xs = 0.05
    ys = 0.05
    xe = 0.95
    ye = 0.95

    if leg:
        if "LegendSettings" in myconfigfile:
            xs = myconfigfile["LegendSettings"][mVarTS.Data()]["Position"][0]
            ys = myconfigfile["LegendSettings"][mVarTS.Data()]["Position"][1]
            xe = myconfigfile["LegendSettings"][mVarTS.Data()]["Position"][2]
            ye = myconfigfile["LegendSettings"][mVarTS.Data()]["Position"][3]
            legend = TLegend(xs, ys, xe, ye)
            size = myconfigfile["LegendSettings"][mVarTS.Data()]["TextSize"]
            legend.SetTextSize(size)
            if "ScaleYSize" in myconfigfile["LegendSettings"][mVarTS.Data()]:
                scale = myconfigfile["LegendSettings"][mVarTS.Data()]["ScaleYSize"]
            else:
                scale = 1.2
            if log:
                frame_m.GetYaxis().SetRangeUser(1.5, frame_m.GetMaximum() * scale)
            else:
                frame_m.GetYaxis().SetRangeUser(1.0, frame_m.GetMaximum() * scale)
        else:
            print(
                "[ERROR] You need to specify position of legend "
                "in configfile using 'LegendSettings'"
            )
            exit(0)
    else:
        frame_m.GetYaxis().SetRangeUser(0.1, frame_m.GetMaximum() * 1.1)
        legend = TLegend(xs, ys, xe, ye)
        legend.SetTextSize(0.09)
        if log:
            # frame_m.GetYaxis().SetRangeUser(1.5,frame_m.GetMaximum()*1.5)
            frame_m.GetYaxis().SetRangeUser(
                float(args.ymin), frame_m.GetMaximum() * 1.1
            )

    legend.SetTextFont(12)
    legend.SetFillColor(4000)
    legend.SetShadowColor(0)
    legend.SetBorderSize(0)
    legend.SetTextFont(132)

    lhcbtext = TLatex()
    lhcbtext.SetTextFont(132)
    lhcbtext.SetTextColor(1)
    if "LegendSettings" in myconfigfile:
        sizelhcbtext = myconfigfile["LegendSettings"][mVarTS.Data()].get(
            "LHCbTextSize", 0.08
        )
        if "SetLegendColumns" in myconfigfile["LegendSettings"][mVarTS.Data()]:
            legend.SetNColumns(
                int(myconfigfile["LegendSettings"][mVarTS.Data()]["SetLegendColumns"])
            )
    else:
        sizelhcbtext = 0.08
    lhcbtext.SetTextSize(sizelhcbtext)
    lhcbtext.SetTextAlign(12)

    chi2ndf = TLatex()
    chi2ndf.SetTextFont(132)
    chi2ndf.SetTextColor(1)
    chi2ndf.SetTextSize(0.06)
    chi2ndf.SetTextAlign(12)

    h = []
    print(compLEG)
    print(desc)

    for i in range(2, compLEG.__len__()):
        print(i)
        print(compLEG[i])
        print(desc[0])
        h.append(TH1F(compLEG[i], compLEG[i], 5, 0, 1))
        h[i - 2].SetFillColor(colorLEGHex[i])
        h[i - 2].SetFillStyle(1001)
        legend.AddEntry(h[i - 2], desc[i], "f")
    pad1.cd()
    frame_m.Draw()
    if leg:
        legend.Draw()
    pad1.Update()

    labelX = GeneralUtils.GetXLabel(ch, mVarTS, TString(mod), debug)

    canvas.Update()

    if yr != "":
        yr = TString("_") + TString(yr)

    saveName = "shapes_{ch}_{mVarTS}_{mod}_{sam}{yr}".format(
        ch=ch, mVarTS=mVarTS, sam=sam, mod=mod, yr=yr
    )
    # ch=ch, mVarTS=mVarTS, sam=sam, mod=''.join(args.modeDs), yr=yr)

    if log:
        saveName += "_log"

    if args.suffix != "":
        saveName += "_" + args.suffix

    if not leg:
        canl = TCanvas("canl", "canl", 1200, 1000)
        canl.cd()
        legend.Draw()
        canl.Update()

    import os  # re-importing os seems to help with something

    for ftype in [".pdf", ".png", ".root", ".C", ".eps"]:
        canvas.SaveAs(os.path.join(args.plot_prefix, saveName + ftype))
        if not leg:
            canl.SaveAs(
                os.path.join(
                    args.plot_prefix, "legend_{ch}{ftype}".format(ch=ch, ftype=ftype)
                )
            )
