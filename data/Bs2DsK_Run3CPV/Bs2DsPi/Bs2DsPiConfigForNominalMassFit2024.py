##########make#####################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    configdict = {}
    diffBdBs = 87.23

    # considered decay mode
    configdict["Decay"] = "Bs2DsPi"
    configdict["CharmModes"] = {"KKPi"}
    configdict["Backgrounds"] = [
        "Bd2DPi",
        "Lb2LcPi",
        "Bs2DsRho",
        "Bs2DsstRho",
        "Bs2DsstPi",
        "Bd2DsPi",
        "Bs2DsK",
        "Bd2DsstPi",
    ]

    # year of data taking
    configdict["YearOfDataTaking"] = {
        "2022",
    }
    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2022": {"Down": 0.500, "Up": 0.500},
    }

    # stripping (necessary in case of PIDK shapes)
    configdict[
        "dataName"
    ] = "b2dxfitters/data/Bs2DsK_Run3CPV/Bs2DsPi/config_Bs2DsPi_2024.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotLb2Dsp", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5100, 5800],
        "InputName": "lab0_DTF_LifetimeFit_MASS",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1920, 2010],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [10000.0, 150000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [1000.0, 45000.0],
        "InputName": "lab1_PT",
    }
    # configdict["BasicVariables"]["nTracks"] = {
    #     "Range": [15.0, 1000.0],
    #     "InputName": "nTracks"
    # }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.5, 1.0],
        "InputName": "BDTGResponse_XGB_1",
    }

    configdict["AdditionalVariables"] = {}
    # configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
    #     "Range": [0.0, 2500.0],
    #     "InputName": "lab0_ENDVERTEX_ZERR"
    # }
    # configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
    #     "Range": [0.0, 3000.0],
    #     "InputName": "lab2_ENDVERTEX_ZERR"
    # }
    # configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
    #     "Range": [0.0, 30.0],
    #     "InputName": "lab0_ENDVERTEX_CHI2"
    # }
    # configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
    #     "Range": [0.0, 30.0],
    #     "InputName": "lab2_ENDVERTEX_CHI2"
    # }
    # configdict["AdditionalVariables"]["lab0_L0HadronDecision_TOS"] = {
    #     "Range": [-1.0, 2.0],
    #     "InputName": "lab0_L0HadronDecision_TOS"
    # }
    # configdict["AdditionalVariables"]["lab0_L0Global_TIS"] = {
    #     "Range": [-1.0, 2.0],
    #     "InputName": "lab0_L0Global_TIS"
    # }

    pid = "(lab3_PIDK >  0. && lab4_PIDK >  0. && lab5_PIDK < 10.)"
    veto_lc_1 = "(fabs(sqrt(sq(sqrt(sq(lab3_MASS)+sq(lab3_P))"
    veto_lc_2 = "+ sqrt(938.272*938.272+lab4_P*lab4_P)"
    veto_lc_3 = "+ sqrt(sq(lab5_MASS)+sq(lab5_P)))"
    veto_lc_4 = "-sq(lab3_PX+lab4_PX+lab5_PX)-sq(lab3_PY+lab4_PY+lab5_PY)"
    veto_lc_5 = (
        "-sq(lab3_PZ+lab4_PZ+lab5_PZ))-2286.4) > 30 || (lab4_PIDK - lab4_PIDp > 5.))"
    )
    veto_lckkp = "lab5_PIDp<10"
    veto_d0 = "(lab34_MM < 1800)"
    veto_semi = "(lab1_isMuon==0 && lab5_isMuon==0)"
    veto_ds_1 = "(fabs(sqrt(sq(sqrt(sq(lab3_MASS)+sq(lab3_P))"
    veto_ds_2 = "+ sqrt(139.6*139.6+lab4_P*lab4_P)"
    veto_ds_3 = "+ sqrt(sq(lab5_MASS)+sq(lab5_P)))"
    veto_ds_4 = "-sq(lab3_PX+lab4_PX+lab5_PX)-sq(lab3_PY+lab4_PY+lab5_PY)"
    veto_ds_5 = "-sq(lab3_PZ+lab4_PZ+lab5_PZ))-1869.6) > 50 || lab4_PIDK > 15.)"
    app = "&&"

    veto_lc = veto_lc_1 + veto_lc_2 + veto_lc_3 + veto_lc_4 + veto_lc_5
    veto_ds = veto_ds_1 + veto_ds_2 + veto_ds_3 + veto_ds_4 + veto_ds_5

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": pid
        + app
        + veto_lc
        + app
        + veto_lckkp
        + app
        + veto_d0
        + app
        + veto_semi
        + app
        + veto_ds,
        "MC": " ",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": False,
        "DsHypo": True,
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "2022": {"All": 5367.61198},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "2022": {"All": 22.01244},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "2022": {"All": 13.98339},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "2022": {"All": 1.15342},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "2022": {"All": 2.79001},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "2022": {"All": 1.32575},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "2022": {"All": 1.8218},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "2022": {"All": -1.7},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "2022": {"All": -0.44943},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "2022": {"All": 0.29707},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "2022": {"All": 0.36312},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   ----  combinatorial background  ----   #
    #   ------------------------------------   #

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
        "2022": {"All": -5.1186e-02},
        "Fixed": False,
    }
    configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
        "2022": {"All": 0.5},
        "Fixed": False,
    }
    #   ------------------------------------   #
    #   -----  Bs2DsstPi background    -----   #
    #   ------------------------------------   #

    configdict["Bs2DsstPiShape"] = {}
    configdict["Bs2DsstPiShape"]["BeautyMass"] = {}
    configdict["Bs2DsstPiShape"]["BeautyMass"]["type"] = "HILLdiniPlusHORNSdini"
    configdict["Bs2DsstPiShape"]["BeautyMass"]["R"] = {
        "2022": {"All": 5.0721e00},
        "Fixed": True,
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["ahill"] = {
        "2022": {"All": 4.6315e03},
        "Fixed": True,
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["ahorns"] = {
        "2022": {"All": 5.0997e03},
        "Fixed": True,
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["bhill"] = {
        "2022": {"All": 5.3104e03},
        "Fixed": True,
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["bhorns"] = {
        "2022": {"All": 5.2070e03},
        "Fixed": True,
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["csihill"] = {
        "2022": {"All": -1.1509e00},
        "Fixed": True,
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["csihorns"] = {
        "2022": {"All": 9.6358e-01},
        "Fixed": True,
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["frac"] = {
        "2022": {"All": 3.1800e-01},
        "Fixed": True,
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["frachorns"] = {
        "2022": {"All": 1.7398e-01},
        "Fixed": True,
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["sigma"] = {
        "2022": {"All": 2.9948e00},
        "Fixed": True,
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["sigmahorns"] = {
        "2022": {"All": 1.4226e01},
        "Fixed": True,
    }
    configdict["Bs2DsstPiShape"]["BeautyMass"]["shift"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   -----  Bd2DsstPi background    -----   #
    #   ------------------------------------   #

    configdict["Bd2DsstPiShape"] = {}
    configdict["Bd2DsstPiShape"]["BeautyMass"] = {}
    configdict["Bd2DsstPiShape"]["BeautyMass"]["type"] = "HILLdiniPlusHORNSdini"
    configdict["Bd2DsstPiShape"]["BeautyMass"]["R"] = configdict["Bs2DsstPiShape"][
        "BeautyMass"
    ]["R"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["ahill"] = {
        "2022": {
            "All": configdict["Bs2DsstPiShape"]["BeautyMass"]["ahill"]["2022"]["All"]
            - diffBdBs
        },
        "Fixed": True,
    }
    configdict["Bd2DsstPiShape"]["BeautyMass"]["ahorns"] = {
        "2022": {
            "All": configdict["Bs2DsstPiShape"]["BeautyMass"]["ahorns"]["2022"]["All"]
            - diffBdBs
        },
        "Fixed": True,
    }
    configdict["Bd2DsstPiShape"]["BeautyMass"]["bhill"] = {
        "2022": {
            "All": configdict["Bs2DsstPiShape"]["BeautyMass"]["bhill"]["2022"]["All"]
            - diffBdBs
        },
        "Fixed": True,
    }
    configdict["Bd2DsstPiShape"]["BeautyMass"]["bhorns"] = {
        "2022": {
            "All": configdict["Bs2DsstPiShape"]["BeautyMass"]["bhorns"]["2022"]["All"]
            - diffBdBs
        },
        "Fixed": True,
    }

    configdict["Bd2DsstPiShape"]["BeautyMass"]["csihill"] = configdict[
        "Bs2DsstPiShape"
    ]["BeautyMass"]["csihill"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["csihorns"] = configdict[
        "Bs2DsstPiShape"
    ]["BeautyMass"]["csihorns"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["frac"] = configdict["Bs2DsstPiShape"][
        "BeautyMass"
    ]["frac"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["frachorns"] = configdict[
        "Bs2DsstPiShape"
    ]["BeautyMass"]["frachorns"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["sigma"] = configdict["Bs2DsstPiShape"][
        "BeautyMass"
    ]["sigma"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["sigmahorns"] = configdict[
        "Bs2DsstPiShape"
    ]["BeautyMass"]["sigmahorns"]
    configdict["Bd2DsstPiShape"]["BeautyMass"]["shift"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   -----   Bs2DsRho background    -----   #
    #   ------------------------------------   #

    configdict["Bs2DsRhoShape"] = {}
    configdict["Bs2DsRhoShape"]["BeautyMass"] = {}
    configdict["Bs2DsRhoShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Bs2DsRhoShape"]["BeautyMass"]["R"] = {
        "2022": {"All": 6.4109e00},
        "Fixed": True,
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["a"] = {
        "2022": {"All": 4.1431e03},
        "Fixed": True,
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["b"] = {
        "2022": {"All": 5.2276e03},
        "Fixed": True,
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["csi"] = {
        "2022": {"All": 4.3152e00},
        "Fixed": True,
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["frac"] = {
        "2022": {"All": 9.6730e-01},
        "Fixed": True,
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["sigma"] = {
        "2022": {"All": 1.5504e01},
        "Fixed": True,
    }
    configdict["Bs2DsRhoShape"]["BeautyMass"]["shift"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   -----   Bd2DsRho background    -----   #
    #   ------------------------------------   #

    configdict["Bd2DsRhoShape"] = {}
    configdict["Bd2DsRhoShape"]["BeautyMass"] = {}
    configdict["Bd2DsRhoShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Bd2DsRhoShape"]["BeautyMass"]["R"] = configdict["Bs2DsRhoShape"][
        "BeautyMass"
    ]["R"]
    configdict["Bd2DsRhoShape"]["BeautyMass"]["a"] = {
        "2022": {
            "All": configdict["Bs2DsRhoShape"]["BeautyMass"]["a"]["2022"]["All"]
            - diffBdBs
        },
        "Fixed": True,
    }
    configdict["Bd2DsRhoShape"]["BeautyMass"]["b"] = {
        "2022": {
            "All": configdict["Bs2DsRhoShape"]["BeautyMass"]["b"]["2022"]["All"]
            - diffBdBs
        },
        "Fixed": True,
    }

    configdict["Bd2DsRhoShape"]["BeautyMass"]["csi"] = configdict["Bs2DsRhoShape"][
        "BeautyMass"
    ]["csi"]
    configdict["Bd2DsRhoShape"]["BeautyMass"]["frac"] = configdict["Bs2DsRhoShape"][
        "BeautyMass"
    ]["frac"]
    configdict["Bd2DsRhoShape"]["BeautyMass"]["sigma"] = configdict["Bs2DsRhoShape"][
        "BeautyMass"
    ]["sigma"]
    configdict["Bd2DsRhoShape"]["BeautyMass"]["shift"] = {
        "2022": {"All": -diffBdBs},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   -----  Bs2DsstRho background   -----   #
    #   ------------------------------------   #

    configdict["Bs2DsstRhoShape"] = {}
    configdict["Bs2DsstRhoShape"]["BeautyMass"] = {}
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["R"] = {
        "2022": {"All": 6.0000e01},
        "Fixed": True,
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["a"] = {
        "2022": {"All": 3.0000e03},
        "Fixed": True,
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["b"] = {
        "2022": {"All": 5.0910e03},
        "Fixed": True,
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["csi"] = {
        "2022": {"All": 5.0000e01},
        "Fixed": True,
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["frac"] = {
        "2022": {"All": 9.9643e-01},
        "Fixed": True,
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["sigma"] = {
        "2022": {"All": 4.2153e01},
        "Fixed": True,
    }
    configdict["Bs2DsstRhoShape"]["BeautyMass"]["shift"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   -----   Bd2DsPi background     -----   #
    #   ------------------------------------   #

    configdict["Bd2DsPiShape"] = {}
    configdict["Bd2DsPiShape"]["BeautyMass"] = {}
    if configdict["SignalShape"]["BeautyMass"]["type"] == "IpatiaPlusJohnsonSU":
        configdict["Bd2DsPiShape"]["BeautyMass"][
            "type"
        ] = "ShiftedSignalIpatiaJohnsonSU"
        configdict["Bd2DsPiShape"]["BeautyMass"]["shift"] = {
            "2022": {"All": -diffBdBs},
            "Fixed": True,
        }
    else:
        configdict["Bd2DsPiShape"]["BeautyMass"]["type"] = "ShiftedSignal"
        configdict["Bd2DsPiShape"]["BeautyMass"]["shift"] = {
            "2022": {"All": diffBdBs},
            "Fixed": True,
        }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale1"] = {
        "2022": {"All": 1.00808721452},
        "Fixed": True,
    }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale2"] = {
        "2022": {"All": 1.03868673310},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    configdict["Yields"] = {}

    configdict["Yields"]["Bd2DPi"] = {"2022": {"KKPi": 0.0}, "Fixed": False}
    configdict["Yields"]["Bs2DsK"] = {"2022": {"KKPi": 10000.0}, "Fixed": False}
    configdict["Yields"]["Lb2LcPi"] = {"2022": {"KKPi": 10000.0}, "Fixed": False}
    configdict["Yields"]["Bd2DsPi"] = {"2022": {"KKPi": 200000.0}, "Fixed": False}
    configdict["Yields"]["Bd2DsstPi"] = {"2022": {"KKPi": 200000.0}, "Fixed": False}
    configdict["Yields"]["Bs2DsstPi"] = {"2022": {"KKPi": 300000.0}, "Fixed": False}
    configdict["Yields"]["Bs2DsRho"] = {"2022": {"KKPi": 200000.0}, "Fixed": False}
    configdict["Yields"]["Bs2DsstRho"] = {"2022": {"KKPi": 100000.0}, "Fixed": False}
    configdict["Yields"]["CombBkg"] = {"2022": {"KKPi": 400000.0}, "Fixed": False}
    configdict["Yields"]["Signal"] = {"2022": {"KKPi": 400000.0}, "Fixed": False}

    # ----------------------------------------------------------------------#
    ###                 MDfit plotting settings
    # ----------------------------------------------------------------------#

    from ROOT import kSolid

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Lb2LcPi",
        "Bd2DPi",
        "Bs2DsK",
        "Bd2DsPi",
        "Bd2DsstPi",
        "Bs2DsstPi",
        "Bs2DsRho",
        "Bs2DsstRho",
    ]
    configdict["PlotSettings"]["colors"] = [
        "#d7301f",
        "#cccccc",
        "#238443",
        "#fbb4b9",
        "#08519c",
        "#c51b8a",
        "#7a0177",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
    ]
    configdict["PlotSettings"]["pattern"] = [
        kSolid,
        3004,
        kSolid,
        3005,
        kSolid,
        3005,
        3005,
        kSolid,
        kSolid,
        kSolid,
    ]
    configdict["PlotSettings"]["patterncolor"] = [
        "#d7301f",
        "#969696",
        "#238443",
        "#feebe2",
        "#08519c",
        "#feebe2",
        "#feebe2",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
