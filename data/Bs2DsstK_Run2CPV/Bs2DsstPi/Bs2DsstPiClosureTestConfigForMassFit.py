from ROOT import kBlue


def getconfig():
    from Bs2DsstPiConfigForNominalMassFit import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    configdict[
        "dataName"
    ] = "../data/Bs2DsstK_Run2CPV/Bs2DsstPi/config_Bs2DsstPi-MCfit.txt"

    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5100, 5450],
        "InputName": "FBs_DsstMC_M",
    }

    BKGCAT = "&&((FBs_BKGCAT < 30 || FBs_BKGCAT == 50)&&"
    "(FDsst_BKGCAT < 30 || FDsst_BKGCAT == 50))"
    configdict["AdditionalCuts"]["All"] = {
        "Data": "FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>111.5&&"
        "(FDsst_M-FDs_M)<181.5&&FBs_Veto==0.&&FDelta_R<1.&&FBDT_Var>0.01" + BKGCAT,
        "MC": "FDs_M>1950.&&FDs_M<1990.&&(FDsst_M-FDs_M)>111.5&&"
        "(FDsst_M-FDs_M)<181.5&&FBs_Veto==0.&&FDelta_R<1.&&FBDT_Var>0.01",
    }

    # --------------------------------------------------
    ###             MDfit fitting settings
    # --------------------------------------------------

    configdict["Yields"] = {}
    configdict["Yields"]["Signal"] = {
        "2016": {
            "NonRes": 20000.0,
            "PhiPi": 20000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0,
        },
        "2015": {
            "NonRes": 10000.0,
            "PhiPi": 10000.0,
            "KstK": 10000.0,
            "KPiPi": 10000.0,
            "PiPiPi": 10000.0,
        },
        "Fixed": False,
    }

    # --------------------------------------------------
    ###             MDfit plotting settings
    # --------------------------------------------------

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = ["Sig"]
    configdict["PlotSettings"]["colors"] = [kBlue - 6]

    return configdict
