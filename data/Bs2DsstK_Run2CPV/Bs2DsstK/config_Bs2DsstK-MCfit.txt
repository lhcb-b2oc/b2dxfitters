#Bs2DsstK NonRes 2015
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s26tos28_Bs2DsstKCPV_Signal_dw.root
Filter-s26tos28_Bs2DsstKCPV_Signal_up.root
tuple
tuple
###

#Bs2DsstK NonRes 2016
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s26tos28_Bs2DsstKCPV_Signal_dw.root
Filter-s26tos28_Bs2DsstKCPV_Signal_up.root
tuple
tuple
###

#Bs2DsstK PhiPi 2015
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s26tos28_Bs2DsstKCPV_Signal_dw.root
Filter-s26tos28_Bs2DsstKCPV_Signal_up.root
tuple
tuple
###

#Bs2DsstK PhiPi 2016
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s26tos28_Bs2DsstKCPV_Signal_dw.root
Filter-s26tos28_Bs2DsstKCPV_Signal_up.root
tuple
tuple
###

#Bs2DsstK KstK 2015
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s26tos28_Bs2DsstKCPV_Signal_dw.root
Filter-s26tos28_Bs2DsstKCPV_Signal_up.root
tuple
tuple
###

#Bs2DsstK KstK 2016
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s26tos28_Bs2DsstKCPV_Signal_dw.root
Filter-s26tos28_Bs2DsstKCPV_Signal_up.root
tuple
tuple
###

#Bs2DsstK KPiPi 2015
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s26tos28_Bs2DsstKCPV_Signal_dw.root
Filter-s26tos28_Bs2DsstKCPV_Signal_up.root
tuple
tuple
###

#Bs2DsstK KPiPi 2016
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s26tos28_Bs2DsstKCPV_Signal_dw.root
Filter-s26tos28_Bs2DsstKCPV_Signal_up.root
tuple
tuple
###

#Bs2DsstK PiPiPi 2015
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s26tos28_Bs2DsstKCPV_Signal_dw.root
Filter-s26tos28_Bs2DsstKCPV_Signal_up.root
tuple
tuple
###

#Bs2DsstK PiPiPi 2016
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s26tos28_Bs2DsstKCPV_Signal_dw.root
Filter-s26tos28_Bs2DsstKCPV_Signal_up.root
tuple
tuple
###

#MC FileName KKPi MD 2012
{"Mode":"Bs2DsRho",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
{"Mode":"Bs2DsstRho",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1",
 "Smooth":2.5}
 {"Mode":"Bs2DsstPi",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
 {"Mode":"Bd2DsstK",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
 {"Mode":"Bs2DsKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
 {"Mode":"Bd2DsKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
  {"Mode":"Bs2DsstKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
  {"Mode":"Bd2DsstKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
###

#MC FileName KKPi MU 2012
{"Mode":"Bs2DsRho",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
{"Mode":"Bs2DsstRho",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1",
 "Smooth":2.5}
  {"Mode":"Bs2DsstPi",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
 {"Mode":"Bd2DsstK",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
 {"Mode":"Bs2DsKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
 {"Mode":"Bd2DsKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
  {"Mode":"Bs2DsstKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
  {"Mode":"Bd2DsstKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
###

#MC FileName KKPi MD 2011
{"Mode":"Bs2DsRho",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
{"Mode":"Bs2DsstRho",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1",
 "Smooth":2.5}
  {"Mode":"Bs2DsstPi",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
 {"Mode":"Bd2DsstK",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
 {"Mode":"Bs2DsKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
 {"Mode":"Bd2DsKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
  {"Mode":"Bs2DsstKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
  {"Mode":"Bd2DsstKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_dw.root",
 "TreeName":"tuple;1"}
###

#MC FileName KKPi MU 2011
{"Mode":"Bs2DsRho",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
{"Mode":"Bs2DsstRho",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1",
 "Smooth":2.5}
  {"Mode":"Bs2DsstPi",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
 {"Mode":"Bd2DsstK",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
 {"Mode":"Bs2DsKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
 {"Mode":"Bd2DsKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
  {"Mode":"Bs2DsstKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
  {"Mode":"Bd2DsstKst",
 "FileName":"/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/Filter-s28r1_Bs2DsstK_Data_up.root",
 "TreeName":"tuple;1"}
###

#Signal Bs2DsstK PhiPi 2012
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s21/
Filter-s21_Bs2DsstK_Signal_dw.root
Filter-s21_Bs2DsstK_Signal_up.root
tuple
tuple
###

#Signal Bs2DsstK KstK 2012
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s21/
Filter-s21_Bs2DsstK_Signal_dw.root
Filter-s21_Bs2DsstK_Signal_up.root
tuple
tuple
###

#Signal Bs2DsstK KPiPi 2012
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s21/
Filter-s21_Bs2DsstK_Signal_dw.root
Filter-s21_Bs2DsstK_Signal_up.root
tuple
tuple
###

#Signal Bs2DsstK PiPiPi 2012
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s21/
Filter-s21_Bs2DsstK_Signal_dw.root
Filter-s21_Bs2DsstK_Signal_up.root
tuple
tuple
###

#Signal Bs2DsstK NonRes 2011
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s21/
Filter-s21_Bs2DsstK_Signal_dw.root
Filter-s21_Bs2DsstK_Signal_up.root
tuple
tuple
###

#Signal Bs2DsstK PhiPi 2011
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s21/
Filter-s21_Bs2DsstK_Signal_dw.root
Filter-s21_Bs2DsstK_Signal_up.root
tuple
tuple
###

#Signal Bs2DsstK KstK 2011
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s21/
Filter-s21_Bs2DsstK_Signal_dw.root
Filter-s21_Bs2DsstK_Signal_up.root
tuple
tuple
###

#Signal Bs2DsstK KPiPi 2011
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s21/
Filter-s21_Bs2DsstK_Signal_dw.root
Filter-s21_Bs2DsstK_Signal_up.root
tuple
tuple
###

#Signal Bs2DsstK PiPiPi 2011
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s21/
Filter-s21_Bs2DsstK_Signal_dw.root
Filter-s21_Bs2DsstK_Signal_up.root
tuple
tuple
###

#Bs2DsstK Combinatorial NonRes 2011
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s24r1/
Filter-s24r1_Bs2DsstK_Data_dw.root
Filter-s24r1_Bs2DsstK_Data_up.root
tuple
tuple
###

#Bs2DsstK Combinatorial NonRes 2012
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s28r1_Bs2DsstK_Data_dw.root
Filter-s28r1_Bs2DsstK_Data_up.root
tuple
tuple
###

#Bs2DsstK Combinatorial PhiPi 2011
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s24r1/
Filter-s24r1_Bs2DsstK_Data_dw.root
Filter-s24r1_Bs2DsstK_Data_up.root
tuple
tuple
###

#Bs2DsstK Combinatorial PhiPi 2012
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s28r1_Bs2DsstK_Data_dw.root
Filter-s28r1_Bs2DsstK_Data_up.root
tuple
tuple
###

#Bs2DsstK Combinatorial KstK 2011
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s24r1/
Filter-s24r1_Bs2DsstK_Data_dw.root
Filter-s24r1_Bs2DsstK_Data_up.root
tuple
tuple
###

#Bs2DsstK Combinatorial KstK 2012
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s28r1_Bs2DsstK_Data_dw.root
Filter-s28r1_Bs2DsstK_Data_up.root
tuple
tuple
###

#Bs2DsstK Combinatorial KPiPi 2011
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s24r1/
Filter-s24r1_Bs2DsstK-K2Pi_Data_dw.root
Filter-s24r1_Bs2DsstK-K2Pi_Data_up.root
tuple
tuple
###

#Bs2DsstK Combinatorial KPiPi 2012
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s28r1_Bs2DsstK-K2Pi_Data_dw.root
Filter-s28r1_Bs2DsstK-K2Pi_Data_up.root
tuple
tuple
###

#Bs2DsstK Combinatorial PiPiPi 2011
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s24r1/
Filter-s24r1_Bs2DsstK-3Pi_Data_dw.root
Filter-s24r1_Bs2DsstK-3Pi_Data_up.root
tuple
tuple
###

#Bs2DsstK Combinatorial PiPiPi 2012
/afs/cern.ch/work/a/abertoli/public/DsstrK/bdt-s28r1/
Filter-s28r1_Bs2DsstK-3Pi_Data_dw.root
Filter-s28r1_Bs2DsstK-3Pi_Data_up.root
tuple
tuple
###

#RatioDataMC 2011 PNTr
/afs/cern.ch/work/s/sgallori/public/forAgnieszka/dataMC_weights/p_nTracks/
weights_DPi_2011_dw.root
weights_DPi_2011_up.root
###

#RatioDataMC 2012 PNTr
/afs/cern.ch/work/s/sgallori/public/forAgnieszka/dataMC_weights/p_nTracks/
weights_DPi_2012_dw.root
weights_DPi_2012_up.root
###

#PIDK Pion 2011
/afs/cern.ch/work/s/sgallori/public/forAgnieszka/PIDCalib_histos/
Pi_Strip21r1_2011_MagDown_nTracks_P.root
Pi_Strip21r1_2011_MagUp_nTracks_P.root
###

#PIDK Pion 2012
/afs/cern.ch/work/s/sgallori/public/forAgnieszka/PIDCalib_histos/
Pi_Strip21_2012_MagDown_nTracks_P.root
Pi_Strip21_2012_MagUp_nTracks_P.root
###

#PIDK Kaon 2011
/afs/cern.ch/work/s/sgallori/public/forAgnieszka/PIDCalib_histos/
K_Strip21r1_2011_MagDown_nTracks_P.root
K_Strip21r1_2011_MagUp_nTracks_P.root
###

#PIDK Kaon 2012
/afs/cern.ch/work/s/sgallori/public/forAgnieszka/PIDCalib_histos/
K_Strip21_2012_MagDown_nTracks_P.root
K_Strip21_2012_MagUp_nTracks_P.root
###

#PIDK Proton 2011
/afs/cern.ch/work/s/sgallori/public/forAgnieszka/PIDCalib_histos/
P_Strip21r1_2011_MagDown_nTracks_P.root
P_Strip21r1_2011_MagUp_nTracks_P.root
###

#PIDK Proton 2012
/afs/cern.ch/work/s/sgallori/public/forAgnieszka/PIDCalib_histos/
P_Strip21_2012_MagDown_nTracks_P.root
P_Strip21_2012_MagUp_nTracks_P.root
###
