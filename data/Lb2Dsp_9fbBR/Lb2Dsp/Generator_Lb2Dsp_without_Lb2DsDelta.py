###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import copy
import math
import os
import sys

# from ROOT import *
# print(os.environ)


sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa

WORKSPACEFILE = (
    "/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/workspaces/syst/work_mc_lb2dsp_run2.root"
)
WORKSPACENAME = "workspace"


def YieldForYear(
    year, total_yield
):  # year, pol - strings, total_yield - sum for all years and polarities
    luminosities = {
        "2015": {"Down": 0.18695, "Up": 0.14105},
        "2016": {"Down": 0.85996, "Up": 0.80504},
        "2017": {"Down": 0.87689, "Up": 0.83311},
        "2018": {"Down": 1.04846, "Up": 1.14154},
    }
    total_luminosity = (
        luminosities["2015"]["Down"]
        + luminosities["2015"]["Up"]
        + luminosities["2016"]["Down"]
        + luminosities["2016"]["Up"]
        + luminosities["2017"]["Down"]
        + luminosities["2017"]["Up"]
        + luminosities["2018"]["Down"]
        + luminosities["2018"]["Up"]
    )

    if year == "20152016":
        yield_for_year = (
            (
                luminosities["2015"]["Down"]
                + luminosities["2015"]["Up"]
                + luminosities["2016"]["Down"]
                + luminosities["2016"]["Up"]
            )
            / total_luminosity
            * total_yield
        )
    else:
        yield_for_year = (
            (luminosities[year]["Down"] + luminosities[year]["Up"])
            / total_luminosity
            * total_yield
        )
    return yield_for_year


def getconfig():
    configdict = {}

    ############################################################
    # List of observables for all the PDFs.
    # The content of this dictionary determines the observables
    # to generate for and how may taggers are present.
    ############################################################
    configdict["Observables"] = {
        "BeautyMass": {
            "Type": "RooRealVar",
            "Title": "B mass (MeV/c^2)",
            "Range": [5200, 6200],
        }
    }

    ############################################################
    # List of mass hypotheses for bachelor
    # The content of this dictionary determines how many
    # bachelor PID bins the final dataset is splitted into
    ############################################################
    configdict["Hypothesys"] = ["Lb2Dsp"]

    ############################################################
    # Signal decay, Charm decay mode and year of data taking
    # Splitting per magnet polarity not implemented, at the moment
    ############################################################
    configdict["Decay"] = "Lb2Dsp"
    configdict["CharmModes"] = [
        "KKPi",
    ]
    configdict["Years"] = [
        "2015",
        "2016",
        "2017",
        "2018",
    ]

    ############################################################
    # For PIDK shapes we need also polarities
    # This is also used to split the yields between years properly
    ############################################################
    configdict["Polarity"] = ["Up", "Down"]
    configdict["IntegratedLuminosity"] = {
        "2015": {"Down": 0.18695, "Up": 0.14105},
        "2016": {"Down": 0.85996, "Up": 0.80504},
        "2017": {"Down": 0.87689, "Up": 0.83311},
        "2018": {"Down": 1.04846, "Up": 1.14154},
    }

    configdict["FractionsLuminosity"] = {
        k: v["Up"] / (v["Up"] + v["Down"])
        for k, v in configdict["IntegratedLuminosity"].items()
    }

    configdict["WorkspaceToRead"] = {"File": WORKSPACEFILE, "Workspace": WORKSPACENAME}
    ############################################################
    # List of components with yields to generate.
    # The content of this dictionary determines, for each
    # PID bin and year, how many PDF components are generated.
    # If there is only signal, a TTree ready for sFit is
    # generated directly, without need for doing a (useless)
    # mass fit.
    ############################################################

    # here I define the total yield for Lb2DsP - obtained in Lb2DsP Run2 analysis
    # Lb2Dsp_yield = 8.3848e+02 #+/-  3.19e+01
    # combinatorial_yield = 4.2840e+02 #+/-  3.81e+01
    # Bs2DsstKst_yield = 1.8199e+01 #+/-  1.81e+00
    # Bs2DsK_yield = 9.9927e+01 #+/-  1.03e+01
    # Bs2DsPi_yield = 2.0509e+02 #+/-  2.21e+01
    # Bs2DsstK_yield =  1.1545e+02 #+/-  1.14e+01
    # Bs2DsKst_yield = 1.6352e+01 #+/-  1.63e+00
    # Bs2DsstPi_yield = 2.0312e+02 #+/-  1.95e+01
    # Bs2DsRho_yield = 1.4102e+02 #+/-  1.35e+01
    # Bs2DsstRho_yield = 3.5118e+01 #+/-  3.47e+00
    # Lb2DsstP_yield = 9.2778e+02 #+/-  4.97e+01
    # Bd2DsK_yield = 4.0926e+01 #+/-  4.09e+00
    # Bd2DsstK_yield = 4.1077e+01 #+/-  4.08e+00

    Lb2Dsp_yield = 8.3097e02  # +/-  3.19e+01
    combinatorial_yield = 4.3731e02  # +/-  3.80e+01
    Bs2DsstKst_yield = 1.8241e01  # +/-  1.81e+00
    Bs2DsK_yield = 9.8732e01  # +/-  1.03e+01
    Bs2DsPi_yield = 2.0114e02  # +/-  2.21e+01
    Bs2DsstK_yield = 1.1568e02  # +/-  1.14e+01
    Bs2DsKst_yield = 1.6363e01  # +/-  1.63e+00
    Bs2DsstPi_yield = 2.0300e02  # +/-  1.95e+01
    Bs2DsRho_yield = 1.4181e02  # +/-  1.35e+01
    Bs2DsstRho_yield = 3.5262e01  # +/-  3.47e+00
    Lb2DsstP_yield = 9.3136e02  # +/-  4.96e+01
    Bd2DsK_yield = 4.0720e01  # +/-  4.09e+00
    Bd2DsstK_yield = 4.1208e01  # +/-  4.08e+00

    configdict["Components"] = {
        "Signal": {
            "Lb2Dsp": {
                "2015": {
                    "KKPi": YieldForYear("2015", Lb2Dsp_yield),
                },
                "2016": {
                    "KKPi": YieldForYear("2016", Lb2Dsp_yield),
                },
                "2017": {
                    "KKPi": YieldForYear("2017", Lb2Dsp_yield),
                },
                "2018": {
                    "KKPi": YieldForYear("2018", Lb2Dsp_yield),
                },
            }
        },
        "Combinatorial": {
            "Lb2Dsp": {
                "2015": {
                    "KKPi": YieldForYear("2015", combinatorial_yield),
                },
                "2016": {
                    "KKPi": YieldForYear("2016", combinatorial_yield),
                },
                "2017": {
                    "KKPi": YieldForYear("2017", combinatorial_yield),
                },
                "2018": {
                    "KKPi": YieldForYear("2018", combinatorial_yield),
                },
            }
        },
        "Bs2DsK": {
            "Lb2Dsp": {
                "2015": {
                    "KKPi": YieldForYear("2015", Bs2DsK_yield),
                },
                "2016": {
                    "KKPi": YieldForYear("2016", Bs2DsK_yield),
                },
                "2017": {
                    "KKPi": YieldForYear("2017", Bs2DsK_yield),
                },
                "2018": {
                    "KKPi": YieldForYear("2018", Bs2DsK_yield),
                },
            }
        },
        "Bd2DsK": {
            "Lb2Dsp": {
                "2015": {
                    "KKPi": YieldForYear("2015", Bd2DsK_yield),
                },
                "2016": {
                    "KKPi": YieldForYear("2016", Bd2DsK_yield),
                },
                "2017": {
                    "KKPi": YieldForYear("2017", Bd2DsK_yield),
                },
                "2018": {
                    "KKPi": YieldForYear("2018", Bd2DsK_yield),
                },
            }
        },
        "Lb2Dsstp": {
            "Lb2Dsp": {
                "2015": {
                    "KKPi": YieldForYear("2015", Lb2DsstP_yield),
                },
                "2016": {
                    "KKPi": YieldForYear("2016", Lb2DsstP_yield),
                },
                "2017": {
                    "KKPi": YieldForYear("2017", Lb2DsstP_yield),
                },
                "2018": {
                    "KKPi": YieldForYear("2018", Lb2DsstP_yield),
                },
            }
        },
        "Bs2DsPi": {
            "Lb2Dsp": {
                "2015": {
                    "KKPi": YieldForYear("2015", Bs2DsPi_yield),
                },
                "2016": {
                    "KKPi": YieldForYear("2016", Bs2DsPi_yield),
                },
                "2017": {
                    "KKPi": YieldForYear("2017", Bs2DsPi_yield),
                },
                "2018": {
                    "KKPi": YieldForYear("2018", Bs2DsPi_yield),
                },
            }
        },
        "Bs2DsstPi": {
            "Lb2Dsp": {
                "2015": {
                    "KKPi": YieldForYear("2015", Bs2DsstPi_yield),
                },
                "2016": {
                    "KKPi": YieldForYear("2016", Bs2DsstPi_yield),
                },
                "2017": {
                    "KKPi": YieldForYear("2017", Bs2DsstPi_yield),
                },
                "2018": {
                    "KKPi": YieldForYear("2018", Bs2DsstPi_yield),
                },
            }
        },
        "Bs2DsRho": {
            "Lb2Dsp": {
                "2015": {
                    "KKPi": YieldForYear("2015", Bs2DsRho_yield),
                },
                "2016": {
                    "KKPi": YieldForYear("2016", Bs2DsRho_yield),
                },
                "2017": {
                    "KKPi": YieldForYear("2017", Bs2DsRho_yield),
                },
                "2018": {
                    "KKPi": YieldForYear("2018", Bs2DsRho_yield),
                },
            }
        },
        "Bs2DsstRho": {
            "Lb2Dsp": {
                "2015": {
                    "KKPi": YieldForYear("2015", Bs2DsstRho_yield),
                },
                "2016": {
                    "KKPi": YieldForYear("2016", Bs2DsstRho_yield),
                },
                "2017": {
                    "KKPi": YieldForYear("2017", Bs2DsstRho_yield),
                },
                "2018": {
                    "KKPi": YieldForYear("2018", Bs2DsstRho_yield),
                },
            }
        },
        "Bs2DsstK": {
            "Lb2Dsp": {
                "2015": {
                    "KKPi": YieldForYear("2015", Bs2DsstK_yield),
                },
                "2016": {
                    "KKPi": YieldForYear("2016", Bs2DsstK_yield),
                },
                "2017": {
                    "KKPi": YieldForYear("2017", Bs2DsstK_yield),
                },
                "2018": {
                    "KKPi": YieldForYear("2018", Bs2DsstK_yield),
                },
            }
        },
        "Bd2DsstK": {
            "Lb2Dsp": {
                "2015": {
                    "KKPi": YieldForYear("2015", Bd2DsstK_yield),
                },
                "2016": {
                    "KKPi": YieldForYear("2016", Bd2DsstK_yield),
                },
                "2017": {
                    "KKPi": YieldForYear("2017", Bd2DsstK_yield),
                },
                "2018": {
                    "KKPi": YieldForYear("2018", Bd2DsstK_yield),
                },
            }
        },
        "Bs2DsKst": {
            "Lb2Dsp": {
                "2015": {
                    "KKPi": YieldForYear("2015", Bs2DsKst_yield),
                },
                "2016": {
                    "KKPi": YieldForYear("2016", Bs2DsKst_yield),
                },
                "2017": {
                    "KKPi": YieldForYear("2017", Bs2DsKst_yield),
                },
                "2018": {
                    "KKPi": YieldForYear("2018", Bs2DsKst_yield),
                },
            }
        },
        "Bs2DsstKst": {
            "Lb2Dsp": {
                "2015": {
                    "KKPi": YieldForYear("2015", Bs2DsstKst_yield),
                },
                "2016": {
                    "KKPi": YieldForYear("2016", Bs2DsstKst_yield),
                },
                "2017": {
                    "KKPi": YieldForYear("2017", Bs2DsstKst_yield),
                },
                "2018": {
                    "KKPi": YieldForYear("2018", Bs2DsstKst_yield),
                },
            }
        },
    }

    ############################################################
    # List of PDFs for "time-independent" observables
    # Dictionary structure: observable->component->bachelor hypo->year->D mode
    ############################################################

    ############################################################
    #                      Signal
    ############################################################

    ############################################################

    def getPdfTemplateConfigFromWorkspace(
        name="Signal_BeautyMass_IpatiaPlusJohnsonSU_both_{mode}_{year}",
        modes=["All"],
        years=["2015", "2016", "2017", "2018"],
        lowercasemode=True,
        **kwargs
    ):
        """
        Generate a dictionary of the form
        {
            year1: {
                mode1: {
                    'Type': 'FromWorkspace',
                    'Name': name.format(mode=m1),
                    'WorkspaceName': WorkspaceName,
                    'WorkspaceFile': WorkspaceFile,
                    **kwargs
                },
                mode2: { ... }
            },
            year2: { ... }
        }
        for all modes m1... given in the `modes` and all years in the `years`
        argument.
        """
        return {
            y: {
                m: dict(
                    Type="FromWorkspace",
                    Name=name.format(mode=m.lower() if lowercasemode else m, year=y),
                    **kwargs
                )
                for m in modes
            }
            for y in years
        }

    # parameters from Jordy's update
    # data parameters from refits - changed 24.04.2022

    sigmaJ_Lb2Dsp_MC = 13.82495  # +/- 0.30
    sigmaJ_Lb2LcPi_MC = 13.92  # +/- 0.11
    sigmaJ_Lb2LcPi_data = 15.159  # +/-  0.0431

    sigmaI_Lb2Dsp_MC = 20.70233  # +/- 2.6
    sigmaI_Lb2LcPi_MC = 25.57  # +/- 0.64
    sigmaI_Lb2LcPi_data = 29.006  # +/-  0.348

    # to translate Lb2Dsp signal MC parameters to Lb2Dsp signal
    # data parameters I just multiply the MC parameters via ratio
    # - from Lb2LcPi sample
    ratio_sigmaJ = sigmaJ_Lb2LcPi_data / sigmaJ_Lb2LcPi_MC
    ratio_sigmaI = sigmaI_Lb2LcPi_data / sigmaI_Lb2LcPi_MC

    # calculating uncertainty for sigmaI in data
    u2_sigmaI_Lb2Dsp_MC = 2.6**2

    u_sigmaI_Lb2LcPi_data = 0.348
    u_sigmaI_Lb2LcPi_MC = 0.64
    u2_ratio_sigmaI = (ratio_sigmaI / sigmaI_Lb2LcPi_MC * u_sigmaI_Lb2LcPi_MC) ** 2 + (
        u_sigmaI_Lb2LcPi_data / sigmaI_Lb2LcPi_MC
    ) ** 2

    math.sqrt(
        u2_sigmaI_Lb2Dsp_MC * ratio_sigmaI * ratio_sigmaI
        + sigmaI_Lb2Dsp_MC * sigmaI_Lb2Dsp_MC * u2_ratio_sigmaI
    )

    # calculating uncertainty for sigmaJ in data

    u2_sigmaJ_Lb2Dsp_MC = 0.30**2

    u_sigmaJ_Lb2LcPi_data = 0.0431
    u_sigmaJ_Lb2LcPi_MC = 0.11
    u2_ratio_sigmaJ = (ratio_sigmaJ / sigmaJ_Lb2LcPi_MC * u_sigmaJ_Lb2LcPi_MC) ** 2 + (
        u_sigmaJ_Lb2LcPi_data / sigmaJ_Lb2LcPi_MC
    ) ** 2

    math.sqrt(
        u2_sigmaJ_Lb2Dsp_MC * ratio_sigmaJ * ratio_sigmaJ
        + sigmaJ_Lb2Dsp_MC * sigmaJ_Lb2Dsp_MC * u2_ratio_sigmaJ
    )

    configdict["PDFList"] = {
        "BeautyMass": {
            "Signal": {
                "Lb2Dsp": {
                    "2015": {
                        "KKPi": {
                            "a1": 1.04495,  # +/- 0.094
                            "a2": 1.5666,  # +/- 0.29
                            "fracI": 0.25,
                            "l": -1.7000e00,
                            "n1": 1.5891,  # +/-  0.067
                            "n2": 2.16415,  # +/-  0.17
                            "nu": -0.175257,  # +/-  0.065
                            "sigmaI": sigmaI_Lb2Dsp_MC
                            * ratio_sigmaI,  # +/- u_sigmaI_Lb2Dsp_data
                            "sigmaJ": sigmaJ_Lb2Dsp_MC
                            * ratio_sigmaJ,  # +/- u_sigmaJ_Lb2Dsp_data
                            "tau": 0.34947,  # +/- 0.018
                            "fb": 0.0,
                            "mean": 5620.78865,
                            # value taken from master config +/-  0.080
                            "zeta": 0.0,
                            "Type": "IpatiaJohnsonSU",
                        },
                    },
                    "2016": {
                        "KKPi": {
                            "a1": 1.04495,  # +/- 0.094
                            "a2": 1.5666,  # +/- 0.29
                            "fracI": 0.25,
                            "l": -1.7000e00,
                            "n1": 1.5891,  # +/-  0.067
                            "n2": 2.16415,  # +/-  0.17
                            "nu": -0.175257,  # +/-  0.065
                            "sigmaI": sigmaI_Lb2Dsp_MC
                            * ratio_sigmaI,  # +/- u_sigmaI_Lb2Dsp_data
                            "sigmaJ": sigmaJ_Lb2Dsp_MC
                            * ratio_sigmaJ,  # +/- u_sigmaJ_Lb2Dsp_data
                            "tau": 0.34947,  # +/- 0.018
                            "fb": 0.0,
                            "mean": 5620.78865,
                            # value taken from master config +/-  0.080
                            "zeta": 0.0,
                            "Type": "IpatiaJohnsonSU",
                        },
                    },
                    "2017": {
                        "KKPi": {
                            "a1": 1.04495,  # +/- 0.094
                            "a2": 1.5666,  # +/- 0.29
                            "fracI": 0.25,
                            "l": -1.7000e00,
                            "n1": 1.5891,  # +/-  0.067
                            "n2": 2.16415,  # +/-  0.17
                            "nu": -0.175257,  # +/-  0.065
                            "sigmaI": sigmaI_Lb2Dsp_MC
                            * ratio_sigmaI,  # +/- u_sigmaI_Lb2Dsp_data
                            "sigmaJ": sigmaJ_Lb2Dsp_MC
                            * ratio_sigmaJ,  # +/- u_sigmaJ_Lb2Dsp_data
                            "tau": 0.34947,  # +/- 0.018
                            "fb": 0.0,
                            "mean": 5620.78865,
                            # value taken from master config +/-  0.080
                            "zeta": 0.0,
                            "Type": "IpatiaJohnsonSU",
                        },
                    },
                    "2018": {
                        "KKPi": {
                            "a1": 1.04495,  # +/- 0.094
                            "a2": 1.5666,  # +/- 0.29
                            "fracI": 0.25,
                            "l": -1.7000e00,
                            "n1": 1.5891,  # +/-  0.067
                            "n2": 2.16415,  # +/-  0.17
                            "nu": -0.175257,  # +/-  0.065
                            "sigmaI": sigmaI_Lb2Dsp_MC
                            * ratio_sigmaI,  # +/- u_sigmaI_Lb2Dsp_data
                            "sigmaJ": sigmaJ_Lb2Dsp_MC
                            * ratio_sigmaJ,  # +/- u_sigmaJ_Lb2Dsp_data
                            "tau": 0.34947,  # +/- 0.018
                            "fb": 0.0,
                            "mean": 5620.78865,
                            # value taken from master config +/-  0.080
                            "zeta": 0.0,
                            "Type": "IpatiaJohnsonSU",
                        },
                    },
                }
            },
            "Combinatorial": {
                "Lb2Dsp": {
                    "2015": {
                        "KKPi": {
                            "c": 1.2200e02,
                            "m0": 5.1670e03,
                            "a": -6.3167e00,
                            "b": 0.0000e00,
                            "Type": "MassDiff",
                        },
                    },
                    "2016": {
                        "KKPi": {
                            "c": 1.2200e02,
                            "m0": 5.1670e03,
                            "a": -6.3167e00,
                            "b": 0.0000e00,
                            "Type": "MassDiff",
                        },
                    },
                    "2017": {
                        "KKPi": {
                            "c": 1.2200e02,
                            "m0": 5.1670e03,
                            "a": -6.3167e00,
                            "b": 0.0000e00,
                            "Type": "MassDiff",
                        },
                    },
                    "2018": {
                        "KKPi": {
                            "c": 1.2200e02,
                            "m0": 5.1670e03,
                            "a": -6.3167e00,
                            "b": 0.0000e00,
                            "Type": "MassDiff",
                        },
                    },
                }
            },
            "Lb2Dsstp": {
                "Lb2Dsp": {
                    "2015": {
                        "KKPi": {
                            "ahill": 5248.47329,  # +/-  6.9
                            "bhill": 5532.30986,  # +/-  3.1
                            "csihill": 0.32533,  # +/-  0.054
                            "sigma": 38.23656,  # +/-  1.5
                            "ahorns": 5253.23423,  # +/-  0.44
                            "bhorns": 5562.55112,  # +/-  0.54
                            "csihorns": 0.59192,  # +/-  0.022
                            "sigmahorns": 13.53707,  # +/-  0.44
                            "R": 4.95615,  # +/-  0.28
                            "frac": 0.993,  # +/-  0.0010
                            "frachorns": 0.31183,  # +/-  0.0091
                            "shift": 0.0000e00,
                            "Type": "HILLdiniPlusHORNSdini",
                        },
                    },
                    "2016": {
                        "KKPi": {
                            "ahill": 5248.47329,  # +/-  6.9
                            "bhill": 5532.30986,  # +/-  3.1
                            "csihill": 0.32533,  # +/-  0.054
                            "sigma": 38.23656,  # +/-  1.5
                            "ahorns": 5253.23423,  # +/-  0.44
                            "bhorns": 5562.55112,  # +/-  0.54
                            "csihorns": 0.59192,  # +/-  0.022
                            "sigmahorns": 13.53707,  # +/-  0.44
                            "R": 4.95615,  # +/-  0.28
                            "frac": 0.993,  # +/-  0.0010
                            "frachorns": 0.31183,  # +/-  0.0091
                            "shift": 0.0000e00,
                            "Type": "HILLdiniPlusHORNSdini",
                        },
                    },
                    "2017": {
                        "KKPi": {
                            "ahill": 5248.47329,  # +/-  6.9
                            "bhill": 5532.30986,  # +/-  3.1
                            "csihill": 0.32533,  # +/-  0.054
                            "sigma": 38.23656,  # +/-  1.5
                            "ahorns": 5253.23423,  # +/-  0.44
                            "bhorns": 5562.55112,  # +/-  0.54
                            "csihorns": 0.59192,  # +/-  0.022
                            "sigmahorns": 13.53707,  # +/-  0.44
                            "R": 4.95615,  # +/-  0.28
                            "frac": 0.993,  # +/-  0.0010
                            "frachorns": 0.31183,  # +/-  0.0091
                            "shift": 0.0000e00,
                            "Type": "HILLdiniPlusHORNSdini",
                        },
                    },
                    "2018": {
                        "KKPi": {
                            "ahill": 5248.47329,  # +/-  6.9
                            "bhill": 5532.30986,  # +/-  3.1
                            "csihill": 0.32533,  # +/-  0.054
                            "sigma": 38.23656,  # +/-  1.5
                            "ahorns": 5253.23423,  # +/-  0.44
                            "bhorns": 5562.55112,  # +/-  0.54
                            "csihorns": 0.59192,  # +/-  0.022
                            "sigmahorns": 13.53707,  # +/-  0.44
                            "R": 4.95615,  # +/-  0.28
                            "frac": 0.993,  # +/-  0.0010
                            "frachorns": 0.31183,  # +/-  0.0091
                            "shift": 0.0000e00,
                            "Type": "HILLdiniPlusHORNSdini",
                        },
                    },
                }
            },
            "Bs2DsstK": {
                "Lb2Dsp": {
                    "2015": {
                        "KKPi": {
                            "mean": 5354.07669,  # +/-  1.4
                            "sigma": 124.82455,  # +/-  5.8
                            "alpha": -0.797316,  # +/-  0.046
                            "n": 49.76984,  # +/-  36.
                            "Type": "CrystalBall",
                        },
                    },
                    "2016": {
                        "KKPi": {
                            "mean": 5354.07669,  # +/-  1.4
                            "sigma": 124.82455,  # +/-  5.8
                            "alpha": -0.797316,  # +/-  0.046
                            "n": 49.76984,  # +/-  36.
                            "Type": "CrystalBall",
                        },
                    },
                    "2017": {
                        "KKPi": {
                            "mean": 5354.07669,  # +/-  1.4
                            "sigma": 124.82455,  # +/-  5.8
                            "alpha": -0.797316,  # +/-  0.046
                            "n": 49.76984,  # +/-  36.
                            "Type": "CrystalBall",
                        },
                    },
                    "2018": {
                        "KKPi": {
                            "mean": 5354.07669,  # +/-  1.4
                            "sigma": 124.82455,  # +/-  5.8
                            "alpha": -0.797316,  # +/-  0.046
                            "n": 49.76984,  # +/-  36.
                            "Type": "CrystalBall",
                        },
                    },
                }
            },
            "Bs2DsK": {
                "Lb2Dsp": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBs2DsKPdf_m_both_{year}"
                ),
            },
            "Bd2DsK": {
                "Lb2Dsp": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBd2DsKPdf_m_both_{year}"
                ),
            },
            "Bs2DsKst": {
                "Lb2Dsp": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBs2DsKstPdf_m_both_{year}"
                ),
            },
            "Bs2DsstKst": {
                "Lb2Dsp": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBs2DsstKstPdf_m_both_{year}"
                ),
            },
            "Bs2DsPi": {
                "Lb2Dsp": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBs2DsPiPdf_m_both_{year}"
                ),
            },
            "Bs2DsstPi": {
                "Lb2Dsp": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBs2DsstPiPdf_m_both_{year}"
                ),
            },
            "Bs2DsRho": {
                "Lb2Dsp": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBs2DsRhoPdf_m_both_{year}"
                ),
            },
            "Bs2DsstRho": {
                "Lb2Dsp": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBs2DsstRhoPdf_m_both_{year}"
                ),
            },
        },
    }

    # copy config from Bs2DsstK to Bd2DsstK and adjust mass shift
    configdict["PDFList"]["BeautyMass"]["Bd2DsstK"] = copy.deepcopy(
        configdict["PDFList"]["BeautyMass"]["Bs2DsstK"]
    )
    for yearkey in configdict["PDFList"]["BeautyMass"]["Bd2DsstK"]["Lb2Dsp"]:
        if yearkey != "all":
            configdict["PDFList"]["BeautyMass"]["Bd2DsstK"]["Lb2Dsp"][yearkey]["KKPi"][
                "mean"
            ] -= 87.23

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    from configutils import main

    main(getconfig())
