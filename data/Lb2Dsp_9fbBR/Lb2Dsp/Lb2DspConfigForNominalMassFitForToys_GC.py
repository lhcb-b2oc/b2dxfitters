###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Lb2DspConfigForNominalMassFitForToys import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    configdict["Yields"]["Bs2DsPi"] = {
        "Run2": {"KKPi": [2.2070e02, 2.29e01]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstPi"] = {
        "Run2": {"KKPi": [1.9516e02, 1.97e01]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsRho"] = {
        "Run2": {"KKPi": [1.3682e02, 1.37e01]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstRho"] = {
        "Run2": {"KKPi": [3.4742e01, 3.48e00]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsK"] = {
        "Run2": {"KKPi": [1.0047e02, 1.03e01]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bd2DsK"] = {
        "Run2": {"KKPi": [4.0675e01, 4.09e00]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstK"] = {
        "Run2": {"KKPi": [1.1334e02, 1.15e01]},
        "Fixed": False,
        "Constrained": True,
    }

    configdict["Yields"]["Bd2DsstK"] = {
        "Run2": {"KKPi": [4.0735e01, 4.09e00]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsKst"] = {
        "Run2": {"KKPi": [1.6304e01, 1.63e00]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstKst"] = {
        "Run2": {"KKPi": [1.8094e01, 1.81e00]},
        "Fixed": False,
        "Constrained": True,
    }

    return configdict
