# Contact:
in case of troubles contact: [Agnieszka Dziurda](agnieszka.dziurda@cern.ch)

# Files:
- Lb2DspConfigForNominalMassFit.py
  - nominal config file
- Lb2DspConfigForDataWorkspace.py
  - config file for data preparation,
  - inherits from Lb2DspConfigForNominalMassFit.py,
  - BDT range changed to [-1,1],
- Lb2DspConfigForMCWorkspace.py
  - config file for MC templates preparation,
  - inherits from Lb2DspConfigForNominalMassFit.py
  - additional informations for MC weighting
- config_Lb2Dsp.txt
  - paths to files

# Saving logfile

Please use one of two options:
- >& logfile.txt &
- |& tee logfile.txt

# How to run code

You need to be in:
cd UraniaDev_v7r0/PhysFit/B2DXFitters/scripts/

# Obtaining data sample:
```
../../../run python prepareWorkspace.py --configName ../data/Lb2Dsp_5fbBR/Lb2Dsp/Lb2DspConfigForDataWorkspace.py --Data --debug -s work_data_lb2dsp.root >& log_work_data_lb2dsp.txt &
```

# Obtaining MC templates:
Due to memory limits probably you won't be able to do it in one go.
The best is to split by year:
- 2015
```
../../../run python prepareWorkspace.py --configName ../data/Lb2Dsp_5fbBR/Lb2Dsp/Lb2DspConfigForMCWorkspace.py --MC --debug -s work_mc_lb2dsp_2015.root --year 2015 >& log_work_mc_lb2dsp_2015.txt &
```
- 2016
```
../../../run python prepareWorkspace.py --configName ../data/Lb2Dsp_5fbBR/Lb2Dsp/Lb2DspConfigForMCWorkspace.py --MC --debug -i work_mc_lb2dsp_2015.root -s work_mc_lb2dsp_20152016.root --year 2016 >& log_work_mc_lb2dsp_20152016.txt &
```
- 2017
```
../../../run python prepareWorkspace.py --configName ../data/Lb2Dsp_5fbBR/Lb2Dsp/Lb2DspConfigForMCWorkspace.py --MC --debug -i work_mc_lb2dsp_20152016.root -s work_mc_lb2dsp_201520162017.root --year 2017 >& log_work_mc_lb2dsp_201520162017.txt &
```
- 2018
```
../../../run python prepareWorkspace.py --configName ../data/Lb2Dsp_5fbBR/Lb2Dsp/Lb2DspConfigForMCWorkspace.py --MC --debug -i work_mc_lb2dsp_201520162017.root -s work_mc_lb2dsp_2015201620172018.root --year 2018 >& log_work_mc_lb2dsp_2015201620172018.txt &
```

# Performing MDFit
```
../../../run python runMDFitter.py  --configName ../data/Lb2Dsp_5fbBR/Lb2Dsp/Lb2DspConfigForNominalMassFit.py --fileName /eos/lhcb/wg/b2oc/Lb2Dsp/workspaces/work_mc_lb2dsp_2015201620172018.root --fileData /eos/lhcb/wg/b2oc/Lb2Dsp/workspaces/work_data_lb2dsp.root --mode kkpi --year 2015 2016 2017 2018 --pol both --merge pol run2 -s WS_MDFit_Lb2Dsp.root --debug >& log_mdfit_lb2dsp.txt &
```

# Plotting results
```
../../../run python plotMDFitter.py WS_MDFit_Lb2Dsp.root --configName ../data/Lb2Dsp_5fbBR/Lb2Dsp/Lb2DspConfigForNominalMassFit.py --mode kkpi --year 2015 2016 2017 2018 --pol both --merge pol run2
```
