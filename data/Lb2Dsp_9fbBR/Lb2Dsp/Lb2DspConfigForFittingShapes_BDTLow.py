###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Lb2DspConfigForNominalMassFit_BDTLow import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    configdict["pdfList"] = {}

    configdict["pdfList"]["Signal"] = {}
    configdict["pdfList"]["Signal"]["Lb2Dsp"] = {}
    configdict["pdfList"]["Signal"]["Lb2Dsp"]["BeautyMass"] = {}
    configdict["pdfList"]["Signal"]["Lb2Dsp"]["BeautyMass"]["DoubleCrystalBall"] = {
        "Title": "B_{s}#rightarrowD_{s}#pi",
        "Bins": 200,
        "Min": 5200.0,
        "Max": 6200.0,
        "Unit": "MeV/c^{2}",
        "sameMean": True,
        "mean": [5623.0, 5500, 5700],
        "sigma1": [18.74, 1.0, 100.0],
        "sigma2": [12.23, 1.0, 100.0],
        "alpha1": [1.657, 0.01, 6.0],
        "alpha2": [-2.063, -6.0, -0.01],
        "n1": [1.518, 0.01, 6.0],
        "n2": [2.30, 0.01, 6.0],
        "frac": [0.5],
    }

    configdict["pdfList"]["Signal"]["Lb2Dsp"]["BeautyMass"]["Ipatia"] = {
        "Title": "B_{s}#rightarrowD_{s}#pi",
        "Bins": 200,
        "Min": 5200.0,
        "Max": 6200.0,
        "Unit": "MeV/c^{2}",
        "mean": [5621.0, 5500, 5700],
        "sigma": [18.74, 1.0, 100.0],
        "zeta": [0.0],
        "fb": [0.0],
        "l": [-1.47, -20.0, -0.01],
        "a1": [2.005, 0.01, 6.0],
        "a2": [3.07, 0.01, 6.0],
        "n1": [1.365, 0.01, 6.0],
        "n2": [2.26, 0.01, 6.0],
    }

    configdict["pdfList"]["Signal"]["Lb2Dsp"]["BeautyMass"]["IpatiaPlusJohnsonSU"] = {
        "Title": "B_{s}#rightarrowD_{s}#pi",
        "Bins": 200,
        "Min": 5200.0,
        "Max": 6200.0,
        "Unit": "MeV/c^{2}",
        "mean": [5620.6, 5500, 5700],
        "sigmaI": [14.5, 10.0, 50.0],
        "sigmaJ": [14.2, 10.0, 50.0],
        "zeta": [0.0],
        "fb": [0.0],
        "a1": [1.4, 0.2, 5.0],
        "a2": [1.7, 0.2, 5.0],
        "n1": [1.6, 0.01, 6.0],
        "n2": [2.1, 0.01, 3.0],
        "l": [-1.7],  # for stability
        "tau": [0.3, 0.0, 2.0],
        "nu": [-0.1, -2.0, 0.5],
        "fracI": [0.25],  # for stability
    }

    configdict["pdfList"]["Lb2Dsstp"] = {}
    configdict["pdfList"]["Lb2Dsstp"]["Lb2Dsp"] = {}
    configdict["pdfList"]["Lb2Dsstp"]["Lb2Dsp"]["BeautyMass"] = {}
    configdict["pdfList"]["Lb2Dsstp"]["Lb2Dsp"]["BeautyMass"][
        "HILLdiniPlusHORNSdini"
    ] = {
        "Title": "B_{s}#rightarrowD_{s}#rho",
        "Bins": 200,
        "Min": 5200.0,
        "Max": 6200.0,
        "Unit": "MeV/c^{2}",
        "ahill": [5250, 4500, 5300],
        "bhill": [5540, 5300, 5800],
        "csihill": [0.4, -2.0, 2.0],
        "sigma": [40.0, 2.0, 80.0],
        "ahorns": [5255.0, 5200, 5325],
        "bhorns": [5560.0, 5450, 5700],
        "csihorns": [0.4, 0.1, 1.8],
        "sigmahorns": [14.0, 3.0, 30.0],
        "shift": [0.0],
        "R": [5.2, 0.5, 40.0],
        "frac": [0.98, 0.0, 1.0],
        "frachorns": [0.3, 0.0, 0.7],
    }

    configdict["pdfList"]["Bs2DsstK"] = {}
    configdict["pdfList"]["Bs2DsstK"]["Lb2Dsp"] = {}
    configdict["pdfList"]["Bs2DsstK"]["Lb2Dsp"]["BeautyMass"] = {}
    configdict["pdfList"]["Bs2DsstK"]["Lb2Dsp"]["BeautyMass"]["HORNSdini"] = {
        "Title": "B_{s}#rightarrowD_{s}#rho",
        "Bins": 100,
        "Min": 5200.0,
        "Max": 6200.0,
        "Unit": "MeV/c^{2}",
        "a": [5000, 3500, 5200],
        "b": [5300, 4300, 5500],
        "csi": [1.1, -2.0, 2.0],
        "sigma": [60.0, 2.0, 100.0],
        "shift": [0.0],
        "R": [4.8, 0.5, 40.0],
        "frac": [0.5, 0.0, 1.0],
    }

    configdict["pdfList"]["Bs2DsstK"]["Lb2Dsp"]["BeautyMass"]["CrystalBall"] = {
        "Title": "B_{s}#rightarrowD_{s}#pi",
        "Bins": 200,
        "Min": 5200.0,
        "Max": 6200.0,
        "Unit": "MeV/c^{2}",
        "mean": [5333.0, 5000, 5700],
        "sigma": [108.0, 1.0, 150.0],
        "alpha": [-0.6, -6.0, -0.01],
        "n": [48.0, 0.01, 50.0],
    }

    configdict["pdfList"]["Bs2DsstK"]["Lb2Dsp"]["BeautyMass"]["DoubleCrystalBall"] = {
        "Title": "B_{s}#rightarrowD_{s}#pi",
        "Bins": 200,
        "Min": 5200.0,
        "Max": 6200.0,
        "Unit": "MeV/c^{2}",
        "sameMean": True,
        "mean": [5300.0, 5000, 5700],
        "sigma1": [50.74, 1.0, 100.0],
        "sigma2": [80.23, 1.0, 150.0],
        "alpha1": [1.657, 0.01, 6.0],
        "alpha2": [-2.063, -6.0, -0.01],
        "n1": [0.05, 0.001, 6.0],
        "n2": [6.0, 0.01, 50.0],
        "frac": [0.5, 0.0, 1.0],
    }

    # Dataset prefix
    configdict["dataSetPrefix"] = {
        "Signal": "dataSetMC_Lb2Dsp_",
        "Lb2Dsstp": "dataSetMC_Lb2Dsstp_",
        "Bs2DsstK": "dataSetMC_Bs2DsstK_",
    }

    # Axes titles
    configdict["AxisTitle"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {"Lb2Dsp": "m(D_{s}^{-}p) [MeV/c^{2}]"}

    return configdict
