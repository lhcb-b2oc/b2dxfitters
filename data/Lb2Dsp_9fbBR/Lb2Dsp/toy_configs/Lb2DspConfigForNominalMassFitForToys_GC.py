###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import math


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Lb2Dsp"
    configdict["CharmModes"] = {"KKPi"}
    configdict["Backgrounds"] = [
        "Bs2DsPi",
        "Bs2DsstPi",
        "Bs2DsRho",
        "Bs2DsstRho",
        "Bd2DsK",
        "Bs2DsK",
        "Bs2DsstK",
        "Bs2DsKst",
        "Bs2DsstKst",
        "Bd2DsstK",
        "Lb2Dsstp",
    ]

    # year of data taking
    configdict["YearOfDataTaking"] = {"2011", "2012", "2015", "2016", "2017", "2018"}

    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2011": {"Down": 0.5600, "Up": 0.4200},
        "2012": {"Down": 0.9912, "Up": 0.9988},
        "2015": {"Down": 0.18695, "Up": 0.14105},
        "2016": {"Down": 0.85996, "Up": 0.80504},
        "2017": {"Down": 0.87689, "Up": 0.83311},
        "2018": {"Down": 1.04846, "Up": 1.14154},
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/Lb2Dsp_9fbBR/Lb2Dsp/config_Lb2Dsp.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotLb2Dsp", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5200, 6200],
        "InputName": "lab0_MassHypo_Dsp",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1948, 1988],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-150.0, 150.0],
        "InputName": "lab1_ProbNNp",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [10000.0, 150000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [1000.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 500.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3",
    }

    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 2500.0],
        "InputName": "lab0_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 3000.0],
        "InputName": "lab2_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab0_ENDVERTEX_CHI2",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab2_ENDVERTEX_CHI2",
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "",
        "MC": "",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": False,
        "DsHypo": True,
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    #######################################################################
    ################                FITTING                ################
    #######################################################################

    # parameters from Jordy's update - table 33
    # data parameters from refits - changed 16.02.2022

    sigmaJ_Lb2Dsp_MC = 14.09  # +/- 0.17
    sigmaJ_Lb2LcPi_MC = 13.92  # +/- 0.11
    sigmaJ_Lb2LcPi_data = 15.159  # +/-  0.0431

    sigmaI_Lb2Dsp_MC = 20.5  # +/- 1.3
    sigmaI_Lb2LcPi_MC = 25.57  # +/- 0.64
    sigmaI_Lb2LcPi_data = 29.006  # +/-  0.348

    # to translate Lb2Dsp signal MC parameters
    # to Lb2Dsp signal data parameters
    # I just multiply the MC parameters via ratio
    # - from Lb2LcPi sample
    ratio_sigmaJ = sigmaJ_Lb2LcPi_data / sigmaJ_Lb2LcPi_MC
    ratio_sigmaI = sigmaI_Lb2LcPi_data / sigmaI_Lb2LcPi_MC

    # calculating uncertainty for sigmaI in data
    u2_sigmaI_Lb2Dsp_MC = 1.3**2

    u_sigmaI_Lb2LcPi_data = 0.348
    u_sigmaI_Lb2LcPi_MC = 0.64
    u2_ratio_sigmaI = (ratio_sigmaI / sigmaI_Lb2LcPi_MC * u_sigmaI_Lb2LcPi_MC) ** 2 + (
        u_sigmaI_Lb2LcPi_data / sigmaI_Lb2LcPi_MC
    ) ** 2

    u_sigmaI_Lb2Dsp_data = math.sqrt(
        u2_sigmaI_Lb2Dsp_MC * ratio_sigmaI * ratio_sigmaI
        + sigmaI_Lb2Dsp_MC * sigmaI_Lb2Dsp_MC * u2_ratio_sigmaI
    )

    # calculating uncertainty for sigmaJ in data

    u2_sigmaJ_Lb2Dsp_MC = 0.17**2

    u_sigmaJ_Lb2LcPi_data = 0.0431
    u_sigmaJ_Lb2LcPi_MC = 0.11
    u2_ratio_sigmaJ = (ratio_sigmaJ / sigmaJ_Lb2LcPi_MC * u_sigmaJ_Lb2LcPi_MC) ** 2 + (
        u_sigmaJ_Lb2LcPi_data / sigmaJ_Lb2LcPi_MC
    ) ** 2

    u_sigmaJ_Lb2Dsp_data = math.sqrt(
        u2_sigmaJ_Lb2Dsp_MC * ratio_sigmaJ * ratio_sigmaJ
        + sigmaJ_Lb2Dsp_MC * sigmaJ_Lb2Dsp_MC * u2_ratio_sigmaJ
    )

    print(sigmaI_Lb2Dsp_MC * ratio_sigmaI, "+/-", u_sigmaI_Lb2Dsp_data)
    print(sigmaJ_Lb2Dsp_MC * ratio_sigmaJ, "+/-", u_sigmaJ_Lb2Dsp_data)
    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {"All": 5620.73813},  # value taken from master config
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run2": {"All": sigmaI_Lb2Dsp_MC * ratio_sigmaI},  # +/- u_sigmaI_Lb2Dsp_data
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run2": {"All": sigmaJ_Lb2Dsp_MC * ratio_sigmaJ},  # +/- u_sigmaJ_Lb2Dsp_data
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run2": {"All": 1.028},  # +/- 0.060
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run2": {"All": 1.57},  # +/- 0.15
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {"All": 1.619},  # +/- 0.064
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {"All": 2.16},  # +/- 0.12
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run2": {"All": -1.7},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run2": {"All": -0.155},  # +/- 0.052
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run2": {"All": 0.361},  # +/- 0.018
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run2": {"All": 0.25},
        "Fixed": True,
    }

    single = False
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    if single:
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
        configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
            "Run2": {"All": -9.0296e-04},
            "Fixed": False,
        }
    else:
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "MassDiff"
        configdict["CombBkgShape"]["BeautyMass"]["c"] = {
            "Run2": {"All": 122.0},  # +/- 21.
            "Fixed": True,
        }
        configdict["CombBkgShape"]["BeautyMass"]["m0"] = {
            "Run2": {"All": 5167.0},  # +/- 14.
            "Fixed": True,
        }
        configdict["CombBkgShape"]["BeautyMass"]["a"] = {
            "Run2": {"All": -6.3167e00},  # +/- 1.18e+00
            "Fixed": True,
        }
        configdict["CombBkgShape"]["BeautyMass"]["b"] = {
            "Run2": {"All": 0.0},
            "Fixed": True,
        }

    configdict["Lb2DsstpShape"] = {}
    configdict["Lb2DsstpShape"]["BeautyMass"] = {}
    configdict["Lb2DsstpShape"]["BeautyMass"]["type"] = "HILLdiniPlusHORNSdini"
    configdict["Lb2DsstpShape"]["BeautyMass"]["R"] = {
        "Run2": {"All": 5.13},  # +/- 0.28
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["ahill"] = {
        "Run2": {"All": 5249.8},  # +/- 5.9
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["ahorns"] = {
        "Run2": {"All": 5252.93},  # +/- 0.44
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["bhill"] = {
        "Run2": {"All": 5533.2},  # +/- 3.2
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["bhorns"] = {
        "Run2": {"All": 5562.62},  # +/- 0.45
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["csihill"] = {
        "Run2": {"All": 0.345},  # +/- 0.068
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["csihorns"] = {
        "Run2": {"All": 0.576},  # +/- 0.019
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["frac"] = {
        "Run2": {"All": 0.99371},  # +/- 0.00086
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["frachorns"] = {
        "Run2": {"All": 0.3073},  # +/- 0.0072
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["sigma"] = {
        "Run2": {"All": 38.8},  # +/- 1.3
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["sigmahorns"] = {
        "Run2": {"All": 13.67},  # +/- 0.38
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["shift"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }

    configdict["Bs2DsstKShape"] = {}
    configdict["Bs2DsstKShape"]["BeautyMass"] = {}
    configdict["Bs2DsstKShape"]["BeautyMass"]["type"] = "CrystalBall"
    configdict["Bs2DsstKShape"]["BeautyMass"]["alpha"] = {
        "Run2": {"All": -0.62},  # +/- 0.12
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["n"] = {
        "Run2": {"All": 48.0},  # +/- 45.
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["sigma"] = {
        "Run2": {"All": 108.0},  # +/- 17.
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["mean"] = {
        "Run2": {"All": 5332.7},  # +/- 9.3
        "Fixed": True,
    }

    configdict["Bd2DsstKShape"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"]["type"] = "CrystalBall"
    configdict["Bd2DsstKShape"]["BeautyMass"]["alpha"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["alpha"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["n"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["n"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["sigma"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["sigma"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": configdict["Bs2DsstKShape"]["BeautyMass"]["mean"]["Run2"]["All"]
            - 87.23
        },
        "Fixed": True,
    }

    configdict["AdditionalParameters"] = {}

    # define yields
    configdict["Yields"] = {}

    configdict["Yields"]["Bs2DsPi"] = {
        "Run2": {"KKPi": [2.0509e02, 2.21e01]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstPi"] = {
        "Run2": {"KKPi": [2.0312e02, 1.95e01]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsRho"] = {
        "Run2": {"KKPi": [1.4102e02, 1.35e01]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstRho"] = {
        "Run2": {"KKPi": [3.5118e01, 3.47e00]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsK"] = {
        "Run2": {"KKPi": [9.9927e01, 1.03e01]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bd2DsK"] = {
        "Run2": {"KKPi": [4.0926e01, 4.09e00]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstK"] = {
        "Run2": {"KKPi": [1.1545e02, 1.14e01]},
        "Fixed": False,
        "Constrained": True,
    }

    configdict["Yields"]["Bd2DsstK"] = {
        "Run2": {"KKPi": [4.1077e01, 4.08e00]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsKst"] = {
        "Run2": {"KKPi": [1.6352e01, 1.63e00]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstKst"] = {
        "Run2": {"KKPi": [1.8199e01, 1.81e00]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Lb2Dsstp"] = {
        "2011": {"KKPi": 500.0},
        "2012": {"KKPi": 500.0},
        "2015": {"KKPi": 500.0},
        "2016": {"KKPi": 500.0},
        "2017": {"KKPi": 500.0},
        "2018": {"KKPi": 500.0},
        "Fixed": False,
    }
    configdict["Yields"]["Signal"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 5000.0},
        "2016": {"KKPi": 5000.0},
        "2017": {"KKPi": 5000.0},
        "2018": {"KKPi": 5000.0},
        "Fixed": False,
    }
    configdict["Yields"]["CombBkg"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 5000.0},
        "2016": {"KKPi": 5000.0},
        "2017": {"KKPi": 5000.0},
        "2018": {"KKPi": 5000.0},
        "Fixed": False,
    }

    ####################################################################
    ################              Plotting              ################
    ####################################################################

    from ROOT import kSolid

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Bs2DsPi",
        "Bs2DsstPi",
        "Bs2DsRho",
        "Bs2DsstRho",
        "Bs2DsK",
        "Bs2DsstK",
        "Bs2DsKst",
        "Bs2DsstKst",
        "Bd2DsK",
        "Bd2DsstK",
        "Lb2Dsstp",
    ]
    configdict["PlotSettings"]["colors"] = [
        "#54278f",
        "#cccccc",
        "#d7301f",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
        "#08519c",
        "#6baed6",
        "#c6dbef",
        "#eff3ff",
        "#3182bd",
        "#9ecae1",
        "#9e9ac8",
    ]

    configdict["PlotSettings"]["pattern"] = [
        kSolid,
        3004,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        3005,
        3005,
        kSolid,
    ]

    configdict["PlotSettings"]["patterncolor"] = [
        "#54278f",
        "#969696",
        "#d7301f",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
        "#08519c",
        "#6baed6",
        "#bdd7e7",
        "#eff3ff",
        "#eff3ff",
        "#eff3ff",
        "#9e9ac8",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.35, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
