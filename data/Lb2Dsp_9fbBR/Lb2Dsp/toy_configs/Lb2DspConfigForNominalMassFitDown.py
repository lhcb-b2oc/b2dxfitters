###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import math

from uncertainties import ufloat

# integrated luminosity in each year of data taking (necessary in case of PIDK shapes)
luminosities_Lb2Dsp = {
    "2011": {"Down": 0.5600, "Up": 0.4200},
    "2012": {"Down": 0.9912, "Up": 0.9988},
    "2015": {"Down": 0.18695, "Up": 0.14105},
    "2016": {"Down": 0.85996, "Up": 0.80504},
    "2017": {"Down": 0.87689, "Up": 0.83311},
    "2018": {"Down": 1.04846, "Up": 1.14154},
}


def round_up(n, decimals=0):
    multiplier = 10**decimals
    return math.ceil(n * multiplier) / multiplier


def TotalFracForPolAndYear(year, pol="Down"):
    """
    Fraction in a Run2

    Helps in obtaining the fraction for years
    in Run2 (2015, 2016, 2017, 2018) and certain polarity
    year, pol - string
    """
    luminosities = luminosities_Lb2Dsp
    total_luminosity = (
        luminosities["2015"]["Down"]
        + luminosities["2015"]["Up"]
        + luminosities["2016"]["Down"]
        + luminosities["2016"]["Up"]
        + luminosities["2017"]["Down"]
        + luminosities["2017"]["Up"]
        + luminosities["2018"]["Down"]
        + luminosities["2018"]["Up"]
    )
    frac_for_year = 0.0
    if year == "20152016":
        frac_for_year = (
            luminosities["2015"][pol] + luminosities["2016"][pol]
        ) / total_luminosity
    else:
        frac_for_year = (luminosities[year][pol]) / total_luminosity
    return frac_for_year


def YieldForPolAndYear(year, total_yield, pol="Down"):
    """
    Helps in translating the yield given for entire Run2 into yields
    for particular years in Run2 (2015, 2016, 2017, 2018) and certain polarity
    year, pol - strings, total_yield - sum for all years IN RUN2 (!) and polarities
    """
    fraction = TotalFracForPolAndYear(year, pol)

    return fraction * total_yield


def FracForYear(year, pol="Down"):
    """
    Fraction in a year of Run2

    Helps in obtaining the fraction for years
    in Run2 (2015, 2016, 2017, 2018) and certain polarity
    year, pol - string
    """
    luminosities = luminosities_Lb2Dsp

    sum_for_year = 0.0
    fraction = 0.0

    if year == "20152016":
        years = ["2015", "2016"]
        pols = ["Up", "Down"]
        for y in years:
            for p in pols:
                sum_for_year += luminosities[y][p]
        fraction = (
            luminosities["2015"][pol] + luminosities["2016"][pol]
        ) / sum_for_year
    else:
        fraction = (luminosities[year][pol]) / (
            luminosities[year]["Up"] + luminosities[year]["Down"]
        )
    return fraction


def TotalFraction(pol="Down"):
    luminosities = luminosities_Lb2Dsp
    denominator = 0.0
    years = ["2015", "2016", "2017", "2018"]
    pols = ["Up", "Down"]
    for y in years:
        for p in pols:
            denominator += luminosities[y][p]

    nominator = 0.0
    for y in years:
        nominator += luminosities[y][pol]

    return nominator / denominator


def TranslateGCForRun2IntoYearPol(nb, year, pol="Down"):
    nb *= TotalFracForPolAndYear(str(year), pol)
    return nb


def TranslateNbForRun2InYear(nb, year, pol="Down"):
    return round_up(nb * FracForYear(year, pol), 0)


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Lb2Dsp"
    configdict["CharmModes"] = {"KKPi"}
    configdict["Backgrounds"] = [
        "Bs2DsPi",
        "Bs2DsstPi",
        "Bs2DsRho",
        "Bs2DsstRho",
        "Bd2DsK",
        "Bs2DsK",
        "Bs2DsstK",
        "Bs2DsKst",
        "Bs2DsstKst",
        "Bd2DsstK",
        "Lb2Dsstp",
    ]

    # year of data taking
    configdict["YearOfDataTaking"] = {"2011", "2012", "2015", "2016", "2017", "2018"}

    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = luminosities_Lb2Dsp

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/Lb2Dsp_9fbBR/Lb2Dsp/config_Lb2Dsp.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotLb2Dsp", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5200, 6200],
        "InputName": "lab0_MassHypo_Dsp",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1948, 1988],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-150.0, 150.0],
        "InputName": "lab1_ProbNNp",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [10000.0, 150000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [1000.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 500.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3",
    }

    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 2500.0],
        "InputName": "lab0_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 3000.0],
        "InputName": "lab2_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab0_ENDVERTEX_CHI2",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab2_ENDVERTEX_CHI2",
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "",
        "MC": "",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": False,
        "DsHypo": True,
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    ####################################################################################
    ######################                FITTING                #######################
    ####################################################################################

    # parameters from Jordy's update
    # data parameters from refits - changed 24.04.2022

    sigmaJ_Lb2Dsp_MC = 13.82495  # +/- 0.30
    sigmaJ_Lb2LcPi_MC = 13.92  # +/- 0.11
    sigmaJ_Lb2LcPi_data = 15.159  # +/-  0.0431

    sigmaI_Lb2Dsp_MC = 20.70233  # +/- 2.6
    sigmaI_Lb2LcPi_MC = 25.57  # +/- 0.64
    sigmaI_Lb2LcPi_data = 29.006  # +/-  0.348

    # to translate Lb2Dsp signal MC parameters to Lb2Dsp signal data parameters
    # I just multiply the MC parameters via ratio - from Lb2LcPi sample
    ratio_sigmaJ = sigmaJ_Lb2LcPi_data / sigmaJ_Lb2LcPi_MC
    ratio_sigmaI = sigmaI_Lb2LcPi_data / sigmaI_Lb2LcPi_MC

    # calculating uncertainty for sigmaI in data
    u2_sigmaI_Lb2Dsp_MC = 2.6**2

    u_sigmaI_Lb2LcPi_data = 0.348
    u_sigmaI_Lb2LcPi_MC = 0.64
    u2_ratio_sigmaI = (ratio_sigmaI / sigmaI_Lb2LcPi_MC * u_sigmaI_Lb2LcPi_MC) ** 2 + (
        u_sigmaI_Lb2LcPi_data / sigmaI_Lb2LcPi_MC
    ) ** 2

    math.sqrt(
        u2_sigmaI_Lb2Dsp_MC * ratio_sigmaI * ratio_sigmaI
        + sigmaI_Lb2Dsp_MC * sigmaI_Lb2Dsp_MC * u2_ratio_sigmaI
    )

    # calculating uncertainty for sigmaJ in data

    u2_sigmaJ_Lb2Dsp_MC = 0.30**2

    u_sigmaJ_Lb2LcPi_data = 0.0431
    u_sigmaJ_Lb2LcPi_MC = 0.11
    u2_ratio_sigmaJ = (ratio_sigmaJ / sigmaJ_Lb2LcPi_MC * u_sigmaJ_Lb2LcPi_MC) ** 2 + (
        u_sigmaJ_Lb2LcPi_data / sigmaJ_Lb2LcPi_MC
    ) ** 2

    math.sqrt(
        u2_sigmaJ_Lb2Dsp_MC * ratio_sigmaJ * ratio_sigmaJ
        + sigmaJ_Lb2Dsp_MC * sigmaJ_Lb2Dsp_MC * u2_ratio_sigmaJ
    )

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {"All": 5620.78865},  # value taken from master config +/-  0.080
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run2": {"All": sigmaI_Lb2Dsp_MC * ratio_sigmaI},  # +/- u_sigmaI_Lb2Dsp_data
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run2": {"All": sigmaJ_Lb2Dsp_MC * ratio_sigmaJ},  # +/- u_sigmaJ_Lb2Dsp_data
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run2": {"All": 1.04495},  # +/- 0.094
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run2": {"All": 1.5666},  # +/- 0.29
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {"All": 1.5891},  # +/-  0.067
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {"All": 2.16415},  # +/-  0.17
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run2": {"All": -1.7},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run2": {"All": -0.175257},  # +/-  0.065
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run2": {"All": 0.34947},  # +/- 0.018
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run2": {"All": 0.25},
        "Fixed": True,
    }

    single = False
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    if single:
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
        configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
            "Run2": {"All": -9.0296e-04},
            "Fixed": False,
        }
    else:
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "MassDiff"
        configdict["CombBkgShape"]["BeautyMass"]["c"] = {
            "Run2": {"All": 122.0},  # +/- 21.
            "Fixed": True,
        }
        configdict["CombBkgShape"]["BeautyMass"]["m0"] = {
            "Run2": {"All": 5167.0},  # +/- 14.
            "Fixed": True,
        }
        configdict["CombBkgShape"]["BeautyMass"]["a"] = {
            "Run2": {"All": -6.3167e00},  # +/- 1.18e+00
            "Fixed": True,
        }
        configdict["CombBkgShape"]["BeautyMass"]["b"] = {
            "Run2": {"All": 0.0},
            "Fixed": True,
        }

    configdict["Lb2DsstpShape"] = {}
    configdict["Lb2DsstpShape"]["BeautyMass"] = {}
    configdict["Lb2DsstpShape"]["BeautyMass"]["type"] = "HILLdiniPlusHORNSdini"
    configdict["Lb2DsstpShape"]["BeautyMass"]["R"] = {
        "Run2": {"All": 4.95615},  # +/-  0.28
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["ahill"] = {
        "Run2": {"All": 5248.47329},  # +/-  6.9
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["ahorns"] = {
        "Run2": {"All": 5253.23423},  # +/-  0.44
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["bhill"] = {
        "Run2": {"All": 5532.30986},  # +/-  3.1
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["bhorns"] = {
        "Run2": {"All": 5562.55112},  # +/-  0.54
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["csihill"] = {
        "Run2": {"All": 0.32533},  # +/-  0.054
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["csihorns"] = {
        "Run2": {"All": 0.59192},  # +/-  0.022
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["frac"] = {
        "Run2": {"All": 0.993},  # +/-  0.0010
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["frachorns"] = {
        "Run2": {"All": 0.31183},  # +/-  0.0091
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["sigma"] = {
        "Run2": {"All": 38.23656},  # +/-  1.5
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["sigmahorns"] = {
        "Run2": {"All": 13.53707},  # +/-  0.44
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["shift"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }

    configdict["Bs2DsstKShape"] = {}
    configdict["Bs2DsstKShape"]["BeautyMass"] = {}
    configdict["Bs2DsstKShape"]["BeautyMass"]["type"] = "CrystalBall"
    configdict["Bs2DsstKShape"]["BeautyMass"]["alpha"] = {
        "Run2": {"All": -0.797316},  # +/-  0.046
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["n"] = {
        "Run2": {"All": 49.76984},  # +/-  36.
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["sigma"] = {
        "Run2": {"All": 124.82455},  # +/-  5.8
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["mean"] = {
        "Run2": {"All": 5354.07669},  # +/-  1.4
        "Fixed": True,
    }

    configdict["Bd2DsstKShape"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"]["type"] = "CrystalBall"
    configdict["Bd2DsstKShape"]["BeautyMass"]["alpha"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["alpha"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["n"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["n"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["sigma"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["sigma"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": configdict["Bs2DsstKShape"]["BeautyMass"]["mean"]["Run2"]["All"]
            - 87.23
        },
        "Fixed": True,
    }

    configdict["AdditionalParameters"] = {}

    # define yields
    configdict["Yields"] = {}

    fraction_pol_Up = (
        TotalFracForPolAndYear("2015", pol="Up")
        + TotalFracForPolAndYear("2016", pol="Up")
        + TotalFracForPolAndYear("2017", pol="Up")
        + TotalFracForPolAndYear("2018", pol="Up")
    )
    fraction_pol_Down = (
        TotalFracForPolAndYear("2015", pol="Down")
        + TotalFracForPolAndYear("2016", pol="Down")
        + TotalFracForPolAndYear("2017", pol="Down")
        + TotalFracForPolAndYear("2018", pol="Down")
    )
    assert (
        fraction_pol_Up + fraction_pol_Down == 1.0
    ), "The polarity fractions don't add up to one."
    #
    # Bs2DsPi_yield = ufloat(241.57, 0.1 * 241.57)
    # Bs2DsPi_yield_Down = Bs2DsPi_yield * fraction_pol_Up
    # the events we should naively expect in Run2 polarity Up
    #
    #
    # print('Expected {} Bs2DsPi events for polarity "Down"' +
    #       ' in Run2.'.format(Bs2DsPi_yield_Down))

    # print(TotalFracForPolAndYear('2015', 'Up'))
    # print(TotalFracForPolAndYear('2016', 'Up'))
    # print(TotalFracForPolAndYear('2017', 'Up'))
    # print(TotalFracForPolAndYear('2018', 'Up'))
    # print(1.1e5)
    # print()
    # print((Lb2LcK_yield * TotalFracForPolAndYear('2015', 'Down')).n,
    #       (Lb2LcK_yield * TotalFracForPolAndYear('2015', 'Down')).s)
    # print((Lb2LcK_yield * TotalFracForPolAndYear('2016', 'Down')).n,
    #       (Lb2LcK_yield * TotalFracForPolAndYear('2016', 'Down')).s)
    # print((Lb2LcK_yield * TotalFracForPolAndYear('2017', 'Down')).n,
    #       (Lb2LcK_yield * TotalFracForPolAndYear('2017', 'Down')).s)
    # print((Lb2LcK_yield * TotalFracForPolAndYear('2018', 'Down')).n,
    #       (Lb2LcK_yield * TotalFracForPolAndYear('2018', 'Down')).s)
    # print(round_up(15000.0 * FracForYear('2015', 'Down'), 0))
    # print(round_up(15000.0 * FracForYear('2016', 'Down'), 0))
    # print(round_up(15000.0 * FracForYear('2017', 'Down'), 0))
    # print(round_up(15000.0 * FracForYear('2018', 'Down'), 0))
    # print("round_up(1.1e5 * FracForYear('2015', 'Down'), 0)",
    #       round_up(1.1e5 * FracForYear('2015', 'Down'), 0))
    # print("round_up(1.1e5 * FracForYear('2016', 'Down'), 0)",
    #       round_up(1.1e5 * FracForYear('2016', 'Down'), 0))
    # print("round_up(1.1e5 * FracForYear('2017', 'Down'), 0)",
    #       round_up(1.1e5 * FracForYear('2017', 'Down'), 0))
    # print("round_up(1.1e5 * FracForYear('2018', 'Down'), 0)",
    #       round_up(1.1e5 * FracForYear('2018', 'Down'), 0))
    # print("round_up(20000.0 * FracForYear('2015', 'Down'), 0)",
    #       round_up(20000.0 * FracForYear('2015', 'Down'), 0))
    # print("round_up(20000.0 * FracForYear('2016', 'Down'), 0)",
    #       round_up(20000.0 * FracForYear('2016', 'Down'), 0))
    # print("round_up(20000.0 * FracForYear('2017', 'Down'), 0)",
    #       round_up(20000.0 * FracForYear('2017', 'Down'), 0))
    # print("round_up(20000.0 * FracForYear('2018', 'Down'), 0)",
    #       round_up(20000.0 * FracForYear('2018', 'Down'), 0))
    # print(round_up(28125.0 * FracForYear('2015', 'Down'), 0))
    # print(round_up(28125.0 * FracForYear('2016', 'Down'), 0))
    # print(round_up(28125.0 * FracForYear('2017', 'Down'), 0))
    # print(round_up(28125.0 * FracForYear('2018', 'Down'), 0))

    ufloat(241.57, 0.1 * 241.57)
    ufloat(199.393, 0.1 * 199.393)
    ufloat(139.127, 0.1 * 139.127)
    ufloat(34.9765, 0.1 * 34.9765)
    ufloat(104.553, 0.1 * 104.553)
    ufloat(40.9951, 0.1 * 40.9951)
    ufloat(115.2, 0.1 * 115.2)
    ufloat(41.0068, 0.1 * 41.0068)
    ufloat(16.3455, 0.1 * 16.3455)
    ufloat(18.1501, 0.1 * 18.1501)

    configdict["Yields"]["Bs2DsPi"] = {
        # "Run2": {
        #    "KKPi": [241.57, 0.1 * 241.57]
        # },
        "Run2": {"KKPi": [121.8, 12.2]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstPi"] = {
        # "Run2": {
        #     "KKPi": [199.393, 0.1 * 199.393]
        # },
        "Run2": {"KKPi": [100.6, 10.1]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsRho"] = {
        # "Run2": {
        #     "KKPi": [139.127, 0.1 * 139.127]
        # },
        "Run2": {"KKPi": [70.2, 7.0]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstRho"] = {
        # "Run2": {
        #     "KKPi": [34.9765, 0.1 * 34.9765]
        # },
        "Run2": {"KKPi": [17.6, 1.8]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsK"] = {
        # "Run2": {
        #     "KKPi": [104.553, 0.1 * 104.553]
        # },
        "Run2": {"KKPi": [52.7, 5.3]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bd2DsK"] = {
        # "Run2": {
        #     "KKPi": [40.9951, 0.1 * 40.9951]
        # },
        "Run2": {"KKPi": [20.7, 2.1]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstK"] = {
        # "Run2": {
        #     "KKPi": [115.2, 0.1 * 115.2]
        # },
        "Run2": {"KKPi": [58.1, 5.8]},
        "Fixed": False,
        "Constrained": True,
    }

    configdict["Yields"]["Bd2DsstK"] = {
        # "Run2": {
        #     "KKPi": [41.0068, 0.1 * 41.0068]
        # },
        "Run2": {"KKPi": [20.7, 2.1]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsKst"] = {
        # "Run2": {
        #     "KKPi":  [16.3455, 0.1 * 16.3455]
        # },
        "Run2": {"KKPi": [8.2, 0.8]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstKst"] = {
        # "Run2": {
        #     "KKPi": [18.1501, 0.1 * 18.1501]
        # },
        "Run2": {"KKPi": [9.2, 0.9]},
        "Fixed": False,
        "Constrained": True,
    }

    # TranslateNbForRun2InYear(nb, year, pol="Up")
    configdict["Yields"]["Lb2Dsstp"] = {
        "2011": {"KKPi": 500.0},
        "2012": {"KKPi": 500.0},
        "2015": {"KKPi": TranslateNbForRun2InYear(500.0, "2015")},
        "2016": {"KKPi": TranslateNbForRun2InYear(500.0, "2016")},
        "2017": {"KKPi": TranslateNbForRun2InYear(500.0, "2017")},
        "2018": {"KKPi": TranslateNbForRun2InYear(500.0, "2018")},
        "Fixed": False,
    }
    configdict["Yields"]["Signal"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": TranslateNbForRun2InYear(5000.0, "2015")},  # 5000.0
        "2016": {"KKPi": TranslateNbForRun2InYear(5000.0, "2016")},
        "2017": {"KKPi": TranslateNbForRun2InYear(5000.0, "2017")},
        "2018": {"KKPi": TranslateNbForRun2InYear(5000.0, "2018")},
        "Fixed": False,
    }
    configdict["Yields"]["CombBkg"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": TranslateNbForRun2InYear(5000.0, "2015")},  # 5000.0
        "2016": {"KKPi": TranslateNbForRun2InYear(5000.0, "2016")},
        "2017": {"KKPi": TranslateNbForRun2InYear(5000.0, "2017")},
        "2018": {"KKPi": TranslateNbForRun2InYear(5000.0, "2018")},
        "Fixed": False,
    }

    ########################################################################
    ##################              Plotting              ##################
    ########################################################################

    from ROOT import kSolid

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Bs2DsPi",
        "Bs2DsstPi",
        "Bs2DsRho",
        "Bs2DsstRho",
        "Bs2DsK",
        "Bs2DsstK",
        "Bs2DsKst",
        "Bs2DsstKst",
        "Bd2DsK",
        "Bd2DsstK",
        "Lb2Dsstp",
    ]
    configdict["PlotSettings"]["colors"] = [
        "#54278f",
        "#cccccc",
        "#d7301f",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
        "#08519c",
        "#6baed6",
        "#c6dbef",
        "#eff3ff",
        "#3182bd",
        "#9ecae1",
        "#9e9ac8",
    ]

    configdict["PlotSettings"]["pattern"] = [
        kSolid,
        3004,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        3005,
        3005,
        kSolid,
    ]

    configdict["PlotSettings"]["patterncolor"] = [
        "#54278f",
        "#969696",
        "#d7301f",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
        "#08519c",
        "#6baed6",
        "#bdd7e7",
        "#eff3ff",
        "#eff3ff",
        "#eff3ff",
        "#9e9ac8",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.35, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
