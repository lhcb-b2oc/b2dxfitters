###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def getconfig():
    from Lb2DspConfigForNominalMassFitForToys import getconfig as getconfig_nominal

    configdict = getconfig_nominal()
    # adding the new parameter value:

    # configdict["CombBkgShape"]["BeautyMass"]["c"] = {
    #         "Run2": {
    #             "All": 122. - 21.
    #         },
    #         "Fixed": True
    #     }

    # configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
    #     "Run2": {
    #         "All": 15.278 + 0.224
    #     },
    #     "Fixed": True
    # }

    # configdict["SignalShape"]["BeautyMass"]["a1"] = {
    #     "Run2": {
    #         "All": 1.0280 + 0.060
    #     },
    #     "Fixed": True
    # }
    # configdict["SignalShape"]["BeautyMass"]["a2"] = {
    #     "Run2": {
    #         "All": 1.57 + 0.15
    #     },
    #     "Fixed": True
    # }
    # configdict["SignalShape"]["BeautyMass"]["n1"] = {
    #     "Run2": {
    #         "All": 1.6190e+00 + 0.064
    #     },
    #     "Fixed": True
    # }
    # configdict["SignalShape"]["BeautyMass"]["n2"] = {
    #     "Run2": {
    #         "All": 2.1600e+00 + 0.12
    #     },
    #     "Fixed": True
    # }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run2": {"All": -1.5500e-01 + 0.052},
        "Fixed": True,
    }
    return configdict
