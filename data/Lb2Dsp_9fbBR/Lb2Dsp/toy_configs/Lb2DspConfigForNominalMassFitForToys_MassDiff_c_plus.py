###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def getconfig():
    from Lb2DspConfigForNominalMassFitForToys import getconfig as getconfig_nominal

    configdict = getconfig_nominal()
    # adding the new parameter value:
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "MassDiff"
    configdict["CombBkgShape"]["BeautyMass"]["c"] = {
        "Run2": {"All": 122.0 + 21.0},
        "Fixed": True,
    }

    return configdict
