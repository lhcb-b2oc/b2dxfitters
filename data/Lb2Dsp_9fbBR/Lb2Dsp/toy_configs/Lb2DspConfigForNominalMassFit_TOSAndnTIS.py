###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import math


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Lb2Dsp"
    configdict["CharmModes"] = {"KKPi"}
    configdict["Backgrounds"] = [
        "Bs2DsPi",
        "Bs2DsstPi",
        "Bs2DsRho",
        "Bs2DsstRho",
        "Bd2DsK",
        "Bs2DsK",
        "Bs2DsstK",
        "Bs2DsKst",
        "Bs2DsstKst",
        "Bd2DsstK",
        "Lb2Dsstp",
    ]

    # year of data taking
    configdict["YearOfDataTaking"] = {"2011", "2012", "2015", "2016", "2017", "2018"}

    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2011": {"Down": 0.5600, "Up": 0.4200},
        "2012": {"Down": 0.9912, "Up": 0.9988},
        "2015": {"Down": 0.18695, "Up": 0.14105},
        "2016": {"Down": 0.85996, "Up": 0.80504},
        "2017": {"Down": 0.87689, "Up": 0.83311},
        "2018": {"Down": 1.04846, "Up": 1.14154},
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/Lb2Dsp_9fbBR/Lb2Dsp/config_Lb2Dsp.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotLb2Dsp", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5200, 6200],
        "InputName": "lab0_MassHypo_Dsp",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1948, 1988],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-150.0, 150.0],
        "InputName": "lab1_ProbNNp",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [10000.0, 150000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [1000.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 500.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3",
    }

    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 2500.0],
        "InputName": "lab0_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 3000.0],
        "InputName": "lab2_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab0_ENDVERTEX_CHI2",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab2_ENDVERTEX_CHI2",
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "(lab0_L0HadronDecision_TOS == 1 && lab0_L0Global_TIS == 0)",
        "MC": "(lab0_L0HadronDecision_TOS == 1 && lab0_L0Global_TIS == 0)",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": False,
        "DsHypo": True,
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    #############################################################################
    ###################                FITTING                ###################
    #############################################################################

    # parameters from Jordy's update
    # data parameters from refits - changed 24.04.2022

    sigmaJ_Lb2Dsp_MC = 13.82495  # +/- 0.30
    sigmaJ_Lb2LcPi_MC = 13.92  # +/- 0.11
    sigmaJ_Lb2LcPi_data = 15.159  # +/-  0.0431

    sigmaI_Lb2Dsp_MC = 20.70233  # +/- 2.6
    sigmaI_Lb2LcPi_MC = 25.57  # +/- 0.64
    sigmaI_Lb2LcPi_data = 29.006  # +/-  0.348

    # to translate Lb2Dsp signal MC parameters
    # to Lb2Dsp signal data parameters
    # I just multiply the MC parameters via ratio
    # - from Lb2LcPi sample
    ratio_sigmaJ = sigmaJ_Lb2LcPi_data / sigmaJ_Lb2LcPi_MC
    ratio_sigmaI = sigmaI_Lb2LcPi_data / sigmaI_Lb2LcPi_MC

    # calculating uncertainty for sigmaI in data
    u2_sigmaI_Lb2Dsp_MC = 2.6**2

    u_sigmaI_Lb2LcPi_data = 0.348
    u_sigmaI_Lb2LcPi_MC = 0.64
    u2_ratio_sigmaI = (ratio_sigmaI / sigmaI_Lb2LcPi_MC * u_sigmaI_Lb2LcPi_MC) ** 2 + (
        u_sigmaI_Lb2LcPi_data / sigmaI_Lb2LcPi_MC
    ) ** 2

    math.sqrt(
        u2_sigmaI_Lb2Dsp_MC * ratio_sigmaI * ratio_sigmaI
        + sigmaI_Lb2Dsp_MC * sigmaI_Lb2Dsp_MC * u2_ratio_sigmaI
    )

    # calculating uncertainty for sigmaJ in data

    u2_sigmaJ_Lb2Dsp_MC = 0.30**2

    u_sigmaJ_Lb2LcPi_data = 0.0431
    u_sigmaJ_Lb2LcPi_MC = 0.11
    u2_ratio_sigmaJ = (ratio_sigmaJ / sigmaJ_Lb2LcPi_MC * u_sigmaJ_Lb2LcPi_MC) ** 2 + (
        u_sigmaJ_Lb2LcPi_data / sigmaJ_Lb2LcPi_MC
    ) ** 2

    math.sqrt(
        u2_sigmaJ_Lb2Dsp_MC * ratio_sigmaJ * ratio_sigmaJ
        + sigmaJ_Lb2Dsp_MC * sigmaJ_Lb2Dsp_MC * u2_ratio_sigmaJ
    )

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {"All": 5.6206e03},  # +/-  1.14e-01
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run2": {"All": 2.0573e01},  # +/-  3.51e+00
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run2": {"All": 1.4196e01},  # +/-  3.94e-01
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run2": {"All": 1.0767e00},  # +/-  1.38e-01
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run2": {"All": 1.6060e00},  # +/-  3.83e-01
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {"All": 1.5528e00},  # +/-  1.02e-01
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {"All": 2.0622e00},  # +/-  2.26e-01
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run2": {"All": -1.7},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run2": {"All": -1.5654e-01},  # +/-  8.94e-02
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run2": {"All": 3.4023e-01},  # +/-  2.94e-02
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run2": {"All": 0.25},
        "Fixed": True,
    }

    single = False
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    if single:
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
        configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
            "Run2": {"All": -9.0296e-04},
            "Fixed": False,
        }
    else:
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "MassDiff"
        configdict["CombBkgShape"]["BeautyMass"]["c"] = {
            "Run2": {"All": 122.0},  # +/- 21.
            "Fixed": True,
        }
        configdict["CombBkgShape"]["BeautyMass"]["m0"] = {
            "Run2": {"All": 5167.0},  # +/- 14.
            "Fixed": True,
        }
        configdict["CombBkgShape"]["BeautyMass"]["a"] = {
            "Run2": {"All": -6.3167e00},  # +/- 1.18e+00
            "Fixed": True,
        }
        configdict["CombBkgShape"]["BeautyMass"]["b"] = {
            "Run2": {"All": 0.0},
            "Fixed": True,
        }

    configdict["Lb2DsstpShape"] = {}
    configdict["Lb2DsstpShape"]["BeautyMass"] = {}
    configdict["Lb2DsstpShape"]["BeautyMass"]["type"] = "HILLdiniPlusHORNSdini"
    configdict["Lb2DsstpShape"]["BeautyMass"]["R"] = {
        "Run2": {"All": 4.9121e00},  # +/-  4.47e-01
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["ahill"] = {
        "Run2": {"All": 5.2426e03},  # +/-  8.26e+00
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["ahorns"] = {
        "Run2": {"All": 5.2529e03},  # +/-  7.10e-01
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["bhill"] = {
        "Run2": {"All": 5.5335e03},  # +/-  4.18e+00
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["bhorns"] = {
        "Run2": {"All": 5.5633e03},  # +/-  7.69e-01
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["csihill"] = {
        "Run2": {"All": 2.9971e-01},  # +/-  6.56e-02
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["csihorns"] = {
        "Run2": {"All": 5.7806e-01},  # +/-  3.38e-02
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["frac"] = {
        "Run2": {"All": 9.9421e-01},  # +/-  1.42e-03
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["frachorns"] = {
        "Run2": {"All": 2.9923e-01},  # +/-  1.15e-02
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["sigma"] = {
        "Run2": {"All": 3.7686e01},  # +/-  2.20e+00
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["sigmahorns"] = {
        "Run2": {"All": 1.3365e01},  # +/-  6.24e-01
        "Fixed": True,
    }
    configdict["Lb2DsstpShape"]["BeautyMass"]["shift"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }

    configdict["Bs2DsstKShape"] = {}
    configdict["Bs2DsstKShape"]["BeautyMass"] = {}
    configdict["Bs2DsstKShape"]["BeautyMass"]["type"] = "CrystalBall"
    configdict["Bs2DsstKShape"]["BeautyMass"]["alpha"] = {
        "Run2": {"All": -6.2439e-01},  # +/-  1.80e-01
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["n"] = {
        "Run2": {"All": 4.9886e01},  # +/-  7.00e+01
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["sigma"] = {
        "Run2": {"All": 1.1089e02},  # +/-  2.87e+01
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["mean"] = {
        "Run2": {"All": 5.3202e03},  # +/-  4.71e+00
        "Fixed": True,
    }

    configdict["Bd2DsstKShape"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"]["type"] = "CrystalBall"
    configdict["Bd2DsstKShape"]["BeautyMass"]["alpha"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["alpha"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["n"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["n"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["sigma"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["sigma"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["mean"] = {
        "Run2": {
            "All": configdict["Bs2DsstKShape"]["BeautyMass"]["mean"]["Run2"]["All"]
            - 87.23
        },
        "Fixed": True,
    }

    configdict["AdditionalParameters"] = {}
    """
    TOSAndnTIS:
    Bs2DsstRho sum of entries 8.752957
    Bs2DsRho sum of entries 29.81632
    Lb2Dsstp sum of entries 27631.881
    Bs2DsstPi sum of entries 13.878749
    Bs2DsPi sum of entries 96.635132
    Bs2DsK sum of entries 251.167376
    Bs2DsstK sum of entries 209.96013000000002
    Bs2DsstKst sum of entries 42.125893000000005
    Bs2DsKst sum of entries 158.96846
    Bd2DsK sum of entries 243.54289999999997

    Nominal:
    Bs2DsstRho sum of entries 23.059936
    Bs2DsRho sum of entries 73.11059
    Lb2Dsstp sum of entries 88735.118
    Bs2DsstPi sum of entries 32.928715
    Bs2DsPi sum of entries 226.57124
    Bs2DsK sum of entries 692.78316
    Bs2DsstK sum of entries 588.71667
    Bs2DsstKst sum of entries 129.66349
    Bs2DsKst sum of entries 449.83124
    Bd2DsK sum of entries 682.134
    """

    TOSAndnTIS_Bs2DsstRho = 8.752957 / 23.059936
    TOSAndnTIS_Bs2DsRho = 29.81632 / 73.11059
    TOSAndnTIS_Lb2Dsstp = 27631.881 / 88735.118
    TOSAndnTIS_Bs2DsstPi = 13.878749 / 32.928715
    TOSAndnTIS_Bs2DsPi = 96.635132 / 226.57124
    TOSAndnTIS_Bs2DsK = 251.167376 / 692.78316
    TOSAndnTIS_Bs2DsstK = 209.96013000000002 / 588.71667
    TOSAndnTIS_Bs2DsstKst = 42.125893000000005 / 129.66349
    TOSAndnTIS_Bs2DsKst = 158.96846 / 449.83124
    TOSAndnTIS_Bd2DsK = 243.54289999999997 / 682.134
    TOSAndnTIS_Bd2DsstK = TOSAndnTIS_Bs2DsstK

    # define yields
    configdict["Yields"] = {}

    configdict["Yields"]["Bs2DsPi"] = {
        "Run2": {
            "KKPi": [241.57 * TOSAndnTIS_Bs2DsPi, 0.1 * 241.57 * TOSAndnTIS_Bs2DsPi]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstPi"] = {
        "Run2": {
            "KKPi": [
                199.393 * TOSAndnTIS_Bs2DsstPi,
                0.1 * 199.393 * TOSAndnTIS_Bs2DsstPi,
            ]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsRho"] = {
        "Run2": {
            "KKPi": [139.127 * TOSAndnTIS_Bs2DsRho, 0.1 * 139.127 * TOSAndnTIS_Bs2DsRho]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstRho"] = {
        "Run2": {
            "KKPi": [
                34.9765 * TOSAndnTIS_Bs2DsstRho,
                0.1 * 34.9765 * TOSAndnTIS_Bs2DsstRho,
            ]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsK"] = {
        "Run2": {
            "KKPi": [104.553 * TOSAndnTIS_Bs2DsK, 0.1 * 104.553 * TOSAndnTIS_Bs2DsK]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bd2DsK"] = {
        "Run2": {
            "KKPi": [40.9951 * TOSAndnTIS_Bd2DsK, 0.1 * 40.9951 * TOSAndnTIS_Bd2DsK]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstK"] = {
        "Run2": {
            "KKPi": [115.2 * TOSAndnTIS_Bs2DsstK, 0.1 * 115.2 * TOSAndnTIS_Bs2DsstK]
        },
        "Fixed": False,
        "Constrained": True,
    }

    configdict["Yields"]["Bd2DsstK"] = {
        "Run2": {
            "KKPi": [41.0068 * TOSAndnTIS_Bd2DsstK, 0.1 * 41.0068 * TOSAndnTIS_Bd2DsstK]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsKst"] = {
        "Run2": {
            "KKPi": [16.3455 * TOSAndnTIS_Bs2DsKst, 0.1 * 16.3455 * TOSAndnTIS_Bs2DsKst]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstKst"] = {
        "Run2": {
            "KKPi": [
                18.1501 * TOSAndnTIS_Bs2DsstKst,
                0.1 * 18.1501 * TOSAndnTIS_Bs2DsstKst,
            ]
        },
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Lb2Dsstp"] = {
        "2011": {"KKPi": 500.0 * TOSAndnTIS_Lb2Dsstp},
        "2012": {"KKPi": 500.0 * TOSAndnTIS_Lb2Dsstp},
        "2015": {"KKPi": 500.0 * TOSAndnTIS_Lb2Dsstp},
        "2016": {"KKPi": 500.0 * TOSAndnTIS_Lb2Dsstp},
        "2017": {"KKPi": 500.0 * TOSAndnTIS_Lb2Dsstp},
        "2018": {"KKPi": 500.0 * TOSAndnTIS_Lb2Dsstp},
        "Fixed": False,
    }
    configdict["Yields"]["Signal"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 5000.0},
        "2016": {"KKPi": 5000.0},
        "2017": {"KKPi": 5000.0},
        "2018": {"KKPi": 5000.0},
        "Fixed": False,
    }
    configdict["Yields"]["CombBkg"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 5000.0},
        "2016": {"KKPi": 5000.0},
        "2017": {"KKPi": 5000.0},
        "2018": {"KKPi": 5000.0},
        "Fixed": False,
    }

    ######################################################################
    #################              Plotting              #################
    ######################################################################

    from ROOT import kSolid

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Bs2DsPi",
        "Bs2DsstPi",
        "Bs2DsRho",
        "Bs2DsstRho",
        "Bs2DsK",
        "Bs2DsstK",
        "Bs2DsKst",
        "Bs2DsstKst",
        "Bd2DsK",
        "Bd2DsstK",
        "Lb2Dsstp",
    ]
    configdict["PlotSettings"]["colors"] = [
        "#54278f",
        "#cccccc",
        "#d7301f",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
        "#08519c",
        "#6baed6",
        "#c6dbef",
        "#eff3ff",
        "#3182bd",
        "#9ecae1",
        "#9e9ac8",
    ]

    configdict["PlotSettings"]["pattern"] = [
        kSolid,
        3004,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        3005,
        3005,
        kSolid,
    ]

    configdict["PlotSettings"]["patterncolor"] = [
        "#54278f",
        "#969696",
        "#d7301f",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
        "#08519c",
        "#6baed6",
        "#bdd7e7",
        "#eff3ff",
        "#eff3ff",
        "#eff3ff",
        "#9e9ac8",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.35, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
