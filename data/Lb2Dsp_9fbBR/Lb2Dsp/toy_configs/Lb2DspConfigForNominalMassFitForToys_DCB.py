###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Lb2DspConfigForNominalMassFitForToys import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    # rescaling procedure to include the difference between MC and data,
    # based on the Lb2LcPi sample MC and data
    # factor = value_data/value_MC

    # MC Lb2LcPi - sigma1 - 11.54646
    # data Lb2LcPi - sigma1 - 1.2711e+01 +/-  6.44e-02
    # data Lb2LcPi - sigma1 - 12.711 +/- 0.0644

    # MC Lb2LcPi - sigma2 - 16.49301
    # data Lb2LcPi - sigma2 - 1.8026e+01 +/-  6.65e-02
    # data Lb2LcPi - sigma2 - 18.026 +/- 0.0665
    sigma1_factor = 12.711 / 11.54646
    sigma2_factor = 18.026 / 16.49301

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "DoubleCrystalBall"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {"All": 5620.71192},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigma1"] = {
        "Run2": {"All": 16.35979 * sigma1_factor},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["sigma2"] = {
        "Run2": {"All": 11.3908 * sigma2_factor},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["alpha1"] = {
        "Run2": {"All": 1.81423},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["alpha2"] = {
        "Run2": {"All": -2.08439},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {"All": 1.62383},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {"All": 1.88089},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["frac"] = {
        "Run2": {"All": 0.5},
        "Fixed": True,
    }

    return configdict
