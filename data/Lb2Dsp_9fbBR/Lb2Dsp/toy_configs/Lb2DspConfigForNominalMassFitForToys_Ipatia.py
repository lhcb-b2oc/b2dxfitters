###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Lb2DspConfigForNominalMassFitForToys import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    # TODO
    # sigma_data_Lb2LcPi / sigma_MC_Lb2LcPi
    sigma_data_Lb2LcPi = 16.781  # +/-  0.0267
    sigma_MC_Lb2LcPi = 15.30751
    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "Ipatia"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {"All": 5620.77254},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigma"] = {
        "Run2": {"All": 14.95964 * sigma_data_Lb2LcPi / sigma_MC_Lb2LcPi},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run2": {"All": 2.23478},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run2": {"All": 2.81516},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {"All": 1.46233},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {"All": 1.8165},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run2": {"All": -4.06535},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }

    return configdict
