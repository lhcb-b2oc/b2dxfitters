###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Lb2DspConfigForNominalMassFitForToys import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    factor = -0.05
    configdict["Yields"]["Bs2DsPi"] = {
        "Run2": {"KKPi": [241.57 + factor * 241.57, 0.1 * 241.57]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstPi"] = {
        "Run2": {"KKPi": [199.393 + factor * 199.393, 0.1 * 199.393]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsRho"] = {
        "Run2": {"KKPi": [139.127 + factor * 139.127, 0.1 * 139.127]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstRho"] = {
        "Run2": {"KKPi": [34.9765 + factor * 34.9765, 0.1 * 34.9765]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsK"] = {
        "Run2": {"KKPi": [104.553 + factor * 104.553, 0.1 * 104.553]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bd2DsK"] = {
        "Run2": {"KKPi": [40.9951 + factor * 40.9951, 0.1 * 40.9951]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstK"] = {
        "Run2": {"KKPi": [115.2 + factor * 115.2, 0.1 * 115.2]},
        "Fixed": False,
        "Constrained": True,
    }

    configdict["Yields"]["Bd2DsstK"] = {
        "Run2": {"KKPi": [41.0068 + factor * 41.0068, 0.1 * 41.0068]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsKst"] = {
        "Run2": {"KKPi": [16.3455 + factor * 16.3455, 0.1 * 16.3455]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Bs2DsstKst"] = {
        "Run2": {"KKPi": [18.1501 + factor * 18.1501, 0.1 * 18.1501]},
        "Fixed": False,
        "Constrained": True,
    }

    return configdict
