###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys

# from ROOT import *
# print(os.environ)


sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa

WORKSPACEFILE = (
    "/eos/lhcb/wg/b2oc/BR_Lb2Dsp_Run12/workspaces" "/syst/work_mc_lb2lcpi_run2.root"
)
WORKSPACENAME = "workspace"

luminosities_Lb2LcPi = {
    "2015": {"Down": 0.18695, "Up": 0.14105},
    "2016": {"Down": 0.85996, "Up": 0.80504},
    "2017": {"Down": 0.87689, "Up": 0.83311},
    "2018": {"Down": 1.04846, "Up": 1.14154},
}


# works only for Run2 - similar as for Lb2Dsp generator
def YieldForYear(year, total_yield):
    """
    Helps in translating the yield given for entire Run2 into yields
    for particular years in Run2 (2015, 2016, 2017, 2018)
    year, pol - strings,
    total_yield - sum for all years IN RUN2 (!) and polarities
    """
    luminosities = luminosities_Lb2LcPi
    total_luminosity = (
        luminosities["2015"]["Down"]
        + luminosities["2015"]["Up"]
        + luminosities["2016"]["Down"]
        + luminosities["2016"]["Up"]
        + luminosities["2017"]["Down"]
        + luminosities["2017"]["Up"]
        + luminosities["2018"]["Down"]
        + luminosities["2018"]["Up"]
    )

    if year == "20152016":
        yield_for_year = (
            (
                luminosities["2015"]["Down"]
                + luminosities["2015"]["Up"]
                + luminosities["2016"]["Down"]
                + luminosities["2016"]["Up"]
            )
            / total_luminosity
            * total_yield
        )
    else:
        yield_for_year = (
            (luminosities[year]["Down"] + luminosities[year]["Up"])
            / total_luminosity
            * total_yield
        )
    return yield_for_year


def getconfig():
    configdict = {}

    ############################################################
    # List of observables for all the PDFs.
    # The content of this dictionary determines the observables
    # to generate for and how may taggers are present.
    ############################################################
    configdict["Observables"] = {
        "BeautyMass": {
            "Type": "RooRealVar",
            "Title": "B mass (MeV/c^2)",
            "Range": [5400, 6200],
        }
    }

    ############################################################
    # List of mass hypotheses for bachelor
    # The content of this dictionary determines how many
    # bachelor PID bins the final dataset is splitted into
    ############################################################
    configdict["Hypothesys"] = ["Lb2LcPi"]

    ############################################################
    # Signal decay, Charm decay mode and year of data taking
    # Splitting per magnet polarity not implemented, at the moment
    ############################################################
    configdict["Decay"] = "Lb2LcPi"
    configdict["CharmModes"] = [
        "pKPi",
    ]
    configdict["Years"] = [
        "2015",
        "2016",
        "2017",
        "2018",
    ]

    ############################################################
    # For PIDK shapes we need also polarities
    # This is also used to split the yields between years properly
    ############################################################
    configdict["Polarity"] = ["Up", "Down"]
    configdict["IntegratedLuminosity"] = luminosities_Lb2LcPi

    configdict["FractionsLuminosity"] = {
        k: v["Up"] / (v["Up"] + v["Down"])
        for k, v in configdict["IntegratedLuminosity"].items()
    }

    configdict["WorkspaceToRead"] = {"File": WORKSPACEFILE, "Workspace": WORKSPACENAME}
    ############################################################
    # List of components with yields to generate.
    # The content of this dictionary determines, for each
    # PID bin and year, how many PDF components are generated.
    # If there is only signal, a TTree ready for sFit is
    # generated directly, without need for doing a (useless)
    # mass fit.
    ############################################################

    # here I define the total yield for Lb2LcPi for Run2
    # - obtained in Lb2DsP Run2 analysis
    # existing YieldForYear function helps in translating
    # the yield for a whole run into each year value
    Lb2LcPi_yield = 404741.67  # +/- 703.81
    combinatorial_yield = 11680.33  # +/- 605.71
    Bd2DPi_yield = 121.10
    Bs2DsPi_yield = 941.00
    Lb2LcK_yield = 2352.52  # +/- 69.17
    Lb2LcRho_yield = 31539.54  # +/- 5237.63
    Lb2ScPi_yield = 52744.25  # +/- 5155.34

    configdict["Components"] = {
        "Signal": {
            "Lb2LcPi": {
                "2015": {
                    "pKPi": YieldForYear("2015", Lb2LcPi_yield),
                },
                "2016": {
                    "pKPi": YieldForYear("2016", Lb2LcPi_yield),
                },
                "2017": {
                    "pKPi": YieldForYear("2017", Lb2LcPi_yield),
                },
                "2018": {
                    "pKPi": YieldForYear("2018", Lb2LcPi_yield),
                },
            }
        },
        "Combinatorial": {
            "Lb2LcPi": {
                "2015": {
                    "pKPi": YieldForYear("2015", combinatorial_yield),
                },
                "2016": {
                    "pKPi": YieldForYear("2016", combinatorial_yield),
                },
                "2017": {
                    "pKPi": YieldForYear("2017", combinatorial_yield),
                },
                "2018": {
                    "pKPi": YieldForYear("2018", combinatorial_yield),
                },
            }
        },
        "Bd2DPi": {
            "Lb2LcPi": {
                "2015": {
                    "pKPi": YieldForYear("2015", Bd2DPi_yield),
                },
                "2016": {
                    "pKPi": YieldForYear("2016", Bd2DPi_yield),
                },
                "2017": {
                    "pKPi": YieldForYear("2017", Bd2DPi_yield),
                },
                "2018": {
                    "pKPi": YieldForYear("2018", Bd2DPi_yield),
                },
            }
        },
        "Bs2DsPi": {
            "Lb2LcPi": {
                "2015": {
                    "pKPi": YieldForYear("2015", Bs2DsPi_yield),
                },
                "2016": {
                    "pKPi": YieldForYear("2016", Bs2DsPi_yield),
                },
                "2017": {
                    "pKPi": YieldForYear("2017", Bs2DsPi_yield),
                },
                "2018": {
                    "pKPi": YieldForYear("2018", Bs2DsPi_yield),
                },
            }
        },
        "Lb2LcK": {
            "Lb2LcPi": {
                "2015": {
                    "pKPi": YieldForYear("2015", Lb2LcK_yield),
                },
                "2016": {
                    "pKPi": YieldForYear("2016", Lb2LcK_yield),
                },
                "2017": {
                    "pKPi": YieldForYear("2017", Lb2LcK_yield),
                },
                "2018": {
                    "pKPi": YieldForYear("2018", Lb2LcK_yield),
                },
            }
        },
        "Lb2LcRho": {
            "Lb2LcPi": {
                "2015": {
                    "pKPi": YieldForYear("2015", Lb2LcRho_yield),
                },
                "2016": {
                    "pKPi": YieldForYear("2016", Lb2LcRho_yield),
                },
                "2017": {
                    "pKPi": YieldForYear("2017", Lb2LcRho_yield),
                },
                "2018": {
                    "pKPi": YieldForYear("2018", Lb2LcRho_yield),
                },
            }
        },
        "Lb2ScPi": {
            "Lb2LcPi": {
                "2015": {
                    "pKPi": YieldForYear("2015", Lb2ScPi_yield),
                },
                "2016": {
                    "pKPi": YieldForYear("2016", Lb2ScPi_yield),
                },
                "2017": {
                    "pKPi": YieldForYear("2017", Lb2ScPi_yield),
                },
                "2018": {
                    "pKPi": YieldForYear("2018", Lb2ScPi_yield),
                },
            }
        },
    }

    ############################################################
    #                      Signal
    ############################################################

    ############################################################

    def getPdfTemplateConfigFromWorkspace(
        name="Signal_BeautyMass_IpatiaPlusJohnsonSU_both_{mode}_{year}",
        modes=["All"],
        years=["2015", "2016", "2017", "2018"],
        lowercasemode=True,
        **kwargs
    ):
        """
        Generate a dictionary of the form
        {
            year1: {
                mode1: {
                    'Type': 'FromWorkspace',
                    'Name': name.format(mode=m1),
                    'WorkspaceName': WorkspaceName,
                    'WorkspaceFile': WorkspaceFile,
                    **kwargs
                },
                mode2: { ... }
            },
            year2: { ... }
        }
        for all modes m1... given in the `modes` and all years in the `years`
        argument.
        """
        return {
            y: {
                m: dict(
                    Type="FromWorkspace",
                    Name=name.format(mode=m.lower() if lowercasemode else m, year=y),
                    **kwargs
                )
                for m in modes
            }
            for y in years
        }

    # the shapes are shared between the years

    configdict["PDFList"] = {
        "BeautyMass": {
            "Signal": {
                "Lb2LcPi": {
                    "2015": {
                        "pKPi": {
                            "a1": 0.84606,  # +/- 0.033
                            "a2": 2.2394,  # +/- 0.20
                            "fracI": 0.25,
                            "l": -1.7,
                            "n1": 1.5051,  # +/- 0.067
                            "n2": 2.0005,  # +/- 0.16
                            "nu": -0.34167,  # +/- 0.063
                            "sigmaI": 2.9006e01,  # +/-  3.48e-01
                            "sigmaJ": 1.5159e01,  # +/-  4.31e-02
                            "tau": 0.31789,  # +/- 0.017
                            "fb": 0.0,
                            "mean": 5617.711,  # \pm0.028
                            "zeta": 0.0,
                            "Type": "IpatiaJohnsonSU",
                        },
                    },
                    "2016": {
                        "pKPi": {
                            "a1": 0.84606,  # +/- 0.033
                            "a2": 2.2394,  # +/- 0.20
                            "fracI": 0.25,
                            "l": -1.7,
                            "n1": 1.5051,  # +/- 0.067
                            "n2": 2.0005,  # +/- 0.16
                            "nu": -0.34167,  # +/- 0.063
                            "sigmaI": 2.9006e01,  # +/-  3.48e-01
                            "sigmaJ": 1.5159e01,  # +/-  4.31e-02
                            "tau": 0.31789,  # +/- 0.017
                            "fb": 0.0,
                            "mean": 5617.711,  # \pm0.028
                            "zeta": 0.0,
                            "Type": "IpatiaJohnsonSU",
                        },
                    },
                    "2017": {
                        "pKPi": {
                            "a1": 0.84606,  # +/- 0.033
                            "a2": 2.2394,  # +/- 0.20
                            "fracI": 0.25,
                            "l": -1.7,
                            "n1": 1.5051,  # +/- 0.067
                            "n2": 2.0005,  # +/- 0.16
                            "nu": -0.34167,  # +/- 0.063
                            "sigmaI": 2.9006e01,  # +/-  3.48e-01
                            "sigmaJ": 1.5159e01,  # +/-  4.31e-02
                            "tau": 0.31789,  # +/- 0.017
                            "fb": 0.0,
                            "mean": 5617.711,  # \pm0.028
                            "zeta": 0.0,
                            "Type": "IpatiaJohnsonSU",
                        },
                    },
                    "2018": {
                        "pKPi": {
                            "a1": 0.84606,  # +/- 0.033
                            "a2": 2.2394,  # +/- 0.20
                            "fracI": 0.25,
                            "l": -1.7,
                            "n1": 1.5051,  # +/- 0.067
                            "n2": 2.0005,  # +/- 0.16
                            "nu": -0.34167,  # +/- 0.063
                            "sigmaI": 2.9006e01,  # +/-  3.48e-01
                            "sigmaJ": 1.5159e01,  # +/-  4.31e-02
                            "tau": 0.31789,  # +/- 0.017
                            "fb": 0.0,
                            "mean": 5617.711,  # \pm0.028
                            "zeta": 0.0,
                            "Type": "IpatiaJohnsonSU",
                        },
                    },
                }
            },
            "Combinatorial": {
                "Lb2LcPi": {
                    "2015": {
                        "pKPi": {
                            "cB": -4.1944e-03,  # +/-  1.59e-04
                            "Type": "Exponential",
                        },
                    },
                    "2016": {
                        "pKPi": {
                            "cB": -4.1944e-03,  # +/-  1.59e-04
                            "Type": "Exponential",
                        },
                    },
                    "2017": {
                        "pKPi": {
                            "cB": -4.1944e-03,  # +/-  1.59e-04
                            "Type": "Exponential",
                        },
                    },
                    "2018": {
                        "pKPi": {
                            "cB": -4.1944e-03,  # +/-  1.59e-04
                            "Type": "Exponential",
                        },
                    },
                }
            },
            "Lb2LcRho": {
                "Lb2LcPi": {
                    "2015": {
                        "pKPi": {
                            "R": 5.3962e00,
                            "a": 4.1087e03,
                            "b": 5.4781e03,
                            "csi": 5.9207e00,
                            "frac": 9.6556e-01,
                            "sigma": 1.6685e01,
                            "shift": 0.0000e00,
                            "Type": "HORNSdini",
                        },
                    },
                    "2016": {
                        "pKPi": {
                            "R": 5.3962e00,
                            "a": 4.1087e03,
                            "b": 5.4781e03,
                            "csi": 5.9207e00,
                            "frac": 9.6556e-01,
                            "sigma": 1.6685e01,
                            "shift": 0.0000e00,
                            "Type": "HORNSdini",
                        },
                    },
                    "2017": {
                        "pKPi": {
                            "R": 5.3962e00,
                            "a": 4.1087e03,
                            "b": 5.4781e03,
                            "csi": 5.9207e00,
                            "frac": 9.6556e-01,
                            "sigma": 1.6685e01,
                            "shift": 0.0000e00,
                            "Type": "HORNSdini",
                        },
                    },
                    "2018": {
                        "pKPi": {
                            "R": 5.3962e00,
                            "a": 4.1087e03,
                            "b": 5.4781e03,
                            "csi": 5.9207e00,
                            "frac": 9.6556e-01,
                            "sigma": 1.6685e01,
                            "shift": 0.0000e00,
                            "Type": "HORNSdini",
                        },
                    },
                }
            },
            "Lb2ScPi": {
                "Lb2LcPi": {
                    "2015": {
                        "pKPi": {
                            "R": 3.7152e00,
                            "a": 4.2100e03,
                            "b": 5.4804e03,
                            "csi": 4.9332e00,
                            "frac": 9.5526e-01,
                            "sigma": 1.3919e01,
                            "shift": 0.0000e00,
                            "Type": "HORNSdini",
                        },
                    },
                    "2016": {
                        "pKPi": {
                            "R": 3.7152e00,
                            "a": 4.2100e03,
                            "b": 5.4804e03,
                            "csi": 4.9332e00,
                            "frac": 9.5526e-01,
                            "sigma": 1.3919e01,
                            "shift": 0.0000e00,
                            "Type": "HORNSdini",
                        },
                    },
                    "2017": {
                        "pKPi": {
                            "R": 3.7152e00,
                            "a": 4.2100e03,
                            "b": 5.4804e03,
                            "csi": 4.9332e00,
                            "frac": 9.5526e-01,
                            "sigma": 1.3919e01,
                            "shift": 0.0000e00,
                            "Type": "HORNSdini",
                        },
                    },
                    "2018": {
                        "pKPi": {
                            "R": 3.7152e00,
                            "a": 4.2100e03,
                            "b": 5.4804e03,
                            "csi": 4.9332e00,
                            "frac": 9.5526e-01,
                            "sigma": 1.3919e01,
                            "shift": 0.0000e00,
                            "Type": "HORNSdini",
                        },
                    },
                }
            },
            "Bd2DPi": {
                "Lb2LcPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBd2DPiPdf_m_both_{year}"
                ),
            },
            "Bs2DsPi": {
                "Lb2LcPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBs2DsPiPdf_m_both_{year}"
                ),
            },
            "Lb2LcK": {
                "Lb2LcPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgLb2LcKPdf_m_both_{year}"
                ),
            },
        },
    }

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    from configutils import main

    main(getconfig())
