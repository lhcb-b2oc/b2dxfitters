###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Bd2DPi"
    configdict["CharmModes"] = {"pKPi"}
    configdict["Backgrounds"] = []

    # year of data taking
    configdict["YearOfDataTaking"] = {"2011", "2012", "2015", "2016", "2017", "2018"}

    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2011": {"Down": 0.5600, "Up": 0.4200},
        "2012": {"Down": 0.9912, "Up": 0.9988},
        "2015": {"Down": 0.18695, "Up": 0.14105},
        "2016": {"Down": 0.85996, "Up": 0.80504},
        "2017": {"Down": 0.87689, "Up": 0.83311},
        "2018": {"Down": 1.04846, "Up": 1.14154},
    }

    configdict["dataName"] = "../data/Lb2Dsp_9fbBR/Lb2LcPi/config_Bd2DPi.txt"

    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotLb2LcPi_DPi", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5200, 5400],
        "InputName": "lab0_MassHypo_LcPi_DPi",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1869.66 - 30, 1869.66 + 30],
        "InputName": "lab2_MassHypo_pKPi_D",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [2000.0, 150000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [250.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 500.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3",
    }

    OfflineSelection = "((((((((((lab1_P>2000 && lab1_P < 150000&& "
    "lab3_P>10000 && lab3_P < 150000&& lab4_P>2000 && lab4_P < 150000&& "
    "lab5_P>2000 && lab5_P < 150 000)&&(lab1_PT > 250 && lab1_PT < 45000&& "
    "lab3_PT > 1000 && lab3_PT < 45000&& lab4_PT > 250 && lab4_PT < 45000&& "
    "lab5_PT > 250 && lab5_PT < 45000))&&(nTracks>15 && nTracks <500))&&"
    "(lab0_ETA > 2.0 && lab0_ETA < 5.0 && lab1_ETA > 2.0 && lab1_ETA < 5.0 && "
    "lab3_ETA > 2.0 && lab3_ETA < 5.0 && lab4_ETA > 2.0 && lab4_ETA < 5.0 && "
    "lab5_ETA > 2.0 && lab5_ETA < 5.0))&&"
    "(lab1_hasRich == 1&& lab3_hasRich == 1&& "
    "lab4_hasRich == 1&& lab5_hasRich == 1))&&"
    "((lab0_LifetimeFit_Lambda_cplus_ctau[0]/0.299*1000)>0))&&"
    "(lab2_FDCHI2_ORIVX > 2.))&&(lab2_FD_ORIVX > 0.))&&"
    "(lab0_MassFitConsD_M[0] > 5000.0 && lab0_MassFitConsD_M[0] < 7000.0))&&"
    "(lab1_M<200))&&(lab1_isMuon == 0 && lab3_isMuon == 0 && "
    "lab4_isMuon == 0 && lab5_isMuon == 0 )"
    PID_cuts = "lab3_ProbNNpi>0.6 && lab4_PIDK>0 && lab5_PIDK<5 && lab1_PIDK<0"
    app = "&&"

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": OfflineSelection + app + PID_cuts,
        "MC": " ",
        "MCID": True,
        # "MCTRUEID": True,
        # "BKGCAT": False,
        "DsHypo": False,
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: pKPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -----     Signal shape         -----   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "Ipatia"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run1": {"pkpi": 5280.48547},
        "Run2": {"pkpi": 5280.94691},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigma"] = {
        "Run1": {"pkpi": 17.32065},
        "Run2": {"pkpi": 19.1675},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run1": {"pkpi": 1.66563},
        "Run2": {"pkpi": 1.6313},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run1": {"pkpi": 3.06536},
        "Run2": {"pkpi": 2.44183},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run1": {"pkpi": 3.16845},
        "Run2": {"pkpi": 2.95043},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run1": {"pkpi": 2.0852},
        "Run2": {"pkpi": 3.10461},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run1": {"pkpi": -5.42959},
        "Run2": {"pkpi": -4.69205},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run1": {"pkpi": 0.0},
        "Run2": {"pkpi": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run1": {"pkpi": 0.0},
        "Run2": {"pkpi": 0.0},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   ----  combinatorial background  ----   #
    #   ------------------------------------   #

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
        "Run1": {"All": [-3.0e-02, -5e-1, 0.0]},
        "Run2": {"All": [-4.4672e-2, -5e-1, 0.0]},
        "Fixed": False,
    }

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    configdict["Yields"] = {}
    configdict["Yields"]["Signal"] = {
        "Run1": {"pKPi": 300000.0},
        "Run2": {"pKPi": 60000.0},
        "Fixed": False,
    }
    configdict["Yields"]["CombBkg"] = {
        "Run1": {"pKPi": 40000.0},
        "Run2": {"pKPi": 10000.0},
        "Fixed": False,
    }

    # ----------------------------------------------------------------------#
    ###                 MDfit plotting settings
    # ----------------------------------------------------------------------#

    from ROOT import kBlue, kRed

    configdict["PlotSettings"] = {}

    configdict["PlotSettings"]["components"] = ["Sig", "CombBkg"]
    configdict["PlotSettings"]["colors"] = [kRed - 7, kBlue - 6]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
