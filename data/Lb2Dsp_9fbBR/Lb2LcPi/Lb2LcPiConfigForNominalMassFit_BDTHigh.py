###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Lb2LcPi"
    configdict["CharmModes"] = {"pKPi"}
    configdict["Backgrounds"] = ["Bd2DPi", "Bs2DsPi", "Lb2LcK", "Lb2LcRho", "Lb2ScPi"]

    # year of data taking
    configdict["YearOfDataTaking"] = {"2011", "2012", "2015", "2016", "2017", "2018"}

    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2011": {"Down": 0.5600, "Up": 0.4200},
        "2012": {"Down": 0.9912, "Up": 0.9988},
        "2015": {"Down": 0.18695, "Up": 0.14105},
        "2016": {"Down": 0.85996, "Up": 0.80504},
        "2017": {"Down": 0.87689, "Up": 0.83311},
        "2018": {"Down": 1.04846, "Up": 1.14154},
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/Lb2Dsp_9fbBR/Lb2LcPi/config_Lb2LcPi.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotLb2LcPi", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5400, 6200],
        "InputName": "lab0_MassFitConsD_M",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [2266, 2306],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [2000.0, 150000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [250.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.94, 1.0],
        "InputName": "BDTGResponse_3",
    }

    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 2500.0],
        "InputName": "lab0_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 3000.0],
        "InputName": "lab2_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab0_ENDVERTEX_CHI2",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab2_ENDVERTEX_CHI2",
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "lab3_ProbNNp>0.6 && lab4_PIDK>0 && lab5_PIDK<5 && lab1_PIDK<0",
        "MC": " ",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": False,
        "DsHypo": False,
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: pKPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    #################################################################################
    #####################                FITTING                #####################
    #################################################################################

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run1": {"All": 5618.90055},
        "Run2": {"All": 5619.496},  # \pm0.073
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run1": {"All": 23.34417},
        "Run2": {"All": 23.4},  # \pm1.1
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run1": {"All": 14.80176},
        "Run2": {"All": 13.77},  # \pm0.16
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run1": {"All": 1.14438},
        "Run2": {"All": 0.854},  # \pm0.052
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run1": {"All": 3.1654},
        "Run2": {"All": 2.47},  # \pm0.79
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run1": {"All": 1.10562},
        "Run2": {"All": 1.619},  # \pm0.093
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run1": {"All": 1.48729},
        "Run2": {"All": 2.66},  # \pm0.54
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run1": {"All": -1.7},
        "Run2": {"All": -1.7},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run1": {"All": 0.0},
        "Run2": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run1": {"All": 0.0},
        "Run2": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run1": {"All": -0.39141},
        "Run2": {"All": -0.37},  # \pm0.12
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run1": {"All": 0.43237},
        "Run2": {"All": 0.287},  # \pm0.018
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run1": {"All": 0.25},
        "Run2": {"All": 0.25},
        "Fixed": True,
    }

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
        "Run1": {"All": -3.0e-03},
        "Run2": {"All": -3.0e-03},
        "Fixed": False,
    }

    # configdict["CombBkgShape"] = {}
    # configdict["CombBkgShape"]["BeautyMass"] = {}
    # configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
    # configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
    #     "Run1": {
    #         "All": -3.0e-03
    #     },
    #     "Run2": {
    #         "All": -3.0e-03
    #     },
    #     "Fixed": False
    # }
    # configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
    #     "Run1": {
    #         "All": 0.
    #     },
    #     "Run2": {
    #         "All": 0.
    #     },
    #     "Fixed": True
    # }
    # configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
    #     "Run1": {
    #         "All": 0.5
    #     },
    #     "Run2": {
    #         "All": 0.5
    #     },
    #     "Fixed": False
    # }

    configdict["Lb2LcRhoShape"] = {}
    configdict["Lb2LcRhoShape"]["BeautyMass"] = {}
    configdict["Lb2LcRhoShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Lb2LcRhoShape"]["BeautyMass"]["R"] = {
        "Run2": {"All": 2.8},  # \pm1.5
        "Fixed": True,
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["a"] = {
        "Run2": {"All": 5112.0},  # \pm75
        "Fixed": True,
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["b"] = {
        "Run2": {"All": 5482.0},  # \pm17
        "Fixed": True,
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["csi"] = {
        "Run2": {"All": 16.0},  # \pm36
        "Fixed": True,
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["frac"] = {
        "Run2": {"All": 0.91},  # \pm0.22
        "Fixed": True,
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["sigma"] = {
        "Run2": {"All": 16.0},  # \pm10
        "Fixed": True,
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["shift"] = {
        "Run2": {"All": 0.0000e00},
        "Fixed": True,
    }

    configdict["Lb2ScPiShape"] = {}
    configdict["Lb2ScPiShape"]["BeautyMass"] = {}
    configdict["Lb2ScPiShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Lb2ScPiShape"]["BeautyMass"]["R"] = {
        "Run2": {"All": 3.30},  # \pm0.22
        "Fixed": True,
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["a"] = {
        "Run2": {"All": 4830.0},  # \pm240.
        "Fixed": True,
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["b"] = {
        "Run2": {"All": 5480.4},  # \pm1.1
        "Fixed": True,
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["csi"] = {
        "Run2": {"All": 4.7},  # \pm2.5
        "Fixed": True,
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["frac"] = {
        "Run2": {"All": 0.9674},  # \pm0.0081
        "Fixed": True,
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["sigma"] = {
        "Run2": {"All": 13.96},  # \pm0.67
        "Fixed": True,
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["shift"] = {
        "Run2": {"All": 0.0000e00},
        "Fixed": True,
    }
    """
    High:
    Bd2DPi sum of entries 614.501996
    Bs2DsPi sum of entries 1106.09769
    Lb2LcK sum of entries 6170.9908
    Lb2LcRho sum of entries 451.34019
    Lb2ScPi sum of entries 20111.266000000003


    Nominal:
    Bd2DPi sum of entries 1047.1947799999998
    Bs2DsPi sum of entries 1775.9464699999999
    Lb2LcK sum of entries 9520.1012
    Lb2LcRho sum of entries 740.14518
    Lb2ScPi sum of entries 34481.386

    """

    BDTHigh_Bd2DPi = 614.501996 / 1047.1947799999998
    BDTHigh_Bs2DsPi = 1106.09769 / 1775.9464699999999
    BDTHigh_Lb2LcK = 6170.9908 / 9520.1012
    BDTHigh_Lb2LcRho = 451.34019 / 740.14518
    BDTHigh_Lb2ScPi = 20111.266000000003 / 34481.386

    # define yields
    configdict["Yields"] = {}

    configdict["Yields"]["Bd2DPi"] = {
        "Run1": {"pKPi": 89.0},
        "Run2": {"pKPi": 121.10 * BDTHigh_Bd2DPi},
        "Fixed": True,
    }
    configdict["Yields"]["Bs2DsPi"] = {
        "Run1": {"pKPi": 28.21},
        "Run2": {"pKPi": 941.0 * BDTHigh_Bs2DsPi},
        "Fixed": True,
    }
    configdict["Yields"]["Lb2LcK"] = {
        "Run1": {"pKPi": [411.0, 13, 0.0]},
        "Run2": {"pKPi": [2300.0 * BDTHigh_Lb2LcK, 71.0 * BDTHigh_Lb2LcK]},
        "Fixed": False,
        "Constrained": True,
    }
    configdict["Yields"]["Lb2LcRho"] = {
        "2011": {"pKPi": 1000.0},
        "2012": {"pKPi": 1000.0},
        "2015": {"pKPi": 15000.0 * BDTHigh_Lb2LcRho},
        "2016": {"pKPi": 15000.0 * BDTHigh_Lb2LcRho},
        "2017": {"pKPi": 15000.0 * BDTHigh_Lb2LcRho},
        "2018": {"pKPi": 15000.0 * BDTHigh_Lb2LcRho},
        "Fixed": False,
    }
    configdict["Yields"]["Lb2ScPi"] = {
        "2011": {"pKPi": 12000.0},
        "2012": {"pKPi": 12000.0},
        "2015": {"pKPi": 20000.0 * BDTHigh_Lb2ScPi},
        "2016": {"pKPi": 20000.0 * BDTHigh_Lb2ScPi},
        "2017": {"pKPi": 20000.0 * BDTHigh_Lb2ScPi},
        "2018": {"pKPi": 20000.0 * BDTHigh_Lb2ScPi},
        "Fixed": False,
    }
    configdict["Yields"]["Signal"] = {
        "2011": {"pKPi": 5e4},
        "2012": {"pKPi": 5e4},
        "2015": {"pKPi": 1.1e5},
        "2016": {"pKPi": 1.1e5},
        "2017": {"pKPi": 1.1e5},
        "2018": {"pKPi": 1.1e5},
        "Fixed": False,
    }
    configdict["Yields"]["CombBkg"] = {
        "2011": {"pKPi": 20000.0},
        "2012": {"pKPi": 20000.0},
        "2015": {"pKPi": 28125.0},
        "2016": {"pKPi": 28125.0},
        "2017": {"pKPi": 28125.0},
        "2018": {"pKPi": 28125.0},
        "Fixed": False,
    }

    ##########################################################################
    ###################              Plotting              ###################
    ##########################################################################

    configdict["PlotSettings"] = {}

    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Bd2DPi",
        "Bs2DsPi",
        "Lb2LcK",
        "Lb2LcRho",
        "Lb2ScPi",
    ]
    configdict["PlotSettings"]["colors"] = [
        "#006837",
        "#cccccc",
        "#fbb4b9",
        "#d7301f",
        "#31a354",
        "#78c679",
        "#c2e699",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
