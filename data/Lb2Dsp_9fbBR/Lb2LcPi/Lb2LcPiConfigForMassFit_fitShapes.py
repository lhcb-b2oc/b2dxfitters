###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# from ROOT import *


def getconfig():
    configdict = {}

    # import ROOT
    # from ROOT import *

    configdict["Decay"] = "Lb2LcPi"

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5200, 6200],
        "Name": "BeautyMass",
        "InputName": "lab0_MassFitConsD_M",
    }

    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1972, 2065],
        "Name": "CharmMass",
        "InputName": "lab2_MM",
    }
    # configdict["BasicVariables"]["BacPIDK"]       = {
    #     "Range"                  : [-150.0,  150.0],
    #     "Name"                   : "BacPIDK",
    #     "InputName"              : "lab1_ProbNNp"
    # }

    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "Name": "BDTG",
        "InputName": "BDTGResponse_3",
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    configdict["constParams"] = []

    # PDF for each fitted component
    # Structure: decay->hypo->observable->pdf
    configdict["pdfList"] = {}

    configdict["pdfList"]["Signal"] = {}
    configdict["pdfList"]["Signal"]["Lb2LcPi"] = {}
    configdict["pdfList"]["Signal"]["Lb2LcPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Signal"]["Lb2LcPi"]["BeautyMass"]["DoubleCrystalBall"] = {
        "Title": "B_{s}#rightarrowD_{s}#pi",
        "Bins": 250,
        "Min": 5200.0,
        "Max": 6200.0,
        "Unit": "MeV/c^{2}",
        "sameMean": True,
        "mean": [5623.0, 5500, 5700],
        "sigma1": [18.74, 1.0, 100.0],
        "sigma2": [12.23, 1.0, 100.0],
        "alpha1": [1.657, 0.01, 6.0],
        "alpha2": [-2.063, -6.0, -0.01],
        "n1": [1.518, 0.01, 6.0],
        "n2": [2.30, 0.01, 6.0],
        "frac": [0.5],
    }

    configdict["pdfList"]["Signal"]["Lb2LcPi"]["BeautyMass"]["Ipatia"] = {
        "Title": "B_{s}#rightarrowD_{s}#pi",
        "Bins": 250,
        "Min": 5200.0,
        "Max": 6200.0,
        "Unit": "MeV/c^{2}",
        "mean": [5621.0, 5500, 5700],
        "sigma": [18.74, 1.0, 100.0],
        "zeta": [0.0],
        "fb": [0.0],
        "l": [-1.47, -20.0, -0.01],
        "a1": [2.005, 0.01, 6.0],
        "a2": [3.07, 0.01, 6.0],
        "n1": [1.365, 0.01, 6.0],
        "n2": [2.26, 0.01, 6.0],
    }

    configdict["pdfList"]["Signal"]["Lb2LcPi"]["BeautyMass"]["IpatiaPlusGaussian"] = {
        "Title": "B_{d}#rightarrowD#pi",
        "Bins": 250,
        "Min": 5400.0,
        "Max": 5800.0,
        "Unit": "MeV/c^{2}",
        "mean": [5367.22, 5250, 5400],
        "sigmaI": [14.0, 1.0, 100.0],
        "sigmaG": [18.55, 1.0, 100.0],
        "zeta": [0.0],
        "fb": [0.0],
        "l": [-6.2, -20.0, -0.01],
        "a1": [1.775, 0.01, 6.0],
        "a2": [2.14, 0.01, 6.0],
        "n1": [1.343, 0.01, 6.0],
        "n2": [2.65, 0.01, 6.0],
        "fracI": [0.685, 0.2, 0.95],
    }

    configdict["pdfList"]["Signal"]["Lb2LcPi"]["BeautyMass"]["IpatiaPlusJohnsonSU"] = {
        "Title": "B_{s}#rightarrowD_{s}#pi",
        "Bins": 250,
        "Min": 5200.0,
        "Max": 6200.0,
        "Unit": "MeV/c^{2}",
        "mean": [5620.6, 5500, 5700],
        "sigmaI": [25.0, 15.0, 50.0],
        "sigmaJ": [15.5, 12.5, 20.0],
        "zeta": [0.0],
        "fb": [0.0],
        "a1": [2.4, 0.2, 5.0],
        "a2": [3.0, 0.2, 5.0],
        "n1": [1.4, 0.01, 6.0],
        "n2": [1.7, 0.01, 3.0],
        "l": [-3.6, -20.0, -0.01],
        "tau": [0.2, 0.0, 2.0],
        "nu": [-1.2, -2.0, 0.5],
        "fracI": [0.95, 0.01, 0.99],
    }

    # Dataset prefix
    configdict["dataSetPrefix"] = {"Signal": "dataSetMC_Lb2LcPi_"}

    # Axes titles
    configdict["AxisTitle"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {"Lb2LcPi": "m(D_{s}^{-}p) [MeV/c^{2}]"}

    return configdict
