###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Lb2LcPiConfigForNominalMassFit_BDTLow import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    configdict["pdfList"] = {}

    configdict["pdfList"]["Signal"] = {}
    configdict["pdfList"]["Signal"]["Lb2LcPi"] = {}
    configdict["pdfList"]["Signal"]["Lb2LcPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Signal"]["Lb2LcPi"]["BeautyMass"]["DoubleCrystalBall"] = {
        "Title": "#Lambda_{b}#rightarrow#Lambda_{c}#pi",
        "Bins": 160,
        "Min": 5400.0,
        "Max": 6200.0,
        "Unit": "MeV/c^{2}",
        "sameMean": True,
        "mean": [5623.0, 5500, 5700],
        "sigma1": [18.74, 1.0, 100.0],
        "sigma2": [12.23, 1.0, 100.0],
        "alpha1": [1.657, 0.01, 6.0],
        "alpha2": [-2.063, -6.0, -0.01],
        "n1": [1.518, 0.01, 6.0],
        "n2": [2.30, 0.01, 6.0],
        "frac": [0.5],
    }

    configdict["pdfList"]["Signal"]["Lb2LcPi"]["BeautyMass"]["Ipatia"] = {
        "Title": "#Lambda_{b}#rightarrow#Lambda_{c}#pi",
        "Bins": 160,
        "Min": 5400.0,
        "Max": 6200.0,
        "Unit": "MeV/c^{2}",
        "mean": [5620.0, 5500, 5700],
        "sigma": [16.0, 1.0, 100.0],
        "zeta": [0.0],
        "fb": [0.0],
        "l": [-3.0 - 20.0, -0.01],
        "a1": [2.0, 0.01, 6.0],
        "a2": [3.0, 0.01, 6.0],
        "n1": [1.3, 0.01, 6.0],
        "n2": [2.0, 0.01, 6.0],
    }

    configdict["pdfList"]["Signal"]["Lb2LcPi"]["BeautyMass"]["IpatiaPlusJohnsonSU"] = {
        "Title": "#Lambda_{b}#rightarrow#Lambda_{c}#pi",
        "Bins": 160,
        "Min": 5400.0,
        "Max": 6200.0,
        "Unit": "MeV/c^{2}",
        "mean": [5620.6, 5500, 5700],
        "sigmaI": [14.5, 10.0, 50.0],
        "sigmaJ": [14.2, 10.0, 50.0],
        "zeta": [0.0],
        "fb": [0.0],
        "a1": [1.4, 0.2, 5.0],
        "a2": [1.7, 0.2, 5.0],
        "n1": [1.6, 0.01, 6.0],
        "n2": [2.1, 0.01, 3.0],
        "l": [-1.7],  # for stability
        "tau": [0.3, 0.0, 2.0],
        "nu": [-0.1, -2.0, 0.5],
        "fracI": [0.25],  # for stability
    }

    configdict["pdfList"]["Lb2LcRho"] = {}
    configdict["pdfList"]["Lb2LcRho"]["Lb2LcPi"] = {}
    configdict["pdfList"]["Lb2LcRho"]["Lb2LcPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Lb2LcRho"]["Lb2LcPi"]["BeautyMass"]["HORNSdini"] = {
        "Title": "#Lambda_{b}#rightarrow#Lambda_{c}#rho",
        "Bins": 100,
        "Min": 5400.0,  # 5250.0,
        "Max": 5600.0,
        "Unit": "MeV/c^{2}",
        "a": [3300, 2500, 5400],
        "b": [5573, 5400, 5800],
        "csi": [2.1, -5.0, 100.0],
        "sigma": [25.0, 2.0, 100.0],
        "shift": [0.0],
        "R": [5.8, 0.5, 40.0],
        "frac": [0.8, 0.0, 1.0],
    }
    #     "Min": 5250.0,
    #     "Max": 6200.0,
    #     "Unit": "MeV/c^{2}",
    #     "a": [4108.7],
    #     "b": [5478.1],
    #     "csi": [5.9207],
    #     "sigma": [16.685],
    #     "shift": [0.0],
    #     "R": [5.3962],
    #     "frac": [0.9656],
    # }

    configdict["pdfList"]["Lb2ScPi"] = {}
    configdict["pdfList"]["Lb2ScPi"]["Lb2LcPi"] = {}
    configdict["pdfList"]["Lb2ScPi"]["Lb2LcPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Lb2ScPi"]["Lb2LcPi"]["BeautyMass"]["HORNSdini"] = {
        "Title": "#Lambda_{b}#rightarrow#Sigma_{c}#pi",
        "Bins": 100,
        "Min": 5400.0,
        "Max": 5600.0,
        "Unit": "MeV/c^{2}",
        "a": [4300, 2500, 5400],
        "b": [5480, 5400, 5800],
        "csi": [5.1, -5.0, 100.0],
        "sigma": [14.0, 2.0, 100.0],
        "shift": [0.0],
        "R": [4.0, 0.5, 40.0],
        "frac": [0.95, 0.0, 1.0],
    }

    # Dataset prefix
    configdict["dataSetPrefix"] = {
        "Signal": "dataSetMC_Lb2LcPi_",
        "Lb2LcRho": "dataSetMC_Lb2LcRho_",
        "Lb2ScPi": "dataSetMC_Lb2ScPi_",
    }

    happyplus = "#lower[-0.95]{#scale[0.6]{+}}"
    happymin = "#lower[-1.15]{#scale[0.7]{-}}"

    # Axes titles
    configdict["AxisTitle"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {
        "Lb2LcPi": "m(#bar{#Lambda}_{c}#kern[-0.3]{"
        + happymin
        + "}#kern[0.1]{#pi"
        + happyplus
        + "})"
    }

    return configdict
