# Contact:
in case of troubles contact: [Agnieszka Dziurda](agnieszka.dziurda@cern.ch)

# Files:
- Lb2LcPiConfigForNominalMassFit.py
  - nominal config file
- Lb2LcPiConfigForDataWorkspace.py
  - config file for data preparation,
  - inherits from Lb2LcPiConfigForNominalMassFit.py,
  - BDT range changed to [-1,1],
- Lb2LcPiConfigForMCWorkspace.py
  - config file for MC templates preparation,
  - inherits from Lb2LcPiConfigForNominalMassFit.py
  - additional	   informations for MC weighting
- Lb2LcPiConfigForSignal.py
  - copied from Marinda, didn't test it
- Lb2LcPiConfigForNominalMassFit_fitShapes2.py
  - copied from Marinda, didn't test it
- config_Lb2LcPi.txt
  - paths to files

# Saving logfile

Please use one of two options:
- >& logfile.txt &
- |& tee logfile.txt


# How to run code

You need to be in:
cd UraniaDev_v7r0/PhysFit/B2DXFitters/scripts/

# Obtaining data sample:

```
../../../run python prepareWorkspace.py --configName ../data/Lb2Dsp_5fbBR/Lb2LcPi/Lb2LcPiConfigForDataWorkspace.py --Data --debug -s work_data_lb2lcpi.root >&  log_work_data_lb2lcpi.txt &
```

# Obtaining MC templates:

```
../../../run python prepareWorkspace.py --configName ../data/Lb2Dsp_5fbBR/Lb2LcPi/Lb2LcPiConfigForMCWorkspace.py --MC --debug -s work_mc_lb2lcpi_run2.root >& log_work_mc_lb2lcpi.txt &
```


# Performing MDFit
- Run 1 and Run 2 - combined, years & polarities combined
```
../../../run python runMDFitter.py --configName ../data/Lb2Dsp_5fbBR/Lb2LcPi/Lb2LcPiConfigForNominalMassFit.py --fileData /eos/lhcb/wg/b2oc/Lb2Dsp/workspaces/work_data_lb2lcpi.root --fileName /eos/lhcb/wg/b2oc/Lb2Dsp/workspaces/work_mc_lb2lcpi.root --mode pkpi --pol both --year 2011 2012 2015 2016 --merge pol run1 run2 -s WS_MDFit_Lb2LcPi_Runs_combined.root --dim 1 --debug > & log_mdfit_lb2lcpi_combined.txt &
```

- Run 1 and Run 2 - simultanous, years & polarities combined
```
../../../run python runMDFitter.py --configName ../data/Lb2Dsp_5fbBR/Lb2LcPi/Lb2LcPiConfigForNominalMassFit.py --fileData /eos/lhcb/wg/b2oc/Lb2Dsp/workspaces/work_data_lb2lcpi.root --fileName /eos/lhcb/wg/b2oc/Lb2Dsp/workspaces/work_mc_lb2lcpi.root --mode pkpi --pol both --year 2011 2012 2015 2016 --merge pol run1 run2 -s WS_MDFit_Lb2LcPi_Runs.root --dim 1 --debug >& log_mdfit_lb2lcpi.txt &
```

- Run 1 - years combined, polarities combined
```
../../../run python runMDFitter.py --configName ../data/Lb2Dsp_5fbBR/Lb2LcPi/Lb2LcPiConfigForNominalMassFit.py --fileData /eos/lhcb/wg/b2oc/Lb2Dsp/workspaces/work_data_lb2lcpi.root --fileName /eos/lhcb/wg/b2oc/Lb2Dsp/workspaces/work_mc_lb2lcpi.root --mode pkpi --pol both --year 2011 2012 --merge pol run1 -s WS_MDFit_Lb2LcPi_Run1.root --dim 1 --debug > & log_mdfit_lb2lcpi_run1.txt &
```

- Run 2 - years combined, polarities combined
```
../../../run python runMDFitter.py --configName ../data/Lb2Dsp_5fbBR/Lb2LcPi/Lb2LcPiConfigForNominalMassFit.py --fileData /eos/lhcb/wg/b2oc/Lb2Dsp/workspaces/work_data_lb2lcpi.root --fileName /eos/lhcb/wg/b2oc/Lb2Dsp/workspaces/work_mc_lb2lcpi.root --mode pkpi --pol both --year 2015 2016 --merge pol run2 -s WS_MDFit_Lb2LcPi_Run2.root --dim 1 --debug > & log_mdfit_lb2lcpi_run2.txt &
```

# Plotting results

- Run 1 and Run 2 - simultanous, years & polarities combined
```
../../../run python plotMDFitter.py WS_MDFit_Lb2LcPi_Runs.root --configName ../data/Lb2Dsp_5fbBR/Lb2LcPi/Lb2LcPiConfigForNominalMassFit.py --mode pkpi --pol both --year 2011 2012 2015 2016 --merge pol run1 run2 --bin 200
```

- Run 1 - years combined, polarities combined
```
../../../run plotMDFitter.py WS_MDFit_Lb2LcPi_Run1.root --configName ../data/Lb2Dsp_5fbBR/Lb2LcPi/Lb2LcPiConfigForNominalMassFit.py --mode pkpi --pol both --year 2012 2011 --merge pol run1 --legend
```

- Run 2 - years combined, polarities combined
```
../../../run plotMDFitter.py WS_MDFit_Lb2LcPi_Run2.root --configName ../data/Lb2Dsp_5fbBR/Lb2LcPi/Lb2LcPiConfigForNominalMassFit.py --mode pkpi --pol both --year 2015 2016 --merge pol run2 --legend
```
