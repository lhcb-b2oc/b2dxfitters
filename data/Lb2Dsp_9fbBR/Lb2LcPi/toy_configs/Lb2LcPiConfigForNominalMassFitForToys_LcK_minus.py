###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def getconfig():
    from Lb2LcPiConfigForNominalMassFitForToys import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    configdict["Yields"]["Lb2LcK"] = {
        "Run1": {"pKPi": [411.0, 13, 0.0]},
        "Run2": {"pKPi": [2300.0 - 71.0, 71.0]},
        "Fixed": False,
        "Constrained": True,
    }

    return configdict
