###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def getconfig():
    from Lb2LcPiConfigForNominalMassFitForToys import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
        "Run1": {"All": -5.0279e-03},
        "Run2": {"All": -6.65087e-03},  # +/-  1.36029e-03
        "Fixed": False,
    }
    configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
        "Run1": {"All": 0.0},
        "Run2": {"All": 0.0},
        "Fixed": True,
    }
    configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
        "Run1": {"All": 8.9591e-01},
        "Run2": {"All": 8.9591e-01},
        "Fixed": False,
    }

    return configdict
