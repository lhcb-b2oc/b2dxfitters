###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Lb2LcPiConfigForNominalMassFitForToys import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "DoubleCrystalBall"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {"pkpi": 5619.77785},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigma1"] = {
        "Run2": {"pkpi": 11.54646},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigma2"] = {
        "Run2": {"pkpi": 16.49301},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["alpha1"] = {
        "Run2": {"pkpi": 1.69679},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["alpha2"] = {
        "Run2": {"pkpi": -2.12146},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {"pkpi": 1.24921},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {"pkpi": 2.30428},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["frac"] = {
        "Run2": {"pkpi": 0.5},
        "Fixed": True,
    }

    return configdict
