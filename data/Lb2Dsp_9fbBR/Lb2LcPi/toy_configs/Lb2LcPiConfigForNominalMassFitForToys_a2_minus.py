###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def getconfig():
    from Lb2LcPiConfigForNominalMassFitForToys import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run1": {"All": 3.1654},
        "Run2": {"All": 2.2394 - 0.20},
        "Fixed": True,
    }

    return configdict
