###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


from uncertainties import ufloat

# integrated luminosity in each year of data taking
# (necessary in case of PIDK shapes)
luminosities_Lb2LcPi = {
    "2011": {"Down": 0.5600, "Up": 0.4200},
    "2012": {"Down": 0.9912, "Up": 0.9988},
    "2015": {"Down": 0.18695, "Up": 0.14105},
    "2016": {"Down": 0.85996, "Up": 0.80504},
    "2017": {"Down": 0.87689, "Up": 0.83311},
    "2018": {"Down": 1.04846, "Up": 1.14154},
}


def TotalFracForYear(year):
    """
    Fraction in a Run2

    Helps in obtaining the fraction for years in Run2 (2015, 2016, 2017, 2018)
    year, pol - string
    """
    luminosities = luminosities_Lb2LcPi
    total_luminosity = (
        luminosities["2015"]["Down"]
        + luminosities["2015"]["Up"]
        + luminosities["2016"]["Down"]
        + luminosities["2016"]["Up"]
        + luminosities["2017"]["Down"]
        + luminosities["2017"]["Up"]
        + luminosities["2018"]["Down"]
        + luminosities["2018"]["Up"]
    )
    frac_for_year = 0.0
    if year == "20152016":
        frac_for_year = (
            luminosities["2015"]["Down"]
            + luminosities["2015"]["Up"]
            + luminosities["2016"]["Down"]
            + luminosities["2016"]["Up"]
        ) / total_luminosity
    else:
        frac_for_year = (
            luminosities[year]["Up"] + luminosities[year]["Down"]
        ) / total_luminosity
    return frac_for_year


def TranslateGCForRun2IntoYear(nb, year):
    nb *= TotalFracForYear(str(year))
    return nb


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Lb2LcPi"
    configdict["CharmModes"] = {"pKPi"}
    configdict["Backgrounds"] = ["Bd2DPi", "Bs2DsPi", "Lb2LcK", "Lb2LcRho", "Lb2ScPi"]

    # year of data taking
    configdict["YearOfDataTaking"] = {"2011", "2012", "2015", "2016", "2017", "2018"}

    configdict["IntegratedLuminosity"] = luminosities_Lb2LcPi

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/Lb2Dsp_9fbBR/Lb2LcPi/config_Lb2LcPi.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotLb2LcPi", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5400, 6200],
        "InputName": "lab0_MassFitConsD_M",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [2266, 2306],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [2000.0, 150000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [250.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3",
    }

    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 2500.0],
        "InputName": "lab0_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 3000.0],
        "InputName": "lab2_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab0_ENDVERTEX_CHI2",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab2_ENDVERTEX_CHI2",
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "lab3_ProbNNp>0.6 && lab4_PIDK>0 && lab5_PIDK<5 && lab1_PIDK<0",
        "MC": " ",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": False,
        "DsHypo": False,
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: pKPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    ############################################################################
    ####################                FITTING             ####################
    ############################################################################

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run1": {"All": 5618.90055},
        "20152016": {"All": 5619.36218},
        "2017": {"All": 5619.36218},
        "2018": {"All": 5619.36218},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run1": {"All": 23.34417},
        "20152016": {"All": 25.57416},  # 30.8749 # +/- 0.360197
        "2017": {"All": 25.57416},  # 30.8749 # +/- 0.360197
        "2018": {"All": 25.57416},  # 30.8749 # +/- 0.360197
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run1": {"All": 14.80176},
        "20152016": {"All": 13.92415},  # 15.0172, # +/- 0.0390602
        "2017": {"All": 13.92415},  # 15.0172, # +/- 0.0390602
        "2018": {"All": 13.92415},  # 15.0172, # +/- 0.0390602
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run1": {"All": 1.14438},
        "20152016": {"All": 0.84606},  # +/- 0.033
        "2017": {"All": 0.84606},  # +/- 0.033
        "2018": {"All": 0.84606},  # +/- 0.033
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run1": {"All": 3.1654},
        "20152016": {"All": 2.2394},  # +/- 0.20
        "2017": {"All": 2.2394},  # +/- 0.20
        "2018": {"All": 2.2394},  # +/- 0.20
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run1": {"All": 1.10562},
        "20152016": {"All": 1.5051},  # +/- 0.067
        "2017": {"All": 1.5051},  # +/- 0.067
        "2018": {"All": 1.5051},  # +/- 0.067
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run1": {"All": 1.48729},
        "20152016": {"All": 2.0005},  # +/- 0.16
        "2017": {"All": 2.0005},  # +/- 0.16
        "2018": {"All": 2.0005},  # +/- 0.16
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run1": {"All": -1.7},
        "20152016": {"All": -1.7},
        "2017": {"All": -1.7},
        "2018": {"All": -1.7},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run1": {"All": 0.0},
        "20152016": {"All": 0.0},
        "2017": {"All": 0.0},
        "2018": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run1": {"All": 0.0},
        "20152016": {"All": 0.0},
        "2017": {"All": 0.0},
        "2018": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run1": {"All": -0.39141},
        "20152016": {"All": -0.34167},  # +/- 0.063
        "2017": {"All": -0.34167},  # +/- 0.063
        "2018": {"All": -0.34167},  # +/- 0.063
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run1": {"All": 0.43237},
        "20152016": {"All": 0.31789},  # +/- 0.017
        "2017": {"All": 0.31789},  # +/- 0.017
        "2018": {"All": 0.31789},  # +/- 0.017
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run1": {"All": 0.25},
        "20152016": {"All": 0.25},
        "2017": {"All": 0.25},
        "2018": {"All": 0.25},
        "Fixed": True,
    }

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
        "Run1": {"All": -3.0e-03},
        "20152016": {"All": -3.0e-03},
        "2017": {"All": -3.0e-03},
        "2018": {"All": -3.0e-03},
        "Fixed": False,
    }

    configdict["Lb2LcRhoShape"] = {}
    configdict["Lb2LcRhoShape"]["BeautyMass"] = {}
    configdict["Lb2LcRhoShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Lb2LcRhoShape"]["BeautyMass"]["R"] = {
        "20152016": {"All": 5.3962e00},
        "2017": {"All": 5.3962e00},
        "2018": {"All": 5.3962e00},
        "Fixed": True,
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["a"] = {
        "20152016": {"All": 4.1087e03},
        "2017": {"All": 4.1087e03},
        "2018": {"All": 4.1087e03},
        "Fixed": True,
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["b"] = {
        "20152016": {"All": 5.4781e03},
        "2017": {"All": 5.4781e03},
        "2018": {"All": 5.4781e03},
        "Fixed": True,
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["csi"] = {
        "20152016": {"All": 5.9207e00},
        "2017": {"All": 5.9207e00},
        "2018": {"All": 5.9207e00},
        "Fixed": True,
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["frac"] = {
        "20152016": {"All": 9.6556e-01},
        "2017": {"All": 9.6556e-01},
        "2018": {"All": 9.6556e-01},
        "Fixed": True,
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["sigma"] = {
        "20152016": {"All": 1.6685e01},
        "2017": {"All": 1.6685e01},
        "2018": {"All": 1.6685e01},
        "Fixed": True,
    }
    configdict["Lb2LcRhoShape"]["BeautyMass"]["shift"] = {
        "20152016": {"All": 0.0000e00},
        "2017": {"All": 0.0000e00},
        "2018": {"All": 0.0000e00},
        "Fixed": True,
    }

    configdict["Lb2ScPiShape"] = {}
    configdict["Lb2ScPiShape"]["BeautyMass"] = {}
    configdict["Lb2ScPiShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Lb2ScPiShape"]["BeautyMass"]["R"] = {
        "20152016": {"All": 3.7152e00},
        "2017": {"All": 3.7152e00},
        "2018": {"All": 3.7152e00},
        "Fixed": True,
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["a"] = {
        "20152016": {"All": 4.2100e03},
        "2017": {"All": 4.2100e03},
        "2018": {"All": 4.2100e03},
        "Fixed": True,
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["b"] = {
        "20152016": {"All": 5.4804e03},
        "2017": {"All": 5.4804e03},
        "2018": {"All": 5.4804e03},
        "Fixed": True,
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["csi"] = {
        "20152016": {"All": 4.9332e00},
        "2017": {"All": 4.9332e00},
        "2018": {"All": 4.9332e00},
        "Fixed": True,
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["frac"] = {
        "20152016": {"All": 9.5526e-01},
        "2017": {"All": 9.5526e-01},
        "2018": {"All": 9.5526e-01},
        "Fixed": True,
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["sigma"] = {
        "20152016": {"All": 1.3919e01},
        "2017": {"All": 1.3919e01},
        "2018": {"All": 1.3919e01},
        "Fixed": True,
    }
    configdict["Lb2ScPiShape"]["BeautyMass"]["shift"] = {
        "20152016": {"All": 0.0000e00},
        "2017": {"All": 0.0000e00},
        "2018": {"All": 0.0000e00},
        "Fixed": True,
    }

    # define yields
    configdict["Yields"] = {}

    configdict["Yields"]["Bd2DPi"] = {
        "Run1": {"pKPi": 89.0},
        "20152016": {"pKPi": TranslateGCForRun2IntoYear(121.10, "20152016")},
        "2017": {"pKPi": TranslateGCForRun2IntoYear(121.10, "2017")},
        "2018": {"pKPi": TranslateGCForRun2IntoYear(121.10, "2018")},
        "Fixed": True,
    }
    configdict["Yields"]["Bs2DsPi"] = {
        "Run1": {"pKPi": 28.21},
        "20152016": {"pKPi": TranslateGCForRun2IntoYear(941.0, "20152016")},
        "2017": {"pKPi": TranslateGCForRun2IntoYear(941.0, "2017")},
        "2018": {"pKPi": TranslateGCForRun2IntoYear(941.0, "2018")},
        "Fixed": True,
    }
    Lb2LcK_yield = ufloat(2300.0, 71.0)

    configdict["Yields"]["Lb2LcK"] = {
        "Run1": {"pKPi": [411.0, 13, 0.0]},
        "20152016": {
            "pKPi": [
                TranslateGCForRun2IntoYear(Lb2LcK_yield, "20152016").n,
                TranslateGCForRun2IntoYear(Lb2LcK_yield, "20152016").s,
            ]
        },
        "2017": {
            "pKPi": [
                TranslateGCForRun2IntoYear(Lb2LcK_yield, "2017").n,
                TranslateGCForRun2IntoYear(Lb2LcK_yield, "2017").s,
            ]
        },
        "2018": {
            "pKPi": [
                TranslateGCForRun2IntoYear(Lb2LcK_yield, "2018").n,
                TranslateGCForRun2IntoYear(Lb2LcK_yield, "2018").s,
            ]
        },
        # "Run2": {
        #     "pKPi": [2300.0, 71.0]
        # },
        "Fixed": False,
        "Constrained": True,
    }
    # TODO: add fraction function for fraction in a one year
    configdict["Yields"]["Lb2LcRho"] = {
        "2011": {"pKPi": 1000.0},
        "2012": {"pKPi": 1000.0},
        "2015": {"pKPi": 15000.0},
        "2016": {"pKPi": 15000.0},
        "2017": {"pKPi": 15000.0},
        "2018": {"pKPi": 15000.0},
        "Fixed": False,
    }
    configdict["Yields"]["Lb2ScPi"] = {
        "2011": {"pKPi": 12000.0},
        "2012": {"pKPi": 12000.0},
        "2015": {"pKPi": 20000.0},
        "2016": {"pKPi": 20000.0},
        "2017": {"pKPi": 20000.0},
        "2018": {"pKPi": 20000.0},
        "Fixed": False,
    }
    configdict["Yields"]["Signal"] = {
        "2011": {"pKPi": 5e4},
        "2012": {"pKPi": 5e4},
        "2015": {"pKPi": 1.1e5},
        "2016": {"pKPi": 1.1e5},
        "2017": {"pKPi": 1.1e5},
        "2018": {"pKPi": 1.1e5},
        "Fixed": False,
    }
    configdict["Yields"]["CombBkg"] = {
        "2011": {"pKPi": 20000.0},
        "2012": {"pKPi": 20000.0},
        "2015": {"pKPi": 28125.0},
        "2016": {"pKPi": 28125.0},
        "2017": {"pKPi": 28125.0},
        "2018": {"pKPi": 28125.0},
        "Fixed": False,
    }

    ############################################################################
    ##################                Plotting                ##################
    ############################################################################

    configdict["PlotSettings"] = {}

    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Bd2DPi",
        "Bs2DsPi",
        "Lb2LcK",
        "Lb2LcRho",
        "Lb2ScPi",
    ]
    configdict["PlotSettings"]["colors"] = [
        "#006837",
        "#cccccc",
        "#fbb4b9",
        "#d7301f",
        "#31a354",
        "#78c679",
        "#c2e699",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
