###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Bs2DsPiConfigForNominalMassFit import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    configdict["pdfList"] = {}
    configdict["pdfList"]["Signal"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsPi"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Signal"]["Bs2DsPi"]["BeautyMass"]["IpatiaPlusJohnsonSU"] = {
        "Title": "B_{s}#rightarrowD_{s}#pi",
        "Bins": 200,
        "Min": 5000.0,
        "Max": 6000.0,
        "Unit": "MeV/c^{2}",
        "mean": [5367.2, 5300, 5400],
        "sigmaI": [24.0, 10.0, 50.0],
        "sigmaJ": [14.1, 10.0, 50.0],
        "zeta": [0.0],
        "fb": [0.0],
        "a1": [1.0, 0.2, 5.0],
        "a2": [2.5, 0.2, 5.0],
        "n1": [1.4, 0.01, 6.0],
        "n2": [1.8, 0.01, 3.0],
        "l": [-1.7],  # for stability
        "tau": [0.3, 0.0, 2.0],
        "nu": [-0.33, -2.0, 0.5],
        "fracI": [0.31, 0.01, 0.99],
    }

    configdict["pdfList"]["Bs2DsstPi"] = {}
    configdict["pdfList"]["Bs2DsstPi"]["Bs2DsPi"] = {}
    configdict["pdfList"]["Bs2DsstPi"]["Bs2DsPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Bs2DsstPi"]["Bs2DsPi"]["BeautyMass"][
        "HILLdiniPlusHORNSdini"
    ] = {
        "Title": "B_{s}#rightarrowD_{s}#rho",
        "Bins": 100,
        "Min": 5000.0,
        "Max": 5400.0,
        "Unit": "MeV/c^{2}",
        "ahill": [4550, 4300, 5300],
        "bhill": [5340, 4300, 5500],
        "csihill": [-1.4, -3.0, 3.0],
        "sigma": [40.0, 2.0, 80.0],
        "ahorns": [5100.0, 5000, 5150],
        "bhorns": [5200.0, 5150, 5300],
        "csihorns": [0.5, 0.1, 2.8],
        "sigmahorns": [14.0, 3.0, 30.0],
        "shift": [0.0],
        "R": [2.0, 0.5, 40.0],
        "frac": [0.3, 0.0, 1.0],
        "frachorns": [0.3, 0.0, 0.7],
    }

    configdict["pdfList"]["Bs2DsRho"] = {}
    configdict["pdfList"]["Bs2DsRho"]["Bs2DsPi"] = {}
    configdict["pdfList"]["Bs2DsRho"]["Bs2DsPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Bs2DsRho"]["Bs2DsPi"]["BeautyMass"]["HORNSdini"] = {
        "Title": "B_{s}#rightarrowD_{s}#rho",
        "Bins": 100,
        "Min": 5000.0,
        "Max": 5400.0,
        "Unit": "MeV/c^{2}",
        "a": [3500, 3000, 4500],
        "b": [5200, 4300, 5500],
        "csi": [1.1, -2.0, 6.0],
        "sigma": [40.0, 2.0, 80.0],
        "shift": [0.0],
        "R": [5.8, 0.5, 40.0],
        "frac": [0.98, 0.0, 1.0],
    }

    configdict["pdfList"]["Bs2DsstRho"] = {}
    configdict["pdfList"]["Bs2DsstRho"]["Bs2DsPi"] = {}
    configdict["pdfList"]["Bs2DsstRho"]["Bs2DsPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Bs2DsstRho"]["Bs2DsPi"]["BeautyMass"]["HORNSdini"] = {
        "Title": "B_{s}#rightarrowD_{s}#rho",
        "Bins": 100,
        "Min": 5000.0,
        "Max": 5800.0,
        "Unit": "MeV/c^{2}",
        "a": [3000],  # 2000, 5000],
        "b": [5.0905e03, 4300, 5500],
        "csi": [0.0],  # , -4.0, 50.0],
        "sigma": [4.3209e01],  # 80.0, 2.0, 90.0],
        "shift": [0.0],
        "R": [10.0, 0.5, 80.0],
        "frac": [9.9923e-01],  # , 0.0, 1.0],
    }

    # Dataset prefix
    configdict["dataSetPrefix"] = {
        "Signal": "dataSetMC_Bs2DsPi_",
        "Bs2DsstPi": "dataSetMC_Bs2DsstPi_",
        "Bs2DsRho": "dataSetMC_Bs2DsRho_",
        "Bs2DsstRho": "dataSetMC_Bs2DsstRho_",
    }

    # Axes titles
    configdict["AxisTitle"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {
        "Bs2DsPi": "m(D_{s}^{-}#pi^{+}) [MeV/c^{2}]"
        # "Bs2DsstPi": "m(D_{s}^{*-}#pi^{+}) [MeV/c^{2}]",
    }

    return configdict
