###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    configdict = {}
    from math import log

    diffBdBs = 87.23

    # considered decay mode
    configdict["Decay"] = "Bs2DsK"
    configdict["CharmModes"] = {"KKPi"}
    configdict["Backgrounds"] = [
        "Bd2DPi",
        "Bd2DK",
        "Lb2LcK",
        "Lb2LcPi",
        "Lb2Dsp",
        "Lb2Dsstp",
        "Bs2DsPi",
        "Bs2DsRho",
        "Bs2DsstPi",
        "Bs2DsstRho",
        "Bd2DsK",
        "Bs2DsstK",
        "Bs2DsKst",
        "Bs2DsstKst",
        "Bd2DsstK",
    ]

    # year of data taking
    configdict["YearOfDataTaking"] = {"2011", "2012", "2015", "2016", "2017", "2018"}
    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2011": {"Down": 0.5600, "Up": 0.4200},
        "2012": {"Down": 0.9912, "Up": 0.9988},
        "2015": {"Down": 0.18695, "Up": 0.14105},
        "2016": {"Down": 0.85996, "Up": 0.80504},
        "2017": {"Down": 0.87689, "Up": 0.83311},
        "2018": {"Down": 1.04846, "Up": 1.14154},
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/Lb2Dsp_9fbBR/Bs2DsK/config_Bs2DsK.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotBs2DsK_for_Lb2Dsp",
        "Extension": "pdf",
    }

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5000, 6000],
        "InputName": "lab0_MassFitConsD_M",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1948, 1988],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [log(10.0), 5.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [10000.0, 150000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [1000.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3",
    }

    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 2500.0],
        "InputName": "lab0_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 3000.0],
        "InputName": "lab2_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab0_ENDVERTEX_CHI2",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab2_ENDVERTEX_CHI2",
    }
    configdict["AdditionalVariables"]["lab0_L0HadronDecision_TOS"] = {
        "Range": [-1.0, 2.0],
        "InputName": "lab0_L0HadronDecision_TOS",
    }
    configdict["AdditionalVariables"]["lab0_L0Global_TIS"] = {
        "Range": [-1.0, 2.0],
        "InputName": "lab0_L0Global_TIS",
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "",
        "MC": "",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": False,
        "DsHypo": True,
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {"All": 5367.92831},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "Run2": {"All": 21.32581},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "Run2": {"All": 13.24589},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "Run2": {"All": 0.9218},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "Run2": {"All": 1.81283},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {"All": 1.41204},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {"All": 2.18386},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "Run2": {"All": -1.7},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "Run2": {"All": -0.18193},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "Run2": {"All": 0.34553},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "Run2": {"All": 0.18332},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   ----  combinatorial background  ----   #
    #   ------------------------------------   #

    single = True
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    if single:
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
        configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
            "Run1": {"All": -1.4315e-03},
            "Run2": {"All": -9.0296e-04},
            "Fixed": False,
        }
    else:
        configdict["CombBkgShape"]["BeautyMass"] = {}
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
        configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
            "Run1": {"All": -3.5211e-02},
            "Run2": {"All": -3.5211e-03},
            "Fixed": False,
        }
        configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
            "Run1": {"All": -3.5211e-02},
            "Run2": {"All": -3.5211e-02},
            "Fixed": False,
        }
        configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
            "Run1": {"All": 4.3067e-01},
            "Run2": {"All": 4.3067e-01},
            "Fixed": False,
        }

    #   ------------------------------------   #
    #   -----   Bs2DsstK background    -----   #
    #   ------------------------------------   #

    configdict["Bs2DsstKShape"] = {}
    configdict["Bs2DsstKShape"]["BeautyMass"] = {}
    configdict["Bs2DsstKShape"]["BeautyMass"]["type"] = "HILLdiniPlusHORNSdini"
    configdict["Bs2DsstKShape"]["BeautyMass"]["R"] = {
        "Run2": {"All": 7.0840e00},
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["ahill"] = {
        "Run2": {"All": 4.6604e03},
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["ahorns"] = {
        "Run2": {"All": 5.1010e03},
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["bhill"] = {
        "Run2": {"All": 5.3131e03},
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["bhorns"] = {
        "Run2": {"All": 5.2019e03},
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["csihill"] = {
        "Run2": {"All": -1.0602e00},
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["csihorns"] = {
        "Run2": {"All": 9.4652e-01},
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["frac"] = {
        "Run2": {"All": 9.7599e-01},
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["frachorns"] = {
        "Run2": {"All": 1.2392e-01},
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["sigma"] = {
        "Run2": {"All": 1.0252e01},
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["sigmahorns"] = {
        "Run2": {"All": 2.2129e01},
        "Fixed": True,
    }
    configdict["Bs2DsstKShape"]["BeautyMass"]["shift"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   -----   Bd2DsstK background    -----   #
    #   ------------------------------------   #

    configdict["Bd2DsstKShape"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"] = {}
    configdict["Bd2DsstKShape"]["BeautyMass"]["type"] = "HILLdiniPlusHORNSdini"
    configdict["Bd2DsstKShape"]["BeautyMass"]["R"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["R"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["ahill"] = {
        "Run2": {
            "All": configdict["Bs2DsstKShape"]["BeautyMass"]["ahill"]["Run2"]["All"]
            - diffBdBs
        },
        "Fixed": True,
    }

    configdict["Bd2DsstKShape"]["BeautyMass"]["ahorns"] = {
        "Run2": {
            "All": configdict["Bs2DsstKShape"]["BeautyMass"]["ahorns"]["Run2"]["All"]
            - diffBdBs
        },
        "Fixed": True,
    }

    configdict["Bd2DsstKShape"]["BeautyMass"]["bhill"] = {
        "Run2": {
            "All": configdict["Bs2DsstKShape"]["BeautyMass"]["bhill"]["Run2"]["All"]
            - diffBdBs
        },
        "Fixed": True,
    }

    configdict["Bd2DsstKShape"]["BeautyMass"]["bhorns"] = {
        "Run2": {
            "All": configdict["Bs2DsstKShape"]["BeautyMass"]["bhorns"]["Run2"]["All"]
            - diffBdBs
        },
        "Fixed": True,
    }

    configdict["Bd2DsstKShape"]["BeautyMass"]["csihill"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["csihill"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["csihorns"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["csihorns"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["frac"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["frac"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["frachorns"] = configdict[
        "Bs2DsstKShape"
    ]["BeautyMass"]["frachorns"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["sigma"] = configdict["Bs2DsstKShape"][
        "BeautyMass"
    ]["sigma"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["sigmahorns"] = configdict[
        "Bs2DsstKShape"
    ]["BeautyMass"]["sigmahorns"]
    configdict["Bd2DsstKShape"]["BeautyMass"]["shift"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   -----   Bs2DsKst background    -----   #
    #   ------------------------------------   #

    configdict["Bs2DsKstShape"] = {}
    configdict["Bs2DsKstShape"]["BeautyMass"] = {}
    configdict["Bs2DsKstShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Bs2DsKstShape"]["BeautyMass"]["R"] = {
        "Run2": {"All": 7.1889e00},
        "Fixed": True,
    }
    configdict["Bs2DsKstShape"]["BeautyMass"]["a"] = {
        "Run2": {"All": 4.1668e03},
        "Fixed": True,
    }
    configdict["Bs2DsKstShape"]["BeautyMass"]["b"] = {
        "Run2": {"All": 5.2281e03},
        "Fixed": True,
    }
    configdict["Bs2DsKstShape"]["BeautyMass"]["csi"] = {
        "Run2": {"All": 1.9987e00},
        "Fixed": True,
    }
    configdict["Bs2DsKstShape"]["BeautyMass"]["frac"] = {
        "Run2": {"All": 9.7662e-01},
        "Fixed": True,
    }
    configdict["Bs2DsKstShape"]["BeautyMass"]["sigma"] = {
        "Run2": {"All": 1.4940e01},
        "Fixed": True,
    }
    configdict["Bs2DsKstShape"]["BeautyMass"]["shift"] = {
        "Run2": {"All": 0.0},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   -----    Bd2DsK background     -----   #
    #   ------------------------------------   #

    configdict["Bd2DsKShape"] = {}
    configdict["Bd2DsKShape"]["BeautyMass"] = {}
    if configdict["SignalShape"]["BeautyMass"]["type"] == "IpatiaPlusJohnsonSU":
        configdict["Bd2DsKShape"]["BeautyMass"]["type"] = "ShiftedSignalIpatiaJohnsonSU"
        configdict["Bd2DsKShape"]["BeautyMass"]["shift"] = {
            "Run1": {"All": -diffBdBs},
            "Run2": {"All": -diffBdBs},
            "Fixed": True,
        }
    else:
        configdict["Bd2DsKShape"]["BeautyMass"]["type"] = "ShiftedSignal"
        configdict["Bd2DsKShape"]["BeautyMass"]["shift"] = {
            "Run1": {"All": diffBdBs},
            "Run2": {"All": diffBdBs},
            "Fixed": True,
        }
    configdict["Bd2DsKShape"]["BeautyMass"]["scale1"] = {
        "Run1": {"All": 1.00808721452},
        "Run2": {"All": 1.00808721452},
        "Fixed": True,
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["scale2"] = {
        "Run1": {"All": 1.03868673310},
        "Run2": {"All": 1.03868673310},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    configdict["Yields"] = {}

    configdict["Yields"]["Bd2DPi"] = {
        "2011": {"KKPi": 0.0},
        "2012": {"KKPi": 0.0},
        "2015": {"KKPi": 8.53131},
        "2016": {"KKPi": 43.8787},
        "2017": {"KKPi": 42.3175},
        "2018": {"KKPi": 45.5676},
        "Fixed": True,
    }
    configdict["Yields"]["Bd2DK"] = {
        "2011": {"KKPi": 0.0},
        "2012": {"KKPi": 0.0},
        "2015": {"KKPi": 0.945833},
        "2016": {"KKPi": 4.28985},
        "2017": {"KKPi": 3.60299},
        "2018": {"KKPi": 3.7844},
        "Fixed": True,
    }
    configdict["Yields"]["Lb2LcPi"] = {
        "2011": {"KKPi": 0.0},
        "2012": {"KKPi": 0.0},
        "2015": {"KKPi": 2.52206 * 3.0},
        "2016": {"KKPi": 17.5631 * 3.0},
        "2017": {"KKPi": 15.6428 * 3.0},
        "2018": {"KKPi": 17.8895 * 3.0},
        "Fixed": True,
    }
    configdict["Yields"]["Lb2LcK"] = {
        "2011": {"KKPi": 0.0},
        "2012": {"KKPi": 0.0},
        "2015": {"KKPi": 0.19138 * 3.0},
        "2016": {"KKPi": 0.902678 * 3.0},
        "2017": {"KKPi": 1.19638 * 3.0},
        "2018": {"KKPi": 1.01726 * 3.0},
        "Fixed": True,
    }

    configdict["Yields"]["Bd2DsK"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 5000.0},
        "2016": {"KKPi": 5000.0},
        "2017": {"KKPi": 5000.0},
        "2018": {"KKPi": 5000.0},
        "Fixed": False,
    }

    configdict["Yields"]["Bd2DsstK"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 240.0},
        "2016": {"KKPi": 1211.0},
        "2017": {"KKPi": 1248.0},
        "2018": {"KKPi": 1598.0},
        "Fixed": False,
    }

    configdict["Yields"]["Bs2DsstK"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 5000.0},
        "2016": {"KKPi": 5000.0},
        "2017": {"KKPi": 5000.0},
        "2018": {"KKPi": 5000.0},
        "Fixed": False,
    }

    configdict["Yields"]["Bs2DsKst"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 5000.0},
        "2016": {"KKPi": 5000.0},
        "2017": {"KKPi": 5000.0},
        "2018": {"KKPi": 5000.0},
        "Fixed": False,
    }

    configdict["Yields"]["Bs2DsstKst"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 5000.0},
        "2016": {"KKPi": 5000.0},
        "2017": {"KKPi": 5000.0},
        "2018": {"KKPi": 5000.0},
        "Fixed": False,
    }

    configdict["Yields"]["Bs2DsPi"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 51.7069},
        "2016": {"KKPi": 262.476},
        "2017": {"KKPi": 269.57},
        "2018": {"KKPi": 345.238},
        "Fixed": True,
    }

    configdict["Yields"]["Bs2DsstPi"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 42.7986},
        "2016": {"KKPi": 217.255},
        "2017": {"KKPi": 223.127},
        "2018": {"KKPi": 285.759},
        "Fixed": True,
    }

    configdict["Yields"]["Bs2DsRho"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 32.2048},
        "2016": {"KKPi": 163.479},
        "2017": {"KKPi": 167.897},
        "2018": {"KKPi": 215.026},
        "Fixed": True,
    }

    configdict["Yields"]["Bs2DsstRho"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 12.7788},
        "2016": {"KKPi": 64.8681},
        "2017": {"KKPi": 66.6213},
        "2018": {"KKPi": 85.3221},
        "Fixed": True,
    }

    configdict["Yields"]["Lb2Dsp"] = {
        "2011": {"KKPi": 0.0},
        "2012": {"KKPi": 0.0},
        "2015": {"KKPi": 1000.0},
        "2016": {"KKPi": 1000.0},
        "2017": {"KKPi": 1000.0},
        "2018": {"KKPi": 1000.0},
        "Fixed": False,
    }

    configdict["Yields"]["Lb2Dsstp"] = {
        "2011": {"KKPi": 0.0},
        "2012": {"KKPi": 0.0},
        "2015": {"KKPi": 200.0},
        "2016": {"KKPi": 200.0},
        "2017": {"KKPi": 200.0},
        "2018": {"KKPi": 200.0},
        "Fixed": False,
    }

    configdict["Yields"]["CombBkg"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 5000.0},
        "2016": {"KKPi": 5000.0},
        "2017": {"KKPi": 5000.0},
        "2018": {"KKPi": 5000.0},
        "Fixed": False,
    }

    configdict["Yields"]["Signal"] = {
        "2011": {"KKPi": 5000.0},
        "2012": {"KKPi": 5000.0},
        "2015": {"KKPi": 5000.0},
        "2016": {"KKPi": 5000.0},
        "2017": {"KKPi": 5000.0},
        "2018": {"KKPi": 5000.0},
        "Fixed": False,
    }

    # ----------------------------------------------------------------------#
    ###                 MDfit plotting settings
    # ----------------------------------------------------------------------#

    from ROOT import kSolid

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Lb2LcPi",
        "Lb2LcK",
        "Bd2DPi",
        "Bd2DK",
        "Lb2Dsp",
        "Lb2Dsstp",
        "Bs2DsPi",
        "Bs2DsstPi",
        "Bs2DsRho",
        "Bs2DsstRho",
        "Bs2DsstK",
        "Bs2DsKst",
        "Bs2DsstKst",
        "Bd2DsK",
        "Bd2DsstK",
    ]
    configdict["PlotSettings"]["colors"] = [
        "#08519c",
        "#cccccc",
        "#238443",
        "#78c679",
        "#fbb4b9",
        "#feebe2",
        "#54278f",
        "#9e9ac8",
        "#d7301f",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
        "#6baed6",
        "#c6dbef",
        "#eff3ff",
        "#3182bd",
        "#9ecae1",
    ]

    configdict["PlotSettings"]["pattern"] = [
        kSolid,
        3004,
        kSolid,
        kSolid,
        3005,
        3005,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        kSolid,
        3005,
        3005,
    ]

    configdict["PlotSettings"]["patterncolor"] = [
        "#08519c",
        "#969696",
        "#238443",
        "#78c679",
        "#feebe2",
        "#c51b8a",
        "#54278f",
        "#9e9ac8",
        "#d7301f",
        "#fc8d59",
        "#fdcc8a",
        "#fef0d9",
        "#6baed6",
        "#c6dbef",
        "#eff3ff",
        "#eff3ff",
        "#eff3ff",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.6, 0.17, 0.90, 0.94],
        # "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.5,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.5,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }

    return configdict
