###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    configdict = {}

    from math import log

    # considered decay mode
    configdict["Decay"] = "Bs2DsK"
    configdict["CharmModes"] = {"KKPi"}
    configdict["Backgrounds"] = []

    # year of data taking
    configdict["YearOfDataTaking"] = {"2011", "2012", "2015", "2016", "2017", "2018"}
    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2011": {"Down": 0.5600, "Up": 0.4200},
        "2012": {"Down": 0.9912, "Up": 0.9988},
        "2015": {"Down": 0.18695, "Up": 0.14105},
        "2016": {"Down": 0.85996, "Up": 0.80504},
        "2017": {"Down": 0.87689, "Up": 0.83311},
        "2018": {"Down": 1.04846, "Up": 1.14154},
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/Lb2Dsp_9fbBR/Bs2DsK/config_Bs2DsK.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotLb2Dsp", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    # here the range was changed for comb mass fit
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5700, 6800],
        "InputName": "lab0_MassFitConsD_M",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1948, 1988],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [log(10.0), 5.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [10000.0, 650000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3",
    }

    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 2500.0],
        "InputName": "lab0_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 3000.0],
        "InputName": "lab2_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab0_ENDVERTEX_CHI2",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab2_ENDVERTEX_CHI2",
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "(lab1_isMuon==0)&&(lab0_MassHypo_Dsp> 5000.0 && "
        "lab0_MassHypo_Dsp < 7000.0)",
        "MC": "lab1_M<200&&lab1_PIDK !=-1000.0&&(lab0_MassHypo_Dsp> 5000.0 && "
        "lab0_MassHypo_Dsp < 7000.0)&&(lab1_PT > 400)&&(lab1_P>10000)",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": False,
        "DsHypo": True,
    }

    ###############################################################################
    ####################                FITTING                ####################
    ###############################################################################

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "DoubleCrystalBallWithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run1": {"All": 5367.51},
        "Run2": {"All": 5367.51},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigma1"] = {
        "Run1": {"All": 17.432},
        "Run2": {"All": 17.432},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["sigma2"] = {
        "Run1": {"All": 11.210},
        "Run2": {"All": 11.210},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["alpha1"] = {
        "Run1": {"All": -2.1016},
        "Run2": {"All": -2.1016},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["alpha2"] = {
        "Run1": {"All": 2.3520},
        "Run2": {"All": 2.3520},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run1": {"All": 2.7904},
        "Run2": {"All": 2.7904},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run1": {"All": 0.61148},
        "Run2": {"All": 0.61148},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["frac"] = {
        "Run1": {"All": 0.50878},
        "Run2": {"All": 0.50878},
        "Fixed": True,
    }

    configdict["SignalShape"]["BeautyMass"]["R"] = {
        "Run1": {"All": 1.00000},
        "Run2": {"All": 1.00000},
        "Fixed": False,
    }

    single = False
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    if single:
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
        configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
            "Run1": {"All": -1.4315e-03},
            "Run2": {"All": -9.0296e-04},
            "Fixed": False,
        }
    else:
        configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
        configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
            "Run1": {"All": -3.5211e-04},
            "Run2": {"All": -3.5211e-02},
            "Fixed": False,
        }
        configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
            "Run1": {"All": -3.5211e-02},
            "Run2": {"All": -3.5211e-03},
            "Fixed": False,
        }
        configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
            "Run1": {"All": 4.3067e-01},
            "Run2": {"All": 4.3067e-01},
            "Fixed": False,
        }

    configdict["Bd2DsPiShape"] = {}
    configdict["Bd2DsPiShape"]["BeautyMass"] = {}
    if (
        configdict["SignalShape"]["BeautyMass"]["type"]
        == "IpatiaJohnsonSUWithWidthRatio"
    ):
        configdict["Bd2DsPiShape"]["BeautyMass"][
            "type"
        ] = "ShiftedSignalIpatiaJohnsonSU"
        configdict["Bd2DsPiShape"]["BeautyMass"]["shift"] = {
            "Run1": {"All": -86.8},
            "Run2": {"All": -86.8},
            "Fixed": True,
        }
    else:
        configdict["Bd2DsPiShape"]["BeautyMass"]["type"] = "ShiftedSignal"
        configdict["Bd2DsPiShape"]["BeautyMass"]["shift"] = {
            "Run1": {"All": 86.8},
            "Run2": {"All": 86.8},
            "Fixed": True,
        }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale1"] = {
        "Run1": {"All": 1.00808721452},
        "Run2": {"All": 1.00808721452},
        "Fixed": True,
    }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale2"] = {
        "Run1": {"All": 1.03868673310},
        "Run2": {"All": 1.03868673310},
        "Fixed": True,
    }

    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {
        "Run1": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Run2": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Fixed": False,
    }
    configdict["AdditionalParameters"]["g1_f2_frac"] = {
        "Run1": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Run2": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Fixed": False,
    }

    configdict["Yields"] = {}
    # yields only for CombBkg
    configdict["Yields"]["CombBkg"] = {
        "2011": {"KKPi": 100000.0},
        "2012": {"KKPi": 100000.0},
        "2015": {"KKPi": 100000.0},
        "2016": {"KKPi": 100000.0},
        "2017": {"KKPi": 100000.0},
        "2018": {"KKPi": 100000.0},
        "Fixed": False,
    }

    ################################################################################
    ######################              Plotting              ######################
    ################################################################################

    from ROOT import kBlue, kGreen, kMagenta, kOrange, kRed

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Bd2DPi",
        "Lb2LcPi",
        "Bs2DsDsstPiRho",
        "Bd2DsPi",
        "Bs2DsK",
    ]
    configdict["PlotSettings"]["colors"] = [
        kRed - 7,
        "#cccccc",
        kOrange,
        kRed,
        kBlue - 10,
        kMagenta - 2,
        kGreen + 3,
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
