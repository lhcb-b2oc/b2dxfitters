###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Bs2DsKConfigForCombinatorialMassFit import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    # full bdt range
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [-1.0, 1.0],
        "InputName": "BDTGResponse_3",
    }
    # line that overwrites Bs2DsPiConfigForNominalMassFit.py
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5600, 6800],
        "InputName": "lab0_MassFitConsD_M",
    }

    # restricting to Run1 data
    configdict["YearOfDataTaking"] = {"2011", "2012"}

    # additional Run1 trigger requirements
    HLT_L0 = "(lab0_L0HadronDecision_TOS == 1 || lab0_L0Global_TIS ==1)"
    HLT_1 = "(lab0_Hlt1TrackAllL0Decision_TOS ==1) "
    HLT_2 = "(lab0_Hlt2Topo2BodyBBDTDecision_TOS ==1 || "
    "lab0_Hlt2Topo3BodyBBDTDecision_TOS == 1 || "
    "lab0_Hlt2Topo4BodyBBDTDecision_TOS ==1)"
    app = "&&"

    configdict["AdditionalCuts"]["All"]["Data"] += (
        app + HLT_L0 + app + HLT_1 + app + HLT_2
    )
    configdict["AdditionalCuts"]["All"]["MC"] += (
        app + HLT_L0 + app + HLT_1 + app + HLT_2
    )

    return configdict
