##########make#####################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Bd2DK"
    configdict["CharmModes"] = {"KPiPi"}
    configdict["Backgrounds"] = ["Bd2DPi", "Bd2DRho", "Bd2DstPi", "Bd2DstK", "Bd2DKst"]

    # year of data taking
    configdict["YearOfDataTaking"] = {
        "2022",
    }
    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2022": {"Down": 0.500, "Up": 0.500},
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/EM_Run3/Bd2DK/config_Bd2DK.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotBd2DK", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5000, 6000],
        "InputName": "lab0_MassFitConsD_M",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1830, 1920],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-150.0, 150.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [2000.0, 150000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [250.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3",
    }

    # additional variables
    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ETA"] = {
        "Range": [0.0, 6.0],
        "InputName": "lab0_ETA",
    }
    configdict["AdditionalVariables"]["lab1_ETA"] = {
        "Range": [0.0, 6.0],
        "InputName": "lab1_ETA",
    }
    configdict["AdditionalVariables"]["lab3_ETA"] = {
        "Range": [0.0, 6.0],
        "InputName": "lab3_ETA",
    }
    configdict["AdditionalVariables"]["lab4_ETA"] = {
        "Range": [0.0, 6.0],
        "InputName": "lab4_ETA",
    }
    configdict["AdditionalVariables"]["lab5_ETA"] = {
        "Range": [0.0, 6.0],
        "InputName": "lab5_ETA",
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 2500.0],
        "InputName": "lab0_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 3000.0],
        "InputName": "lab2_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab0_P"] = {
        "Range": [0.0, 1600000.0],
        "InputName": "lab0_P",
    }
    configdict["AdditionalVariables"]["lab0_PT"] = {
        "Range": [0.0, 40000.0],
        "InputName": "lab0_PT",
    }

    # additional cuts on samples
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "",
        "MC": "lab2_FDCHI2_ORIVX>2 && "
        "lab0_LifetimeFit_Dplus_ctau[0] >0.0 && "
        "lab2_FD_ORIVX>0.0",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": True,
        "DsHypo": True,
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    # weighting templates
    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2022": {
                "FileLabel": "#PIDK Kaon 2022",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK>5.0&&IsMuon==0.0_All;",
            },
        },
        "PIDBachMisID": {
            "2022": {
                "FileLabel": "#PIDK Pion 2022",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK>5.0&&IsMuon==0.0_All;",
            },
        },
        "RatioDataMC": {
            "2022": {
                "FileLabel": "#DataMC 2022",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio",
            },
        },
    }

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "2022": {"All": 5.2801e03},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "2022": {"All": 22.01244},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "2022": {"All": 13.98339},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "2022": {"All": 1.15342},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "2022": {"All": 2.79001},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "2022": {"All": 1.32575},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "2022": {"All": 1.8218},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "2022": {"All": -1.7},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "2022": {"All": -0.44943},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "2022": {"All": 0.29707},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "2022": {"All": 0.25},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   ----  combinatorial background  ----   #
    #   ------------------------------------   #

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
        "2022": {"All": -1.1186e-03},
        "Fixed": False,
    }

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    configdict["Yields"] = {}
    configdict["Yields"]["Bd2DKst"] = {
        "2022": {
            "KPiPi": 10000.0,
        },
        "Fixed": False,
        #  "Constrained": True,
    }
    configdict["Yields"]["Bd2DstK"] = {
        "2022": {
            "KPiPi": 10000.0,
        },
        "Fixed": False,
    }
    configdict["Yields"]["Bd2DPi"] = {
        "2022": {
            "KPiPi": 1000.0,
        },
        "Fixed": False,
    }
    configdict["Yields"]["Bd2DstPi"] = {
        "2022": {
            "KPiPi": 1000.00,
        },
        "Fixed": False,
    }
    configdict["Yields"]["Bd2DRho"] = {
        "2022": {
            "KPiPi": 1000.00,
        },
        "Fixed": False,
    }
    configdict["Yields"]["CombBkg"] = {"2022": {"KPiPi": 10000.0}, "Fixed": False}
    configdict["Yields"]["Signal"] = {"2022": {"KPiPi": 20000.0}, "Fixed": False}

    # ----------------------------------------------------------------------#
    ###                 MDfit plotting settings
    # ----------------------------------------------------------------------#

    from ROOT import kSolid

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Bd2DPi",
        "Bd2DRho",
        "Bd2DstPi",
        "Bd2DKst",
        "Bd2DstK",
    ]
    configdict["PlotSettings"]["colors"] = [
        "#01665e",
        "#cccccc",
        "#8c510a",  # bd2dpi
        "#d8b365",  # bd2dstpi
        "#f6e8c3",  # bd2drho
        "#5ab4ac",
        "#c7eae5",
    ]
    configdict["PlotSettings"]["pattern"] = [kSolid, 3004, 3005, 3005, 3005, 3005, 3005]
    configdict["PlotSettings"]["patterncolor"] = [
        "#fbb4b9",
        "#cccccc",
        "#c7eae5",
        "#8c510a",
        "#8c510a",
        "#01665e",
        "#01665e",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
