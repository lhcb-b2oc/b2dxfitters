##########make#####################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Bd2DPi"
    configdict["CharmModes"] = {"KPiPi"}
    configdict["Backgrounds"] = []

    # year of data taking
    configdict["YearOfDataTaking"] = {
        "2022",
    }
    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2022": {"Down": 0.500, "Up": 0.500},
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/EM_Run3/Bd2DPi/config_Bd2DPi.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotBd2DPi", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5000, 6000],
        "InputName": "lab0_MassFitConsD_M",
    }

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "Gaussian"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "2022": {"All": 5.2801e03},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigma"] = {
        "2022": {"All": 50.0},
        "Fixed": False,
    }

    #   ------------------------------------   #
    #   ----  combinatorial background  ----   #
    #   ------------------------------------   #

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
        "2022": {"All": -1.1186e-02},
        "Fixed": False,
    }

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    configdict["Yields"] = {}
    configdict["Yields"]["CombBkg"] = {"2022": {"KPiPi": 100000.0}, "Fixed": False}
    configdict["Yields"]["Signal"] = {"2022": {"KPiPi": 200000.0}, "Fixed": False}

    # ----------------------------------------------------------------------#
    ###                 MDfit plotting settings
    # ----------------------------------------------------------------------#

    from ROOT import kSolid

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = ["Sig", "CombBkg"]
    configdict["PlotSettings"]["colors"] = [
        "#8c510a",  # Sig
        "#cccccc",  # combo
    ]
    configdict["PlotSettings"]["pattern"] = [
        kSolid,
        3004,
    ]
    configdict["PlotSettings"]["patterncolor"] = [
        "#fbb4b9",
        "#cccccc",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
