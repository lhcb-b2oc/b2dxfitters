###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys

# from ROOT import *
# print(os.environ)


sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa


def getconfig():
    configdict = {}

    ############################################################
    # List of observables for all the PDFs.
    # The content of this dictionary determines the observables
    # to generate for and how may taggers are present.
    ############################################################
    configdict["Observables"] = {
        "BeautyMass": {
            "Type": "RooRealVar",
            "Title": "B mass (MeV/c^2)",
            "Range": [5000, 6000],
        }
    }

    ############################################################
    # List of mass hypotheses for bachelor
    # The content of this dictionary determines how many
    # bachelor PID bins the final dataset is splitted into
    ############################################################
    configdict["Hypothesys"] = ["Bd2DPi"]

    ############################################################
    # Signal decay, Charm decay mode and year of data taking
    # Splitting per magnet polarity not implemented, at the moment
    ############################################################
    configdict["Decay"] = "Bd2DPi"
    configdict["CharmModes"] = [
        "KPiPi",
    ]
    configdict["Years"] = [
        "2022",
    ]

    ############################################################
    # For PIDK shapes we need also polarities
    # This is also used to split the yields between years properly
    ############################################################
    configdict["Polarity"] = ["Up", "Down"]
    configdict["IntegratedLuminosity"] = {
        "2022": {"Down": 0.5, "Up": 0.5},
    }

    configdict["FractionsLuminosity"] = {
        k: v["Up"] / (v["Up"] + v["Down"])
        for k, v in configdict["IntegratedLuminosity"].items()
    }

    ############################################################
    # List of components with yields to generate.
    # The content of this dictionary determines, for each
    # PID bin and year, how many PDF components are generated.
    # If there is only signal, a TTree ready for sFit is
    # generated directly, without need for doing a (useless)
    # mass fit.
    ############################################################

    configdict["Components"] = {
        "Signal": {
            "Bd2DPi": {
                "2022": {"KPiPi": 231000.0},
            }
        },
        "Combinatorial": {
            "Bd2DPi": {
                "2022": {"KPiPi": 101224.0},
            }
        },
    }

    ############################################################
    #                      Signal
    ############################################################

    ############################################################

    def getPdfTemplateConfigFromWorkspace(
        name="Signal_BeautyMass_IpatiaPlusJohnsonSU_both_{mode}_{year}",
        modes=["All"],
        years=["2022"],
        lowercasemode=True,
        **kwargs
    ):
        """
        Generate a dictionary of the form
        {
            year1: {
                mode1: {
                    'Type': 'FromWorkspace',
                    'Name': name.format(mode=m1),
                    'WorkspaceName': WorkspaceName,
                    'WorkspaceFile': WorkspaceFile,
                    **kwargs
                },
                mode2: { ... }
            },
            year2: { ... }
        }
        for all modes m1... given in the `modes` and all years in the `years`
        argument.
        """
        return {
            y: {
                m: dict(
                    Type="FromWorkspace",
                    Name=name.format(mode=m.lower() if lowercasemode else m, year=y),
                    **kwargs
                )
                for m in modes
            }
            for y in years
        }

    # the shapes are shared between the years

    configdict["PDFList"] = {
        "BeautyMass": {
            "Signal": {
                "Bd2DPi": {
                    "2022": {
                        "KPiPi": {"sigma": 50.0, "mean": 5.2801e03, "Type": "Gaussian"}
                    },
                }
            },
            "Combinatorial": {
                "Bd2DPi": {
                    "2022": {"KPiPi": {"cB": -1.1186e-03, "Type": "Exponential"}},
                }
            },
        },
    }

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    from configutils import main

    main(getconfig())
