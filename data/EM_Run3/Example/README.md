# Contact:
in case of troubles contact: [Agnieszka Dziurda](agnieszka.dziurda@cern.ch)

# Generating events (from scripts directory):
```
../../../run python toyFactory.py --configName ../data/EM_Run3/Example/Generator_Gaussian.py --saveTree --workfileOut workOut.root --treefileOut treetoy.root --outputdir ./ --seed 12345 --pol up
```

# Fitting events (from scripts directory):

```
../../../run python runMDFitter.py --configName ../data/EM_Run3/Example/ExampleConfigForNominalMassFit.py --mode KPiPi --year 2022 --pol up --fileName workOut.root -s WS_MDFitter_example.root  --debug
```

# Plotting (from scripts directory):

```
../../../run python plotMDFitter.py WS_MDFitter_example.root --configName ../data/EM_Run3/Example/ExampleConfigForNominalMassFit.py --mode KPiPi --year 2022 --pol up
```
