##########make#####################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Bu2DPi"
    configdict["CharmModes"] = {"KPi"}
    configdict["Backgrounds"] = [
        "Bu2DK",
        "Bu2DstPi",
        "Bd2DstPi",
        "Bu2DstgPi",
        "Bu2DPiPi",
        "Bu2DstPiPi",
    ]

    # year of data taking
    configdict["YearOfDataTaking"] = {
        "2022",
    }
    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2022": {"Down": 0.500, "Up": 0.500},
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/EM_Run3/Bd2DK/config_Bd2DK.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotBd2DK", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [4900, 6000],
        "InputName": "lab0_MassFitConsD_M",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1830, 1920],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-150.0, 150.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [10000.0, 150000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [1000.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3",
    }

    # additional variables
    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ETA"] = {
        "Range": [0.0, 6.0],
        "InputName": "lab0_ETA",
    }
    configdict["AdditionalVariables"]["lab1_ETA"] = {
        "Range": [0.0, 6.0],
        "InputName": "lab1_ETA",
    }
    configdict["AdditionalVariables"]["lab3_ETA"] = {
        "Range": [0.0, 6.0],
        "InputName": "lab3_ETA",
    }
    configdict["AdditionalVariables"]["lab4_ETA"] = {
        "Range": [0.0, 6.0],
        "InputName": "lab4_ETA",
    }
    configdict["AdditionalVariables"]["lab5_ETA"] = {
        "Range": [0.0, 6.0],
        "InputName": "lab5_ETA",
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 2500.0],
        "InputName": "lab0_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 3000.0],
        "InputName": "lab2_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab0_P"] = {
        "Range": [0.0, 1600000.0],
        "InputName": "lab0_P",
    }
    configdict["AdditionalVariables"]["lab0_PT"] = {
        "Range": [0.0, 40000.0],
        "InputName": "lab0_PT",
    }

    # additional cuts on samples
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "",
        "MC": "lab2_FDCHI2_ORIVX>2 && "
        "lab0_LifetimeFit_Dplus_ctau[0] >0.0 && "
        "lab2_FD_ORIVX>0.0",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": True,
        "DsHypo": True,
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    # weighting templates
    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2022": {
                "FileLabel": "#PIDK Kaon 2022",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK>5.0&&IsMuon==0.0_All;",
            },
        },
        "PIDBachMisID": {
            "2022": {
                "FileLabel": "#PIDK Pion 2022",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK>5.0&&IsMuon==0.0_All;",
            },
        },
        "RatioDataMC": {
            "2022": {
                "FileLabel": "#DataMC 2022",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio",
            },
        },
    }

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "2022": {"All": 5.2801e03},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "2022": {"All": 16.52},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "2022": {"All": 14.66},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "2022": {"All": 1.07},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "2022": {"All": 1.63},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "2022": {"All": 1.51},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "2022": {"All": 2.13},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "2022": {"All": -4.36},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "2022": {"All": -0.04},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "2022": {"All": 0.42},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "2022": {"All": 0.21},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   ----  combinatorial background  ----   #
    #   ------------------------------------   #

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
        "2022": {"All": -1.1186e-03},
        "Fixed": False,
    }

    configdict["Bu2DKShape"] = {}
    configdict["Bu2DKShape"]["BeautyMass"] = {}
    configdict["Bu2DKShape"]["BeautyMass"]["type"] = "DoubleCrystalBall"
    configdict["Bu2DKShape"]["BeautyMass"]["mean"] = {
        "2022": {"All": 5233.28},
        "Fixed": True,
    }
    configdict["Bu2DKShape"]["BeautyMass"]["sigma1"] = {
        "2022": {"All": 22.70},
        "Fixed": True,
    }
    configdict["Bu2DKShape"]["BeautyMass"]["sigma2"] = {
        "2022": {"All": 15.46},
        "Fixed": True,
    }
    configdict["Bu2DKShape"]["BeautyMass"]["alpha1"] = {
        "2022": {"All": 0.60},
        "Fixed": True,
    }
    configdict["Bu2DKShape"]["BeautyMass"]["alpha2"] = {
        "2022": {"All": -2.21},
        "Fixed": True,
    }
    configdict["Bu2DKShape"]["BeautyMass"]["n1"] = {
        "2022": {"All": 4.18},
        "Fixed": True,
    }
    configdict["Bu2DKShape"]["BeautyMass"]["n2"] = {
        "2022": {"All": 2.00},
        "Fixed": True,
    }
    configdict["Bu2DKShape"]["BeautyMass"]["frac"] = {
        "2022": {"All": 0.75},
        "Fixed": True,
    }

    configdict["Bu2DstPiShape"] = {}
    configdict["Bu2DstPiShape"]["BeautyMass"] = {}
    configdict["Bu2DstPiShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Bu2DstPiShape"]["BeautyMass"]["a"] = {
        "2022": {"All": 5012.89},
        "Fixed": True,
    }
    configdict["Bu2DstPiShape"]["BeautyMass"]["b"] = {
        "2022": {"All": 5113.27},
        "Fixed": True,
    }
    configdict["Bu2DstPiShape"]["BeautyMass"]["csi"] = {
        "2022": {"All": 0.85},
        "Fixed": True,
    }
    configdict["Bu2DstPiShape"]["BeautyMass"]["sigma"] = {
        "2022": {"All": 11.56},
        "Fixed": True,
    }
    configdict["Bu2DstPiShape"]["BeautyMass"]["shift"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["Bu2DstPiShape"]["BeautyMass"]["R"] = {
        "2022": {"All": 1.68},
        "Fixed": True,
    }
    configdict["Bu2DstPiShape"]["BeautyMass"]["frac"] = {
        "2022": {"All": 0.68},
        "Fixed": True,
    }

    configdict["Bd2DstPiShape"] = {}
    configdict["Bd2DstPiShape"]["BeautyMass"] = {}
    configdict["Bd2DstPiShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Bd2DstPiShape"]["BeautyMass"]["a"] = {
        "2022": {"All": 5012.53},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["b"] = {
        "2022": {"All": 5104.45},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["csi"] = {
        "2022": {"All": 0.87},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["sigma"] = {
        "2022": {"All": 12.16},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["shift"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["R"] = {
        "2022": {"All": 1.73},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["frac"] = {
        "2022": {"All": 0.77},
        "Fixed": True,
    }

    configdict["Bu2DstgPiShape"] = {}
    configdict["Bu2DstgPiShape"]["BeautyMass"] = {}
    configdict["Bu2DstgPiShape"]["BeautyMass"]["type"] = "HILLdini"
    configdict["Bu2DstgPiShape"]["BeautyMass"]["a"] = {
        "2022": {"All": 4903.75},
        "Fixed": True,
    }
    configdict["Bu2DstgPiShape"]["BeautyMass"]["b"] = {
        "2022": {"All": 5220.71},
        "Fixed": True,
    }
    configdict["Bu2DstgPiShape"]["BeautyMass"]["csi"] = {
        "2022": {"All": 0.52},
        "Fixed": True,
    }
    configdict["Bu2DstgPiShape"]["BeautyMass"]["sigma"] = {
        "2022": {"All": 16.84},
        "Fixed": True,
    }
    configdict["Bu2DstgPiShape"]["BeautyMass"]["shift"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["Bu2DstgPiShape"]["BeautyMass"]["R"] = {
        "2022": {"All": 3.78},
        "Fixed": True,
    }
    configdict["Bu2DstgPiShape"]["BeautyMass"]["frac"] = {
        "2022": {"All": 0.97},
        "Fixed": True,
    }

    configdict["Bu2DPiPiShape"] = {}
    configdict["Bu2DPiPiShape"]["BeautyMass"] = {}
    configdict["Bu2DPiPiShape"]["BeautyMass"]["type"] = "DoubleHORNSdini"
    configdict["Bu2DPiPiShape"]["BeautyMass"]["a1"] = {
        "2022": {"All": 3051.22},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["b1"] = {
        "2022": {"All": 5130.64},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["csi1"] = {
        "2022": {"All": 1.0},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["sigma1"] = {
        "2022": {"All": 15.27},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["shift1"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["R1"] = {
        "2022": {"All": 1.0},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["frac1"] = {
        "2022": {"All": 1.0},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["a2"] = {
        "2022": {"All": 3999.98},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["b2"] = {
        "2022": {"All": 5037.81},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["csi2"] = {
        "2022": {"All": 1.0},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["sigma2"] = {
        "2022": {"All": 28.89},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["shift2"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["R2"] = {
        "2022": {"All": 1.0},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["frac2"] = {
        "2022": {"All": 1.0},
        "Fixed": True,
    }
    configdict["Bu2DPiPiShape"]["BeautyMass"]["frac"] = {
        "2022": {"All": 0.79},
        "Fixed": True,
    }

    configdict["Bu2DstPiPiShape"] = {}
    configdict["Bu2DstPiPiShape"]["BeautyMass"] = {}
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["type"] = "DoubleHORNSdini"
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["a1"] = {
        "2022": {"All": 2009.61},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["b1"] = {
        "2022": {"All": 4965.01},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["csi1"] = {
        "2022": {"All": 1.0},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["sigma1"] = {
        "2022": {"All": 16.13},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["shift1"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["R1"] = {
        "2022": {"All": 5.00},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["frac1"] = {
        "2022": {"All": 0.98},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["a2"] = {
        "2022": {"All": 3580.66},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["b2"] = {
        "2022": {"All": 4886.56},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["csi2"] = {
        "2022": {"All": 1.0},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["sigma2"] = {
        "2022": {"All": 19.42},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["shift2"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["R2"] = {
        "2022": {"All": 5.00},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["frac2"] = {
        "2022": {"All": 0.98},
        "Fixed": True,
    }
    configdict["Bu2DstPiPiShape"]["BeautyMass"]["frac"] = {
        "2022": {"All": 0.95},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    configdict["Yields"] = {}
    configdict["Yields"]["Bu2DK"] = {"2022": {"KPi": 10000.0}, "Fixed": False}
    configdict["Yields"]["Bu2DstPi"] = {"2022": {"KPi": 90000.0}, "Fixed": False}
    configdict["Yields"]["Bd2DstPi"] = {"2022": {"KPi": 90000.0}, "Fixed": False}
    configdict["Yields"]["Bu2DstgPi"] = {"2022": {"KPi": 90000.0}, "Fixed": False}
    configdict["Yields"]["Bu2DPiPi"] = {"2022": {"KPi": 90000.0}, "Fixed": False}
    configdict["Yields"]["Bu2DstPiPi"] = {"2022": {"KPi": 20000.0}, "Fixed": False}
    configdict["Yields"]["CombBkg"] = {"2022": {"KPi": 10000.0}, "Fixed": False}
    configdict["Yields"]["Signal"] = {"2022": {"KPi": 158000.0}, "Fixed": False}

    # ----------------------------------------------------------------------#
    ###                 MDfit plotting settings
    # ----------------------------------------------------------------------#

    from ROOT import kSolid

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Bu2DK",
        "Bu2DPiPi",
        "Bu2DstPiPi",
        "Bu2DstgPi",
        "Bu2DstPi",
        "Bd2DstPi",
    ]

    configdict["PlotSettings"]["colors"] = [
        "#253494",
        "#cccccc",
        "#54278f",
        "#2c7fb8",
        "#41b6c4",
        "#6baed6",
        "#c6dbef",
        "#f6e8c3",
    ]
    configdict["PlotSettings"]["pattern"] = [
        kSolid,
        3344,
        3144,
        3144,
        3144,
        3144,
        3144,
        3005,
    ]

    configdict["PlotSettings"]["patterncolor"] = [
        "#fbb4b9",
        "#cccccc",
        "#9e9ac8",
        "#253494",
        "#2c7fb8",
        "#9ecae1",
        "#eff3ff",
        "#8c510a",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
