###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys

# from ROOT import *
# print(os.environ)


sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa

WORKSPACEFILE = (
    "/eos/lhcb/wg/b2oc/XS_b2X_EM_2022/Bd2DPi/Templates/work_mc_bd2dpi_em.root"
)
WORKSPACENAME = "workspace"


def getconfig():
    configdict = {}

    ############################################################
    # List of observables for all the PDFs.
    # The content of this dictionary determines the observables
    # to generate for and how may taggers are present.
    ############################################################
    configdict["Observables"] = {
        "BeautyMass": {
            "Type": "RooRealVar",
            "Title": "B mass (MeV/c^2)",
            "Range": [4900, 6000],
        }
    }

    ############################################################
    # List of mass hypotheses for bachelor
    # The content of this dictionary determines how many
    # bachelor PID bins the final dataset is splitted into
    ############################################################
    configdict["Hypothesys"] = ["Bu2DPi"]

    ############################################################
    # Signal decay, Charm decay mode and year of data taking
    # Splitting per magnet polarity not implemented, at the moment
    ############################################################
    configdict["Decay"] = "Bu2DPi"
    configdict["CharmModes"] = [
        "KPi",
    ]
    configdict["Years"] = [
        "2022",
    ]

    ############################################################
    # For PIDK shapes we need also polarities
    # This is also used to split the yields between years properly
    ############################################################
    configdict["Polarity"] = ["Up", "Down"]
    configdict["IntegratedLuminosity"] = {
        "2022": {"Down": 0.5, "Up": 0.5},
    }

    configdict["FractionsLuminosity"] = {
        k: v["Up"] / (v["Up"] + v["Down"])
        for k, v in configdict["IntegratedLuminosity"].items()
    }

    configdict["WorkspaceToRead"] = {"File": WORKSPACEFILE, "Workspace": WORKSPACENAME}
    ############################################################
    # List of components with yields to generate.
    # The content of this dictionary determines, for each
    # PID bin and year, how many PDF components are generated.
    # If there is only signal, a TTree ready for sFit is
    # generated directly, without need for doing a (useless)
    # mass fit.
    ############################################################

    configdict["Components"] = {
        "Signal": {
            "Bu2DPi": {
                "2022": {"KPi": 15800.0},
            }
        },
        "Combinatorial": {
            "Bu2DPi": {
                "2022": {"KPi": 1000.0},
            }
        },
        "Bu2DK": {
            "Bu2DPi": {
                "2022": {"KPi": 1000.0},
            }
        },
        "Bu2DstPi": {
            "Bu2DPi": {
                "2022": {"KPi": 9000.0},
            }
        },
        "Bd2DstPi": {
            "Bu2DPi": {
                "2022": {"KPi": 6000.0},
            }
        },
        "Bu2DstgPi": {
            "Bu2DPi": {
                "2022": {"KPi": 9000.0},
            }
        },
        "Bu2DPiPi": {
            "Bu2DPi": {
                "2022": {"KPi": 6000.0},
            }
        },
        "Bu2DstPiPi": {
            "Bu2DPi": {
                "2022": {"KPi": 2000.0},
            }
        },
    }

    ############################################################
    #                      Signal
    ############################################################

    ############################################################

    def getPdfTemplateConfigFromWorkspace(
        name="Signal_BeautyMass_IpatiaPlusJohnsonSU_both_{mode}_{year}",
        modes=["All"],
        years=["2022"],
        lowercasemode=True,
        **kwargs
    ):
        """
        Generate a dictionary of the form
        {
            year1: {
                mode1: {
                    'Type': 'FromWorkspace',
                    'Name': name.format(mode=m1),
                    'WorkspaceName': WorkspaceName,
                    'WorkspaceFile': WorkspaceFile,
                    **kwargs
                },
                mode2: { ... }
            },
            year2: { ... }
        }
        for all modes m1... given in the `modes` and all years in the `years`
        argument.
        """
        return {
            y: {
                m: dict(
                    Type="FromWorkspace",
                    Name=name.format(mode=m.lower() if lowercasemode else m, year=y),
                    **kwargs
                )
                for m in modes
            }
            for y in years
        }

    # the shapes are shared between the years

    configdict["PDFList"] = {
        "BeautyMass": {
            "Signal": {
                "Bu2DPi": {
                    "2022": {
                        "KPi": {
                            "a1": 1.07,
                            "a2": 1.63,
                            "fracI": 0.21,
                            "l": -4.36,
                            "n1": 1.51,
                            "n2": 2.13,
                            "nu": -0.04,
                            "sigmaI": 16.52,
                            "sigmaJ": 14.66,
                            "tau": 0.42,
                            "fb": 0.0,
                            "mean": 5.2801e03,
                            "zeta": 0.0,
                            "Type": "IpatiaJohnsonSU",
                        }
                    },
                }
            },
            "Combinatorial": {
                "Bu2DPi": {
                    "2022": {"KPi": {"cB": -1.1186e-03, "Type": "Exponential"}},
                }
            },
            "Bu2DK": {
                "Bu2DPi": {
                    "2022": {
                        "KPi": {
                            "mean": 5233.28,
                            "sigma1": 22.70,
                            "sigma2": 15.46,
                            "alpha1": 0.60,
                            "alpha2": -2.21,
                            "n1": 4.18,
                            "n2": 2.00,
                            "frac": 0.75,
                            "Type": "DoubleCrystalBall",
                        }
                    },
                }
            },
            "Bu2DstPi": {
                "Bu2DPi": {
                    "2022": {
                        "KPi": {
                            "a": 5012.89,
                            "b": 5113.27,
                            "csi": 0.85,
                            "sigma": 11.56,
                            "shift": 0.0,
                            "R": 1.68,
                            "frac": 0.68,
                            "Type": "HORNSdini",
                        }
                    },
                }
            },
            "Bd2DstPi": {
                "Bu2DPi": {
                    "2022": {
                        "KPi": {
                            "a": 5012.53,
                            "b": 5104.45,
                            "csi": 0.87,
                            "sigma": 12.16,
                            "shift": 0.0,
                            "R": 1.73,
                            "frac": 0.77,
                            "Type": "HORNSdini",
                        }
                    },
                }
            },
            "Bu2DstgPi": {
                "Bu2DPi": {
                    "2022": {
                        "KPi": {
                            "a": 4903.75,
                            "b": 5220.71,
                            "csi": 0.52,
                            "sigma": 16.84,
                            "shift": 0.0,
                            "R": 3.78,
                            "frac": 0.97,
                            "Type": "HILLdini",
                        }
                    },
                }
            },
            "Bu2DPiPi": {
                "Bu2DPi": {
                    "2022": {
                        "KPi": {
                            "a1": 3051.22,
                            "b1": 5130.64,
                            "csi1": 1.0,
                            "sigma1": 15.27,
                            "shift1": 0.0,
                            "R1": 1.0,
                            "frac1": 1.0,
                            "a2": 3999.98,
                            "b2": 5037.81,
                            "csi2": 1.0,
                            "sigma2": 28.89,
                            "shift2": 0.0,
                            "R2": 1.0,
                            "frac2": 1.0,
                            "frac": 0.79,
                            "Type": "DoubleHORNSdini",
                        }
                    },
                }
            },
            "Bu2DstPiPi": {
                "Bu2DPi": {
                    "2022": {
                        "KPi": {
                            "a1": 2009.61,
                            "b1": 4965.01,
                            "csi1": 1.0,
                            "sigma1": 16.13,
                            "shift1": 0.0,
                            "R1": 5.00,
                            "frac1": 0.98,
                            "a2": 3580.66,
                            "b2": 4886.56,
                            "csi2": 1.0,
                            "sigma2": 19.42,
                            "shift2": 0.0,
                            "R2": 5.00,
                            "frac2": 0.98,
                            "frac": 0.95,
                            "Type": "DoubleHORNSdini",
                        }
                    },
                }
            },
        },
    }

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    from configutils import main

    main(getconfig())
