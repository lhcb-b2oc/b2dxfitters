###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys

# from ROOT import *
# print(os.environ)


sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa

WORKSPACEFILE = (
    "/eos/lhcb/wg/b2oc/XS_b2X_EM_2022/Lb2LcPi/Templates/work_mc_lb2lcpi_em.root"
)
WORKSPACENAME = "workspace"


def getconfig():
    configdict = {}

    ############################################################
    # List of observables for all the PDFs.
    # The content of this dictionary determines the observables
    # to generate for and how may taggers are present.
    ############################################################
    configdict["Observables"] = {
        "BeautyMass": {
            "Type": "RooRealVar",
            "Title": "B mass (MeV/c^2)",
            "Range": [5400, 6200],
        }
    }

    ############################################################
    # List of mass hypotheses for bachelor
    # The content of this dictionary determines how many
    # bachelor PID bins the final dataset is splitted into
    ############################################################
    configdict["Hypothesys"] = ["Lb2LcPi"]

    ############################################################
    # Signal decay, Charm decay mode and year of data taking
    # Splitting per magnet polarity not implemented, at the moment
    ############################################################
    configdict["Decay"] = "Lb2LcPi"
    configdict["CharmModes"] = [
        "pKPi",
    ]
    configdict["Years"] = [
        "2022",
    ]

    ############################################################
    # For PIDK shapes we need also polarities
    # This is also used to split the yields between years properly
    ############################################################
    configdict["Polarity"] = ["Up", "Down"]
    configdict["IntegratedLuminosity"] = {
        "2022": {"Down": 0.5, "Up": 0.5},
    }

    configdict["FractionsLuminosity"] = {
        k: v["Up"] / (v["Up"] + v["Down"])
        for k, v in configdict["IntegratedLuminosity"].items()
    }

    configdict["WorkspaceToRead"] = {"File": WORKSPACEFILE, "Workspace": WORKSPACENAME}
    ############################################################
    # List of components with yields to generate.
    # The content of this dictionary determines, for each
    # PID bin and year, how many PDF components are generated.
    # If there is only signal, a TTree ready for sFit is
    # generated directly, without need for doing a (useless)
    # mass fit.
    ############################################################

    configdict["Components"] = {
        "Signal": {
            "Lb2LcPi": {
                "2022": {"pKPi": 156000.0},
            }
        },
        "Combinatorial": {
            "Lb2LcPi": {
                "2022": {"pKPi": 2259.0},
            }
        },
        "Bd2DPi": {
            "Lb2LcPi": {
                "2022": {"pKPi": 46.0},
            }
        },
        "Bs2DsPi": {
            "Lb2LcPi": {
                "2022": {"pKPi": 355.0},
            }
        },
        "Lb2LcK": {
            "Lb2LcPi": {
                "2022": {"pKPi": 916.0},
            }
        },
        "Lb2LcRho": {
            "Lb2LcPi": {
                "2022": {"pKPi": 19585.0},
            }
        },
        "Lb2ScPi": {
            "Lb2LcPi": {
                "2022": {"pKPi": 14416.0},
            }
        },
    }

    ############################################################
    #                      Signal
    ############################################################

    ############################################################

    def getPdfTemplateConfigFromWorkspace(
        name="Signal_BeautyMass_IpatiaPlusJohnsonSU_both_{mode}_{year}",
        modes=["All"],
        years=["2022"],
        lowercasemode=True,
        **kwargs
    ):
        """
        Generate a dictionary of the form
        {
            year1: {
                mode1: {
                    'Type': 'FromWorkspace',
                    'Name': name.format(mode=m1),
                    'WorkspaceName': WorkspaceName,
                    'WorkspaceFile': WorkspaceFile,
                    **kwargs
                },
                mode2: { ... }
            },
            year2: { ... }
        }
        for all modes m1... given in the `modes` and all years in the `years`
        argument.
        """
        return {
            y: {
                m: dict(
                    Type="FromWorkspace",
                    Name=name.format(mode=m.lower() if lowercasemode else m, year=y),
                    **kwargs
                )
                for m in modes
            }
            for y in years
        }

    # the shapes are shared between the years

    configdict["PDFList"] = {
        "BeautyMass": {
            "Signal": {
                "Lb2LcPi": {
                    "2022": {
                        "pKPi": {
                            "a1": 0.846,  # +/- 0.033
                            "a2": 2.24,  # +/- 0.20
                            "fracI": 0.25,
                            "l": -1.7,
                            "n1": 1.505,  # +/- 0.067
                            "n2": 2.00,  # +/- 0.16
                            "nu": -0.342,  # +/- 0.063
                            "sigmaI": 30.16,  # +/- 0.32
                            "sigmaJ": 15.094,  # +/- 0.040
                            "tau": 0.318,  # +/- 0.017
                            "fb": 0.0,
                            "mean": 5617.690,  # +/- 0.028
                            "zeta": 0.0,
                            "Type": "IpatiaJohnsonSU",
                        }
                    },
                }
            },
            "Combinatorial": {
                "Lb2LcPi": {
                    "2022": {
                        "pKPi": {"cB": -0.230e-03, "Type": "Exponential"}  # ± 0.00024
                    },
                }
            },
            "Bd2DPi": {
                "Lb2LcPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBd2DPiPdf_m_both_2022"
                ),
            },
            "Bs2DsPi": {
                "Lb2LcPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBs2DsPiPdf_m_both_2022"
                ),
            },
            "Lb2LcK": {
                "Lb2LcPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgLb2LcKPdf_m_both_2022"
                ),
            },
            "Lb2LcRho": {
                "Lb2LcPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgLb2LcRhoPdf_m_both_2022"
                ),
            },
            "Lb2ScPi": {
                "Lb2LcPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgLb2ScPiPdf_m_both_2022"
                ),
            },
        },
    }

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    from configutils import main

    main(getconfig())
