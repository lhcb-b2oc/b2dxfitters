###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Lb2LcPi"
    configdict["CharmModes"] = {"pKPi"}
    configdict["Backgrounds"] = ["Bd2DPi", "Bs2DsPi", "Lb2LcK", "Lb2LcRho", "Lb2ScPi"]

    # year of data taking
    configdict["YearOfDataTaking"] = {"2022"}

    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2022": {"Down": 0.500, "Up": 0.500},
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/EM_Run3/Lb2LcPi/config_Lb2LcPi.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotLb2LcPi", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5400, 6200],
        "InputName": "lab0_MassFitConsD_M",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [2266, 2306],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [2000.0, 150000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [250.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3",
    }

    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 2500.0],
        "InputName": "lab0_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
        "Range": [0.0, 3000.0],
        "InputName": "lab2_ENDVERTEX_ZERR",
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab0_ENDVERTEX_CHI2",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 30.0],
        "InputName": "lab2_ENDVERTEX_CHI2",
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": False,
        "DsHypo": False,
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: pKPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2015": {
                "FileLabel": "#PIDK Pion 2022",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK<0.0_All;",
            },
        },
        "PIDBachMisID": {
            "2015": {
                "FileLabel": "#PIDK Kaon 2022",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK<0.0_All;",
            },
        },
        "PIDChild1Eff": {
            "2015": {
                "FileLabel": "#PIDK Proton 2022",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "P_TotLc_MC15TuneV1_ProbNNp>0.6_All;",
            },
        },
        "PIDChild2Eff": {
            "2015": {
                "FileLabel": "#PIDK Kaon 2022",
                "Var": ["lab4_ETA", "lab4_P"],
                "HistName": "K_DLLK>0.0_All;",
            },
        },
        "PIDChild3Eff": {
            "2015": {
                "FileLabel": "#PIDK Pion 2022",
                "Var": ["lab5_ETA", "lab5_P"],
                "HistName": "Pi_DLLK<5.0_All;",
            },
        },
        "PIDChildKaonProton": {
            "2015": {
                "FileLabel": "#PIDK Kaon 2022",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "K_MC15TuneV1_ProbNNp>0.6_All;",
            },
        },
        "PIDChildPionProton": {
            "2015": {
                "FileLabel": "#PIDK Pion 2022",
                "Var": ["lab3_ETA", "lab3_P"],
                "HistName": "Pi_MC15TuneV1_ProbNNp>0.6_All;",
            },
        },
        "RatioDataMC": {
            "2022": {
                "FileLabel": "#DataMC 2022",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio",
            },
        },
        "Shift": {
            "2015": {"BeautyMass": 0.0, "CharmMass": 0.0},
        },
    }

    #########################################################################
    #################                FITTING                #################
    #########################################################################

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "2022": {"All": 5618.90055},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "2022": {"All": 30.16},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "2022": {"All": 15.094},  # +/- 0.040
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "2022": {"All": 0.846},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "2022": {"All": 2.24},  # +/- 0.20
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "2022": {"All": 1.505},  # +/- 0.067
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "2022": {"All": 2.00},  # +/- 0.16
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "2022": {"All": -1.7},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "2022": {"All": -0.342},  # +/- 0.063
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "2022": {"All": 0.318},  # +/- 0.017
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "2022": {"All": 0.25},
        "Fixed": True,
    }

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
        "2022": {"All": -0.230e-03},  # ± 0.00024
        "Fixed": False,
    }

    # define yields
    configdict["Yields"] = {}

    configdict["Yields"]["Bd2DPi"] = {"2022": {"pKPi": 46.0}, "Fixed": True}
    configdict["Yields"]["Bs2DsPi"] = {"2022": {"pKPi": 355.0}, "Fixed": True}
    configdict["Yields"]["Lb2LcK"] = {
        "2022": {"pKPi": 916.0},  # [2385.0, 69.0]
        "Fixed": True,
        #        "Constrained": False
    }
    configdict["Yields"]["Lb2LcRho"] = {"2022": {"pKPi": 20000.0}, "Fixed": False}
    configdict["Yields"]["Lb2ScPi"] = {"2022": {"pKPi": 20000.0}, "Fixed": False}
    configdict["Yields"]["Signal"] = {"2022": {"pKPi": 1.5e5}, "Fixed": False}
    configdict["Yields"]["CombBkg"] = {"2022": {"pKPi": 5000.0}, "Fixed": False}

    ######################################################################
    #################              Plotting              #################
    ######################################################################

    configdict["PlotSettings"] = {}

    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Bd2DPi",
        "Bs2DsPi",
        "Lb2LcK",
        "Lb2LcRho",
        "Lb2ScPi",
    ]
    configdict["PlotSettings"]["colors"] = [
        "#006837",
        "#cccccc",
        "#01665e",
        "#d7301f",
        "#31a354",
        "#78c679",
        "#c2e699",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
