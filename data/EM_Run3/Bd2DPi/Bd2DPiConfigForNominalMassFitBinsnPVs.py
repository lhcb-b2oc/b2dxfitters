###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Open the file and load the file
# with open('configs/fitting_config.yaml') as f:
#    data = yaml.load(f, Loader=SafeLoader)


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Bd2DPi"
    configdict["CharmModes"] = {"KPiPi"}
    configdict["Backgrounds"] = ["Bd2DK", "Bd2DRho", "Bd2DstPi", "Lb2LcPi", "Bs2DsPi"]

    # year of data taking
    configdict["YearOfDataTaking"] = {
        "2022",
    }
    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2022": {"Down": 0.500, "Up": 0.500},
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "options/fitter/tests/config_Bd2DPi_data.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotBd2DPi", "Extension": "pdf"}

    configdict["MoreVariables"] = True
    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5100, 5600],  # 5100/5000 5600/6000
        "InputName": "lab0_DTF_MassFitConsD_MASS",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1830, 1920],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-400.0, 150.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [2000.0, 150000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [250.0, 45000.0],
        "InputName": "lab1_PT",
    }
    # configdict["BasicVariables"]["nTracks"] = {
    #     "Range": [15.0, 1000.0],
    #     "InputName": "nTracks"
    # }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    # configdict["BasicVariables"]["BDTG"] = {
    #     "Range": [0.4, 1.0],
    #     "InputName": "BDTGResponse_3"
    # }

    configdict["AdditionalVariables"] = {}

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "",
        "MC": "",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": True,
        "DsHypo": True,
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    # Binning
    configdict["Binning"] = {}
    configdict["Binning"]["lab0_ETA"] = [2.0, 5.0]
    configdict["Binning"]["nPVs"] = [0.0, 3.0, 6.0, 20.0]
    # s = len(configdict["Binning"]["lab0_PT"])
    # for s in range(0, s):
    #    configdict["Binning"]["lab0_PT"][
    #        s] = configdict["Binning"]["lab0_PT"][s] * 1000.0

    configdict["MoreVariables"] = True

    if configdict["MoreVariables"] is True:
        configdict["AdditionalVariables"] = {}
        configdict["AdditionalVariables"]["lab0_PT"] = {
            "Range": [0.0, 40000.0],
            "InputName": "lab0_PT",
        }
        configdict["AdditionalVariables"]["lab0_ETA"] = {
            "Range": [2.0, 4.5],
            "InputName": "lab0_ETA",
        }
        configdict["AdditionalVariables"]["lab0_MVA"] = {
            "Range": [-0.1, 1.1],
            "InputName": "lab0_MVA",
        }
        configdict["AdditionalVariables"]["lab0_BPVLTIME"] = {
            "Range": [0.0, 0.5],
            "InputName": "lab0_BPVLTIME",
        }
        configdict["AdditionalVariables"]["lab0_LifetimeFit_cTau"] = {
            "Range": [0.0, 50.0],
            "InputName": "lab0_DTF_LifetimeFit_CTAU",
        }
        configdict["AdditionalVariables"]["lab0_LifetimeFit_cTauErr"] = {
            "Range": [0.0, 0.2],
            "InputName": "lab0_DTF_LifetimeFit_CTAUERR",
        }
        configdict["AdditionalVariables"]["lab0_SSPion_Mistag"] = {
            "Range": [0.0, 0.6],
            "InputName": "lab0_Run2_SSPion_Omega",
        }
        configdict["AdditionalVariables"]["lab0_SSPion_Decision"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_Run2_SSPion_Dec",
        }
        configdict["AdditionalVariables"]["lab0_SSKaon_Mistag"] = {
            "Range": [0.0, 0.6],
            "InputName": "lab0_Run2_SSKaon_Omega",
        }
        configdict["AdditionalVariables"]["lab0_SSKaon_Decision"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_Run2_SSKaon_Dec",
        }
        configdict["AdditionalVariables"]["lab0_SSProton_Mistag"] = {
            "Range": [0.0, 0.6],
            "InputName": "lab0_Run2_SSProton_Omega",
        }
        configdict["AdditionalVariables"]["lab0_SSProton_Decision"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_Run2_SSProton_Dec",
        }
        configdict["AdditionalVariables"]["lab0_OSKaon_Mistag"] = {
            "Range": [0.0, 0.6],
            "InputName": "lab0_Run2_OSKaon_Omega",
        }
        configdict["AdditionalVariables"]["lab0_OSKaon_Decision"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_Run2_OSKaon_Dec",
        }
        configdict["AdditionalVariables"]["lab0_OSElectron_Mistag"] = {
            "Range": [0.0, 0.6],
            "InputName": "lab0_Run2_OSElectron_Omega",
        }
        configdict["AdditionalVariables"]["lab0_OSElectron_Decision"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_Run2_OSElectron_Dec",
        }
        configdict["AdditionalVariables"]["lab0_OSMuon_Mistag"] = {
            "Range": [0.0, 0.6],
            "InputName": "lab0_Run2_OSMuon_Omega",
        }
        configdict["AdditionalVariables"]["lab0_OSMuon_Decision"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_Run2_OSMuon_Dec",
        }
        configdict["AdditionalVariables"]["lab3_PIDK"] = {
            "Range": [-10.0, 250.0],
            "InputName": "lab3_PIDK",
        }
        configdict["AdditionalVariables"]["lab4_PIDK"] = {
            "Range": [-300.0, 10.0],
            "InputName": "lab4_PIDK",
        }
        configdict["AdditionalVariables"]["lab5_PIDK"] = {
            "Range": [-300.0, 10.0],
            "InputName": "lab5_PIDK",
        }
        configdict["AdditionalVariables"]["lab0_BPVIP"] = {
            "Range": [0.0, 1.0],
            "InputName": "lab0_BPVIP",
        }
        configdict["AdditionalVariables"]["lab0_BPVIPCHI2"] = {
            "Range": [0.0, 30.0],
            "InputName": "lab0_BPVIPCHI2",
        }
        configdict["AdditionalVariables"]["lab0_CHI2DOF"] = {
            "Range": [0.0, 30.0],
            "InputName": "lab0_CHI2DOF",
        }
        configdict["AdditionalVariables"]["lab0_BPVDIRA"] = {
            "Range": [-1.0, 1.0],
            "InputName": "lab0_BPVDIRA",
        }
        configdict["AdditionalVariables"]["nLongTracks"] = {
            "Range": [0.0, 500.0],
            "InputName": "nLongTracks",
        }
        configdict["AdditionalVariables"]["nPVs"] = {
            "Range": [0.0, 30.0],
            "InputName": "nPVs",
        }

    ###############################################################
    ############                FITTING                ############
    ###############################################################
    configdict["pdfList"] = {}
    configdict["pdfList"]["Signal"] = {}
    configdict["pdfList"]["Signal"]["Bd2DPi"] = {}
    configdict["pdfList"]["Signal"]["Bd2DPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Signal"]["Bd2DPi"]["BeautyMass"]["IpatiaJohnsonSU"] = {
        "Title": "#B^{-}#rightarrow#D^{0}#pi",
        "Bins": 200,
        "Min": 5000.0,
        "Max": 6000.0,
        "Unit": "MeV/c^{2}",
        "sameMean": True,
        "mean": [5267.2, 5200, 5400],
        "sigmaI": [24.0, 10.0, 50.0],
        "sigmaJ": [14.1, 10.0, 50.0],
        "zeta": [0.0],
        "a1": [1.0, 0.2, 5.0],
        "a2": [2.5, 0.2, 5.0],
        "n1": [1.4, 0.01, 6.0],
        "n2": [1.8, 0.01, 3.0],
        "l": [-1.7],
        "tau": [0.3, 0.0, 2.0],
        "nu": [-0.33, -2.0, 0.5],
        "fracI": [0.31, 0.01, 0.99],
    }

    # Dataset prefix
    configdict["dataSetPrefix"] = {
        "Signal": "dataSetMC_Bd2DPi_",
    }

    # Axes titles
    configdict["AxisTitle"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {"Bd2DPi": "m(#D^{0}#pi^{-}) [MeV/c^{2}]"}

    # weighting templates
    # configdict["WeightingMassTemplates"] = {
    #    "PIDBachEff": {
    #        "2022": {
    #            "FileLabel": "#PIDK Pion 2022",
    #            "Var": ["lab1_ETA", "lab1_P"],
    #            "HistName": "Pi_DLLK<0.0&&IsMuon==0.0_All;"
    #        },
    #    },
    #    "PIDBachMisID": {
    #        "2022": {
    #            "FileLabel": "#PIDK Kaon 2022",
    #            "Var": ["lab1_ETA", "lab1_P"],
    #            "HistName": "K_DLLK<0.0&&IsMuon==0.0_All;"
    #        },
    #    },
    #    "RatioDataMC": {
    #        "2022": {
    #            "FileLabel": "#DataMC 2022",
    #            "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
    #            "HistName": "histRatio"
    #        },
    #    },
    # }

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "2022": {"All": 5.2785e03},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "2022": {"All": 1.8445e01},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "2022": {"All": 1.3791e01},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "2022": {"All": 1.3235e00},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "2022": {"All": 3.4296e00},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "2022": {"All": 1.2932e00},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "2022": {"All": 1.6709e00},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "2022": {"All": -2.0838e00},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "2022": {"All": -8.0794e-01},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "2022": {"All": 2.7196e-01},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "2022": {"All": 3.7279e-01},
        "Fixed": False,
    }

    #   ------------------------------------   #
    #   ----  combinatorial background  ----   #
    #   ------------------------------------   #

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"][
        "type"
    ] = "DoubleExponential"  # "Exponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
        "2022": {"All": -1.0e-03},
        "Fixed": False,
    }
    configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
        "2022": {"All": -1.0e-06},
        "Fixed": False,
    }
    configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
        "2022": {"All": 0.95},
        "Fixed": False,
    }

    #   ------------------------------------   #
    #   -------  Bd2DsPi background  -------   #
    #   ------------------------------------   #

    configdict["Bd2DstPiShape"] = {}
    configdict["Bd2DstPiShape"]["BeautyMass"] = {}
    configdict["Bd2DstPiShape"]["BeautyMass"]["type"] = "HILLdiniPlusHORNSdini"
    configdict["Bd2DstPiShape"]["BeautyMass"]["ahill"] = {
        "2022": {
            "All": 4.9135e03,
        },
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["bhill"] = {
        "2022": {
            "All": 5.1490e03,
        },
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["csihill"] = {
        "2022": {
            "All": -2.6505e-02,
        },
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["sigma"] = {
        "2022": {
            "All": 40.0,
        },
        "Fixed": False,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["ahorns"] = {
        "2022": {
            "All": 5.1117e03,
        },
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["bhorns"] = {
        "2022": {
            "All": 5.0221e03,
        },
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["csihorns"] = {
        "2022": {
            "All": 1.0357e00,
        },
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["sigmahorns"] = {
        "2022": {
            "All": 28.0,
        },
        "Fixed": False,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["shift"] = {
        "2022": {
            "All": 0.0,
        },
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["R"] = {
        "2022": {
            "All": 3.4109e00,
        },
        "Fixed": True,
    }

    configdict["Bd2DstPiShape"]["BeautyMass"]["frac"] = {
        "2022": {
            "All": 3.0e-01,
        },
        "Fixed": False,
    }

    configdict["Bd2DstPiShape"]["BeautyMass"]["frachorns"] = {
        "2022": {
            "All": 3.0e-01,
        },
        "Fixed": False,
    }

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    configdict["Yields"] = {}
    configdict["Yields"]["Bs2DsPi"] = {
        "2022": {
            "KPiPi": 0.0,
        },
        "Fixed": True
        #  "Constrained": True
    }
    configdict["Yields"]["Lb2LcPi"] = {
        "2022": {
            "KPiPi": 0.0,
        },
        "Fixed": True,
    }
    configdict["Yields"]["Bd2DK"] = {
        "2022": {
            "KPiPi": 5000.0,
        },
        "Fixed": False,
    }
    configdict["Yields"]["Bd2DstPi"] = {
        "2022": {
            "KPiPi": 2.0 * 85000.0,
        },
        "Fixed": False,
    }
    configdict["Yields"]["Bd2DRho"] = {
        "2022": {
            "KPiPi": 0.0,
        },
        "Fixed": True,
    }
    configdict["Yields"]["CombBkg"] = {
        "2022": {
            "KPiPi": 2.0 * 85000.0,
        },
        "Fixed": False,
    }
    configdict["Yields"]["Signal"] = {
        "2022": {
            "KPiPi": 770000.0,
        },
        "Fixed": False,
    }

    # ----------------------------------------------------------------------#
    ###                 MDfit plotting settings
    # ----------------------------------------------------------------------#

    from ROOT import kSolid

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Lb2LcPi",
        "Bd2DK",
        "Bs2DsPi",
        "Bd2DRho",
        "Bd2DstPi",
    ]
    configdict["PlotSettings"]["colors"] = [
        "#8c510a",  # Sig
        "#cccccc",  # combo
        "#006837",  # lb2lcpi
        "#01665e",  # bd2dpi
        "#d7301f",  #
        "#d8b365",  # "#54278f",
        "#f6e8c3",
    ]
    configdict["PlotSettings"]["pattern"] = [
        kSolid,
        3004,
        kSolid,
        3005,
        kSolid,
        3005,
        3005,
    ]
    configdict["PlotSettings"]["patterncolor"] = [
        "#fbb4b9",
        "#cccccc",
        "#006837",
        "#c7eae5",
        "#d7301f",
        "#8c510a",
        "#8c510a",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
