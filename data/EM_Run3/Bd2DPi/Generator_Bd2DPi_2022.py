###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys

# from ROOT import *
# print(os.environ)


sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa

WORKSPACEFILE = (
    "/eos/lhcb/wg/b2oc/XS_b2X_EM_2022/Bd2DPi/Templates/work_mc_bd2dpi_em.root"
)
WORKSPACENAME = "workspace"


def getconfig():
    configdict = {}

    ############################################################
    # List of observables for all the PDFs.
    # The content of this dictionary determines the observables
    # to generate for and how may taggers are present.
    ############################################################
    configdict["Observables"] = {
        "BeautyMass": {
            "Type": "RooRealVar",
            "Title": "B mass (MeV/c^2)",
            "Range": [5000, 6000],
        }
    }

    ############################################################
    # List of mass hypotheses for bachelor
    # The content of this dictionary determines how many
    # bachelor PID bins the final dataset is splitted into
    ############################################################
    configdict["Hypothesys"] = ["Bd2DPi"]

    ############################################################
    # Signal decay, Charm decay mode and year of data taking
    # Splitting per magnet polarity not implemented, at the moment
    ############################################################
    configdict["Decay"] = "Bd2DPi"
    configdict["CharmModes"] = [
        "KPiPi",
    ]
    configdict["Years"] = [
        "2022",
    ]

    ############################################################
    # For PIDK shapes we need also polarities
    # This is also used to split the yields between years properly
    ############################################################
    configdict["Polarity"] = ["Up", "Down"]
    configdict["IntegratedLuminosity"] = {
        "2022": {"Down": 0.5, "Up": 0.5},
    }

    configdict["FractionsLuminosity"] = {
        k: v["Up"] / (v["Up"] + v["Down"])
        for k, v in configdict["IntegratedLuminosity"].items()
    }

    configdict["WorkspaceToRead"] = {"File": WORKSPACEFILE, "Workspace": WORKSPACENAME}
    ############################################################
    # List of components with yields to generate.
    # The content of this dictionary determines, for each
    # PID bin and year, how many PDF components are generated.
    # If there is only signal, a TTree ready for sFit is
    # generated directly, without need for doing a (useless)
    # mass fit.
    ############################################################

    configdict["Components"] = {
        "Signal": {
            "Bd2DPi": {
                "2022": {"KPiPi": 231000.0},
            }
        },
        "Combinatorial": {
            "Bd2DPi": {
                "2022": {"KPiPi": 101224.0},
            }
        },
        "Bd2DK": {
            "Bd2DPi": {
                "2022": {"KPiPi": 19822.0},
            }
        },
        "Bd2DRho": {
            "Bd2DPi": {
                "2022": {"KPiPi": 88235.0},
            }
        },
        "Bd2DstPi": {
            "Bd2DPi": {
                "2022": {"KPiPi": 65121.0},
            }
        },
        "Lb2LcPi": {
            "Bd2DPi": {
                "2022": {"KPiPi": 545.0},
            }
        },
        "Bs2DsPi": {
            "Bd2DPi": {
                "2022": {"KPiPi": 685.0},
            }
        },
    }

    ############################################################
    #                      Signal
    ############################################################

    ############################################################

    def getPdfTemplateConfigFromWorkspace(
        name="Signal_BeautyMass_IpatiaPlusJohnsonSU_both_{mode}_{year}",
        modes=["All"],
        years=["2022"],
        lowercasemode=True,
        **kwargs
    ):
        """
        Generate a dictionary of the form
        {
            year1: {
                mode1: {
                    'Type': 'FromWorkspace',
                    'Name': name.format(mode=m1),
                    'WorkspaceName': WorkspaceName,
                    'WorkspaceFile': WorkspaceFile,
                    **kwargs
                },
                mode2: { ... }
            },
            year2: { ... }
        }
        for all modes m1... given in the `modes` and all years in the `years`
        argument.
        """
        return {
            y: {
                m: dict(
                    Type="FromWorkspace",
                    Name=name.format(mode=m.lower() if lowercasemode else m, year=y),
                    **kwargs
                )
                for m in modes
            }
            for y in years
        }

    # the shapes are shared between the years

    configdict["PDFList"] = {
        "BeautyMass": {
            "Signal": {
                "Bd2DPi": {
                    "2022": {
                        "KPiPi": {
                            "a1": 1.15342,
                            "a2": 2.79001,
                            "fracI": 0.25,
                            "l": -1.7,
                            "n1": 1.32575,
                            "n2": 1.8218,
                            "nu": -0.44943,
                            "sigmaI": 22.01244,
                            "sigmaJ": 13.98339,
                            "tau": 0.29707,
                            "fb": 0.0,
                            "mean": 5.2801e03,
                            "zeta": 0.0,
                            "Type": "IpatiaJohnsonSU",
                        }
                    },
                }
            },
            "Combinatorial": {
                "Bd2DPi": {
                    "2022": {"KPiPi": {"cB": -1.1186e-03, "Type": "Exponential"}},
                }
            },
            "Bd2DK": {
                "Bd2DPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBd2DKPdf_m_both_2022"
                ),
            },
            "Bd2DRho": {
                "Bd2DPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBd2DRhoPdf_m_both_2022"
                ),
            },
            "Bd2DstPi": {
                "Bd2DPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBd2DstPiPdf_m_both_2022"
                ),
            },
            "Bs2DsPi": {
                "Bd2DPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBs2DsPiPdf_m_both_2022"
                ),
            },
            "Lb2LcPi": {
                "Bd2DPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgLb2LcPiPdf_m_both_2022"
                ),
            },
        },
    }

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    from configutils import main

    main(getconfig())
