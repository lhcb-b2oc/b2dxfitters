##########make#####################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Bd2DPi"
    configdict["CharmModes"] = {"KPiPi"}
    configdict["Backgrounds"] = ["Bd2DK", "Bd2DRho", "Bd2DstPi", "Lb2LcPi", "Bs2DsPi"]

    # year of data taking
    configdict["YearOfDataTaking"] = {
        "2022",
    }
    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {
        "2022": {"Down": 0.500, "Up": 0.500},
    }

    # stripping (necessary in case of PIDK shapes)
    configdict["dataName"] = "../data/EM_Run3/Bd2DPi/config_Bd2DPi.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotBd2DPi", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5000, 6000],
        "InputName": "lab0_MassFitConsD_M",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1830, 1920],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-150.0, 150.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [2000.0, 150000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [250.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.4, 1.0],
        "InputName": "BDTGResponse_3",
    }

    # additional variables
    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_ETA"] = {
        "Range": [0.0, 6.0],
        "InputName": "lab0_ETA",
    }
    #    configdict["AdditionalVariables"]["lab1_ETA"] = {
    #        "Range": [0.0, 6.0],
    #        "InputName": "lab1_ETA"
    #    }
    #    configdict["AdditionalVariables"]["lab3_ETA"] = {
    #        "Range": [0.0, 6.0],
    #        "InputName": "lab3_ETA"
    #    }
    #    configdict["AdditionalVariables"]["lab4_ETA"] = {
    #        "Range": [0.0, 6.0],
    #        "InputName": "lab4_ETA"
    #    }
    #    configdict["AdditionalVariables"]["lab5_ETA"] = {
    #        "Range": [0.0, 6.0],
    #        "InputName": "lab5_ETA"
    #    }
    #    configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
    #        "Range": [0.0, 2500.0],
    #        "InputName": "lab0_ENDVERTEX_ZERR"
    #    }
    #    configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
    #        "Range": [0.0, 3000.0],
    #        "InputName": "lab2_ENDVERTEX_ZERR"
    #    }
    configdict["AdditionalVariables"]["lab0_P"] = {
        "Range": [0.0, 1600000.0],
        "InputName": "lab0_P",
    }
    configdict["AdditionalVariables"]["lab0_PT"] = {
        "Range": [0.0, 40000.0],
        "InputName": "lab0_PT",
    }

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "",
        "MC": "lab2_FDCHI2_ORIVX>2 && "
        "lab0_LifetimeFit_Dplus_ctau[0] >0.0 && "
        "lab2_FD_ORIVX>0.0",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": True,
        "DsHypo": True,
    }

    # Binning
    configdict["Binning"] = {}
    configdict["Binning"]["lab0_ETA"] = [2.0, 2.5, 3.0, 3.5, 5.0]
    configdict["Binning"]["lab0_PT"] = [
        0.0,
        0.5,
        1.0,
        1.5,
        2.0,
        2.5,
        3.0,
        3.5,
        4.0,
        4.5,
        5.0,
        5.5,
        6.0,
        6.5,
        7.0,
        7.5,
        8.0,
        8.5,
        9.0,
        9.5,
        10.5,
        11.5,
        12.5,
        14.0,
        16.5,
        23.5,
        40.0,
    ]
    s = len(configdict["Binning"]["lab0_PT"])
    for s in range(0, s):
        configdict["Binning"]["lab0_PT"][s] = (
            configdict["Binning"]["lab0_PT"][s] * 1000.0
        )

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    # weighting templates
    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2022": {
                "FileLabel": "#PIDK Pion 2022",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "Pi_DLLK<0.0&&IsMuon==0.0_All;",
            },
        },
        "PIDBachMisID": {
            "2022": {
                "FileLabel": "#PIDK Kaon 2022",
                "Var": ["lab1_ETA", "lab1_P"],
                "HistName": "K_DLLK<0.0&&IsMuon==0.0_All;",
            },
        },
        "RatioDataMC": {
            "2022": {
                "FileLabel": "#DataMC 2022",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio",
            },
        },
    }

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    #   ------------------------------------   #
    #   -------     Signal shape     -------   #
    #   ------------------------------------   #

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "IpatiaPlusJohnsonSU"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "2022": {"All": 5.2801e03},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "2022": {"All": 22.01244},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "2022": {"All": 13.98339},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "2022": {"All": 1.15342},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "2022": {"All": 2.79001},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "2022": {"All": 1.32575},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "2022": {"All": 1.8218},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "2022": {"All": -1.7},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "2022": {"All": -0.44943},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "2022": {"All": 0.29707},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "2022": {"All": 0.25},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   ----  combinatorial background  ----   #
    #   ------------------------------------   #

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
        "2022": {"All": -1.1186e-02},
        "Fixed": False,
    }

    #   ------------------------------------   #
    #   -------  specific background  ------   #
    #   ------------------------------------   #

    configdict["Bd2DstPiShape"] = {}
    configdict["Bd2DstPiShape"]["BeautyMass"] = {}
    configdict["Bd2DstPiShape"]["BeautyMass"]["type"] = "HILLdiniPlusHORNSdini"
    configdict["Bd2DstPiShape"]["BeautyMass"]["R"] = {
        "2022": {"All": 3.4109e00},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["ahill"] = {
        "2022": {"All": 4.9135e03},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["ahorns"] = {
        "2022": {"All": 5.1117e03},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["bhill"] = {
        "2022": {"All": 5.1490e03},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["bhorns"] = {
        "2022": {"All": 5.0221e03},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["csihill"] = {
        "2022": {"All": -2.6505e-02},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["csihorns"] = {
        "2022": {"All": 1.0357e00},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["frac"] = {
        "2022": {"All": 9.4594e-01},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["frachorns"] = {
        "2022": {"All": 8.6075e-01},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["sigma"] = {
        "2022": {"All": 7.38},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["sigmahorns"] = {
        "2022": {"All": 12.37},
        "Fixed": True,
    }
    configdict["Bd2DstPiShape"]["BeautyMass"]["shift"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }

    configdict["Bd2DRhoShape"] = {}
    configdict["Bd2DRhoShape"]["BeautyMass"] = {}
    configdict["Bd2DRhoShape"]["BeautyMass"]["type"] = "HORNSdini"
    configdict["Bd2DRhoShape"]["BeautyMass"]["R"] = {
        "2022": {"All": 7.88531},
        "Fixed": True,
    }
    configdict["Bd2DRhoShape"]["BeautyMass"]["a"] = {
        "2022": {"All": 4.2217e03},
        "Fixed": True,
    }
    configdict["Bd2DRhoShape"]["BeautyMass"]["b"] = {
        "2022": {"All": 5.1415e03},
        "Fixed": True,
    }
    configdict["Bd2DRhoShape"]["BeautyMass"]["csi"] = {
        "2022": {"All": 5.3492e00},
        "Fixed": True,
    }
    configdict["Bd2DRhoShape"]["BeautyMass"]["frac"] = {
        "2022": {"All": 9.8273e-01},
        "Fixed": True,
    }
    configdict["Bd2DRhoShape"]["BeautyMass"]["sigma"] = {
        "2022": {"All": 1.5420e01},
        "Fixed": True,
    }
    configdict["Bd2DRhoShape"]["BeautyMass"]["shift"] = {
        "2022": {"All": 0.0},
        "Fixed": True,
    }

    #   ------------------------------------   #
    #   ----------     Yields     ----------   #
    #   ------------------------------------   #

    configdict["Yields"] = {}
    configdict["Yields"]["Bs2DsPi"] = {
        "2022": {
            "KPiPi": 685.0,
        },
        "Fixed": True,
        #  "Constrained": True,
    }
    configdict["Yields"]["Lb2LcPi"] = {
        "2022": {
            "KPiPi": 545.0,
        },
        "Fixed": True,
    }
    configdict["Yields"]["Bd2DK"] = {
        "2022": {
            "KPiPi": 19822.0,
        },
        "Fixed": False,
    }
    configdict["Yields"]["Bd2DstPi"] = {
        "2022": {
            "KPiPi": 80000.00,
        },
        "Fixed": False,
    }
    configdict["Yields"]["Bd2DRho"] = {
        "2022": {
            "KPiPi": 80000.00,
        },
        "Fixed": False,
    }
    configdict["Yields"]["CombBkg"] = {"2022": {"KPiPi": 100000.0}, "Fixed": False}
    configdict["Yields"]["Signal"] = {"2022": {"KPiPi": 200000.0}, "Fixed": False}

    # ----------------------------------------------------------------------#
    ###                 MDfit plotting settings
    # ----------------------------------------------------------------------#

    from ROOT import kSolid

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Lb2LcPi",
        "Bd2DK",
        "Bs2DsPi",
        "Bd2DRho",
        "Bd2DstPi",
    ]
    configdict["PlotSettings"]["colors"] = [
        "#8c510a",  # Sig
        "#cccccc",  # combo
        "#006837",  # lb2lcpi
        "#01665e",  # bd2dpi
        "#d7301f",  #
        "#d8b365",  # "#54278f",
        "#f6e8c3",
    ]
    configdict["PlotSettings"]["pattern"] = [
        kSolid,
        3004,
        kSolid,
        3005,
        kSolid,
        3005,
        3005,
    ]
    configdict["PlotSettings"]["patterncolor"] = [
        "#fbb4b9",
        "#cccccc",
        "#006837",
        "#c7eae5",
        "#d7301f",
        "#8c510a",
        "#8c510a",
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
