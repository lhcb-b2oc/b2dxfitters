###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Bd2DPiConfigForNominalMassFit import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    configdict["pdfList"] = {}
    configdict["pdfList"]["Signal"] = {}
    configdict["pdfList"]["Signal"]["Bd2DPi"] = {}
    configdict["pdfList"]["Signal"]["Bd2DPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Signal"]["Bd2DPi"]["BeautyMass"]["IpatiaPlusJohnsonSU"] = {
        "Title": "B_{d}#rightarrowD#pi",
        "Bins": 200,
        "Min": 5100.0,
        "Max": 5500.0,
        "Unit": "MeV/c^{2}",
        "mean": [5367.2, 5200, 5400],
        "sigmaI": [2.5441e01],
        "sigmaJ": [14.1, 10.0, 50.0],
        "zeta": [0.0],
        "fb": [0.0],
        "a1": [6.2115e-01],
        "a2": [1.3354e00],
        "n1": [1.6736e00],
        "n2": [2.9992e00],
        "l": [-1.7],  # for stability
        "tau": [4.0111e-01],
        "nu": [-1.8557e-01],
        "fracI": [0.2],  # , 0.01, 0.99]
    }

    configdict["pdfList"]["Bd2DstPi"] = {}
    configdict["pdfList"]["Bd2DstPi"]["Bd2DPi"] = {}
    configdict["pdfList"]["Bd2DstPi"]["Bd2DPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Bd2DstPi"]["Bd2DPi"]["BeautyMass"][
        "HILLdiniPlusHORNSdini"
    ] = {
        "Title": "B_{d}#rightarrowD^{*}#pi",
        "Bins": 100,
        "Min": 5000.0,
        "Max": 5200.0,
        "Unit": "MeV/c^{2}",
        "ahill": [4.9135e03],
        "bhill": [5.1490e03],
        "csihill": [-2.6505e-02],
        "sigma": [40.0, 2.0, 80.0],
        "ahorns": [5.1117e03],
        "bhorns": [5.0221e03],
        "csihorns": [1.0357e00],
        "sigmahorns": [28.0, 3.0, 40.0],
        "shift": [0.0],
        "R": [3.4109e00],
        "frac": [9.4594e-01],  # 0.3, 0.0, 1.0],
        "frachorns": [8.6075e-01],  # 0.3, 0.0, 1.0]
    }

    configdict["pdfList"]["Bd2DRho"] = {}
    configdict["pdfList"]["Bd2DRho"]["Bd2DPi"] = {}
    configdict["pdfList"]["Bd2DRho"]["Bd2DPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Bd2DRho"]["Bd2DPi"]["BeautyMass"]["HORNSdini"] = {
        "Title": "B_{d}#rightarrowD#rho",
        "Bins": 100,
        "Min": 5000.0,
        "Max": 5400.0,
        "Unit": "MeV/c^{2}",
        "a": [3500, 3000, 4500],
        "b": [5200, 4300, 5500],
        "csi": [1.1, -2.0, 6.0],
        "sigma": [40.0, 2.0, 80.0],
        "shift": [0.0],
        "R": [5.8, 0.5, 40.0],
        "frac": [0.98, 0.0, 1.0],
    }

    # Dataset prefix
    configdict["dataSetPrefix"] = {
        "Signal": "dataSetMC_Bd2DPi_",
        "Bd2DstPi": "dataSetMC_Bd2DstPi_",
        "Bd2DRho": "dataSetMC_Bd2DRho_",
    }

    # Axes titles
    configdict["AxisTitle"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {
        "Bd2DPi": "m(D^{-}#pi^{+}) [MeV/c^{2}]",
        "Bd2DstPi": "m(D^{-}#pi^{+}) [MeV/c^{2}]",
    }

    return configdict
