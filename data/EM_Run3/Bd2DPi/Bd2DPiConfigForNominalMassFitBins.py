###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Bd2DPiConfigForNominalMassFit import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    configdict["Bd2DstPiShape"]["BeautyMass"]["sigmahorns"] = {}
    configdict["Bd2DstPiShape"]["BeautyMass"]["sigmahorns"] = {
        "2022": {
            "All": {
                "p0": 11.136 / 100.0,
                "p1": 0.118 / 100.0,
                "p2": 0.000,
                "var": "lab0_PT",
            }
        },
        "Fixed": True,
    }

    configdict["Bd2DRhoShape"]["BeautyMass"]["sigma"] = {}
    configdict["Bd2DRhoShape"]["BeautyMass"]["sigma"] = {
        "2022": {
            "All": {
                "p0": 13.771 / 100.0,
                "p1": 0.143 / 100.0,
                "p2": 0.000,
                "var": "lab0_PT",
            }
        },
        "Fixed": True,
    }

    configdict["Yields"]["Bs2DsPi"] = {
        "2022": {
            "KPiPi": 0.0,
        },
        "Fixed": True,
    }
    configdict["Yields"]["Lb2LcPi"] = {
        "2022": {
            "KPiPi": 0.0,
        },
        "Fixed": True,
    }

    return configdict
