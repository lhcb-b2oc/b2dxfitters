###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Bd2DPiConfigForNominalMassFit import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    configdict["pdfList"] = {}
    configdict["pdfList"]["Signal"] = {}
    configdict["pdfList"]["Signal"]["Bd2DPi"] = {}
    configdict["pdfList"]["Signal"]["Bd2DPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Signal"]["Bd2DPi"]["BeautyMass"]["IpatiaPlusJohnsonSU"] = {
        "Title": "B_{d}#rightarrowD#pi",
        "Bins": 200,
        "Min": 5000.0,
        "Max": 6000.0,
        "Unit": "MeV/c^{2}",
        "mean": [5267.2, 5200, 5400],
        "sigmaI": [24.0, 10.0, 50.0],
        "sigmaJ": [14.1, 10.0, 50.0],
        "zeta": [0.0],
        "fb": [0.0],
        "a1": [1.0, 0.2, 5.0],
        "a2": [2.5, 0.2, 5.0],
        "n1": [1.4, 0.01, 6.0],
        "n2": [1.8, 0.01, 3.0],
        "l": [-1.7],  # for stability
        "tau": [0.3, 0.0, 2.0],
        "nu": [-0.33, -2.0, 0.5],
        "fracI": [0.31, 0.01, 0.99],
    }

    configdict["pdfList"]["Bd2DstPi"] = {}
    configdict["pdfList"]["Bd2DstPi"]["Bd2DPi"] = {}
    configdict["pdfList"]["Bd2DstPi"]["Bd2DPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Bd2DstPi"]["Bd2DPi"]["BeautyMass"][
        "HILLdiniPlusHORNSdini"
    ] = {
        "Title": "B_{d}#rightarrowD^{*}#pi",
        "Bins": 100,
        "Min": 5000.0,
        "Max": 5200.0,
        "Unit": "MeV/c^{2}",
        "ahill": [4550, 4300, 5300],
        "bhill": [5140, 4300, 5300],
        "csihill": [-1.4, -4.0, 3.0],
        "sigma": [40.0, 2.0, 80.0],
        "ahorns": [4900.0, 4900, 5150],
        "bhorns": [5000.0, 4900, 5200],
        "csihorns": [0.5, 0.1, 4.0],
        "sigmahorns": [28.0, 3.0, 40.0],
        "shift": [0.0],
        "R": [1.0, 0.5, 40.0],
        "frac": [0.3, 0.0, 1.0],
        "frachorns": [0.3, 0.0, 1.0],
    }

    configdict["pdfList"]["Bd2DRho"] = {}
    configdict["pdfList"]["Bd2DRho"]["Bd2DPi"] = {}
    configdict["pdfList"]["Bd2DRho"]["Bd2DPi"]["BeautyMass"] = {}
    configdict["pdfList"]["Bd2DRho"]["Bd2DPi"]["BeautyMass"]["HORNSdini"] = {
        "Title": "B_{d}#rightarrowD#rho",
        "Bins": 100,
        "Min": 5000.0,
        "Max": 5400.0,
        "Unit": "MeV/c^{2}",
        "a": [3500, 3000, 4500],
        "b": [5200, 4300, 5500],
        "csi": [1.1, -2.0, 6.0],
        "sigma": [40.0, 2.0, 80.0],
        "shift": [0.0],
        "R": [5.8, 0.5, 40.0],
        "frac": [0.98, 0.0, 1.0],
    }

    # Dataset prefix
    configdict["dataSetPrefix"] = {
        "Signal": "dataSetMC_Bd2DPi_",
        "Bd2DstPi": "dataSetMC_Bd2DstPi_",
        "Bd2DRho": "dataSetMC_Bd2DRho_",
    }

    # Axes titles
    configdict["AxisTitle"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {}
    configdict["AxisTitle"]["BeautyMass"] = {
        "Bd2DPi": "m(D^{-}#pi^{+}) [MeV/c^{2}]",
        "Bd2DstPi": "m(D^{-}#pi^{+}) [MeV/c^{2}]",
    }

    return configdict
