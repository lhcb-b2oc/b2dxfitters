###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys

# from ROOT import *
# print(os.environ)


sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa

WORKSPACEFILE = (
    "/eos/lhcb/wg/b2oc/XS_b2X_EM_2022/Bs2DsPi/Templates/work_mc_bs2dspi_em.root"
)
WORKSPACENAME = "workspace"


def getconfig():
    configdict = {}

    ############################################################
    # List of observables for all the PDFs.
    # The content of this dictionary determines the observables
    # to generate for and how may taggers are present.
    ############################################################
    configdict["Observables"] = {
        "BeautyMass": {
            "Type": "RooRealVar",
            "Title": "B mass (MeV/c^2)",
            "Range": [5000, 6000],
        }
    }

    ############################################################
    # List of mass hypotheses for bachelor
    # The content of this dictionary determines how many
    # bachelor PID bins the final dataset is splitted into
    ############################################################
    configdict["Hypothesys"] = ["Bs2DsPi"]

    ############################################################
    # Signal decay, Charm decay mode and year of data taking
    # Splitting per magnet polarity not implemented, at the moment
    ############################################################
    configdict["Decay"] = "Bs2DsPi"
    configdict["CharmModes"] = [
        "KKPi",
    ]
    configdict["Years"] = [
        "2022",
    ]

    ############################################################
    # For PIDK shapes we need also polarities
    # This is also used to split the yields between years properly
    ############################################################
    configdict["Polarity"] = ["Up", "Down"]
    configdict["IntegratedLuminosity"] = {
        "2022": {"Down": 0.5, "Up": 0.5},
    }

    configdict["FractionsLuminosity"] = {
        k: v["Up"] / (v["Up"] + v["Down"])
        for k, v in configdict["IntegratedLuminosity"].items()
    }

    configdict["WorkspaceToRead"] = {"File": WORKSPACEFILE, "Workspace": WORKSPACENAME}
    ############################################################
    # List of components with yields to generate.
    # The content of this dictionary determines, for each
    # PID bin and year, how many PDF components are generated.
    # If there is only signal, a TTree ready for sFit is
    # generated directly, without need for doing a (useless)
    # mass fit.
    ############################################################

    configdict["Components"] = {
        "Signal": {
            "Bs2DsPi": {
                "2022": {"KKPi": 38600.0},
            }
        },
        "Combinatorial": {
            "Bs2DsPi": {
                "2022": {"KKPi": 11562.0},
            }
        },
        "Bs2DsstPi": {
            "Bs2DsPi": {
                "2022": {"KKPi": 31860.0},
            }
        },
        "Bs2DsRho": {
            "Bs2DsPi": {
                "2022": {"KKPi": 22230.0},
            }
        },
        "Bs2DsstRho": {
            "Bs2DsPi": {
                "2022": {"KKPi": 5588.0},
            }
        },
        "Bd2DsPi": {
            "Bs2DsPi": {
                "2022": {"KKPi": 1070.0},
            }
        },
        "Bd2DsstPi": {
            "Bs2DsPi": {
                "2022": {"KKPi": 1350.0},
            }
        },
        "Bd2DPi": {
            "Bs2DsPi": {
                "2022": {"KKPi": 526.0},
            }
        },
        "Lb2LcPi": {
            "Bs2DsPi": {
                "2022": {"KKPi": 421.0},
            }
        },
        "Bs2DsK": {
            "Bs2DsPi": {
                "2022": {"KKPi": 235.0},
            }
        },
    }

    ############################################################
    #                      Signal
    ############################################################

    ############################################################

    def getPdfTemplateConfigFromWorkspace(
        name="Signal_BeautyMass_IpatiaPlusJohnsonSU_both_{mode}_{year}",
        modes=["All"],
        years=["2022"],
        lowercasemode=True,
        **kwargs
    ):
        """
        Generate a dictionary of the form
        {
            year1: {
                mode1: {
                    'Type': 'FromWorkspace',
                    'Name': name.format(mode=m1),
                    'WorkspaceName': WorkspaceName,
                    'WorkspaceFile': WorkspaceFile,
                    **kwargs
                },
                mode2: { ... }
            },
            year2: { ... }
        }
        for all modes m1... given in the `modes` and all years in the `years`
        argument.
        """
        return {
            y: {
                m: dict(
                    Type="FromWorkspace",
                    Name=name.format(mode=m.lower() if lowercasemode else m, year=y),
                    **kwargs
                )
                for m in modes
            }
            for y in years
        }

    # the shapes are shared between the years

    configdict["PDFList"] = {
        "BeautyMass": {
            "Signal": {
                "Bs2DsPi": {
                    "2022": {
                        "KKPi": {
                            "a1": 1.15342,
                            "a2": 2.79001,
                            "fracI": 0.25,
                            "l": -1.7,
                            "n1": 1.32575,
                            "n2": 1.8218,
                            "nu": -0.44943,
                            "sigmaI": 22.01244,
                            "sigmaJ": 13.98339,
                            "tau": 0.29707,
                            "fb": 0.0,
                            "mean": 5367.61198,
                            "zeta": 0.0,
                            "Type": "IpatiaJohnsonSU",
                        }
                    },
                }
            },
            "Combinatorial": {
                "Bs2DsPi": {
                    "2022": {"KKPi": {"cB": -1.1186e-03, "Type": "Exponential"}},
                }
            },
            "Bs2DsstPi": {
                "Bs2DsPi": {
                    "2022": {
                        "KKPi": {
                            "ahill": 4.6315e03,
                            "bhill": 5.3104e03,
                            "csihill": -1.1509e00,
                            "sigma": 2.9948e00,
                            "ahorns": 5.0997e03,
                            "bhorns": 5.2070e03,
                            "csihorns": 9.6358e-01,
                            "sigmahorns": 1.4226e01,
                            "shift": 0.0,
                            "R": 5.0721e00,
                            "frac": 3.1800e-01,
                            "frachorns": 1.7398e-01,
                            "Type": "HILLdiniPlusHORNSdini",
                        }
                    },
                }
            },
            "Bd2DsstPi": {
                "Bs2DsPi": {
                    "2022": {
                        "KKPi": {
                            "ahill": 4.6315e03 - 87.23,
                            "bhill": 5.3104e03 - 87.23,
                            "csihill": -1.1509e00,
                            "sigma": 2.9948e00,
                            "ahorns": 5.0997e03 - 87.23,
                            "bhorns": 5.2070e03 - 87.23,
                            "csihorns": 9.6358e-01,
                            "sigmahorns": 1.4226e01,
                            "shift": 0.0,
                            "R": 5.0721e00,
                            "frac": 3.1800e-01,
                            "frachorns": 1.7398e-01,
                            "Type": "HILLdiniPlusHORNSdini",
                        }
                    },
                }
            },
            "Bd2DsPi": {
                "Bs2DsPi": {
                    "2022": {
                        "KKPi": {
                            "a1": 1.15342,
                            "a2": 2.79001,
                            "fracI": 0.25,
                            "l": -1.7,
                            "n1": 1.32575,
                            "n2": 1.8218,
                            "nu": -0.44943,
                            "sigmaI": 22.01244,
                            "sigmaJ": 13.98339,
                            "tau": 0.29707,
                            "fb": 0.0,
                            "mean": 5367.61198 - 87.23,
                            "zeta": 0.0,
                            "Type": "IpatiaJohnsonSU",
                        }
                    },
                }
            },
            "Bs2DsRho": {
                "Bs2DsPi": {
                    "2022": {
                        "KKPi": {
                            "a": 4.1431e03,
                            "b": 5.2276e03,
                            "csi": 4.3152e00,
                            "sigma": 1.5504e01,
                            "shift": 0.0,
                            "R": 6.4109e00,
                            "frac": 9.6730e-01,
                            "Type": "HORNSdini",
                        }
                    },
                }
            },
            "Bs2DsstRho": {
                "Bs2DsPi": {
                    "2022": {
                        "KKPi": {
                            "a": 3.0000e03,
                            "b": 5.0910e03,
                            "csi": 5.0000e01,
                            "sigma": 4.2153e01,
                            "shift": 0.0,
                            "R": 6.0000e01,
                            "frac": 9.9643e-01,
                            "Type": "HORNSdini",
                        }
                    },
                }
            },
            "Bd2DPi": {
                "Bs2DsPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBd2DPiPdf_m_both_2022"
                ),
            },
            "Bs2DsK": {
                "Bs2DsPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgBs2DsKPdf_m_both_2022"
                ),
            },
            "Lb2LcPi": {
                "Bs2DsPi": getPdfTemplateConfigFromWorkspace(
                    "PhysBkgLb2LcPiPdf_m_both_2022"
                ),
            },
        },
    }

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    from configutils import main

    main(getconfig())
