###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Bs2DsKConfigForNominalMassFit import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotBs2DsK_Nominal", "Extension": "pdf"}

    lab2_FDCHI2_0 = "lab2_FDCHI2_ORIVX > 0"
    lab2_FDCHI2_9 = "lab2_FDCHI2_ORIVX > 9"

    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["KKPi"] = {"Data": lab2_FDCHI2_0}
    configdict["AdditionalCuts"]["KPiPi"] = {"Data": lab2_FDCHI2_9}
    configdict["AdditionalCuts"]["PiPiPi"] = {"Data": lab2_FDCHI2_9}

    # --------------------------------------------------------
    ###             MDfit fitting settings
    # --------------------------------------------------------

    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"][
        "type"
    ] = "IpatiaJohnsonSU"  # WithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {"All": 5367.51},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "20152016": {"All": 5367.51},
        "2017": {"All": 5367.51},
        "2018": {"All": 5367.51},
        "Fixed": False,
    }

    # PIDCorr
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run2": {"All": 0.00000},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run2": {"All": 0.00000},
        "Fixed": True,
    }

    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "20152016": {
            "NonRes": 0.267046,
            "PhiPi": 0.272534,
            "KstK": 0.232789,
            "KPiPi": 0.28194,
            "PiPiPi": 0.28194,
        },
        "2017": {
            "NonRes": 0.314537,
            "PhiPi": 0.266499,
            "KstK": 0.262134,
            "KPiPi": 0.270575,
            "PiPiPi": 0.270575,
        },
        "2018": {
            "NonRes": 0.380822,
            "PhiPi": 0.23871,
            "KstK": 0.39719,
            "KPiPi": 0.540159,
            "PiPiPi": 0.540159,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "20152016": {
            "NonRes": 1.95297,
            "PhiPi": 2.6426,
            "KstK": 2.77204,
            "KPiPi": 3.02235,
            "PiPiPi": 3.02235,
        },
        "2017": {
            "NonRes": 2.76145,
            "PhiPi": 2.95991,
            "KstK": 2.72509,
            "KPiPi": 3.48439,
            "PiPiPi": 3.48439,
        },
        "2018": {
            "NonRes": 2.4077,
            "PhiPi": 2.79521,
            "KstK": 2.27732,
            "KPiPi": 2.39562,
            "PiPiPi": 2.39562,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "20152016": {
            "NonRes": 50.0,
            "PhiPi": 50.0,
            "KstK": 50.0,
            "KPiPi": 50.0,
            "PiPiPi": 50.0,
        },
        "2017": {
            "NonRes": 50.0,
            "PhiPi": 50.0,
            "KstK": 50.0,
            "KPiPi": 50.0,
            "PiPiPi": 50.0,
        },
        "2018": {
            "NonRes": 50.0,
            "PhiPi": 50.0,
            "KstK": 50.0,
            "KPiPi": 50.0,
            "PiPiPi": 50.0,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "20152016": {
            "NonRes": 2.02956,
            "PhiPi": 1.07021,
            "KstK": 0.974427,
            "KPiPi": 1.15776,
            "PiPiPi": 1.15776,
        },
        "2017": {
            "NonRes": 1.27801,
            "PhiPi": 0.966063,
            "KstK": 0.999712,
            "KPiPi": 0.979765,
            "PiPiPi": 0.979765,
        },
        "2018": {
            "NonRes": 1.6869,
            "PhiPi": 0.986023,
            "KstK": 1.17564,
            "KPiPi": 1.51773,
            "PiPiPi": 1.51773,
        },
        "Fixed": True,
    }

    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "20152016": {
            "NonRes": -1.54649,
            "PhiPi": -1.47673,
            "KstK": -1.40369,
            "KPiPi": -1.39036,
            "PiPiPi": -1.39036,
        },
        "2017": {
            "NonRes": -1.59434,
            "PhiPi": -1.36771,
            "KstK": -1.46337,
            "KPiPi": -1.33975,
            "PiPiPi": -1.33975,
        },
        "2018": {
            "NonRes": -1.76979,
            "PhiPi": -1.33691,
            "KstK": -1.61501,
            "KPiPi": -3.35,
            "PiPiPi": -3.35,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "20152016": {
            "NonRes": -0.19274,
            "PhiPi": -0.202706,
            "KstK": -0.140866,
            "KPiPi": -0.0483708,
            "PiPiPi": -0.0483708,
        },
        "2017": {
            "NonRes": -0.162521,
            "PhiPi": -0.125343,
            "KstK": -0.168803,
            "KPiPi": -0.035089,
            "PiPiPi": -0.035089,
        },
        "2018": {
            "NonRes": -0.210806,
            "PhiPi": -0.159861,
            "KstK": -0.1796,
            "KPiPi": -0.286881,
            "PiPiPi": -0.286881,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "20152016": {
            "NonRes": 0.34648,
            "PhiPi": 0.360024,
            "KstK": 0.343189,
            "KPiPi": 0.352791,
            "PiPiPi": 0.352791,
        },
        "2017": {
            "NonRes": 0.319533,
            "PhiPi": 0.328242,
            "KstK": 0.344825,
            "KPiPi": 0.311098,
            "PiPiPi": 0.311098,
        },
        "2018": {
            "NonRes": 0.294316,
            "PhiPi": 0.345727,
            "KstK": 0.337835,
            "KPiPi": 0.338194,
            "PiPiPi": 0.338194,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "20152016": {
            "NonRes": 0.114842,
            "PhiPi": 0.124757,
            "KstK": 0.121322,
            "KPiPi": 0.19915,
            "PiPiPi": 0.19915,
        },
        "2017": {
            "NonRes": 0.142232,
            "PhiPi": 0.162128,
            "KstK": 0.126357,
            "KPiPi": 0.246785,
            "PiPiPi": 0.246785,
        },
        "2018": {
            "NonRes": 0.16328,
            "PhiPi": 0.14134,
            "KstK": 0.126309,
            "KPiPi": 0.141831,
            "PiPiPi": 0.141831,
        },
        "Fixed": True,
    }

    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "20152016": {
            "NonRes": 37.4012,
            "PhiPi": 39.6851,
            "KstK": 39.2369,
            "KPiPi": 33.3208,
            "PiPiPi": 33.3208,
        },
        "2017": {
            "NonRes": 33.8186,
            "PhiPi": 39.2647,
            "KstK": 35.7117,
            "KPiPi": 30.8107,
            "PiPiPi": 30.8107,
        },
        "2018": {
            "NonRes": 29.9745,
            "PhiPi": 39.897,
            "KstK": 34.7219,
            "KPiPi": 28.6863,
            "PiPiPi": 28.6863,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "20152016": {
            "NonRes": 14.1919,
            "PhiPi": 14.393,
            "KstK": 14.2233,
            "KPiPi": 14.5642,
            "PiPiPi": 14.5642,
        },
        "2017": {
            "NonRes": 13.6276,
            "PhiPi": 13.818,
            "KstK": 13.8814,
            "KPiPi": 13.9234,
            "PiPiPi": 13.9234,
        },
        "2018": {
            "NonRes": 13.4904,
            "PhiPi": 13.9978,
            "KstK": 13.7956,
            "KPiPi": 14.234,
            "PiPiPi": 14.234,
        },
        "Fixed": False,
    }

    # Ds signal shapes
    configdict["SignalShape"]["CharmMass"] = {}
    configdict["SignalShape"]["CharmMass"][
        "type"
    ] = "IpatiaJohnsonSU"  # "IpatiaPlusGaussian" #WithWidthRatio"
    configdict["SignalShape"]["CharmMass"]["mean"] = {
        "20152016": {"All": 1968.49},
        "2017": {"All": 1968.49},
        "2018": {"All": 1968.49},
        "Fixed": False,
    }
    configdict["SignalShape"]["CharmMass"]["a1"] = {
        "20152016": {
            "NonRes": 0.16053,
            "PhiPi": 0.18997,
            "KstK": 0.17253,
            "KPiPi": 0.183346,
            "PiPiPi": 0.183346,
        },
        "2017": {
            "NonRes": 0.12713,
            "PhiPi": 0.20276,
            "KstK": 0.14209,
            "KPiPi": 0.173038,
            "PiPiPi": 0.173038,
        },
        "2018": {
            "NonRes": 0.16241,
            "PhiPi": 0.16530,
            "KstK": 0.13652,
            "KPiPi": 0.184369,
            "PiPiPi": 0.184369,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["a2"] = {
        "20152016": {
            "NonRes": 0.39485,
            "PhiPi": 0.37501,
            "KstK": 0.37944,
            "KPiPi": 0.844259,
            "PiPiPi": 0.844259,
        },
        "2017": {
            "NonRes": 0.26094,
            "PhiPi": 0.49399,
            "KstK": 0.31042,
            "KPiPi": 0.805209,
            "PiPiPi": 0.805209,
        },
        "2018": {
            "NonRes": 0.37773,
            "PhiPi": 0.29843,
            "KstK": 0.27201,
            "KPiPi": 0.888122,
            "PiPiPi": 0.888122,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["tau"] = {
        "20152016": {
            "NonRes": 0.46214,
            "PhiPi": 0.48550,
            "KstK": 0.44602,
            "KPiPi": 0.380109,
            "PiPiPi": 0.380109,
        },
        "2017": {
            "NonRes": 0.45391,
            "PhiPi": 0.46851,
            "KstK": 0.45419,
            "KPiPi": 0.387104,
            "PiPiPi": 0.387104,
        },
        "2018": {
            "NonRes": 0.46527,
            "PhiPi": 0.49029,
            "KstK": 0.44849,
            "KPiPi": 0.387943,
            "PiPiPi": 0.387943,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["fracI"] = {
        "20152016": {
            "NonRes": 0.086438,
            "PhiPi": 0.12966,
            "KstK": 0.087039,
            "KPiPi": 0.312702,
            "PiPiPi": 0.312702,
        },
        "2017": {
            "NonRes": 0.080211,
            "PhiPi": 0.13619,
            "KstK": 0.067769,
            "KPiPi": 0.271536,
            "PiPiPi": 0.271536,
        },
        "2018": {
            "NonRes": 0.094782,
            "PhiPi": 0.12088,
            "KstK": 0.069336,
            "KPiPi": 0.308127,
            "PiPiPi": 0.308127,
        },
        "Fixed": True,
    }

    configdict["SignalShape"]["CharmMass"]["sigmaJ"] = {
        "20152016": {
            "NonRes": 6.4363,
            "PhiPi": 6.4202,
            "KstK": 6.5514,
            "KPiPi": 9.20648,
            "PiPiPi": 9.20648,
        },
        "2017": {
            "NonRes": 6.3217,
            "PhiPi": 6.2182,
            "KstK": 6.4968,
            "KPiPi": 9.03914,
            "PiPiPi": 9.03914,
        },
        "2018": {
            "NonRes": 6.3125,
            "PhiPi": 6.3379,
            "KstK": 6.4939,
            "KPiPi": 9.08361,
            "PiPiPi": 9.08361,
        },
        "Fixed": False,
    }

    configdict["SignalShape"]["CharmMass"]["sigmaI"] = {
        "20152016": {
            "NonRes": 29.915,
            "PhiPi": 26.734,
            "KstK": 26.271,
            "KPiPi": 30.4756,
            "PiPiPi": 30.4756,
        },
        "2017": {
            "NonRes": 26.660,
            "PhiPi": 27.198,
            "KstK": 26.868,
            "KPiPi": 30.3659,
            "PiPiPi": 30.3659,
        },
        "2018": {
            "NonRes": 27.776,
            "PhiPi": 24.312,
            "KstK": 24.853,
            "KPiPi": 29.3058,
            "PiPiPi": 29.3058,
        },
        "Fixed": True,
    }

    configdict["SignalShape"]["CharmMass"]["nu"] = {
        "Run2": {"All": 0.00},
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["fb"] = {
        "Run2": {"All": 0.00},
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["zeta"] = {
        "Run2": {"All": 0.00},
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["l"] = {"Run2": {"All": -1.1}, "Fixed": True}
    configdict["SignalShape"]["CharmMass"]["n1"] = {"Run2": {"All": 8.0}, "Fixed": True}
    configdict["SignalShape"]["CharmMass"]["n2"] = {"Run2": {"All": 8.0}, "Fixed": True}
    # combinatorial background
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
        "20152016": {
            "NonRes": -5.5365e-04,
            "PhiPi": -2.6327e-04,
            "KstK": -6.4593e-04,
            "KPiPi": -4.7510e-04,
            "PiPiPi": -4.7510e-04,
        },
        "2017": {
            "NonRes": -5.5365e-04,
            "PhiPi": -2.6327e-04,
            "KstK": -6.4593e-04,
            "KPiPi": -4.7510e-04,
            "PiPiPi": -4.7510e-04,
        },
        "2018": {
            "NonRes": -5.5365e-04,
            "PhiPi": -2.6327e-04,
            "KstK": -6.4593e-04,
            "KPiPi": -4.7510e-04,
            "PiPiPi": -4.7510e-04,
        },
        "Fixed": True,
    }
    configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
        "20152016": {
            "NonRes": -2.8299e-02,
            "PhiPi": -2.7999e-02,
            "KstK": -2.2682e-02,
            "KPiPi": -2.9265e-02,
            "PiPiPi": -2.9265e-02,
        },
        "2017": {
            "NonRes": -2.8299e-02,
            "PhiPi": -2.7999e-02,
            "KstK": -2.2682e-02,
            "KPiPi": -2.9265e-02,
            "PiPiPi": -2.9265e-02,
        },
        "2018": {
            "NonRes": -2.8299e-02,
            "PhiPi": -2.7999e-02,
            "KstK": -2.2682e-02,
            "KPiPi": -2.9265e-02,
            "PiPiPi": -2.9265e-02,
        },
        "Fixed": False,
    }

    configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
        "20152016": {
            "NonRes": 4.3067e-01,
            "PhiPi": 6.5400e-01,
            "KstK": 3.7409e-01,
            "KPiPi": 0.5,
            "PiPiPi": 0.5,
        },
        "2017": {
            "NonRes": 4.3067e-01,
            "PhiPi": 6.5400e-01,
            "KstK": 3.7409e-01,
            "KPiPi": 0.5,
            "PiPiPi": 0.5,
        },
        "2018": {
            "NonRes": 4.3067e-01,
            "PhiPi": 6.5400e-01,
            "KstK": 3.7409e-01,
            "KPiPi": 0.5,
            "PiPiPi": 0.5,
        },
        "Fixed": False,
    }

    configdict["CombBkgShape"]["CharmMass"] = {}
    configdict["CombBkgShape"]["CharmMass"]["type"] = "ExponentialPlusSignal"
    configdict["CombBkgShape"]["CharmMass"]["cD"] = {
        "20152016": {
            "NonRes": -5.7520e-03,
            "PhiPi": -5.7273e-03,
            "KstK": -8.3967e-03,
            "KPiPi": -4.9193e-03,
            "PiPiPi": -4.5455e-02,
        },
        "2017": {
            "NonRes": -5.7520e-03,
            "PhiPi": -5.7273e-03,
            "KstK": -8.3967e-03,
            "KPiPi": -4.9193e-03,
            "PiPiPi": -4.5455e-02,
        },
        "2018": {
            "NonRes": -5.7520e-03,
            "PhiPi": -5.7273e-03,
            "KstK": -8.3967e-03,
            "KPiPi": -4.9193e-03,
            "PiPiPi": -4.5455e-02,
        },
        "Fixed": False,
    }
    configdict["CombBkgShape"]["CharmMass"]["fracD"] = {
        "20152016": {
            "NonRes": 0.88620,
            "PhiPi": 0.37379,
            "KstK": 0.59093,
            "KPiPi": 0.5,
            "PiPiPi": 0.5,
        },
        "2017": {
            "NonRes": 0.88620,
            "PhiPi": 0.37379,
            "KstK": 0.59093,
            "KPiPi": 0.5,
            "PiPiPi": 0.5,
        },
        "2018": {
            "NonRes": 0.88620,
            "PhiPi": 0.37379,
            "KstK": 0.59093,
            "KPiPi": 0.5,
            "PiPiPi": 0.5,
        },
        "Fixed": False,
    }

    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Fixed": False,
    }
    configdict["AdditionalParameters"]["g1_f2_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Fixed": False,
    }
    configdict["AdditionalParameters"]["g1_f3_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Fixed": False,
    }
    configdict["AdditionalParameters"]["g2_f1_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.6, "Range": [0.5, 1.0]}}},
        "Fixed": False,
    }
    configdict["AdditionalParameters"]["g2_f2_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Fixed": False,
    }
    configdict["AdditionalParameters"]["g2_f3_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Fixed": False,
    }
    configdict["AdditionalParameters"]["g3_f1_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.75, "Range": [0.0, 1.0]}}},
        "Fixed": True,
    }
    configdict["AdditionalParameters"]["g5_f1_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.9, "Range": [0.8, 1.0]}}},
        "Fixed": False,
    }

    # Bd2Dsh background
    # shape for BeautyMass, for CharmMass as well as BacPIDK
    # taken by default the same as signal
    configdict["Bd2DsKShape"] = {}
    configdict["Bd2DsKShape"]["BeautyMass"] = {}
    if (
        configdict["SignalShape"]["BeautyMass"]["type"]
        == "IpatiaJohnsonSUWithWidthRatio"
    ):  # WithWidthRatio":
        configdict["Bd2DsKShape"]["BeautyMass"][
            "type"
        ] = "ShiftedSignalIpatiaJohnsonSUWithWidthRatio"
    else:
        configdict["Bd2DsKShape"]["BeautyMass"]["type"] = "ShiftedSignalIpatiaJohnsonSU"
    configdict["Bd2DsKShape"]["BeautyMass"]["shift"] = {
        "Run2": {"All": -86.8},
        "Fixed": True,
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["scale1"] = {
        "Run2": {"All": 1.00808721452},
        "Fixed": True,
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["scale2"] = {
        "Run2": {"All": 1.03868673310},
        "Fixed": True,
    }

    configdict["Yields"] = {}

    configdict["Yields"]["Bd2DK"] = {
        "2015": {
            "NonRes": 12.8,
            "PhiPi": 0.22,
            "KstK": 3.76,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "2016": {
            "NonRes": 54.56,
            "PhiPi": 1.02,
            "KstK": 15.8,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "2017": {
            "NonRes": 62.1,
            "PhiPi": 1.474,
            "KstK": 17.68,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "2018": {
            "NonRes": 74.84,
            "PhiPi": 1.439,
            "KstK": 21.29,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "Fixed": True,
    }

    configdict["Yields"]["Bd2DPi"] = {
        "2015": {
            "NonRes": 5.12,
            "PhiPi": 0.09,
            "KstK": 1.51,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "2016": {
            "NonRes": 21.824,
            "PhiPi": 0.42,
            "KstK": 6.32,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "2017": {
            "NonRes": 24.84,
            "PhiPi": 0.59,
            "KstK": 7.07,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "2018": {
            "NonRes": 29.936,
            "PhiPi": 0.57,
            "KstK": 8.5,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "Fixed": True,
    }

    configdict["Yields"]["Lb2LcK"] = {
        "2015": {
            "NonRes": 7.77,
            "PhiPi": 0.41,
            "KstK": 1.76,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "2016": {
            "NonRes": 44.32,
            "PhiPi": 4.15,
            "KstK": 9.49,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "2017": {
            "NonRes": 43.33,
            "PhiPi": 3.48,
            "KstK": 8.86,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "2018": {
            "NonRes": 51.00,
            "PhiPi": 4.96,
            "KstK": 10.62,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "Fixed": True,
    }

    configdict["Yields"]["Lb2LcPi"] = {
        "2015": {
            "NonRes": 3.08,
            "PhiPi": 0.16,
            "KstK": 0.7,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "2016": {
            "NonRes": 17.72,
            "PhiPi": 1.66,
            "KstK": 3.80,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "2017": {
            "NonRes": 17.32,
            "PhiPi": 1.39,
            "KstK": 3.55,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "2018": {
            "NonRes": 20.40,
            "PhiPi": 1.98,
            "KstK": 4.25,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "Fixed": True,
    }

    configdict["Yields"]["Bs2DsDsstKKst"] = {
        "2015": {
            "NonRes": 2000.0,
            "PhiPi": 2000.0,
            "KstK": 2000.0,
            "KPiPi": 1000.0,
            "PiPiPi": 1000.0,
        },
        "2016": {
            "NonRes": 4000.0,
            "PhiPi": 4000.0,
            "KstK": 4000.0,
            "KPiPi": 2000.0,
            "PiPiPi": 2000.0,
        },
        "2017": {
            "NonRes": 4000.0,
            "PhiPi": 4000.0,
            "KstK": 4000.0,
            "KPiPi": 2000.0,
            "PiPiPi": 2000.0,
        },
        "2018": {
            "NonRes": 4000.0,
            "PhiPi": 4000.0,
            "KstK": 4000.0,
            "KPiPi": 2000.0,
            "PiPiPi": 2000.0,
        },
        "Fixed": False,
    }

    configdict["Yields"]["BsLb2DsDsstPPiRho"] = {
        "2015": {
            "NonRes": 1000.0,
            "PhiPi": 1000.0,
            "KstK": 1000.0,
            "KPiPi": 200.0,
            "PiPiPi": 200.0,
        },
        "2016": {
            "NonRes": 2000.0,
            "PhiPi": 2000.0,
            "KstK": 2000.0,
            "KPiPi": 500.0,
            "PiPiPi": 500.0,
        },
        "2017": {
            "NonRes": 2000.0,
            "PhiPi": 2000.0,
            "KstK": 2000.0,
            "KPiPi": 500.0,
            "PiPiPi": 500.0,
        },
        "2018": {
            "NonRes": 2000.0,
            "PhiPi": 2000.0,
            "KstK": 2000.0,
            "KPiPi": 500.0,
            "PiPiPi": 500.0,
        },
        "Fixed": False,
    }
    configdict["Yields"]["CombBkg"] = {
        "2015": {
            "NonRes": 10000.0,
            "PhiPi": 20000.0,
            "KstK": 10000.0,
            "KPiPi": 10000.0,
            "PiPiPi": 10000.0,
        },
        "2016": {
            "NonRes": 20000.0,
            "PhiPi": 30000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0,
        },
        "2017": {
            "NonRes": 20000.0,
            "PhiPi": 30000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0,
        },
        "2018": {
            "NonRes": 20000.0,
            "PhiPi": 40000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0,
        },
        "Fixed": False,
    }
    configdict["Yields"]["Signal"] = {
        "2015": {
            "NonRes": 10000.0,
            "PhiPi": 20000.0,
            "KstK": 10000.0,
            "KPiPi": 10000.0,
            "PiPiPi": 10000.0,
        },
        "2016": {
            "NonRes": 20000.0,
            "PhiPi": 40000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0,
        },
        "2017": {
            "NonRes": 40000.0,
            "PhiPi": 40000.0,
            "KstK": 30000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0,
        },
        "2018": {
            "NonRes": 40000.0,
            "PhiPi": 60000.0,
            "KstK": 40000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0,
        },
        "Fixed": False,
    }

    # --------------------------------------------------------
    ###             MDfit plotting settings
    # --------------------------------------------------------
    from ROOT import kBlue, kGreen, kMagenta, kRed, kYellow

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = {
        "EPDF": [
            "Sig",
            "CombBkg",
            "Lb2LcK",
            "Lb2LcPi",
            "Bd2DK",
            "Bd2DPi",
            "BsLb2DsDsstPPiRho",
            "Bs2DsDsstKKst",
        ],
        "PDF": [
            "Sig",
            "CombBkg",
            "Lb2LcK",
            "Lb2LcPi",
            "Lb2DsDsstP",
            "Bs2DsDsstPiRho",
            "Bd2DK",
            "Bd2DPi",
            "Bs2DsDsstKKst",
        ],
        "Legend": [
            "Sig",
            "CombBkg",
            "Lb2LcKPi",
            "Lb2DsDsstP",
            "Bs2DsDsstPiRho",
            "Bd2DKPi",
            "Bs2DsDsstKKst",
        ],
    }
    configdict["PlotSettings"]["colors"] = {
        "PDF": [
            kRed - 7,
            kMagenta - 2,
            kGreen - 3,
            kGreen - 3,
            kYellow - 9,
            kBlue - 6,
            kRed,
            kRed,
            kBlue - 10,
        ],
        "Legend": [
            kRed - 7,
            kMagenta - 2,
            kGreen - 3,
            kYellow - 9,
            kBlue - 6,
            kRed,
            kBlue - 10,
        ],
    }

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.75, 0.9],
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.9],
        "ScaleYSize": 1.5,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }

    return configdict
