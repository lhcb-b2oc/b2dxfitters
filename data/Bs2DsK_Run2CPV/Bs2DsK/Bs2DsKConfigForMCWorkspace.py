###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def getconfig():
    from Bs2DsKConfigForNominalMassFit import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    version = "2015201620172018"
    WS_years = []
    for y in ["2015", "2016", "2017", "2018"]:
        if y in version:
            WS_years.append(y)

    configdict["YearOfDataTaking"] = set(WS_years)

    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotBs2DsK_Nominal", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5300, 5800],
        "InputName": "lab0_MassFitConsD_M",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1930, 2015],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0.4, 15.0],
        "InputName": "lab0_LifetimeFit_ctau",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [3000.0, 650000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [1.61, 5.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 500.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0.01, 0.1],
        "InputName": "lab0_LifetimeFit_ctauErr",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.475, 1.0],
        "InputName": "BDTGResponse_XGB_1",
    }

    vetoD0 = "lab34_MM < 1800."
    vetoKst = "(fabs(lab15_MM-892)>30.)"
    vetoD0_misID = "(fabs(sqrt(pow(sqrt(pow(139.6,2)+pow(lab1_P,2))+"
    "sqrt(pow(lab3_M,2)+pow(lab3_P,2)),2)-pow(lab1_PX+lab3_PX,2)-"
    "pow(lab1_PY+lab3_PY,2)-pow(lab1_PZ+lab3_PZ,2)) - 1864.83) > 30.)"

    vetoD0_kpipi = "lab34_MM < 1750. && (fabs(lab15_MM-1864.83)>30.)"
    vetoKst_kpipi = "lab15_MM>1000."

    vetoD_pipipi = "fabs(lab135_MM-1869.58)>30."
    vetoD0_pipipi = "lab34_MM<1700. && lab45_MM<1700. && "
    "fabs(lab13_MM-1864.83)>30. && fabs(lab15_MM-1864.83)>30."
    vetoKst_pipipi = "lab13_MM>1000. && lab15_MM>1000."

    lab2_FDCHI2_0 = "lab2_FDCHI2_ORIVX > 0"
    lab2_FDCHI2_9 = "lab2_FDCHI2_ORIVX > 9"

    t = "&&"

    # additional cuts applied to data sets
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "MC": "lab1_M>200",
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": True,
        "DsHypo": True,
    }
    configdict["AdditionalCuts"]["KKPi"] = {
        "MC": lab2_FDCHI2_0 + t + vetoD0 + t + vetoKst + t + vetoD0_misID
    }
    # KPiPi and PiPiPi Not used by workspace. Added for completeness
    configdict["AdditionalCuts"]["KPiPi"] = {
        "MC": lab2_FDCHI2_9 + t + vetoD0_kpipi + t + vetoKst_kpipi + t + vetoD0_misID
    }
    configdict["AdditionalCuts"]["PiPiPi"] = {
        "MC": lab2_FDCHI2_9 + t + vetoD_pipipi + t + vetoD0_pipipi + t + vetoKst_pipipi
    }

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    # order of particles: KKPi, KPiPi, PiPiPi
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    # weighting templates by PID eff/misID
    configdict["WeightingMassTemplates"] = {
        "PIDBachEff": {
            "2015": {
                "FileLabel": "#PIDK Kaon 2015",
                "Var": ["nTracks", "lab1_P"],
                "HistName": "K_DLLK > 5 && IsMuon ==0.0 && "
                "HasRich ==1.0 && DLLe < 5.0_All",
            },
            "2016": {
                "FileLabel": "#PIDK Kaon 2016",
                "Var": ["nTracks", "lab1_P"],
                "HistName": "K_DLLK > 5 && IsMuon ==0.0 && "
                "HasRich ==1.0 && DLLe < 5.0_All",
            },
            "2017": {
                "FileLabel": "#PIDK Kaon 2017",
                "Var": ["nTracks", "lab1_P"],
                "HistName": "K_DLLK > 5 && IsMuon ==0.0 && "
                "HasRich ==1.0 && DLLe < 5.0_All",
            },
            "2018": {
                "FileLabel": "#PIDK Kaon 2018",
                "Var": ["nTracks", "lab1_P"],
                "HistName": "K_DLLK > 5 && IsMuon ==0.0 && "
                "HasRich ==1.0 && DLLe < 5.0_All",
            },
        },
        "PIDBachMisID": {
            "2015": {
                "FileLabel": "#PIDK Pion 2015",
                "Var": ["nTracks", "lab1_P"],
                "HistName": "Pi_DLLK > 5 && IsMuon ==0.0 && "
                "HasRich ==1.0 && DLLe < 5.0_All",
            },
            "2016": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["nTracks", "lab1_P"],
                "HistName": "Pi_DLLK > 5 && IsMuon ==0.0 && "
                "HasRich ==1.0 && DLLe < 5.0_All",
            },
            "2017": {
                "FileLabel": "#PIDK Pion 2017",
                "Var": ["nTracks", "lab1_P"],
                "HistName": "Pi_DLLK > 5 && IsMuon ==0.0 && "
                "HasRich ==1.0 && DLLe < 5.0_All",
            },
            "2018": {
                "FileLabel": "#PIDK Pion 2018",
                "Var": ["nTracks", "lab1_P"],
                "HistName": "Pi_DLLK > 5 && IsMuon ==0.0 && "
                "HasRich ==1.0 && DLLe < 5.0_All",
            },
        },
        "PIDBachProtonMisID": {
            "2015": {
                "FileLabel": "#PIDK Proton 2015",
                "Var": ["nTracks", "lab1_P"],
                "HistName": "P_DLLK > 5 && IsMuon ==0.0 && "
                "HasRich ==1.0 && DLLe < 5.0_All",
            },
            "2016": {
                "FileLabel": "#PIDK Proton 2016",
                "Var": ["nTracks", "lab1_P"],
                "HistName": "P_DLLK > 5 && IsMuon ==0.0 && "
                "HasRich ==1.0 && DLLe < 5.0_All",
            },
            "2017": {
                "FileLabel": "#PIDK Proton 2017",
                "Var": ["nTracks", "lab1_P"],
                "HistName": "P_DLLK > 5 && IsMuon ==0.0 && "
                "HasRich ==1.0 && DLLe < 5.0_All",
            },
            "2018": {
                "FileLabel": "#PIDK Proton 2018",
                "Var": ["nTracks", "lab1_P"],
                "HistName": "P_DLLK > 5 && IsMuon ==0.0 && "
                "HasRich ==1.0 && DLLe < 5.0_All",
            },
        },
        "PIDChildKaonPionMisID": {
            "2015": {
                "FileLabel": "#PIDK Pion 2015",
                "Var": ["nTracks", "lab3_P"],
                "HistName": "Pi_DLLK>10 && DLLp <10 && "
                "IsMuon ==0.0 && HasRich ==1.0_All",
            },
            "2016": {
                "FileLabel": "#PIDK Pion 2016",
                "Var": ["nTracks", "lab3_P"],
                "HistName": "Pi_DLLK>10 && DLLp <10 && "
                "IsMuon ==0.0 && HasRich ==1.0_All",
            },
            "2017": {
                "FileLabel": "#PIDK Pion 2017",
                "Var": ["nTracks", "lab3_P"],
                "HistName": "Pi_DLLK>10 && DLLp <10 && "
                "IsMuon ==0.0 && HasRich ==1.0_All",
            },
            "2018": {
                "FileLabel": "#PIDK Pion 2018",
                "Var": ["nTracks", "lab3_P"],
                "HistName": "Pi_DLLK>10 && DLLp <10 && "
                "IsMuon ==0.0 && HasRich ==1.0_All",
            },
        },
        "PIDChildProtonMisID": {
            "2015": {
                "FileLabel": "#PIDK Proton 2015",
                "Var": ["nTracks", "lab3_P"],
                "HistName": "P_DLLK>5  && (DLLp - DLLK < -5) && "
                "IsMuon ==0.0 && HasRich ==1.0_All",
            },
            "2016": {
                "FileLabel": "#PIDK Proton 2016",
                "Var": ["nTracks", "lab3_P"],
                "HistName": "P_DLLK>5  && (DLLp - DLLK < -5) && "
                "IsMuon ==0.0 && HasRich ==1.0_All",
            },
            "2017": {
                "FileLabel": "#PIDK Proton 2017",
                "Var": ["nTracks", "lab3_P"],
                "HistName": "P_DLLK>5  && (DLLp - DLLK < -5) && "
                "IsMuon ==0.0 && HasRich ==1.0_All",
            },
            "2018": {
                "FileLabel": "#PIDK Proton 2018",
                "Var": ["nTracks", "lab3_P"],
                "HistName": "P_DLLK>5  && (DLLp - DLLK < -5) && "
                "IsMuon ==0.0 && HasRich ==1.0_All",
            },
        },
        "RatioDataMC": {
            "2015": {
                "FileLabel": "#DataMC 2015",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio",
            },
            "2016": {
                "FileLabel": "#DataMC 2016",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio",
            },
            "2017": {
                "FileLabel": "#DataMC 2017",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio",
            },
            "2018": {
                "FileLabel": "#DataMC 2018",
                "Var": ["lab0_ENDVERTEX_ZERR", "lab2_ENDVERTEX_ZERR"],
                "HistName": "histRatio",
            },
        },
        "Shift": {
            "2015": {"BeautyMass": -1.5, "CharmMass": 0.0},
            "2016": {"BeautyMass": -1.5, "CharmMass": 0.0},
            "2017": {"BeautyMass": -1.5, "CharmMass": 0.0},
            "2018": {"BeautyMass": -1.5, "CharmMass": 0.0},
        },
    }

    # ----------------------------------------------------------------------#
    ###                 MDfit fitting settings
    # ----------------------------------------------------------------------#

    # Bs signal shapes
    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "DoubleCrystalBallWithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {"All": 5367.51},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigma1"] = {
        "Run2": {
            "NonRes": 16.3671,
            "PhiPi": 16.291,
            "KstK": 16.1508,
            "KPiPi": 16.2347,
            "PiPiPi": 16.9128,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["sigma2"] = {
        "Run2": {
            "NonRes": 10.8735,
            "PhiPi": 10.9339,
            "KstK": 10.9438,
            "KPiPi": 10.9559,
            "PiPiPi": 11.1463,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["alpha1"] = {
        "Run2": {
            "NonRes": -2.25801,
            "PhiPi": -2.17009,
            "KstK": -2.28933,
            "KPiPi": -2.2631,
            "PiPiPi": -2.26345,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["alpha2"] = {
        "Run2": {
            "NonRes": 2.34818,
            "PhiPi": 2.06867,
            "KstK": 2.17721,
            "KPiPi": 2.14858,
            "PiPiPi": 2.01223,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {
            "NonRes": 2.82686,
            "PhiPi": 2.96533,
            "KstK": 3.09614,
            "KPiPi": 3.21419,
            "PiPiPi": 3.11023,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {
            "NonRes": 0.407317,
            "PhiPi": 0.886509,
            "KstK": 0.717488,
            "KPiPi": 0.688164,
            "PiPiPi": 0.887276,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["frac"] = {
        "Run2": {"NonRes": 0.5, "PhiPi": 0.5, "KstK": 0.5, "KPiPi": 0.5, "PiPiPi": 0.5},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "NonRes": 1.0663e00,
            "PhiPi": 1.1014e00,
            "Kstk": 1.0789e00,
            "KPiPi": 1.1124e00,
            "PiPiPi": 1.0607e00,
        },
        "Fixed": False,
    }

    # Ds signal shapes
    configdict["SignalShape"]["CharmMass"] = {}
    configdict["SignalShape"]["CharmMass"]["type"] = "DoubleCrystalBallWithWidthRatio"
    configdict["SignalShape"]["CharmMass"]["mean"] = {
        "Run2": {"All": 1968.49},
        "Fixed": False,
    }
    configdict["SignalShape"]["CharmMass"]["sigma1"] = {
        "Run2": {
            "NonRes": 5.28549,
            "PhiPi": 5.33497,
            "KstK": 5.73149,
            "KPiPi": 6.54633,
            "PiPiPi": 7.92704,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["sigma2"] = {
        "Run2": {
            "NonRes": 6.10816,
            "PhiPi": 5.7301,
            "KstK": 5.95999,
            "KPiPi": 7.61983,
            "PiPiPi": 8.68266,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["alpha1"] = {
        "Run2": {
            "NonRes": -1.19829,
            "PhiPi": -1.0928,
            "KstK": -1.23869,
            "KPiPi": -1.18576,
            "PiPiPi": -1.03718,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["alpha2"] = {
        "Run2": {
            "NonRes": 1.22329,
            "PhiPi": 1.15676,
            "KstK": 1.2273,
            "KPiPi": 1.10872,
            "PiPiPi": 0.815563,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["n1"] = {
        "Run2": {
            "NonRes": 8.17318,
            "PhiPi": 10.1772,
            "KstK": 10.1878,
            "KPiPi": 10.0,
            "PiPiPi": 19.9973,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["n2"] = {
        "Run2": {
            "NonRes": 5.23555,
            "PhiPi": 5.72422,
            "KstK": 6.20116,
            "KPiPi": 8.84269,
            "PiPiPi": 48.4227,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["frac"] = {
        "Run2": {
            "NonRes": 0.50,
            "PhiPi": 0.50,
            "KstK": 0.5,
            "KPiPi": 0.5,
            "PiPiPi": 0.5,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["R"] = {
        "Run2": {
            "NonRes": 1.0657e00,
            "PhiPi": 1.0562e00,
            "KstK": 1.0715e00,
            "KPiPi": 1.0365e00,
            "PiPiPi": 1.0837e00,
        },
        "Fixed": False,
    }

    # combinatorial background
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
        "Run2": {
            "NonRes": -8.5211e-03,
            "PhiPi": -5.0873e-03,
            "KstK": -8.3392e-03,
            "KPiPi": -5.0361e-03,
            "PiPiPi": -5.5277e-03,
        },
        "Fixed": False,
    }
    configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
        "Run2": {"NonRes": 0.0, "PhiPi": 0.0, "KstK": 0.0, "KPiPi": 0.0, "PiPiPi": 0.0},
        "Fixed": True,
    }
    configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
        "Run2": {
            "NonRes": 4.3067e-01,
            "PhiPi": 6.5400e-01,
            "KstK": 3.7409e-01,
            "KPiPi": 1.0,
            "PiPiPi": 1.0,
        },
        "Fixed": {"KPiPi": True, "PiPiPi": True},
    }
    configdict["CombBkgShape"]["CharmMass"] = {}
    configdict["CombBkgShape"]["CharmMass"]["type"] = "ExponentialPlusSignal"
    # "ExponentialPlusDoubleCrystalBallWithWidthRatioSharedMean"
    configdict["CombBkgShape"]["CharmMass"]["cD"] = {
        "Run2": {
            "NonRes": -4.4329e-03,
            "PhiPi": -8.8642e-03,
            "KstK": -5.2652e-03,
            "KPiPi": -5.0743e-03,
            "PiPiPi": -5.1877e-03,
        },
        "Fixed": False,
    }
    configdict["CombBkgShape"]["CharmMass"]["fracD"] = {
        "Run2": {
            "NonRes": 0.88620,
            "PhiPi": 0.37379,
            "KstK": 0.59093,
            "KPiPi": 0.5,
            "PiPiPi": 0.5,
        },
        "Fixed": False,
    }

    configdict["CombBkgShape"]["BacPIDK"] = {}
    configdict["CombBkgShape"]["BacPIDK"]["type"] = "FixedWithKaonPionProton"
    configdict["CombBkgShape"]["BacPIDK"]["fracPIDK1"] = {
        "Run2": {"NonRes": 0.9, "PhiPi": 0.9, "KstK": 0.9, "KPiPi": 0.8, "PiPiPi": 0.8},
        "Fixed": False,
    }
    configdict["CombBkgShape"]["BacPIDK"]["fracPIDK2"] = {
        "Run2": {"NonRes": 0.9, "PhiPi": 0.9, "KstK": 0.9, "KPiPi": 0.8, "PiPiPi": 0.8},
        "Fixed": False,
    }

    # Bd2Dsh background
    # shape for BeautyMass, for CharmMass as well as BacPIDK
    # taken by default the same as signal
    configdict["Bd2DsKShape"] = {}
    configdict["Bd2DsKShape"]["BeautyMass"] = {}
    configdict["Bd2DsKShape"]["BeautyMass"]["type"] = "ShiftedSignal"
    configdict["Bd2DsKShape"]["BeautyMass"]["shift"] = {
        "Run2": {"All": 86.8},
        "Fixed": True,
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["scale1"] = {
        "Run2": {"All": 0.998944636665},
        "Fixed": True,
    }
    configdict["Bd2DsKShape"]["BeautyMass"]["scale2"] = {
        "Run2": {"All": 1.00022181515},
        "Fixed": True,
    }

    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 1.0, "Range": [0.0, 1.0]}}},
        "Fixed": True,
    }
    configdict["AdditionalParameters"]["g2_f1_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Fixed": False,
    }
    configdict["AdditionalParameters"]["g2_f2_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Fixed": True,
    }
    configdict["AdditionalParameters"]["g3_f1_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.75, "Range": [0.0, 1.0]}}},
        "Fixed": True,
    }
    configdict["AdditionalParameters"]["g5_f1_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Fixed": False,
    }

    configdict["Yields"] = {}
    configdict["Yields"]["Bd2DPi"] = {
        "2015": {"NonRes": 1.7, "PhiPi": 0.1, "KstK": 0.3, "KPiPi": 0.0, "PiPiPi": 0.0},
        "2016": {"NonRes": 3.2, "PhiPi": 0.2, "KstK": 0.7, "KPiPi": 0.0, "PiPiPi": 0.0},
        "Fixed": True,
    }
    configdict["Yields"]["Bd2DK"] = {
        "2015": {"NonRes": 3.9, "PhiPi": 0.3, "KstK": 0.8, "KPiPi": 0.0, "PiPiPi": 0.0},
        "2016": {"NonRes": 7.6, "PhiPi": 0.6, "KstK": 1.7, "KPiPi": 0.0, "PiPiPi": 0.0},
        "Fixed": True,
    }
    configdict["Yields"]["Lb2LcPi"] = {
        "2015": {"NonRes": 4.9, "PhiPi": 1.1, "KstK": 1.6, "KPiPi": 0.0, "PiPiPi": 0.0},
        "2016": {
            "NonRes": 10.7,
            "PhiPi": 2.0,
            "KstK": 3.4,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "Fixed": True,
    }
    configdict["Yields"]["Lb2LcK"] = {
        "2015": {
            "NonRes": 10.9,
            "PhiPi": 2.4,
            "KstK": 3.6,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "2016": {
            "NonRes": 26.7,
            "PhiPi": 5.0,
            "KstK": 8.5,
            "KPiPi": 0.0,
            "PiPiPi": 0.0,
        },
        "Fixed": True,
    }

    configdict["Yields"]["Bs2DsDsstKKst"] = {
        "2015": {
            "NonRes": 500.0,
            "PhiPi": 500.0,
            "KstK": 500.0,
            "KPiPi": 500.0,
            "PiPiPi": 500.0,
        },
        "2016": {
            "NonRes": 1000.0,
            "PhiPi": 1000.0,
            "KstK": 1000.0,
            "KPiPi": 1000.0,
            "PiPiPi": 1000.0,
        },
        "Fixed": False,
    }
    configdict["Yields"]["BsLb2DsDsstPPiRho"] = {
        "2015": {
            "NonRes": 2250.0,
            "PhiPi": 5000.0,
            "KstK": 3300.0,
            "KPiPi": 900.0,
            "PiPiPi": 2600.0,
        },
        "2016": {
            "NonRes": 4500.0,
            "PhiPi": 10000.0,
            "KstK": 6600.0,
            "KPiPi": 1800.0,
            "PiPiPi": 2600.0,
        },
        "Fixed": False,
    }
    configdict["Yields"]["CombBkg"] = {
        "2015": {
            "NonRes": 10000.0,
            "PhiPi": 10000.0,
            "KstK": 10000.0,
            "KPiPi": 10000.0,
            "PiPiPi": 10000.0,
        },
        "2016": {
            "NonRes": 20000.0,
            "PhiPi": 20000.0,
            "KstK": 20000.0,
            "KPiPi": 20000.0,
            "PiPiPi": 20000.0,
        },
        "Fixed": False,
    }
    configdict["Yields"]["Signal"] = {
        "2015": {
            "NonRes": 1000.0,
            "PhiPi": 1000.0,
            "KstK": 1000.0,
            "KPiPi": 1000.0,
            "PiPiPi": 1000.0,
        },
        "2016": {
            "NonRes": 2000.0,
            "PhiPi": 2000.0,
            "KstK": 2000.0,
            "KPiPi": 2000.0,
            "PiPiPi": 2000.0,
        },
        "Fixed": False,
    }

    # ----------------------------------------------------------------------#
    ###                 MDfit plotting settings
    # ----------------------------------------------------------------------#

    from ROOT import kBlue, kGreen, kMagenta, kRed, kYellow

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = {
        "EPDF": [
            "Sig",
            "CombBkg",
            "Lb2LcK",
            "Lb2LcPi",
            "Bd2DK",
            "Bd2DPi",
            "BsLb2DsDsstPPiRho",
            "Bs2DsDsstKKst",
        ],
        "PDF": [
            "Sig",
            "CombBkg",
            "Lb2LcK",
            "Lb2LcPi",
            "Lb2DsDsstP",
            "Bs2DsDsstPiRho",
            "Bd2DK",
            "Bd2DPi",
            "Bs2DsDsstKKst",
        ],
        "Legend": [
            "Sig",
            "CombBkg",
            "Lb2LcKPi",
            "Lb2DsDsstP",
            "Bs2DsDsstPiRho",
            "Bd2DKPi",
            "Bs2DsDsstKKst",
        ],
    }
    configdict["PlotSettings"]["colors"] = {
        "PDF": [
            kRed - 7,
            kMagenta - 2,
            kGreen - 3,
            kGreen - 3,
            kYellow - 9,
            kBlue - 6,
            kRed,
            kRed,
            kBlue - 10,
        ],
        "Legend": [
            kRed - 7,
            kMagenta - 2,
            kGreen - 3,
            kYellow - 9,
            kBlue - 6,
            kRed,
            kBlue - 10,
        ],
    }

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.35, 0.9],
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.5,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.5,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }

    return configdict
