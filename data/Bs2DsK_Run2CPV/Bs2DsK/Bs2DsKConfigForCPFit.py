###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import itertools
import os
import sys
from math import pi

import Bs2DsKConfigForNominalMassFit


def getconfig():
    configdict = Bs2DsKConfigForNominalMassFit.getconfig()

    # PHYSICAL PARAMETERS
    configdict["Gammas"] = 0.6600  # in ps^{-1}
    configdict["DeltaGammas"] = 0.085
    configdict["DeltaMs"] = 17.7683  # in ps^{-1}
    configdict["Labels"] = ["20152016", "2017", "2018"]
    configdict["StrongPhase"] = 20.0 / 180.0 * pi
    configdict["WeakPhase"] = 70.0 / 180.0 * pi
    configdict["ModLf"] = 0.372
    configdict["CPlimit"] = {"upper": 4.0, "lower": -4.0}

    configdict["Asymmetries"] = {"Detection": 0.0, "Production": 0.0}

    configdict["ConstrainsForTaggingCalib"] = False

    configdict["SWeightCorrection"] = "Read"

    configdict["Resolution"] = {
        "20152016": {
            "scaleFactor": {"p0": 0.008377, "p1": 1.002, "p2": 0.0},
            "meanBias": -0.002254,
        },
        "2017": {
            "scaleFactor": {"p0": 0.006092, "p1": 1.048, "p2": 0.0},
            "meanBias": -0.003047,
        },
        "2018": {
            "scaleFactor": {"p0": 0.0055521, "p1": 1.052, "p2": 0.0},
            "meanBias": -0.002394,
        },
    }

    configdict["TaggingCalibration"] = {
        "OS": {
            "20152016": {
                "p0": 3.8483e-01,
                "dp0": 7.7974e-03,
                "p1": 9.8998e-01,
                "dp1": -8.3170e-04,
                "average": 0.3562,
                "tagEff": 0.4126,
                "aTagEff": 6.6836e-03,
                "use": True,
            },
            "2017": {
                "p0": 3.7631e-01,
                "dp0": 4.1433e-03,
                "p1": 8.8059e-01,
                "dp1": 7.2437e-02,
                "average": 0.3463,
                "tagEff": 0.4084,
                "aTagEff": 3.0749e-03,
                "use": True,
            },
            "2018": {
                "p0": 3.7444e-01,
                "dp0": 1.1832e-02,
                "p1": 8.8212e-01,
                "dp1": 2.4240e-02,
                "average": 0.3464,
                "tagEff": 0.4123,
                "aTagEff": -3.1091e-03,
                "use": True,
            },
        },
        "SS": {
            "20152016": {
                "p0": 4.3452e-01,
                "dp0": -1.6347e-02,
                "p1": 7.4698e-01,
                "dp1": 6.9697e-03,
                "average": 0.4162,
                "tagEff": 0.6918,
                "aTagEff": -1.3327e-03,
                "use": True,
            },
            "2017": {
                "p0": 4.3728e-01,
                "dp0": -2.2045e-02,
                "p1": 7.0713e-01,
                "dp1": 5.8384e-02,
                "average": 0.4164,
                "tagEff": 0.6992,
                "aTagEff": 1.7918e-03,
                "use": True,
            },
            "2018": {
                "p0": 4.3734e-01,
                "dp0": -1.2254e-02,
                "p1": 7.8264e-01,
                "dp1": 4.9329e-02,
                "average": 0.4156,
                "tagEff": 0.6973,
                "aTagEff": -7.4611e-03,
                "use": True,
            },
        },
    }

    configdict["Acceptance"] = {
        "knots": [0.50, 1.0, 1.5, 2.0, 3.0, 12.0],
        "20152016": {
            "values": [2.91e-01, 4.42e-01, 7.58e-01, 9.26e-01, 1.136e00, 1.073e00]
        },
        "2017": {
            "values": [3.05e-01, 4.12e-01, 7.59e-01, 8.13e-01, 9.72e-01, 1.120e00]
        },
        "2018": {
            "values": [3.29e-01, 4.59e-01, 7.56e-01, 8.92e-01, 1.001e00, 1.195e00]
        },
    }

    configdict["constParams"] = [
        "Gammas_Bs2DsK",
        "deltaGammas_Bs2DsK",
        "DeltaMs_Bs2DsK",
        "tagEff_OS",
        "tagEff_SS",
        "average_OS",
        "average_SS",
        "var1_20152016",
        "var1_2017",
        "var1_2018",
        "var2_20152016",
        "var2_2017",
        "var2_2018",
        "var3_20152016",
        "var3_2017",
        "var3_2018",
        "var4_20152016",
        "var4_2017",
        "var4_2018",
        "var5_20152016",
        "var5_2017",
        "var5_2018",
        "var6_20152016",
        "var6_2017",
        "var6_2018",
        "dp0_OS_20152016",
        "dp0_OS_2017",
        "dp0_OS_2018",
        "dp0_SS_20152016",
        "dp0_SS_2017",
        "dp0_SS_2018",
        "dp1_OS_20152016",
        "dp1_OS_2017",
        "dp1_OS_2018",
        "dp1_SS_20152016",
        "dp1_SS_2017",
        "dp1_SS_2018",
        "p0_OS_20152016",
        "p0_OS_2017",
        "p0_OS_2018",
        "p0_SS_20152016",
        "p0_SS_2017",
        "p0_SS_2018",
        "p1_OS_20152016",
        "p1_OS_2017",
        "p1_OS_2018",
        "p1_SS_20152016",
        "p1_SS_2017",
        "p1_SS_2018",
        "aTagEff_OS_20152016",
        "aTagEff_OS_2017",
        "aTagEff_OS_2018",
        "aTagEff_SS_20152016",
        "aTagEff_SS_2017",
        "aTagEff_SS_2018",
        "aprod",
    ]

    # Add some name variants
    configdict["constParams"] += [
        c + "_" + label
        for c, label in itertools.product(
            configdict["constParams"], configdict["Labels"]
        )
    ]

    return configdict


if __name__ == "__main__":
    sys.path.insert(0, os.path.join(os.path.dirname(__file__), "Toys"))
    from configutils import main

    main(getconfig())
