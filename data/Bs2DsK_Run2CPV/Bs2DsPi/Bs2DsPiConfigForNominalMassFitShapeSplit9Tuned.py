###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Bs2DsPi"
    configdict["CharmModes"] = {"NonRes", "PhiPi", "KstK", "PiPiPi"}
    configdict["Backgrounds"] = [
        "Bd2DPi",
        "Lb2LcPi",
        "Bs2DsRho",
        "Bs2DsstPi",
        "Bd2DsPi",
        "Bs2DsK",
    ]
    # year of data taking
    configdict["YearOfDataTaking"] = {
        "2015",
        "2016",
        "2017",
        "2018",
    }  # ,"2016"} #, "2017","2018"} #,"2017","2018"}
    # stripping (necessary in case of PIDK shapes)
    configdict["Stripping"] = {"2012": "21", "2011": "21r1"}
    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {  # "2011": {"Down": 0.56,   "Up": 0.42},
        # "2012": {"Down": 0.9912, "Up": 0.9988},
        "2015": {"Down": 0.18695, "Up": 0.14105},
        "2016": {"Down": 0.85996, "Up": 0.80504},
        "2017": {"Down": 0.87689, "Up": 0.83311},
        "2018": {"Down": 1.04846, "Up": 1.14154},
    }
    # file name with paths to MC/data samples
    configdict["dataName"] = "../data/Bs2DsK_Run2CPV/Bs2DsPi/config_Bs2DsPi.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {
        "Directory": "PlotBs2DsPi_Nominal",
        "Extension": "pdf",
    }

    configdict["MoreVariables"] = True

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5300, 5800],
        "InputName": "lab0_MassFitConsD_M",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1930, 2015],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0.4, 15.0],
        "InputName": "lab0_LifetimeFit_ctau",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [3000.0, 650000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0.01, 0.1],
        "InputName": "lab0_LifetimeFit_ctauErr",
    }
    configdict["BasicVariables"]["BacCharge"] = {
        "Range": [-1000.0, 1000.0],
        "InputName": "lab1_ID",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.475, 1.0],
        "InputName": "BDTGResponse_XGB_1",
    }
    configdict["BasicVariables"]["TagDecOS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "OS_Combination_DEC",
    }
    configdict["BasicVariables"]["TagDecSS"] = {
        "Range": [-1.0, 1.0],
        "InputName": "lab0_SSKaonLatest_TAGDEC",
    }
    configdict["BasicVariables"]["MistagOS"] = {
        "Range": [-3.0, 1.0],
        "InputName": "OS_Combination_ETA",
    }
    configdict["BasicVariables"]["MistagSS"] = {
        "Range": [-3.0, 1.0],
        "InputName": "lab0_SSKaonLatest_TAGETA",
    }

    # tagging calibration
    # configdict["TaggingCalibration"] = {}
    # configdict["TaggingCalibration"]["OS"] = {
    #   "p0": 0.375,  "p1": 0.982, "average": 0.3688}
    # configdict["TaggingCalibration"]["SS"] = {
    #   "p0": 0.4429, "p1": 0.977, "average": 0.4377}

    "(lab0_Hlt2IncPhiDecision_TOS ==1 "
    "|| lab0_Hlt2Topo2BodyBBDTDecision_TOS == 1 || "
    "lab0_Hlt2Topo3BodyBBDTDecision_TOS == 1))"
    # additional cuts applied to data sets
    #   configdict["AdditionalCuts"] = {}
    #   configdict["AdditionalCuts"]["All"]    = {
    #   "Data": "lab1_isMuon==0", # && lab5_PIDp<10",
    #   "MC"  : "lab1_M<200&&lab1_PIDK !=-1000.0&&lab2_FD_ORIVX > 0.&&"
    #       "(lab0_LifetimeFit_Dplus_ctau[0]>0)",
    #   "MCID":True, "MCTRUEID":True, "BKGCAT":True, "DsHypo":True}

    #   thigh = "lab3_PIDK<2.0 &&lab4_PIDK < 2. && lab5_PIDK < 2.&&
    #   lab3_PIDp < 5. && lab4_PIDp < 5. && lab5_PIDp < 5."
    #   lab13D0 = "&&fabs(sqrt(pow(sqrt(pow(493.7,2)+pow(lab1_P,2))
    #   +sqrt(pow(lab3_M,2)+pow(lab3_P,2)),2)-pow(lab1_PX+lab3_PX,2)
    #   -pow(lab1_PY+lab3_PY,2)-pow(lab1_PZ+lab3_PZ,2))-1870)>20.0";
    #   lab15D0 = "&&fabs(sqrt(pow(sqrt(pow(493.7,2)+pow(lab1_P,2))
    #   +sqrt(pow(lab5_M,2)+pow(lab5_P,2)),2)-pow(lab1_PX+lab5_PX,2)
    #   -pow(lab1_PY+lab5_PY,2)-pow(lab1_PZ+lab5_PZ,2))-1870)>20.0"

    #    configdict["AdditionalCuts"]["PiPiPi"]   = {
    #   "Data": "lab2_FDCHI2_ORIVX > 2", "MC" : "lab2_FDCHI2_ORIVX > 2"}
    #   configdict["AdditionalCuts"]["PiPiPi"]   = {
    #   "Data": thigh+lab13D0+lab15D0, "MC" : ""} #lab2_FDCHI2_ORIVX > 2"}
    #   configdict["AdditionalCuts"]["PiPiPi"] = {
    #   "Data": "lab2_FDCHI2_ORIVX > 9", "MC" : "lab2_FDCHI2_ORIVX > 9"}

    # children prefixes used in MCID, MCTRUEID, BKGCAT cuts
    configdict["DsChildrenPrefix"] = {
        "Child1": "lab3",
        "Child2": "lab4",
        "Child3": "lab5",
    }

    # additional variables in data sets
    if configdict["MoreVariables"] is True:
        configdict["AdditionalVariables"] = {}
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSCharm_TAGDEC",
        }
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSElectronLatest_TAGDEC",
        }
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSKaonLatest_TAGDEC",
        }
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSMuonLatest_TAGDEC",
        }
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_OSVtxCh_TAGDEC",
        }
        configdict["AdditionalVariables"]["lab0_SSPion_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_SSPion_TAGDEC",
        }
        configdict["AdditionalVariables"]["lab0_SSProton_TAGDEC"] = {
            "Range": [-2.0, 2.0],
            "InputName": "lab0_SSProton_TAGDEC",
        }
        configdict["AdditionalVariables"]["lab0_OSCharm_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSCharm_TAGETA",
        }
        configdict["AdditionalVariables"]["lab0_OSElectronLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSElectronLatest_TAGETA",
        }
        configdict["AdditionalVariables"]["lab0_OSKaonLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSKaonLatest_TAGETA",
        }
        configdict["AdditionalVariables"]["lab0_OSMuonLatest_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSMuonLatest_TAGETA",
        }
        configdict["AdditionalVariables"]["lab0_OSVtxCh_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_OSVtxCh_TAGETA",
        }
        configdict["AdditionalVariables"]["lab0_SSPion_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_SSPion_TAGETA",
        }
        configdict["AdditionalVariables"]["lab0_SSProton_TAGETA"] = {
            "Range": [-3.0, 1.0],
            "InputName": "lab0_SSProton_TAGETA",
        }
        configdict["AdditionalVariables"]["lab0_P"] = {
            "Range": [0.0, 1600000.0],
            "InputName": "lab0_P",
        }
        configdict["AdditionalVariables"]["lab0_PT"] = {
            "Range": [0.0, 40000.0],
            "InputName": "lab0_PT",
        }
        configdict["AdditionalVariables"]["lab0_ETA"] = {
            "Range": [0.0, 6.0],
            "InputName": "lab0_ETA",
        }
        configdict["AdditionalVariables"]["nCandidate"] = {
            "Range": [0.0, 500.0],
            "InputName": "nCandidate",
        }
        configdict["AdditionalVariables"]["totCandidates"] = {
            "Range": [0.0, 500.0],
            "InputName": "totCandidates",
        }
        configdict["AdditionalVariables"]["runNumber"] = {
            "Range": [0.0, 1000000.0],
            "InputName": "runNumber",
        }
        configdict["AdditionalVariables"]["eventNumber"] = {
            "Range": [0.0, 10000000000.0],
            "InputName": "eventNumber",
        }
        configdict["AdditionalVariables"]["BDTGResponse_3"] = {
            "Range": [-1.0, 1.0],
            "InputName": "BDTGResponse_3",
        }
        configdict["AdditionalVariables"]["nSPDHits"] = {
            "Range": [0.0, 1400.0],
            "InputName": "nSPDHits",
        }
        configdict["AdditionalVariables"]["nTracksLinear"] = {
            "Range": [15.0, 1000.0],
            "InputName": "clone_nTracks",
        }
        configdict["AdditionalVariables"]["lab2_IPCHI2_OWNPV"] = {
            "Range": [0.0, 800000.0],
            "InputName": "lab2_IPCHI2_OWNPV",
        }
        configdict["AdditionalVariables"]["lab2_FDCHI2_OWNPV"] = {
            "Range": [0.0, 8000000.0],
            "InputName": "lab2_FDCHI2_OWNPV",
        }
        configdict["AdditionalVariables"]["lab2_FDCHI2_ORIVX"] = {
            "Range": [0.0, 800000.0],
            "InputName": "lab2_FDCHI2_ORIVX",
        }
        configdict["AdditionalVariables"]["lab2_MINIPCHI2"] = {
            "Range": [0.0, 200000.0],
            "InputName": "lab2_MINIPCHI2",
        }
        configdict["AdditionalVariables"]["lab0_LifetimeFit_Dplus_ctau"] = {
            "Range": [-1000000.0, 60000.0],
            "InputName": "lab0_LifetimeFit_Dplus_ctau",
        }
        configdict["AdditionalVariables"]["lab2_P"] = {
            "Range": [0.0, 6000000.0],
            "InputName": "lab2_P",
        }
        configdict["AdditionalVariables"]["lab2_PT"] = {
            "Range": [0.0, 500000.0],
            "InputName": "lab2_PT",
        }
        configdict["AdditionalVariables"]["lab3_P"] = {
            "Range": [0.0, 6000000.0],
            "InputName": "lab3_P",
        }
        configdict["AdditionalVariables"]["lab3_PT"] = {
            "Range": [0.0, 500000.0],
            "InputName": "lab3_PT",
        }
        configdict["AdditionalVariables"]["lab4_P"] = {
            "Range": [0.0, 6000000.0],
            "InputName": "lab4_P",
        }
        configdict["AdditionalVariables"]["lab4_PT"] = {
            "Range": [0.0, 500000.0],
            "InputName": "lab4_PT",
        }
        configdict["AdditionalVariables"]["lab5_P"] = {
            "Range": [0.0, 6000000.0],
            "InputName": "lab5_P",
        }
        configdict["AdditionalVariables"]["lab5_PT"] = {
            "Range": [0.0, 500000.0],
            "InputName": "lab5_PT",
        }
        configdict["AdditionalVariables"]["lab34_MM"] = {
            "Range": [0.0, 1900.0],
            "InputName": "lab34_MM",
        }
        configdict["AdditionalVariables"]["lab45_MM"] = {
            "Range": [0.0, 1900.0],
            "InputName": "lab45_MM",
        }
        configdict["AdditionalVariables"]["lab2_MassHypo_KKPi_Lambda"] = {
            "Range": [0.0, 5000.0],
            "InputName": "lab2_MassHypo_KKPi_Lambda",
        }
        configdict["AdditionalVariables"]["lab2_MassHypo_KKPi_D"] = {
            "Range": [0.0, 5000.0],
            "InputName": "lab2_MassHypo_KKPi_D",
        }

        configdict["AdditionalVariables"]["lab0_ENDVERTEX_X"] = {
            "Range": [-300.0, 200.0],
            "InputName": "lab0_ENDVERTEX_X",
        }
        configdict["AdditionalVariables"]["lab0_ENDVERTEX_Y"] = {
            "Range": [-300.0, 200.0],
            "InputName": "lab0_ENDVERTEX_Y",
        }
        configdict["AdditionalVariables"]["lab2_ENDVERTEX_Y"] = {
            "Range": [-300.0, 200.0],
            "InputName": "lab2_ENDVERTEX_Y",
        }
        configdict["AdditionalVariables"]["lab2_ENDVERTEX_Y"] = {
            "Range": [-300.0, 200.0],
            "InputName": "lab2_ENDVERTEX_Y",
        }

        configdict["AdditionalVariables"]["lab0_OWNPV_X"] = {
            "Range": [-5.0, 5.0],
            "InputName": "lab0_OWNPV_X",
        }
        configdict["AdditionalVariables"]["lab0_OWNPV_Y"] = {
            "Range": [-5.0, 5.0],
            "InputName": "lab0_OWNPV_Y",
        }
        configdict["AdditionalVariables"]["lab2_OWNPV_X"] = {
            "Range": [-5.0, 5.0],
            "InputName": "lab2_OWNPV_X",
        }
        configdict["AdditionalVariables"]["lab2_OWNPV_Y"] = {
            "Range": [-5.0, 5.0],
            "InputName": "lab2_OWNPV_Y",
        }

        configdict["AdditionalVariables"]["lab0_ENDVERTEX_ZERR"] = {
            "Range": [0.0, 2500.0],
            "InputName": "lab0_ENDVERTEX_ZERR",
        }
        configdict["AdditionalVariables"]["lab2_ENDVERTEX_ZERR"] = {
            "Range": [0.0, 3000.0],
            "InputName": "lab2_ENDVERTEX_ZERR",
        }
        configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
            "Range": [0.0, 30.0],
            "InputName": "lab0_ENDVERTEX_CHI2",
        }
        configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
            "Range": [0.0, 30.0],
            "InputName": "lab2_ENDVERTEX_CHI2",
        }

        configdict["AdditionalVariables"]["lab0_OWNPV_CHI2"] = {
            "Range": [-300.0, 300.0],
            "InputName": "lab0_OWNPV_CHI2",
        }
        configdict["AdditionalVariables"]["lab0_IPCHI2_OWNPV"] = {
            "Range": [0.0, 300000.0],
            "InputName": "lab0_IPCHI2_OWNPV",
        }
        configdict["AdditionalVariables"]["lab0_IP_OWNPV"] = {
            "Range": [0.0, 7.0],
            "InputName": "lab0_IP_OWNPV",
        }
        configdict["AdditionalVariables"]["lab0_FD_OWNPV"] = {
            "Range": [0.0, 2500.0],
            "InputName": "lab0_FD_OWNPV",
        }
        configdict["AdditionalVariables"]["lab0_FDCHI2_OWNPV"] = {
            "Range": [-100000.0, 30000000.0],
            "InputName": "lab0_FDCHI2_OWNPV",
        }

        configdict["AdditionalVariables"]["lab1_IPCHI2_OWNPV"] = {
            "Range": [0.0, 900000.0],
            "InputName": "lab1_IPCHI2_OWNPV",
        }
        configdict["AdditionalVariables"]["lab3_IPCHI2_OWNPV"] = {
            "Range": [0.0, 900000.0],
            "InputName": "lab3_IPCHI2_OWNPV",
        }
        configdict["AdditionalVariables"]["lab4_IPCHI2_OWNPV"] = {
            "Range": [0.0, 900000.0],
            "InputName": "lab4_IPCHI2_OWNPV",
        }
        configdict["AdditionalVariables"]["lab5_IPCHI2_OWNPV"] = {
            "Range": [0.0, 900000.0],
            "InputName": "lab5_IPCHI2_OWNPV",
        }

        configdict["AdditionalVariables"]["lab3_IP_OWNPV"] = {
            "Range": [0.0, 200.0],
            "InputName": "lab3_IP_OWNPV",
        }
        configdict["AdditionalVariables"]["lab4_IP_OWNPV"] = {
            "Range": [0.0, 200.0],
            "InputName": "lab4_IP_OWNPV",
        }
        configdict["AdditionalVariables"]["lab5_IP_OWNPV"] = {
            "Range": [0.0, 200.0],
            "InputName": "lab5_IP_OWNPV",
        }

        configdict["AdditionalVariables"]["lab0_LifetimeFit_chi2"] = {
            "Range": [0.0, 5000000.0],
            "InputName": "lab0_LifetimeFit_chi2",
        }

        configdict["AdditionalVariables"]["lab1_TRACK_GhostProb"] = {
            "Range": [0.0, 0.4],
            "InputName": "lab1_TRACK_GhostProb",
        }
        configdict["AdditionalVariables"]["lab3_TRACK_GhostProb"] = {
            "Range": [0.0, 0.4],
            "InputName": "lab3_TRACK_GhostProb",
        }
        configdict["AdditionalVariables"]["lab4_TRACK_GhostProb"] = {
            "Range": [0.0, 0.4],
            "InputName": "lab4_TRACK_GhostProb",
        }
        configdict["AdditionalVariables"]["lab5_TRACK_GhostProb"] = {
            "Range": [0.0, 0.4],
            "InputName": "lab5_TRACK_GhostProb",
        }
    # -------------------------------------------------------
    ###             MDfit fitting settings
    # -------------------------------------------------------

    # Bs signal shapes
    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"][
        "type"
    ] = "IpatiaJohnsonSU"  # WithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {"All": 5367.51},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "20152016": {"All": 5367.51},
        "2017": {"All": 5367.51},
        "2018": {"All": 5367.51},
        "Fixed": False,
    }
    # configdict["SignalShape"]["BeautyMass"]["sigmaI"]  = {
    # "Run2": {"All" : 21.259},   "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["sigmaJ"]  = {
    # "Run2": {"All" : 14.582},   "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["a1"]      = {
    # "Run2": {"All" : 1.0886},   "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["a2"]      = {
    # "Run2": {"All" : 1.4644},   "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["n1"]      = {
    # "Run2": {"All" : 1.4891},   "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["n2"]      = {
    # "Run2": {"All" : 4.2422},   "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["fb"]      = {
    # "Run2": {"All" : 0.00000},   "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["zeta"]    = {
    # "Run2": {"All" : 0.00000},   "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["l"]       = {
    # "Run2": {"All" : -2.4782},   "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["nu"]      = {
    # "Run2": {"All" : -0.23173}, "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["tau"]     = {
    # "Run2": {"All" : 0.38464},  "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["fracI"]   = {
    # "Run2": {"All" : 0.26563},  "Fixed" : True}

    # configdict["SignalShape"]["BeautyMass"]["sigmaI"]  = {
    # "Run2": {"All" : 31.886},     "Fixed": True}
    # configdict["SignalShape"]["BeautyMass"]["sigmaJ"]  = {
    # "Run2": {"All" : 14.0607},     "Fixed": True}
    # configdict["SignalShape"]["BeautyMass"]["a1"]      = {
    # "Run2": {"All" : 0.464337},    "Fixed": True}
    # configdict["SignalShape"]["BeautyMass"]["a2"]      = {
    # "Run2": {"All" : 3.03669},     "Fixed": True}
    # configdict["SignalShape"]["BeautyMass"]["n1"]      = {
    # "Run2": {"All" : 2.56054},     "Fixed": True}
    # configdict["SignalShape"]["BeautyMass"]["n2"]      = {
    # "Run2": {"All" : 1.24849},     "Fixed": True}
    # configdict["SignalShape"]["BeautyMass"]["fb"]      = {
    # "Run2": {"All" : 0.00000},   "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["zeta"]    = {
    # "Run2": {"All" : 0.00000},   "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["l"]       = {
    # "Run2": {"All" : -1.41107},   "Fixed" : True}
    # configdict["SignalShape"]["BeautyMass"]["nu"]      = {
    # "Run2": {"All" : -0.212432}, "Fixed": True}
    # configdict["SignalShape"]["BeautyMass"]["tau"]     = {
    # "Run2": {"All" : 0.331957}, "Fixed": True}

    # PIDCorr
    configdict["SignalShape"]["BeautyMass"]["fb"] = {
        "Run2": {"All": 0.00000},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["zeta"] = {
        "Run2": {"All": 0.00000},
        "Fixed": True,
    }

    configdict["SignalShape"]["BeautyMass"]["a1"] = {
        "20152016": {
            "NonRes": 0.267046,
            "PhiPi": 0.272534,
            "KstK": 0.232789,
            "PiPiPi": 0.28194,
        },
        "2017": {
            "NonRes": 0.314537,
            "PhiPi": 0.266499,
            "KstK": 0.262134,
            "PiPiPi": 0.270575,
        },
        "2018": {
            "NonRes": 0.380822,
            "PhiPi": 0.23871,
            "KstK": 0.39719,
            "PiPiPi": 0.540159,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["a2"] = {
        "20152016": {
            "NonRes": 1.95297,
            "PhiPi": 2.6426,
            "KstK": 2.77204,
            "PiPiPi": 3.02235,
        },
        "2017": {
            "NonRes": 2.76145,
            "PhiPi": 2.95991,
            "KstK": 2.72509,
            "PiPiPi": 3.48439,
        },
        "2018": {
            "NonRes": 2.4077,
            "PhiPi": 2.79521,
            "KstK": 2.27732,
            "PiPiPi": 2.39562,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "20152016": {"NonRes": 50.0, "PhiPi": 50.0, "KstK": 50.0, "PiPiPi": 50.0},
        "2017": {"NonRes": 50.0, "PhiPi": 50.0, "KstK": 50.0, "PiPiPi": 50.0},
        "2018": {"NonRes": 50.0, "PhiPi": 50.0, "KstK": 50.0, "PiPiPi": 50.0},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "20152016": {
            "NonRes": 2.02956,
            "PhiPi": 1.07021,
            "KstK": 0.974427,
            "PiPiPi": 1.15776,
        },
        "2017": {
            "NonRes": 1.27801,
            "PhiPi": 0.966063,
            "KstK": 0.999712,
            "PiPiPi": 0.979765,
        },
        "2018": {
            "NonRes": 1.6869,
            "PhiPi": 0.986023,
            "KstK": 1.17564,
            "PiPiPi": 1.51773,
        },
        "Fixed": True,
    }

    configdict["SignalShape"]["BeautyMass"]["l"] = {
        "20152016": {
            "NonRes": -1.54649,
            "PhiPi": -1.47673,
            "KstK": -1.40369,
            "PiPiPi": -1.39036,
        },
        "2017": {
            "NonRes": -1.59434,
            "PhiPi": -1.36771,
            "KstK": -1.46337,
            "PiPiPi": -1.33975,
        },
        "2018": {
            "NonRes": -1.76979,
            "PhiPi": -1.33691,
            "KstK": -1.61501,
            "PiPiPi": -3.35,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["nu"] = {
        "20152016": {
            "NonRes": -0.19274,
            "PhiPi": -0.202706,
            "KstK": -0.140866,
            "PiPiPi": -0.0483708,
        },
        "2017": {
            "NonRes": -0.162521,
            "PhiPi": -0.125343,
            "KstK": -0.168803,
            "PiPiPi": -0.035089,
        },
        "2018": {
            "NonRes": -0.210806,
            "PhiPi": -0.159861,
            "KstK": -0.1796,
            "PiPiPi": -0.286881,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["tau"] = {
        "20152016": {
            "NonRes": 0.34648,
            "PhiPi": 0.360024,
            "KstK": 0.343189,
            "PiPiPi": 0.352791,
        },
        "2017": {
            "NonRes": 0.319533,
            "PhiPi": 0.328242,
            "KstK": 0.344825,
            "PiPiPi": 0.311098,
        },
        "2018": {
            "NonRes": 0.294316,
            "PhiPi": 0.345727,
            "KstK": 0.337835,
            "PiPiPi": 0.338194,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["fracI"] = {
        "20152016": {
            "NonRes": 0.114842,
            "PhiPi": 0.124757,
            "KstK": 0.121322,
            "PiPiPi": 0.19915,
        },
        "2017": {
            "NonRes": 0.142232,
            "PhiPi": 0.162128,
            "KstK": 0.126357,
            "PiPiPi": 0.246785,
        },
        "2018": {
            "NonRes": 0.16328,
            "PhiPi": 0.14134,
            "KstK": 0.126309,
            "PiPiPi": 0.141831,
        },
        "Fixed": True,
    }

    configdict["SignalShape"]["BeautyMass"]["sigmaI"] = {
        "20152016": {
            "NonRes": 37.4012,
            "PhiPi": 39.6851,
            "KstK": 39.2369,
            "PiPiPi": 33.3208,
        },
        "2017": {
            "NonRes": 33.8186,
            "PhiPi": 39.2647,
            "KstK": 35.7117,
            "PiPiPi": 30.8107,
        },
        "2018": {
            "NonRes": 29.9745,
            "PhiPi": 39.897,
            "KstK": 34.7219,
            "PiPiPi": 28.6863,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["sigmaJ"] = {
        "20152016": {
            "NonRes": 14.1919,
            "PhiPi": 14.393,
            "KstK": 14.2233,
            "PiPiPi": 14.5642,
        },
        "2017": {
            "NonRes": 13.6276,
            "PhiPi": 13.818,
            "KstK": 13.8814,
            "PiPiPi": 13.9234,
        },
        "2018": {
            "NonRes": 13.4904,
            "PhiPi": 13.9978,
            "KstK": 13.7956,
            "PiPiPi": 14.234,
        },
        "Fixed": False,
    }

    # configdict["SignalShape"]["BeautyMass"]["fracI"]   = {
    # "Run2": {
    # "NonRes" : 0.191413,
    # "KstK" : 0.191413,
    # "PhiPi" : 0.191413,
    # "PiPiPi" : 0.191413},
    # "Fixed": False}

    # Ds signal shapes
    configdict["SignalShape"]["CharmMass"] = {}
    configdict["SignalShape"]["CharmMass"][
        "type"
    ] = "IpatiaJohnsonSU"  # "IpatiaPlusGaussian" #WithWidthRatio"
    # configdict["SignalShape"]["CharmMass"]["mean"]    = {
    # "Run2": {"All":1968.49}, "Fixed":False}
    # configdict["SignalShape"]["CharmMass"]["mean"]    = {
    # "Run1": {"KKPi":1968.49, "PiPiPi":1968.49}, "Fixed":False}
    configdict["SignalShape"]["CharmMass"]["mean"] = {
        "20152016": {"All": 1968.49},
        "2017": {"All": 1968.49},
        "2018": {"All": 1968.49},
        "Fixed": False,
    }
    # configdict["SignalShape"]["CharmMass"]["mean"]    = {
    # "20152016": {
    #   "NonRes":1968.49, "PhiPi":1968.49, "KstK":1968.49, "PiPiPi":1968.49},
    # "2017":     {
    #   "NonRes":1968.49, "PhiPi":1968.49, "KstK":1968.49, "PiPiPi":1968.49},
    # "2018":     {
    #   "NonRes":1968.49, "PhiPi":1968.49, "KstK":1968.49, "PiPiPi":1968.49},
    # "Fixed":False}

    # pidcorr
    configdict["SignalShape"]["CharmMass"]["a1"] = {
        "20152016": {
            "NonRes": 0.16053,
            "PhiPi": 0.18997,
            "KstK": 0.17253,
            "PiPiPi": 0.183346,
        },
        "2017": {
            "NonRes": 0.12713,
            "PhiPi": 0.20276,
            "KstK": 0.14209,
            "PiPiPi": 0.173038,
        },
        "2018": {
            "NonRes": 0.16241,
            "PhiPi": 0.16530,
            "KstK": 0.13652,
            "PiPiPi": 0.184369,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["a2"] = {
        "20152016": {
            "NonRes": 0.39485,
            "PhiPi": 0.37501,
            "KstK": 0.37944,
            "PiPiPi": 0.844259,
        },
        "2017": {
            "NonRes": 0.26094,
            "PhiPi": 0.49399,
            "KstK": 0.31042,
            "PiPiPi": 0.805209,
        },
        "2018": {
            "NonRes": 0.37773,
            "PhiPi": 0.29843,
            "KstK": 0.27201,
            "PiPiPi": 0.888122,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["tau"] = {
        "20152016": {
            "NonRes": 0.46214,
            "PhiPi": 0.48550,
            "KstK": 0.44602,
            "PiPiPi": 0.380109,
        },
        "2017": {
            "NonRes": 0.45391,
            "PhiPi": 0.46851,
            "KstK": 0.45419,
            "PiPiPi": 0.387104,
        },
        "2018": {
            "NonRes": 0.46527,
            "PhiPi": 0.49029,
            "KstK": 0.44849,
            "PiPiPi": 0.387943,
        },
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["fracI"] = {
        "20152016": {
            "NonRes": 0.086438,
            "PhiPi": 0.12966,
            "KstK": 0.087039,
            "PiPiPi": 0.312702,
        },
        "2017": {
            "NonRes": 0.080211,
            "PhiPi": 0.13619,
            "KstK": 0.067769,
            "PiPiPi": 0.271536,
        },
        "2018": {
            "NonRes": 0.094782,
            "PhiPi": 0.12088,
            "KstK": 0.069336,
            "PiPiPi": 0.308127,
        },
        "Fixed": True,
    }

    configdict["SignalShape"]["CharmMass"]["sigmaJ"] = {
        "20152016": {
            "NonRes": 6.4363,
            "PhiPi": 6.4202,
            "KstK": 6.5514,
            "PiPiPi": 9.20648,
        },
        "2017": {"NonRes": 6.3217, "PhiPi": 6.2182, "KstK": 6.4968, "PiPiPi": 9.03914},
        "2018": {"NonRes": 6.3125, "PhiPi": 6.3379, "KstK": 6.4939, "PiPiPi": 9.08361},
        "Fixed": False,
    }

    configdict["SignalShape"]["CharmMass"]["sigmaI"] = {
        "20152016": {
            "NonRes": 29.915,
            "PhiPi": 26.734,
            "KstK": 26.271,
            "PiPiPi": 30.4756,
        },
        "2017": {"NonRes": 26.660, "PhiPi": 27.198, "KstK": 26.868, "PiPiPi": 30.3659},
        "2018": {"NonRes": 27.776, "PhiPi": 24.312, "KstK": 24.853, "PiPiPi": 29.3058},
        "Fixed": True,
    }

    configdict["SignalShape"]["CharmMass"]["nu"] = {
        "Run2": {"All": 0.00},
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["fb"] = {
        "Run2": {"All": 0.00},
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["zeta"] = {
        "Run2": {"All": 0.00},
        "Fixed": True,
    }
    configdict["SignalShape"]["CharmMass"]["l"] = {"Run2": {"All": -1.1}, "Fixed": True}
    configdict["SignalShape"]["CharmMass"]["n1"] = {"Run2": {"All": 8.0}, "Fixed": True}
    configdict["SignalShape"]["CharmMass"]["n2"] = {"Run2": {"All": 8.0}, "Fixed": True}
    # combinatorial background
    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "DoubleExponential"
    #                                                     "Fixed":True }
    configdict["CombBkgShape"]["BeautyMass"]["cB2"] = {
        "20152016": {
            "NonRes": -5.5365e-04,
            "PhiPi": -2.6327e-04,
            "KstK": -6.4593e-04,
            "PiPiPi": -4.7510e-04,
        },
        "2017": {
            "NonRes": -5.5365e-04,
            "PhiPi": -2.6327e-04,
            "KstK": -6.4593e-04,
            "PiPiPi": -4.7510e-04,
        },
        "2018": {
            "NonRes": -5.5365e-04,
            "PhiPi": -2.6327e-04,
            "KstK": -6.4593e-04,
            "PiPiPi": -4.7510e-04,
        },
        "Fixed": True,
    }
    configdict["CombBkgShape"]["BeautyMass"]["cB1"] = {
        "20152016": {
            "NonRes": -2.8299e-02,
            "PhiPi": -2.7999e-02,
            "KstK": -2.2682e-02,
            "PiPiPi": -2.9265e-02,
        },
        "2017": {
            "NonRes": -2.8299e-02,
            "PhiPi": -2.7999e-02,
            "KstK": -2.2682e-02,
            "PiPiPi": -2.9265e-02,
        },
        "2018": {
            "NonRes": -2.8299e-02,
            "PhiPi": -2.7999e-02,
            "KstK": -2.2682e-02,
            "PiPiPi": -2.9265e-02,
        },
        "Fixed": False,
    }

    configdict["CombBkgShape"]["BeautyMass"]["frac"] = {
        "20152016": {
            "NonRes": 4.3067e-01,
            "PhiPi": 6.5400e-01,
            "KstK": 3.7409e-01,
            "PiPiPi": 0.5,
        },
        "2017": {
            "NonRes": 4.3067e-01,
            "PhiPi": 6.5400e-01,
            "KstK": 3.7409e-01,
            "PiPiPi": 0.5,
        },
        "2018": {
            "NonRes": 4.3067e-01,
            "PhiPi": 6.5400e-01,
            "KstK": 3.7409e-01,
            "PiPiPi": 0.5,
        },
        "Fixed": False,
    }

    configdict["CombBkgShape"]["CharmMass"] = {}
    configdict["CombBkgShape"]["CharmMass"]["type"] = "ExponentialPlusSignal"
    configdict["CombBkgShape"]["CharmMass"]["cD"] = {
        "20152016": {"NonRes": -1e-2, "PhiPi": -1e-2, "KstK": -1e-2, "PiPiPi": -1e-2},
        "2017": {"NonRes": -1e-2, "PhiPi": -1e-2, "KstK": -1e-2, "PiPiPi": -1e-2},
        "2018": {"NonRes": -1e-2, "PhiPi": -1e-2, "KstK": -1e-2, "PiPiPi": -1e-2},
        "Fixed": False,
    }
    configdict["CombBkgShape"]["CharmMass"]["fracD"] = {
        "20152016": {
            "NonRes": 0.88620,
            "PhiPi": 0.37379,
            "KstK": 0.59093,
            "PiPiPi": 0.5,
        },
        "2017": {"NonRes": 0.88620, "PhiPi": 0.37379, "KstK": 0.59093, "PiPiPi": 0.5},
        "2018": {"NonRes": 0.88620, "PhiPi": 0.37379, "KstK": 0.59093, "PiPiPi": 0.5},
        "Fixed": False,
    }

    configdict["CombBkgShape"]["BacPIDK"] = {}
    configdict["CombBkgShape"]["BacPIDK"]["type"] = "FixedWithKaonPion"
    configdict["CombBkgShape"]["BacPIDK"]["components"] = {
        "Kaon": True,
        "Pion": True,
        "Proton": False,
    }
    configdict["CombBkgShape"]["BacPIDK"]["fracPIDK1"] = {
        "Run2": {"NonRes": 0.9, "PhiPi": 0.9, "KstK": 0.9, "PiPiPi": 0.8},
        "Fixed": False,
    }
    configdict["CombBkgShape"]["BacPIDK"]["fracPIDK2"] = {
        "Run2": {"NonRes": 0.9, "PhiPi": 0.9, "KstK": 0.9, "PiPiPi": 0.8},
        "Fixed": False,
    }

    # Bd2Dsh background
    # shape for BeautyMass, for CharmMass as well as BacPIDK
    # taken by default the same as signal
    configdict["Bd2DsPiShape"] = {}
    configdict["Bd2DsPiShape"]["BeautyMass"] = {}
    if (
        configdict["SignalShape"]["BeautyMass"]["type"]
        == "IpatiaJohnsonSUWithWidthRatio"
    ):  # WithWidthRatio":
        configdict["Bd2DsPiShape"]["BeautyMass"][
            "type"
        ] = "ShiftedSignalIpatiaJohnsonSUWithWidthRatio"
    else:
        configdict["Bd2DsPiShape"]["BeautyMass"][
            "type"
        ] = "ShiftedSignalIpatiaJohnsonSU"
    configdict["Bd2DsPiShape"]["BeautyMass"]["shift"] = {
        "Run2": {"All": -87.38},
        "Fixed": True,
    }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale1"] = {
        "Run2": {"All": 1.00808721452},
        "Fixed": True,
    }
    configdict["Bd2DsPiShape"]["BeautyMass"]["scale2"] = {
        "Run2": {"All": 1.03868673310},
        "Fixed": True,
    }

    #
    configdict["AdditionalParameters"] = {}
    configdict["AdditionalParameters"]["g1_f1_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Fixed": True,
    }
    configdict["AdditionalParameters"]["g1_f2_frac"] = {
        "Run2": {"All": {"Both": {"CentralValue": 0.5, "Range": [0.0, 1.0]}}},
        "Fixed": True,
    }

    # expected yields
    configdict["Yields"] = {}

    configdict["Yields"]["Bd2DPi"] = {
        "2015": {"NonRes": 192.0, "PhiPi": 3.41, "KstK": 56.45, "PiPiPi": 0.0},
        "2016": {"NonRes": 818.5, "PhiPi": 15.77, "KstK": 237.06, "PiPiPi": 0.0},
        "2017": {"NonRes": 931.5, "PhiPi": 22.11, "KstK": 265.23, "PiPiPi": 0.0},
        "2018": {"NonRes": 1122.68, "PhiPi": 21.59, "KstK": 319.38, "PiPiPi": 0.0},
        "Fixed": True,
    }

    configdict["Yields"]["Lb2LcPi"] = {
        "2015": {"NonRes": 116.52, "PhiPi": 6.22, "KstK": 26.48, "PiPiPi": 0.0},
        "2016": {"NonRes": 664.81, "PhiPi": 62.27, "KstK": 142.3, "PiPiPi": 0.0},
        "2017": {"NonRes": 650.18, "PhiPi": 52.03, "KstK": 133.1, "PiPiPi": 0.0},
        "2018": {"NonRes": 765.80, "PhiPi": 74.43, "KstK": 159.3, "PiPiPi": 0.0},
        "Fixed": True,
    }

    configdict["Yields"]["Bs2DsK"] = {
        "2015": {"NonRes": 29.48, "PhiPi": 65.86, "KstK": 45.74, "PiPiPi": 30.07},
        "2016": {"NonRes": 151.5, "PhiPi": 339.4, "KstK": 235.56, "PiPiPi": 155.09},
        "2017": {"NonRes": 145.1, "PhiPi": 323.1, "KstK": 227.0, "PiPiPi": 148.946},
        "2018": {"NonRes": 173.5, "PhiPi": 391.4, "KstK": 277.3, "PiPiPi": 176.751},
        "Fixed": True,
    }

    configdict["Yields"]["Bs2DsDsstPiRho"] = {
        "2015": {"NonRes": 1000.0, "PhiPi": 1000.0, "KstK": 1000.0, "PiPiPi": 200.0},
        "2016": {"NonRes": 2000.0, "PhiPi": 2000.0, "KstK": 2000.0, "PiPiPi": 500.0},
        "2017": {"NonRes": 2000.0, "PhiPi": 2000.0, "KstK": 2000.0, "PiPiPi": 500.0},
        "2018": {"NonRes": 2000.0, "PhiPi": 2000.0, "KstK": 2000.0, "PiPiPi": 500.0},
        "Fixed": False,
    }
    configdict["Yields"]["CombBkg"] = {
        "2015": {
            "NonRes": 10000.0,
            "PhiPi": 20000.0,
            "KstK": 10000.0,
            "PiPiPi": 10000.0,
        },
        "2016": {
            "NonRes": 20000.0,
            "PhiPi": 30000.0,
            "KstK": 20000.0,
            "PiPiPi": 20000.0,
        },
        "2017": {
            "NonRes": 20000.0,
            "PhiPi": 30000.0,
            "KstK": 20000.0,
            "PiPiPi": 20000.0,
        },
        "2018": {
            "NonRes": 20000.0,
            "PhiPi": 40000.0,
            "KstK": 20000.0,
            "PiPiPi": 20000.0,
        },
        "Fixed": False,
    }
    configdict["Yields"]["Signal"] = {
        "2015": {
            "NonRes": 10000.0,
            "PhiPi": 20000.0,
            "KstK": 10000.0,
            "PiPiPi": 10000.0,
        },
        "2016": {
            "NonRes": 20000.0,
            "PhiPi": 40000.0,
            "KstK": 20000.0,
            "PiPiPi": 20000.0,
        },
        "2017": {
            "NonRes": 40000.0,
            "PhiPi": 40000.0,
            "KstK": 30000.0,
            "PiPiPi": 20000.0,
        },
        "2018": {
            "NonRes": 40000.0,
            "PhiPi": 60000.0,
            "KstK": 40000.0,
            "PiPiPi": 20000.0,
        },
        "Fixed": False,
    }

    # ------------------------------------------------------------------
    ###                     MDfit plotting settings
    # ------------------------------------------------------------------
    from ROOT import kBlue, kGreen, kOrange, kRed

    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = [
        "Sig",
        "CombBkg",
        "Bd2DPi",
        "Lb2LcPi",
        "Bs2DsDsstPiRho",
        "Bs2DsK",
    ]
    configdict["PlotSettings"]["colors"] = [
        kRed - 7,
        kBlue - 6,
        kOrange,
        kRed,
        kBlue - 10,
        kGreen + 3,
    ]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.76, 0.9],
        "ScaleYSize": 1.2,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.9],
        "ScaleYSize": 1.7,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }

    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict


if __name__ == "__main__":
    from Toys.configutils import main

    main(getconfig())
