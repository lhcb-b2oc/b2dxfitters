###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys
import numpy as np
from ..nominal import DMSFit

sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa


def getconfig():
    configdict = DMSFit.getconfig()

    # covariance matrix taken from HFLAV 2020
    mean = [0.6624, 0.085]
    cov = np.array(
        [[0.0016**2, -0.124 * 0.0016 * 0.004], [-0.124 * 0.0016 * 0.004, 0.004**2]]
    )
    configdict["Gammas"] = [mean, cov]
    configdict["DeltaGammas"] = [mean, cov]

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    from configutils import main

    main(getconfig())
