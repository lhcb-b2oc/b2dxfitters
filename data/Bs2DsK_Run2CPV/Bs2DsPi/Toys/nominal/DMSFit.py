###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import itertools
import os
import sys
from math import pi
from ..nominal import MassFit

sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa


def getconfig():
    configdict = MassFit.getconfig()

    # PHYSICAL PARAMETERS
    configdict["Gammas"] = 0.6624
    configdict["DeltaGammas"] = 0.089424
    configdict["DeltaMs"] = 17.757  # in ps^{-1}
    configdict["TagEffSig"] = 0.403
    configdict["TagOmegaSig"] = 0.391
    configdict["StrongPhase"] = 20.0 / 180.0 * pi
    configdict["WeakPhase"] = 70.0 / 180.0 * pi
    configdict["ModLf"] = 0.372
    configdict["CPlimit"] = {"upper": 4.0, "lower": -4.0}
    configdict["Labels"] = ["20152016", "2017", "2018"]

    configdict["Asymmetries"] = {
        "Detection": 0.0,
        "Production": 0.0,
    }

    configdict["FixAcceptance"] = False
    configdict["ConstrainsForTaggingCalib"] = False
    configdict["SWeightCorrection"] = "Read"

    configdict["Resolution"] = {
        "20152016": {
            "scaleFactor": {"p0": 0.006, "p1": 1.05, "p2": 0.0},
            "meanBias": -0.003,
        },
        "2017": {
            "scaleFactor": {"p0": 0.006, "p1": 1.05, "p2": 0.0},
            "meanBias": -0.003,
        },
        "2018": {
            "scaleFactor": {"p0": 0.006, "p1": 1.05, "p2": 0.0},
            "meanBias": -0.003,
        },
    }

    configdict["TaggingCalibration"] = {
        "SS": {
            y: {
                "p0": 0.417,
                "dp0": 0.0,
                "p1": 1.0,
                "dp1": 0.0,
                "average": 0.416,
                "tagEff": 0.70,
                "aTagEff": 0.0,
                "use": True,
            }
            for y in configdict["Labels"]
        },
        "OS": {
            y: {
                "p0": 0.356,
                "dp0": 0.0,
                "p1": 1.0,
                "dp1": 0.0,
                "average": 0.35,
                "tagEff": 0.41,
                "aTagEff": 0.0,
                "use": True,
            }
            for y in configdict["Labels"]
        },
    }

    configdict["Acceptance"] = {
        "knots": [0.5, 1.0, 1.5, 2.0, 3.0, 12.0],
        "20152016": {"values": [0.5, 0.7, 0.9, 1.1, 1.2, 1.2]},
        "2017": {"values": [0.5, 0.7, 0.9, 1.1, 1.2, 1.2]},
        "2018": {"values": [0.5, 0.7, 0.9, 1.1, 1.2, 1.2]},
    }

    configdict["constParams"] = [
        "Gammas_Bs2DsPi",
        "deltaGammas_Bs2DsPi",
        "C_Bs2DsPi",
        "Cbar_Bs2DsPi",
        "S_Bs2DsPi",
        "Sbar_Bs2DsPi",
        "D_Bs2DsPi",
        "Dbar_Bs2DsPi",
        "average_OS",
        "average_SS",
    ]

    # Add some name variants
    configdict["constParams"] += [
        c + "_" + label
        for c, label in itertools.product(
            configdict["constParams"], configdict["Labels"]
        )
    ]

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    import pprint

    pprint.pprint(getconfig())
