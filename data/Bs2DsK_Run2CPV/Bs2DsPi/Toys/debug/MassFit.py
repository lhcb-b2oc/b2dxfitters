import os
import sys
from nominal import MassFit
import pprint

sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa


def getconfig():
    configdict = MassFit.getconfig()

    configdict["YearOfDataTaking"] = ["2015", "2016"]
    configdict["CharmModes"] = ["PhiPi"]

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    pprint.pprint(getconfig())
