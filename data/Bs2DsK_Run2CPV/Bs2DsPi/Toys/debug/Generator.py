###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys
from ..nominal import Generator
import pprint

sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa


def getconfig():
    configdict = Generator.getconfig()

    # Only generate 2015 + 2016
    # this should effectively test merging of samples, as well
    configdict["Years"] = ["2015", "2016"]

    # only generate one D decay mode to speed up generation
    configdict["CharmModes"] = ["PhiPi"]

    # use protodata for tagging categries, this will screw the time distribution
    # but speed up the generation
    configdict["ForceProtoForTagging"] = True

    # pop a few configuration entries to simplify the stdout
    for component in list(configdict["Components"].keys()):
        configdict["Components"][component]["Bs2DsPi"].pop("2017")
        configdict["Components"][component]["Bs2DsPi"].pop("2018")
        for year in list(configdict["Components"][component]["Bs2DsPi"].keys()):
            configdict["Components"][component]["Bs2DsPi"][year].pop("KPiPi")
            configdict["Components"][component]["Bs2DsPi"][year].pop("KstK")
            configdict["Components"][component]["Bs2DsPi"][year].pop("NonRes")
            configdict["Components"][component]["Bs2DsPi"][year].pop("PiPiPi")
    for pdf in list(configdict["PDFList"].keys()):
        for component in list(configdict["PDFList"][pdf].keys()):
            configdict["PDFList"][pdf][component]["Bs2DsPi"].pop("2017")
            configdict["PDFList"][pdf][component]["Bs2DsPi"].pop("2018")
            for year in list(configdict["PDFList"][pdf][component]["Bs2DsPi"].keys()):
                configdict["PDFList"][pdf][component]["Bs2DsPi"][year].pop(
                    "KPiPi", None
                )
                configdict["PDFList"][pdf][component]["Bs2DsPi"][year].pop("KstK", None)
                configdict["PDFList"][pdf][component]["Bs2DsPi"][year].pop(
                    "NonRes", None
                )
                configdict["PDFList"][pdf][component]["Bs2DsPi"][year].pop(
                    "PiPiPi", None
                )

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    pprint.pprint(getconfig())
