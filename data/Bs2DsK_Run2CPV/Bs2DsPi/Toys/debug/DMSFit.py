import itertools
import os
import sys
from ..nominal import DMSFit
import pprint

sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa


def getconfig():
    configdict = DMSFit.getconfig()

    configdict["Labels"] = ["20152016"]
    configdict["constParams"] = [
        "Gammas_Bs2DsPi",
        "deltaGammas_Bs2DsPi",
        "C_Bs2DsPi",
        "Cbar_Bs2DsPi",
        "S_Bs2DsPi",
        "Sbar_Bs2DsPi",
        "D_Bs2DsPi",
        "Dbar_Bs2DsPi",
        "tagEff_OS",
        "tagEff_SS",
        "aTagEff_OS",
        "aTagEff_SS",
        "average_OS",
        "average_SS",
        "adet",
        "aprod",
        "var1",
        "var3",
        "var4",
        "var5",
        "var6",
        "var7",
        "p_0_GLMCalib_OS",
        "p_1_GLMCalib_OS",
        "p_0_GLMCalib_SS",
        "p_1_GLMCalib_SS",
        "dp_0_GLMCalib_OS",
        "dp_1_GLMCalib_OS",
        "dp_0_GLMCalib_SS",
        "dp_1_GLMCalib_SS",
    ]

    # Add some name variants
    configdict["constParams"] += [
        c + "_" + label
        for c, label in itertools.product(
            configdict["constParams"], configdict["Labels"]
        )
    ]

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    pprint.pprint(getconfig())
