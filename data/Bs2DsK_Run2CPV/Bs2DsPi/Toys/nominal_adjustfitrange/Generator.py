###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import math
import os
import sys
from ..nominal import Generator

sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa


def getconfig():
    config = Generator.getconfig()

    config["Observables"]["BeautyTime"]["Range"][0] = 2 * math.pi / 17.757

    return config


# just print the full config if run directly
if __name__ == "__main__":
    from configutils import main

    main(getconfig())
