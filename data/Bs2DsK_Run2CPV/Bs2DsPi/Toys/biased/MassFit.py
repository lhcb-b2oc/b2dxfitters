import os
import sys
import pprint
from ..nominal import MassFit

sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa


def getconfig():
    return MassFit.getconfig()


# just print the full config if run directly
if __name__ == "__main__":
    pprint.pprint(getconfig())
