import os
import sys

import nominal.DMSFit

sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))  # noqa


def getconfig():
    configdict = nominal.DMSFit.getconfig()

    # set the bias correction to the generated value
    for label in configdict["Labels"]:
        configdict["Resolution"][label]["meanBias"] = -0.003

    return configdict


# just print the full config if run directly
if __name__ == "__main__":
    import pprint

    pprint.pprint(getconfig())
