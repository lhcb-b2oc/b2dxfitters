###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys
from ..nominal import MassFit
from configutils import main

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))


def getconfig():
    configdict = MassFit.getconfig()
    return configdict


if __name__ == "__main__":
    main(getconfig())
