# coding=utf-8
def getconfig():
    from Bs2DsPiConfigForNominalMassFit import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    from math import pi

    # PHYSICAL PARAMETERS
    configdict["Gammas"] = 0.6643  # 0.0020   # in ps^{-1}
    configdict["DeltaGammas"] = -0.083  # -0.006
    configdict["DeltaMs"] = 17.757  # in ps^{-1}
    configdict["TagEffSig"] = 0.403
    configdict["TagOmegaSig"] = 0.391
    configdict["StrongPhase"] = 20.0 / 180.0 * pi
    configdict["WeakPhase"] = 70.0 / 180.0 * pi
    configdict["ModLf"] = 0.372
    configdict["CPlimit"] = {"upper": 4.0, "lower": -4.0}
    configdict["Labels"] = ["20152016", "2017", "2018"]

    configdict["Asymmetries"] = {"Detection": 1.0 / 100.0, "Production": 1.1 / 100.0}
    #    configdict["Asymmetries"] = {"Detection":0.0,
    #                                 "Production":0.0}

    configdict["FixAcceptance"] = False
    configdict["ConstrainsForTaggingCalib"] = False

    configdict["Resolution"] = {
        "20152016": {
            "scaleFactor": {
                "p0": 0.012963396890204528,
                "p1": 1.0078118390740174,
                "p2": 0.0,
            },
            "meanBias": 0.0,
        },
        "2017": {
            "scaleFactor": {
                "p0": 0.010580008378118407,
                "p1": 1.0159541446819076,
                "p2": 0.0,
            },
            "meanBias": 0.0,
        },
        "2018": {
            "scaleFactor": {"p0": 0.0099584, "p1": 1.03, "p2": 0.0},
            "meanBias": 0.0,
        },
    }

    configdict["TaggingCalibration"] = {}
    configdict["TaggingCalibration"]["SS"] = {
        "20152016": {
            "p0": 0.35771986387158167,
            "dp0": 0.0,
            "p1": 0.7846712046020927,
            "dp1": 0.0,
            "cov": [
                [1.4835562842084295e-05, 0, 1.4323682925988167e-05, 0],
                [0, 1, 0, 0],
                [1.4323682925988162e-05, 0, 0.0018033648107572144, 0],
                [0, 0, 0, 1],
            ],
            "average": 0.3473,
            "tagEff": 0.68958,
            "aTagEff": 0.0,
            "use": True,
        },
        "2017": {
            "p0": 0.3641003173215767,
            "dp0": 0.0,
            "p1": 0.7470588738413062,
            "dp1": 0.0,
            "cov": [
                [1.4413311191542025e-05, 0, 1.1646137173882225e-05, 0],
                [0, 1, 0, 0],
                [1.1646137173882232e-05, 0, 0.0018061584008630552, 0],
                [0, 0, 0, 1],
            ],
            "average": 0.3471,
            "tagEff": 0.696944,
            "aTagEff": 0.0,
            "use": True,
        },
        "2018": {
            "p0": 0.3603373491499432,
            "dp0": 0.0,
            "p1": 0.824513686572027,
            "dp1": 0.0,
            "cov": [
                [1.2231360043369147e-05, 0, 1.1371485140332045e-05, 0],
                [0, 1, 0, 0],
                [1.1371485140332048e-05, 0, 0.001476457096890698, 0],
                [0, 0, 0, 1],
            ],
            "average": 0.3478,
            "tagEff": 0.694198,
            "aTagEff": 0.0,
            "use": True,
        },
    }

    configdict["TaggingCalibration"]["OS"] = {
        "20152016": {
            "p0": 0.44378291564334543,
            "dp0": 0.0,
            "p1": 0.9865812407095272,
            "dp1": 0.0,
            "cov": [
                [2.441734827059775e-05, 0, 1.1826435719812195e-05, 0],
                [0, 1, 0, 0],
                [1.1826435719812202e-05, 0, 0.001976280718063521, 0],
                [0, 0, 0, 1],
            ],
            "average": 0.4161,
            "tagEff": 0.38217399999999996,
            "aTagEff": 0.0,
            "use": True,
        },
        "2017": {
            "p0": 0.43650153480095993,
            "dp0": 0.0,
            "p1": 0.8848535353178453,
            "dp1": 0.0,
            "cov": [
                [2.4080054440434006e-05, 0, 1.3358932737027233e-05, 0],
                [0, 1, 0, 0],
                [1.3358932737027234e-05, 0, 0.002017263777945721, 0],
                [0, 0, 0, 1],
            ],
            "average": 0.4157,
            "tagEff": 0.404583,
            "aTagEff": 0.0,
            "use": True,
        },
        "2018": {
            "p0": 0.4332147154288526,
            "dp0": 0.0,
            "p1": 0.9463306351550287,
            "dp1": 0.0,
            "cov": [
                [2.029576969116387e-05, 0, 9.625829833410184e-06, 0],
                [0, 1, 0, 0],
                [9.625829833410185e-06, 0, 0.0016473943928661929, 0],
                [0, 0, 0, 1],
            ],
            "average": 0.4151,
            "tagEff": 0.402886,
            "aTagEff": 0.0,
            "use": True,
        },
    }

    configdict["Acceptance"] = {
        "knots": [0.50, 1.0, 1.5, 2.0, 3.0, 12.0],
        "20152016": {
            "values": [3.774e-01, 5.793e-01, 7.752e-01, 1.0043e00, 1.0937e00, 1.1872e00]
        },
        "2017": {
            "values": [3.774e-01, 5.793e-01, 7.752e-01, 1.0043e00, 1.0937e00, 1.1872e00]
        },
        "2018": {
            "values": [3.774e-01, 5.793e-01, 7.752e-01, 1.0043e00, 1.0937e00, 1.1872e00]
        },
    }

    # configdict["Acceptance"] = {
    #       "knots": [0.50, 1.0,  1.5, 2.0, 3.0, 12.0],
    #       "values": [
    #           3.774e-01,5.793e-01,7.752e-01,
    #           1.0043e+00,1.0937e+00,1.1872e+00
    #       ] }

    configdict["constParams"] = []
    configdict["constParams"].append("Gammas_Bs2DsPi")
    configdict["constParams"].append("deltaGammas_Bs2DsPi")
    configdict["constParams"].append("C_Bs2DsPi")
    configdict["constParams"].append("Cbar_Bs2DsPi")
    configdict["constParams"].append("S_Bs2DsPi")
    configdict["constParams"].append("Sbar_Bs2DsPi")
    configdict["constParams"].append("D_Bs2DsPi")
    configdict["constParams"].append("Dbar_Bs2DsPi")
    configdict["constParams"].append("tagEff_OS")
    configdict["constParams"].append("tagEff_SS")
    configdict["constParams"].append("aTagEff_OS")
    configdict["constParams"].append("aTagEff_SS")

    if configdict["FixAcceptance"] is True:
        configdict["constParams"].append("var1")
        configdict["constParams"].append("var2")
        configdict["constParams"].append("var3")
        configdict["constParams"].append("var4")
        configdict["constParams"].append("var5")
        configdict["constParams"].append("var6")
        configdict["constParams"].append("var7")
    # if configdict["ConstrainsForTaggingCalib"] == False:
    #    configdict["constParams"].append('p0_OS')
    #    configdict["constParams"].append('p0_SS')
    #    configdict["constParams"].append('p1_OS')
    #    configdict["constParams"].append('p1_SS')
    configdict["constParams"].append("dp0_OS")
    configdict["constParams"].append("dp0_SS")
    configdict["constParams"].append("dp1_OS")
    configdict["constParams"].append("dp1_SS")
    configdict["constParams"].append("p0_mean_OS")
    configdict["constParams"].append("p0_mean_SS")
    configdict["constParams"].append("p1_mean_OS")
    configdict["constParams"].append("p1_mean_SS")
    configdict["constParams"].append("dp0_mean_OS")
    configdict["constParams"].append("dp0_mean_SS")
    configdict["constParams"].append("dp1_mean_OS")
    configdict["constParams"].append("dp1_mean_SS")
    configdict["constParams"].append("average_OS")
    configdict["constParams"].append("average_SS")

    constPar = configdict["constParams"]

    configdict["addConstParams"] = []
    for const in constPar:
        for lab in configdict["Labels"]:
            nameVar = const + "_" + lab
            configdict["addConstParams"].append(nameVar)
    configdict["constParams"] = configdict["constParams"] + configdict["addConstParams"]

    return configdict
