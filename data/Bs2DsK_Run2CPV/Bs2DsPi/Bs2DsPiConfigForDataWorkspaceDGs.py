def getconfig():
    from Bs2DsPiConfigForNominalMassFit import getconfig as getconfig_nominal

    configdict = getconfig_nominal()

    configdict["YearOfDataTaking"] = {"2015", "2016"}

    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5300, 5800],
        "InputName": "lab0_MassFitConsD_M",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1930, 2015],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacPT"] = {
        "Range": [400.0, 45000.0],
        "InputName": "lab1_PT",
    }
    configdict["BasicVariables"]["BacP"] = {
        "Range": [3000.0, 650000.0],
        "InputName": "lab1_P",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK",
    }
    configdict["BasicVariables"]["nTracks"] = {
        "Range": [15.0, 1000.0],
        "InputName": "nTracks",
    }
    configdict["BasicVariables"]["BeautyTime"] = {
        "Range": [0.4, 15.0],
        "InputName": "lab0_LifetimeFitConsD_ctau",
    }
    configdict["BasicVariables"]["BeautyTimeErr"] = {
        "Range": [0.01, 0.1],
        "InputName": "lab0_LifetimeFitConsD_ctauErr",
    }
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [-1.0, 1.0],
        "InputName": "BDTGResponse_XGB_1",
    }

    configdict["AdditionalVariables"] = {}
    configdict["AdditionalVariables"]["lab0_P"] = {
        "Range": [0.0, 1600000.0],
        "InputName": "lab0_P",
    }
    configdict["AdditionalVariables"]["lab0_PT"] = {
        "Range": [0.0, 40000.0],
        "InputName": "lab0_PT",
    }
    configdict["AdditionalVariables"]["lab0_IPCHI2_OWNPV"] = {
        "Range": [0.0, 10000.0],
        "InputName": "lab0_IPCHI2_OWNPV",
    }
    configdict["AdditionalVariables"]["lab1_IPCHI2_OWNPV"] = {
        "Range": [0.0, 180000.0],
        "InputName": "lab1_IPCHI2_OWNPV",
    }
    configdict["AdditionalVariables"]["lab2_P"] = {
        "Range": [0.0, 1800000.0],
        "InputName": "lab2_P",
    }
    configdict["AdditionalVariables"]["lab2_PT"] = {
        "Range": [0.0, 90000.0],
        "InputName": "lab2_PT",
    }
    configdict["AdditionalVariables"]["lab2_IPCHI2_OWNPV"] = {
        "Range": [0.0, 180000.0],
        "InputName": "lab2_IPCHI2_OWNPV",
    }
    configdict["AdditionalVariables"]["lab3_P"] = {
        "Range": [0.0, 1800000.0],
        "InputName": "lab3_P",
    }
    configdict["AdditionalVariables"]["lab3_PT"] = {
        "Range": [0.0, 90000.0],
        "InputName": "lab3_PT",
    }
    configdict["AdditionalVariables"]["lab3_IPCHI2_OWNPV"] = {
        "Range": [0.0, 180000.0],
        "InputName": "lab3_IPCHI2_OWNPV",
    }
    configdict["AdditionalVariables"]["lab4_P"] = {
        "Range": [0.0, 1800000.0],
        "InputName": "lab4_P",
    }
    configdict["AdditionalVariables"]["lab4_PT"] = {
        "Range": [0.0, 90000.0],
        "InputName": "lab4_PT",
    }
    configdict["AdditionalVariables"]["lab4_IPCHI2_OWNPV"] = {
        "Range": [0.0, 180000.0],
        "InputName": "lab4_IPCHI2_OWNPV",
    }
    configdict["AdditionalVariables"]["lab5_P"] = {
        "Range": [0.0, 1800000.0],
        "InputName": "lab5_P",
    }
    configdict["AdditionalVariables"]["lab5_PT"] = {
        "Range": [0.0, 90000.0],
        "InputName": "lab5_PT",
    }
    configdict["AdditionalVariables"]["lab5_IPCHI2_OWNPV"] = {
        "Range": [0.0, 180000.0],
        "InputName": "lab5_IPCHI2_OWNPV",
    }
    configdict["AdditionalVariables"]["lab0_DIRA_OWNPV"] = {
        "Range": [0.0, 1.1],
        "InputName": "lab0_DIRA_OWNPV",
    }
    configdict["AdditionalVariables"]["lab0_LifetimeFit_Dplus_ctau"] = {
        "Range": [-50000.0, 400000.0],
        "InputName": "lab0_LifetimeFit_Dplus_ctau",
    }
    configdict["AdditionalVariables"]["lab0_LifetimeFit_Dplus_ctauErr"] = {
        "Range": [0.0, 10000.0],
        "InputName": "lab0_LifetimeFit_Dplus_ctauErr",
    }
    configdict["AdditionalVariables"]["lab0_LifetimeFit_Dplus_decayLength"] = {
        "Range": [-50000.0, 50000.0],
        "InputName": "lab0_LifetimeFit_Dplus_decayLength",
    }
    configdict["AdditionalVariables"]["lab0_LifetimeFit_Dplus_decayLengthErr"] = {
        "Range": [0.0, 2000.0],
        "InputName": "lab0_LifetimeFit_Dplus_decayLengthErr",
    }
    configdict["AdditionalVariables"]["lab0_LifetimeFitConsD_chi2"] = {
        "Range": [0.0, 60000000.0],
        "InputName": "lab0_LifetimeFitConsD_chi2",
    }
    configdict["AdditionalVariables"]["lab0_LifetimeFitConsD_nDOF"] = {
        "Range": [-50.0, 50.0],
        "InputName": "lab0_LifetimeFitConsD_nDOF",
    }
    configdict["AdditionalVariables"]["lab0_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 50.0],
        "InputName": "lab0_ENDVERTEX_CHI2",
    }
    configdict["AdditionalVariables"]["lab2_ENDVERTEX_CHI2"] = {
        "Range": [0.0, 50.0],
        "InputName": "lab2_ENDVERTEX_CHI2",
    }
    configdict["AdditionalVariables"]["lab0_LifetimeFit_chi2"] = {
        "Range": [0.0, 10000000.0],
        "InputName": "lab0_LifetimeFit_chi2",
    }
    configdict["AdditionalVariables"]["lab0_LifetimeFit_nDOF"] = {
        "Range": [-50.0, 50.0],
        "InputName": "lab0_LifetimeFit_nDOF",
    }

    return configdict
