from ROOT import kOrange, kRed


def getconfig():
    configdict = {}

    # considered decay mode
    configdict["Decay"] = "Lb2LcPi"
    configdict["CharmModes"] = {"PhiPi"}
    configdict["Backgrounds"] = []
    # configdict["CharmModes"] = {"KPiPi"}
    # year of data taking
    configdict["YearOfDataTaking"] = {"2015", "2016"}
    # stripping (necessary in case of PIDK shapes)
    configdict["Stripping"] = {"2012": "21", "2011": "21r1"}
    # integrated luminosity in each year of data taking
    # (necessary in case of PIDK shapes)
    configdict["IntegratedLuminosity"] = {  # "2011": {"Down": 0.56,   "Up": 0.42},
        # "2012": {"Down": 0.9912, "Up": 0.9988},
        # "2015": {"Down": 0.1804, "Up": 0.1476},
        "2016": {"Down": 0.8658, "Up": 0.7229},
        # "2017": {"Down": 0.90,    "Up": 0.80}
    }
    # file name with paths to MC/data samples
    configdict["dataName"] = "../data/Bs2DsK_Run2CPV/misID_Lb2LcPi/config_Lb2LcPi.txt"
    # settings for control plots
    configdict["ControlPlots"] = {}
    configdict["ControlPlots"] = {"Directory": "PlotLb2LcPi", "Extension": "pdf"}

    # basic variables
    configdict["BasicVariables"] = {}
    configdict["BasicVariables"]["BeautyMass"] = {
        "Range": [5500, 6000],
        "InputName": "lab0_MassHypo_LcPi_LambdaFav",
    }
    configdict["BasicVariables"]["CharmMass"] = {
        "Range": [1930, 2065],
        "InputName": "lab2_MM",
    }
    configdict["BasicVariables"]["BacPIDK"] = {
        "Range": [-7.0, 5.0],
        "InputName": "lab1_PIDK",
    }  # -7.0,6.0
    configdict["BasicVariables"]["BDTG"] = {
        "Range": [0.1, 1.0],
        "InputName": "BDTGResponse_3",
    }

    # additional cuts applied to data sets

    Bsmass = "&&lab0_MassFitConsD_M >5300 && lab0_MassFitConsD_M <5800"
    HLTcut = "&&((lab0_Hlt1TrackMVADecision_TOS == 1 || "
    "lab0_Hlt1TwoTrackMVADecision_TOS == 1) && "
    "(lab0_Hlt2PhiIncPhiDecision_TOS ==1 || "
    "lab0_Hlt2Topo2BodyDecision_TOS == 1 || lab0_Hlt2Topo3BodyDecision_TOS == 1))"
    # additional cuts applied to data sets
    configdict["AdditionalCuts"] = {}
    configdict["AdditionalCuts"]["All"] = {
        "Data": "lab1_PIDmu<2" + Bsmass + HLTcut,
        "MC": "lab1_M<200&&lab1_PIDK !=-1000.0" + Bsmass,
        "MCID": True,
        "MCTRUEID": True,
        "BKGCAT": True,
        "DsHypo": True,
    }
    configdict["AdditionalCuts"]["KKPi"] = {
        "Data": "lab2_FDCHI2_ORIVX > 2",
        "MC": "lab2_FDCHI2_ORIVX > 2",
    }
    configdict["AdditionalCuts"]["KPiPi"] = {
        "Data": "lab2_FDCHI2_ORIVX > 9",
        "MC": "lab2_FDCHI2_ORIVX > 9",
    }
    configdict["AdditionalCuts"]["PiPiPi"] = {
        "Data": "lab2_FDCHI2_ORIVX > 9",
        "MC": "lab2_FDCHI2_ORIVX > 9",
    }

    # ----------------------------------------------------
    ###             MDfit fitting settings
    # ----------------------------------------------------

    # Bs signal shapes
    configdict["SignalShape"] = {}
    configdict["SignalShape"]["BeautyMass"] = {}
    configdict["SignalShape"]["BeautyMass"]["type"] = "DoubleCrystalBallWithWidthRatio"
    configdict["SignalShape"]["BeautyMass"]["mean"] = {
        "Run2": {"All": 5619.60},
        "Fixed": False,
    }
    configdict["SignalShape"]["BeautyMass"]["sigma1"] = {
        "Run2": {"All": 17.432},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["sigma2"] = {
        "Run2": {"All": 11.210},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["alpha1"] = {
        "Run2": {"All": -2.1016},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["alpha2"] = {
        "Run2": {"All": 2.3520},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n1"] = {
        "Run2": {"All": 2.7904},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["n2"] = {
        "Run2": {"All": 0.61148},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["frac"] = {
        "Run2": {"All": 0.50878},
        "Fixed": True,
    }
    configdict["SignalShape"]["BeautyMass"]["R"] = {
        "Run2": {
            "NonRes": 1.00,
            "PhiPi": 1.0,
            "Kstk": 1.0,
            "KPiPi": 1.0,
            "PiPiPi": 1.0,
        },
        "Fixed": False,
    }

    configdict["CombBkgShape"] = {}
    configdict["CombBkgShape"]["BeautyMass"] = {}
    configdict["CombBkgShape"]["BeautyMass"]["type"] = "Exponential"
    configdict["CombBkgShape"]["BeautyMass"]["cB"] = {
        "Run2": {
            "NonRes": -3.5211e-02,
            "PhiPi": -3.0873e-02,
            "KstK": -2.3392e-02,
            "KPiPi": -1.0361e-02,
            "PiPiPi": -1.5277e-02,
        },
        "Fixed": False,
    }

    # expected yields
    configdict["Yields"] = {}
    configdict["Yields"]["CombBkg"] = {
        "2015": {
            "NonRes": 2000.0,
            "PhiPi": 1000.0,
            "KstK": 1000.0,
            "KPiPi": 1000.0,
            "PiPiPi": 1000.0,
        },
        "2016": {
            "NonRes": 2000.0,
            "PhiPi": 2000.0,
            "KstK": 2000.0,
            "KPiPi": 2000.0,
            "PiPiPi": 2000.0,
        },
        "2017": {
            "NonRes": 4000.0,
            "PhiPi": 2000.0,
            "KstK": 2000.0,
            "KPiPi": 2000.0,
            "PiPiPi": 2000.0,
        },
        "Fixed": False,
    }
    configdict["Yields"]["Signal"] = {
        "2015": {
            "NonRes": 10000.0,
            "PhiPi": 1000.0,
            "KstK": 1000.0,
            "KPiPi": 1000.0,
            "PiPiPi": 1000.0,
        },
        "2016": {
            "NonRes": 20000.0,
            "PhiPi": 2000.0,
            "KstK": 2000.0,
            "KPiPi": 2000.0,
            "PiPiPi": 2000.0,
        },
        "2017": {
            "NonRes": 20000.0,
            "PhiPi": 2000.0,
            "KstK": 2000.0,
            "KPiPi": 2000.0,
            "PiPiPi": 2000.0,
        },
        "Fixed": False,
    }

    # ----------------------------------------------------
    ###             MDfit plotting settings
    # ----------------------------------------------------
    configdict["PlotSettings"] = {}
    configdict["PlotSettings"]["components"] = ["Sig", "CombBkg"]
    configdict["PlotSettings"]["colors"] = [kRed - 7, kOrange]

    configdict["LegendSettings"] = {}
    configdict["LegendSettings"]["BeautyMass"] = {
        "Position": [0.53, 0.60, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.375, 0.875],
        "ScaleYSize": 1.0,
    }
    configdict["LegendSettings"]["CharmMass"] = {
        "Position": [0.20, 0.69, 0.93, 0.93],
        "TextSize": 0.05,
        "LHCbText": [0.8, 0.66],
        "ScaleYSize": 1.0,
        "SetLegendColumns": 2,
        "LHCbTextSize": 0.075,
    }
    configdict["LegendSettings"]["BacPIDK"] = {
        "Position": [0.53, 0.45, 0.90, 0.91],
        "TextSize": 0.05,
        "LHCbText": [0.20, 0.9],
        "ScaleYSize": 1.2,
    }

    return configdict
