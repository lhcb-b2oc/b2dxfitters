###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ROOT import RooFit, RooRealVar

from B2DXFitters import CombBkgPTPdf


def test_CombBkgPTPdf():
    # Parameters for the combinatorial background PDF in time
    CombBkgPdf_a = 0.1209
    CombBkgPdf_f = 0.996
    CombBkgPdf_alpha = 4.149
    CombBkgPdf_beta = 1.139

    time = RooRealVar("time", "time", 0.0, 10.0, "ps")

    combBkgPdf_a = RooRealVar("CombBkgPdf_a", "CombBkgPdf_a", CombBkgPdf_a)
    combBkgPdf_f = RooRealVar("CombBkgPdf_f", "CombBkgPdf_f", CombBkgPdf_f)
    combBkgPdf_alpha = RooRealVar(
        "CombBkgPdf_alpha", "CombBkgPdf_alpha", CombBkgPdf_alpha
    )
    combBkgPdf_beta = RooRealVar("CombBkgPdf_beta", "CombBkgPdf_beta", CombBkgPdf_beta)

    bkgPDF = CombBkgPTPdf(
        "CombBkgPTPdf",
        "CombBkgPTPdf",
        time,
        combBkgPdf_a,
        combBkgPdf_f,
        combBkgPdf_alpha,
        combBkgPdf_beta,
    )

    fr = time.frame()

    bkgPDF.plotOn(fr)
    bkgPDF.paramOn(
        fr,
        RooFit.ShowConstants(),
        RooFit.Layout(0.5, 0.90, 0.85),
        RooFit.Format("NEU", RooFit.AutoPrecision(4)),
    )

    fr.Draw()
