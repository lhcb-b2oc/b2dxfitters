"""
Load the B2DXFitters library
Load the B2DXFitters library,
assuming that the library directory is in $LD_LIBRARY_PATH
"""

from __future__ import absolute_import, division, print_function


from ROOT import gSystem

if "libB2DXFitters" not in gSystem.GetLibraries():
    print("B2DXFitters - INFO: loading B2DXFitters library")
    gSystem.Load("libB2DXFitters.so")

"""

__decorated = False
if not __decorated:
    extra = [
        "BasicMDFitPdf","Bd2DhModels","Bs2Dsh2011TDAnaModels",
        "Bs2DshDsHHHPi0Models","Bs2DshModels",
        "Bs2DssthModels","CombBkgPTPdf","CPObservable",
        "DecayTreeTupleSucksFitter","DecRateCoeff_Bd","DLLTagCombiner",
        "EnsureCintexIsActive","FitMeTool","GeneralModels",
        "GeneralUtils","HistPID1D","HistPID2D","Inverse","Lb2XhModels",
        "MassFitUtils","MCBackground","MDFitterSettings",
        "MistagCalibration","MistagDistribution","PIDCalibrationSample",
        "PlotSettings","PropertimeResolutionModels","RangeAcceptance",
        "RooAbsEffResModel","RooAbsGaussModelEfficiency",
        "RooApollonios","RooBinned1DQuinticBase","RooBinned2DBicubicBase",
        "RooBinnedFun","RooBinnedPdf","RooComplementCoef",
        "RooCubicSplineFun","RooCubicSplineKnot","RooCubicSplinePdf.",
        "RooEffConvGenContext","RooEffHistProd","RooEffResModel",
        "RooGaussEfficiencyModel","RooHILLdini","RooHORNSdini","RooIpatia2",
        "RooJohnsonSU","RooKConvGenContext","RooKResModel",
        "RooSimultaneousResModel","RooSwimmingAcceptance","SFitUtils",
        "SharedArray","SquaredSum","TagDLLToTagDec",
        "TagDLLToTagEta","TaggingCat","WeightingUtils"
    ]

    from P2VV.ROOTDecorators import set_classes_creates
    set_classes_creates(dict((cl, []) for cl in extra))
    __decorated = True
"""
