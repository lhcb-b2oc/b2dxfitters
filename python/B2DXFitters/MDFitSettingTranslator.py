###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from ROOT import TString

from B2DXFitters import MDFitterSettings


# class TranslatorFake(object):
class TranslatorFake:
    def _init_(self):
        name = "MDFitFakeSettings"
        md = MDFitterSettings(name, name)
        md.SetDataCuts("All", "")
        self.mdfit = md
        return self.mdfit


# class Translator(object):
class Translator:
    def __init__(self, myconfigfile, name, full):
        md = MDFitterSettings(name, name)
        years = ["2011", "2012", "2015", "2016", "2017", "2018", "2022"]

        if "dataName" in myconfigfile:
            md.SetConfigFile(myconfigfile["dataName"])

        if "BasicVariables" in myconfigfile:
            names = myconfigfile["BasicVariables"]
            for name in sorted(names):
                if name == "BeautyMass":
                    md.SetMassB(
                        myconfigfile["BasicVariables"][name]["InputName"],
                        name,
                        myconfigfile["BasicVariables"][name]["Range"][0],
                        myconfigfile["BasicVariables"][name]["Range"][1],
                    )
                elif name == "CharmMass":
                    md.SetMassD(
                        myconfigfile["BasicVariables"][name]["InputName"],
                        name,
                        myconfigfile["BasicVariables"][name]["Range"][0],
                        myconfigfile["BasicVariables"][name]["Range"][1],
                    )
                elif name == "BeautyTime":
                    md.SetTime(
                        myconfigfile["BasicVariables"][name]["InputName"],
                        name,
                        myconfigfile["BasicVariables"][name]["Range"][0],
                        myconfigfile["BasicVariables"][name]["Range"][1],
                    )
                elif name == "BeautyTimeErr":
                    md.SetTerr(
                        myconfigfile["BasicVariables"][name]["InputName"],
                        name,
                        myconfigfile["BasicVariables"][name]["Range"][0],
                        myconfigfile["BasicVariables"][name]["Range"][1],
                    )
                elif name == "BacP":
                    md.SetMom(
                        myconfigfile["BasicVariables"][name]["InputName"],
                        name,
                        myconfigfile["BasicVariables"][name]["Range"][0],
                        myconfigfile["BasicVariables"][name]["Range"][1],
                    )
                elif name == "BacPT":
                    md.SetTrMom(
                        myconfigfile["BasicVariables"][name]["InputName"],
                        name,
                        myconfigfile["BasicVariables"][name]["Range"][0],
                        myconfigfile["BasicVariables"][name]["Range"][1],
                    )
                elif name == "BacPIDK":
                    md.SetPIDK(
                        myconfigfile["BasicVariables"][name]["InputName"],
                        name,
                        myconfigfile["BasicVariables"][name]["Range"][0],
                        myconfigfile["BasicVariables"][name]["Range"][1],
                    )
                elif name == "nTracks":
                    md.SetTracks(
                        myconfigfile["BasicVariables"][name]["InputName"],
                        name,
                        myconfigfile["BasicVariables"][name]["Range"][0],
                        myconfigfile["BasicVariables"][name]["Range"][1],
                    )
                elif name == "BacCharge":
                    md.SetID(
                        myconfigfile["BasicVariables"][name]["InputName"],
                        name,
                        myconfigfile["BasicVariables"][name]["Range"][0],
                        myconfigfile["BasicVariables"][name]["Range"][1],
                    )
                elif name == "BDTG":
                    md.SetBDTG(
                        myconfigfile["BasicVariables"][name]["InputName"],
                        name,
                        myconfigfile["BasicVariables"][name]["Range"][0],
                        myconfigfile["BasicVariables"][name]["Range"][1],
                    )
                elif "TagDec" in name or "obsTag" in name:
                    md.AddTagVar(
                        myconfigfile["BasicVariables"][name]["InputName"],
                        name,
                        myconfigfile["BasicVariables"][name]["Range"][0],
                        myconfigfile["BasicVariables"][name]["Range"][1],
                    )
                elif "Mistag" in name or "obsEta" in name:
                    md.AddTagOmegaVar(
                        myconfigfile["BasicVariables"][name]["InputName"],
                        name,
                        myconfigfile["BasicVariables"][name]["Range"][0],
                        myconfigfile["BasicVariables"][name]["Range"][1],
                    )

        else:
            print("[ERROR] You didn't defined any observable!")
            exit(0)

        # MDFitterSettings is not used anymore for tagging calib parameters
        # in runSFit_new. Disabling to prevent KeyErrors
        #
        # if myconfigfile.has_key('TaggingCalibration'):
        #     tags = ["OS", "SS"]
        #     for tag in tags:
        #         if myconfigfile["TaggingCalibration"].has_key(tag):
        #             use = True

        #             if myconfigfile["TaggingCalibration"][tag].has_key("use"):
        #                 use = myconfigfile["TaggingCalibration"][tag]["use"]
        #                 md.SetCalibration(tag,
        #                               myconfigfile["TaggingCalibration"][tag][
        #                               "p0"],
        #                               myconfigfile["TaggingCalibration"][tag][
        #                               "p1"],
        #                               myconfigfile["TaggingCalibration"][tag][
        #                               "average"],
        #                               use)

        if full == True:
            if "ObtainPIDTemplates" in myconfigfile:
                size1 = len(myconfigfile["ObtainPIDTemplates"]["Variables"])
                size2 = len(myconfigfile["ObtainPIDTemplates"]["Bins"])
                if size1 != size2:
                    print(
                        "[ERROR] Size of variables for PID weighting is not "
                        "equal to size bins. Please check if you set a number "
                        "of bins for each variable"
                    )
                    exit(0)
                md.SetWeightingDim(size1)
                for var in myconfigfile["ObtainPIDTemplates"]["Variables"]:
                    md.AddPIDWeightVar(var)
                for bin in myconfigfile["ObtainPIDTemplates"]["Bins"]:
                    md.AddPIDWeightBin(bin)

            if "Calibrations" in myconfigfile:
                year = myconfigfile["Stripping"]
                particle = ["Pion", "Kaon", "Proton", "Combinatorial"]

                for y in year:
                    for p in particle:
                        if p in myconfigfile["Calibrations"][y]:
                            polarity = myconfigfile["Calibrations"][y][p]
                            for Pol in polarity:
                                if Pol == "Down":
                                    pol = TString("down")
                                elif Pol == "Up":
                                    pol = TString("up")
                                else:
                                    pol = TString("unknown")

                                strip = myconfigfile["Stripping"][y]
                                dataName = ""
                                workName = ""
                                pidVarName = ""
                                weightName = ""
                                var1Name = ""
                                var2Name = ""
                                fileName = myconfigfile["Calibrations"][y][p][Pol][
                                    "FileName"
                                ]
                                if "Type" in myconfigfile["Calibrations"][y][p][Pol]:
                                    t = myconfigfile["Calibrations"][y][p][Pol]["Type"]
                                    if t == "Special" or fileName == "":
                                        if (
                                            "WorkName"
                                            in myconfigfile["Calibrations"][y][p][Pol]
                                        ):
                                            workName = myconfigfile["Calibrations"][y][
                                                p
                                            ][Pol]["WorkName"]
                                        if (
                                            "DataName"
                                            in myconfigfile["Calibrations"][y][p][Pol]
                                        ):
                                            dataName = myconfigfile["Calibrations"][y][
                                                p
                                            ][Pol]["DataName"]
                                        if (
                                            "PIDVarName"
                                            in myconfigfile["Calibrations"][y][p][Pol]
                                        ):
                                            pidVarName = myconfigfile["Calibrations"][
                                                y
                                            ][p][Pol]["PIDVarName"]
                                        if (
                                            "WeightName"
                                            in myconfigfile["Calibrations"][y][p][Pol]
                                        ):
                                            weightName = myconfigfile["Calibrations"][
                                                y
                                            ][p][Pol]["WeightName"]
                                        if (
                                            "Variables"
                                            in myconfigfile["Calibrations"][y][p][Pol]
                                        ):
                                            var1Name = myconfigfile["Calibrations"][y][
                                                p
                                            ][Pol]["Variables"][0]
                                            var2Name = myconfigfile["Calibrations"][y][
                                                p
                                            ][Pol]["Variables"][1]
                                        if p == "Combinatorial":
                                            if fileName == "":
                                                md.SetPIDComboShapeFor5Modes()
                                else:
                                    t = ""

                                md.SetCalibSample(
                                    fileName,
                                    workName,
                                    dataName,
                                    p,
                                    y,
                                    pol,
                                    strip,
                                    t,
                                    var1Name,
                                    var2Name,
                                    pidVarName,
                                    weightName,
                                )

        if "IntegratedLuminosity" in myconfigfile:
            for year in years:
                if year in myconfigfile["IntegratedLuminosity"]:
                    md.SetLum(
                        year,
                        myconfigfile["IntegratedLuminosity"][year]["Down"],
                        myconfigfile["IntegratedLuminosity"][year]["Up"],
                    )

        if "AdditionalVariables" in myconfigfile:
            len(myconfigfile["AdditionalVariables"])
            for t in myconfigfile["AdditionalVariables"]:
                args = [myconfigfile["AdditionalVariables"][t]["InputName"]]
                if "OutputName" in myconfigfile["AdditionalVariables"][t]:
                    args.append(myconfigfile["AdditionalVariables"][t]["OutputName"])
                else:
                    args.append(t)
                tR = TString(t)
                if (
                    tR.Contains("DECAY") == False and tR.Contains("decay") == False
                ) and (tR.Contains("DEC") or tR.Contains("dec")):
                    args.append(-1.0)
                    args.append(1.0)
                    args.append("Additional Tagger")
                else:
                    args.append(myconfigfile["AdditionalVariables"][t]["Range"][0])
                    args.append(myconfigfile["AdditionalVariables"][t]["Range"][1])
                    args.append("Additional")
                args.append(t)
                if "Log" in myconfigfile["AdditionalVariables"][t]:
                    args.append(myconfigfile["AdditionalVariables"][t]["Log"])
                md.AddVariable(*args)

        if "AdditionalCuts" in myconfigfile:
            Dmodes = [
                "All",
                "KKPi",
                "KPiPi",
                "PiPiPi",
                "NonRes",
                "KstK",
                "PhiPi",
                "HHHPi0",
            ]
            for Dmode in Dmodes:
                if Dmode in myconfigfile["AdditionalCuts"]:
                    if "Data" in myconfigfile["AdditionalCuts"][Dmode]:
                        md.SetDataCuts(
                            Dmode, myconfigfile["AdditionalCuts"][Dmode]["Data"]
                        )
                    if "MC" in myconfigfile["AdditionalCuts"][Dmode]:
                        md.SetMCCuts(Dmode, myconfigfile["AdditionalCuts"][Dmode]["MC"])
                    if "MCID" in myconfigfile["AdditionalCuts"][Dmode]:
                        md.SetIDCut(
                            Dmode, myconfigfile["AdditionalCuts"][Dmode]["MCID"]
                        )
                    if "MCTRUEID" in myconfigfile["AdditionalCuts"][Dmode]:
                        md.SetTRUEIDCut(
                            Dmode, myconfigfile["AdditionalCuts"][Dmode]["MCTRUEID"]
                        )
                    if "BKGCAT" in myconfigfile["AdditionalCuts"][Dmode]:
                        md.SetBKGCATCut(
                            Dmode, myconfigfile["AdditionalCuts"][Dmode]["BKGCAT"]
                        )
                    if "DsHypo" in myconfigfile["AdditionalCuts"][Dmode]:
                        md.SetDsHypoCut(
                            Dmode, myconfigfile["AdditionalCuts"][Dmode]["DsHypo"]
                        )

        if "WeightingMassTemplates" in myconfigfile:
            md.SetMassWeighting(False)
            if "Shift" in myconfigfile["WeightingMassTemplates"]:
                shift = myconfigfile["WeightingMassTemplates"]["Shift"]
                print(shift)
                for s in shift:
                    if s in years:
                        var = myconfigfile["WeightingMassTemplates"]["Shift"][s]
                        for v in var:
                            md.SetShift(
                                s,
                                myconfigfile["WeightingMassTemplates"]["Shift"][s][v],
                                v,
                            )
                    else:
                        for year in years:
                            md.SetShift(
                                year,
                                myconfigfile["WeightingMassTemplates"]["Shift"][s],
                                s,
                            )

            hists = myconfigfile["WeightingMassTemplates"]
            histName = ""
            for hist in hists:
                if hist != "Shift":
                    if hist != "RatioDataMC":
                        md.SetMassWeighting(True)

                    if "FileLabel" in myconfigfile["WeightingMassTemplates"][hist]:
                        if "Var" in myconfigfile["WeightingMassTemplates"][hist]:
                            var = myconfigfile["WeightingMassTemplates"][hist]["Var"]
                        if "HistName" in myconfigfile["WeightingMassTemplates"][hist]:
                            histName = myconfigfile["WeightingMassTemplates"][hist][
                                "HistName"
                            ]
                        givenYears = myconfigfile["WeightingMassTemplates"][hist][
                            "FileLabel"
                        ]
                        for gY in givenYears:
                            fileName = myconfigfile["WeightingMassTemplates"][hist][
                                "FileLabel"
                            ][gY]
                            md.SetPIDProperties(
                                hist, gY, fileName, var[0], var[1], histName
                            )

                    else:
                        for year in years:
                            if year in myconfigfile["WeightingMassTemplates"][hist]:
                                if (
                                    "Var"
                                    in myconfigfile["WeightingMassTemplates"][hist][
                                        year
                                    ]
                                ):
                                    var = myconfigfile["WeightingMassTemplates"][hist][
                                        year
                                    ]["Var"]
                                if (
                                    "HistName"
                                    in myconfigfile["WeightingMassTemplates"][hist][
                                        year
                                    ]
                                ):
                                    histName = myconfigfile["WeightingMassTemplates"][
                                        hist
                                    ][year]["HistName"]
                                if (
                                    "FileLabel"
                                    in myconfigfile["WeightingMassTemplates"][hist][
                                        year
                                    ]
                                ):
                                    fileName = myconfigfile["WeightingMassTemplates"][
                                        hist
                                    ][year]["FileLabel"]
                                md.SetPIDProperties(
                                    hist, year, fileName, var[0], var[1], histName
                                )

            if "RatioDataMC" in myconfigfile["WeightingMassTemplates"]:
                md.SetRatioDataMC(True)

        if "DsChildrenPrefix" in myconfigfile:
            if "Child1" in myconfigfile["DsChildrenPrefix"]:
                md.SetChildPrefix(0, myconfigfile["DsChildrenPrefix"]["Child1"])
            if "Child2" in myconfigfile["DsChildrenPrefix"]:
                md.SetChildPrefix(1, myconfigfile["DsChildrenPrefix"]["Child2"])
            if "Child3" in myconfigfile["DsChildrenPrefix"]:
                md.SetChildPrefix(2, myconfigfile["DsChildrenPrefix"]["Child3"])

        self.mdfit = md

    def getConfig(self):
        return self.mdfit
