###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys
from pathlib import Path

import ROOT


def _find_lib():
    lib_name = "libB2DXFitters"
    if ROOT.gSystem.FindDynamicLibrary(lib_name, quiet=True):
        return lib_name
    # Check if this is an editable install
    root_dir = Path(__file__).parent / ".." / ".."
    if (root_dir / "standalone" / "CMakeLists.txt").is_file():
        sys.stderr.write("This appears to be an editable install\n")
        build_lib = str(root_dir / "standalone" / "build" / lib_name)
        if ROOT.gSystem.FindDynamicLibrary(build_lib, quiet=True):
            sys.stderr.write(f"Will load {build_lib}\n")
            return build_lib
        sys.stderr.write("Failed to find library!\n")
        sys.stderr.write("You probably need to build the shape classes using:\n")
        sys.stderr.write("  $ mkdir standalone/build\n")
        sys.stderr.write("  $ cd standalone/build\n")
        sys.stderr.write(
            "  $ cmake .. -DPROJECT_VERSION=0.0.0 "
            "-DCMAKE_INSTALL_PREFIX=$CONDA_PREFIX -DCMAKE_BUILD_TYPE=Release\n"
        )
        sys.stderr.write("  $ make\n")
    raise ImportError(f"Failed to load libB2DXFitters ({lib_name})")

    # return os.path.join(
    #     "_skbuild",
    #     "{}-{}".format(_SKBUILD_PLAT_NAME, ".".join(map(str, sys.version_info[:2]))),
    # )


if ROOT.gSystem.Load(_find_lib()) < 0:
    raise NotImplementedError()

from ROOT import (
    Bs2Dsh2011TDAnaModels,
    CombBkgPTPdf,
    CPObservable,
    DecRateCoeff_Bd,
    DLLTagCombiner,
    FitMeTool,
    GeneralModels,
    GeneralUtils,
    HistPID1D,
    HistPID2D,
    Inverse,
    MCBackground,
    MDFitterSettings,
    MistagCalibration,
    MistagDistribution,
    PIDCalibrationSample,
    PlotSettings,
    RangeAcceptance,
    RooAbsGaussModelEfficiency,
    RooApollonios,
    RooBinned1DQuintic,
    RooBinned1DQuinticPdf,
    RooBinned2DBicubic,
    RooBinned2DBicubicPdf,
    RooBinnedFun,
    RooBinnedPdf,
    RooComplementCoef,
    RooCubicSplineFun,
    RooCubicSplineKnot,
    RooCubicSplinePdf,
    RooEffConvGenContext,
    RooEffHistProd,
    RooEffResModel,
    RooGaussEfficiencyModel,
    RooHILLdini,
    RooHORNSdini,
    RooIpatia2,
    RooJohnsonSU,
    RooKConvGenContext,
    RooKResModel,
    RooSimultaneousResModel,
    RooSwimmingAcceptance,
    SquaredSum,
    TagDLLToTagDec,
    TagDLLToTagEta,
    TaggingCat,
)
