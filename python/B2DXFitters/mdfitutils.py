###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from ROOT import RooDataSet, RooRealVar, TString

from B2DXFitters import Bs2Dsh2011TDAnaModels, GeneralUtils

# from B2DXFitters import *
from B2DXFitters.WS import WS as WS


def getBins(myconfigfile, bins, debug):
    binning = myconfigfile["Binning"]
    ranges = {}
    mean = {}
    j = 0
    for var in binning:
        ranges[var] = []
        mean[var] = []
        nb = len(myconfigfile["Binning"][var])
        m = -1.0
        for i in range(0, nb - 1):
            rd = myconfigfile["Binning"][var][i]
            ru = myconfigfile["Binning"][var][i + 1]
            ranges[var].append([rd, ru])
            m = (ru - rd) / 2.0
            m = rd + m
            mean[var].append(m)
        print(bins[j])
        myconfigfile["Binning"][var] = {}
        myconfigfile["Binning"][var]["Bins"] = ranges[var]
        myconfigfile["Binning"][var]["Mean"] = mean[var][bins[j]]
        j = j + 1

    if debug:
        for var in binning:
            print(
                "[INFO] Binning for var:",
                var,
                " binnsing is: ",
                myconfigfile["Binning"][var]["Bins"],
            )
            print(
                "[INFO] Mean value for var:",
                var,
                " binning is: ",
                myconfigfile["Binning"][var]["Mean"],
            )

    return myconfigfile


def getCombinedData(workspace, decay, mc, mode, pol, year, debug):
    if debug:
        print("[INFO] getCombinedData(...)")
        print("[INFO] workspace: %s" % (workspace.GetName()))
        print(
            "[INFO] decay: %s, mode: %s, sample: %s, year: %s"
            % (decay, mode, pol, year)
        )
        if mc:
            print("[INFO] You are combining MC data sets")

    name_prefix = "dataSet{}_{}_".format("MC" if mc else "", decay)

    smys = getSampleModeYear(mode, pol, year, debug)

    data = None
    for smy in smys:
        name = name_prefix + smy.Data()
        if data == None:
            # use copy constructor here!
            data = RooDataSet(workspace.data(name), name + "_comb")
        else:
            data.append(workspace.data(name))

    return data


def getExpectedValue(var, par, year, dsmode, pol, myconfigfile):
    # print var, par, year, dsmode
    if type(myconfigfile[var][par][year.Data()][dsmode.Data()]) == float:
        return myconfigfile[var][par][year.Data()][dsmode.Data()]
    elif type(myconfigfile[var][par][year.Data()][dsmode.Data()]) == list:
        return myconfigfile[var][par][year.Data()][dsmode.Data()][0]
    elif type(myconfigfile[var][par][year.Data()][dsmode.Data()][pol.Data()]) == float:
        return myconfigfile[var][par][year.Data()][dsmode.Data()][pol.Data()]
    elif type(myconfigfile[var][par][year.Data()][dsmode.Data()][pol.Data()]) == list:
        return myconfigfile[var][par][year.Data()][dsmode.Data()][pol.Data()][0]
    else:
        print("Can't obtain the expected value.")
    exit(0)


def getRangeVar(var, par, year, dsmode, pol, myconfigfile):
    "Returns the range of variable if this is specified as a list"
    if type(myconfigfile[var][par][year.Data()][dsmode.Data()]) == float:
        return None
    elif type(myconfigfile[var][par][year.Data()][dsmode.Data()]) == list:
        rangeList = myconfigfile[var][par][year.Data()][dsmode.Data()]
        if len(rangeList) == 3:
            return rangeList
        else:
            return None
    elif type(myconfigfile[var][par][year.Data()][dsmode.Data()][pol.Data()]) == float:
        return None
    elif type(myconfigfile[var][par][year.Data()][dsmode.Data()][pol.Data()]) == list:
        rangeList = myconfigfile[var][par][year.Data()][dsmode.Data()][pol.Data()]
        if len(rangeList) == 3:
            return rangeList
        else:
            return None
    else:
        print("Error in obtaining the range")
    exit(0)


def getYield(mode, year, dsmode, pol, merge, myconfigfile):
    # Yield is a float like ...{"PhiPi" : 50.0}
    if type(myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]) == float:
        return myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]
    # Yield has a range or an error
    elif type(myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]) == list:
        return myconfigfile["Yields"][mode][year.Data()][dsmode.Data()][0]
    # Yield is further nested in pol like ...{"PhiPi" : {"Up":50.0,"Down":50.0}}
    elif "pol" in merge:
        if (
            "Up" in myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]
            and "Down" in myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]
        ):
            return (
                myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]["Up"]
                + myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]["Down"]
            )
        elif "Both" in myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]:
            return myconfigfile["Yields"][mode][year.Data()][dsmode.Data()]["Both"]
        else:
            print("[ERROR] Polarity set for merging, no input data")
            exit(-1)
    # elif p.Data() in myconfigfile["Yields"][mode][y.Data()][dsmode.Data()]:
    #     return myconfigfile["Yields"][mode][y.Data()][dsmode.Data()][p.Data()]
    else:
        return None


def getConstrainedIfSoConfigured(var, par, obs, mode, YieldVar, myconfigfile):
    "Check variable is Gaussian Constrained"
    if YieldVar:
        varsDict = myconfigfile[par]
    else:
        varsDict = myconfigfile[par][obs]
    if "Constrained" in varsDict[mode]:
        if varsDict[mode]["Constrained"]:
            print(
                "[INFO] Parameter: %s is Gaussian Contrained "
                "in fit" % (var.GetName())
            )
            return True
        else:
            return False
    else:
        print(
            "[ERROR] Unknown if var %s is Gaussian Constrained, "
            "assume it isn't." % (var.GetName())
        )
        return False


def getErrorConstrained(var, mode, myconfigfile):
    "Obtain Error on Gaussian Constrained variable if configured"
    if type(mode) == list:
        if len(mode) == 2:
            varError = mode[1]
            return varError
        else:
            print("[ERROR] Unknown error, %s is set constant" % (var.GetName()))
            var.SetConstant()
            return None
    else:
        print("[ERROR] Unknown error, %s is set constant" % (var.GetName()))
        var.SetConstant()
        return None


def getExpectedYield(mode, year, dsmode, pol, mergeList, myconfigfile):
    p = GeneralUtils.CheckPolarityCapital(TString(pol), False)
    y = GeneralUtils.CheckDataYearCapital(TString(year), False)
    yields = 0.0

    for me in mergeList:
        if (
            (me.find("20152016") > -1)
            or (me.find("20152017") > -1)
            or (me.find("20152018") > -1)
            or (me.find("20162017") > -1)
            or (me.find("20162018") > -1)
            or (me.find("20172018") > -1)
        ):
            run = []
            if "20152016" in mergeList:
                run = ["2015", "2016"]
            elif "20152017" in mergeList:
                run = ["2015", "2017"]
            elif "20152018" in mergeList:
                run = ["2015", "2018"]
            elif "20162017" in mergeList:
                run = ["2016", "2017"]
            elif "20162018" in mergeList:
                run = ["2016", "2018"]
            elif "20172018" in mergeList:
                run = ["2017", "2018"]
            if run != None:
                for r in run:
                    if r in myconfigfile["Yields"][mode]:
                        yields += getYield(
                            mode, TString(r), dsmode, p, mergeList, myconfigfile
                        )

    if "run1" or "run2" or "runs" in mergeList:
        if y.Data() in myconfigfile["Yields"][mode]:
            yields = getYield(mode, y, dsmode, p, mergeList, myconfigfile)
        else:
            Run1 = ["2011", "2012"]
            Run2 = ["2015", "2016", "2017", "2018"]
            if y == "Run1":
                for r in Run1:
                    for my in mergeList:
                        if my == r:
                            if my in myconfigfile["Yields"][mode]:
                                yields += getYield(
                                    mode,
                                    TString(my),
                                    dsmode,
                                    p,
                                    mergeList,
                                    myconfigfile,
                                )
            elif y == "Run2":
                for r in Run2:
                    for my in mergeList:
                        if my == r:
                            if my in myconfigfile["Yields"][mode]:
                                yields += getYield(
                                    mode,
                                    TString(my),
                                    dsmode,
                                    p,
                                    mergeList,
                                    myconfigfile,
                                )
            elif y == "Runs":
                Runs = Run1 + Run2
                for r in Runs:
                    for my in mergeList:
                        if my == r:
                            if my in myconfigfile["Yields"][mode]:
                                yields += getYield(
                                    mode,
                                    TString(my),
                                    dsmode,
                                    p,
                                    mergeList,
                                    myconfigfile,
                                )
    else:
        if year != "":
            yields = getYield(mode, TString(year), dsmode, p, mergeList, myconfigfile)
        elif year == "run1":
            print(
                "[ERROR] Merge option tells that "
                "polarities should be merged, "
                "while magnet polarity is %s" % (pol)
            )
            exit(0)
        else:
            print("[ERROR] Wrong year: %s" % (year))
            exit(0)

    return yields


def setConstantIfSoConfigured(var, par, mode, dmode, pol, myconfigfile):
    print(myconfigfile[par][mode])
    print(myconfigfile[par][mode]["Fixed"])
    if type(myconfigfile[par][mode]["Fixed"]) == bool:
        if myconfigfile[par][mode]["Fixed"] == True:
            var.setConstant()
            print(
                "[INFO] Parameter: %s set to be constant with value %lf"
                % (var.GetName(), var.getValV())
            )
        else:
            print(
                "[INFO] Parameter: %s floats in the fit in range(%lf,%lf)"
                % (var.GetName(), var.getMin(), var.getMax())
            )
    elif dmode in myconfigfile[par][mode]["Fixed"]:
        if myconfigfile[par][mode]["Fixed"][dmode]:
            var.setConstant()
            print(
                "[INFO] Parameter: %s set to be constant with value %lf"
                % (var.GetName(), var.getValV())
            )
        else:
            print(
                "[INFO] Parameter: %s floats in the fit in range(%lf,%lf)"
                % (var.GetName(), var.getMin(), var.getMax())
            )
    else:
        print(
            "[INFO] Parameter: %s floats in the fit in range(%lf,%lf)"
            % (var.GetName(), var.getMin(), var.getMax())
        )


def getObservables(MDSettings, workData, debug):
    #    if (not toys ):
    observables = MDSettings.GetObsSet(False, True, True, True, True, True)
    #    else:
    #        observables = MDSettings.GetObsSet( \
    # False,True,True,True,False, False)

    #    if MDSettings.CheckTagVar() == True:
    #        tagDecCombName = TString("tagDecComb")
    #        tagDecComb = GeneralUtils.GetCategory( \
    # workData, tagDecCombName, debug)
    #        tagOmegaCombName= TString("tagOmegaComb")
    #        tagOmegaComb = GeneralUtils.GetObservable( \
    # workData, tagOmegaCombName, debug)

    #        observables.add(tagDecComb)
    #        observables.add(tagOmegaComb)

    if debug:
        observables.Print("v")

    return observables


def readVariables(myconfigfile, binning, label, prefix, workInt, debug):
    # sm, merge, bound,
    #              debug):

    t = TString("_")
    variables = []

    names = []

    labelTS = TString(label)
    print(labelTS)
    if label in myconfigfile:
        if myconfigfile[label]["type"] != "RooKeysPdf":
            var = myconfigfile[label]
            for v in var:
                if (
                    v != "type"
                    and v != "scaleSigma"
                    and v != "components"
                    and v != "name"
                    and v != "decay"
                ):
                    TString(label)
                    year = myconfigfile[label][v]
                    for y in year:
                        if y != "Fixed" and y != "Constrained":
                            mode = myconfigfile[label][v][y]
                            for m in mode:
                                pol = myconfigfile[label][v][y][m]
                                if type(pol) == float:
                                    pol = ["both"]
                                elif type(pol) == list:
                                    pol = ["both"]

                                for p in pol:
                                    pp = GeneralUtils.CheckPolarity(TString(p), False)
                                    yy = GeneralUtils.CheckDataYear(TString(y), False)
                                    mm = GeneralUtils.CheckDMode(TString(m), False)
                                    if mm == "":
                                        mm = GeneralUtils.CheckKKPiMode(
                                            TString(m), False
                                        )
                                    if m == "All":
                                        mm = "all"
                                    if yy == "run1":
                                        yr2 = TString("Run1")
                                    else:
                                        yr2 = TString(y)
                                    if pp == "both":
                                        p2 = TString("Both")
                                    else:
                                        p2 = TString(p)
                                    t = TString("_")
                                    ssmm = t + pp + t + mm + t + yy

                                    nameVar = TString(prefix) + t + v + ssmm

                                    if nameVar in names:
                                        continue
                                    else:
                                        names.append(nameVar)

                                    if type(myconfigfile[label][v][y][m]) == dict:
                                        p0 = 0.0
                                        p1 = 0.0
                                        p2 = 0.0
                                        print(
                                            "[INFO] Polynomial parametrisation "
                                            "of variable"
                                        )
                                        if "p0" in myconfigfile[label][v][y][m]:
                                            p0 = myconfigfile[label][v][y][m]["p0"]
                                        if "p0" in myconfigfile[label][v][y][m]:
                                            p1 = myconfigfile[label][v][y][m]["p1"]
                                        if "p0" in myconfigfile[label][v][y][m]:
                                            p2 = myconfigfile[label][v][y][m]["p2"]
                                        if "var" in myconfigfile[label][v][y][m]:
                                            var = myconfigfile[label][v][y][m]["var"]
                                        meanval = binning[var]["Mean"]
                                        expectedValue = (
                                            p0 + meanval * p1 + meanval * p2 * p2
                                        )
                                    else:
                                        expectedValue = getExpectedValue(
                                            label, v, yr2, TString(m), p2, myconfigfile
                                        )
                                    vTS = TString(v)
                                    if vTS.Contains("frac"):
                                        boundDown = 0.0
                                        boundUp = 1.0
                                    elif vTS.Contains("sigma"):
                                        boundDown = expectedValue - 0.5 * expectedValue
                                        boundUp = expectedValue + 0.8 * expectedValue
                                    elif vTS.Contains("mean"):
                                        boundDown = expectedValue - 50.0
                                        boundUp = expectedValue + 50.0
                                    elif type(expectedValue) in [list, tuple]:
                                        (
                                            expectedValue,
                                            boundUp,
                                            boundDown,
                                        ) = expectedValue
                                    else:
                                        if expectedValue < 0.0:
                                            boundUp = 0.0
                                            boundDown = expectedValue * 2.0
                                        else:
                                            boundDown = 0.0

                                            boundUp = expectedValue * 2.0
                                    rangeVar = None
                                    if type(myconfigfile[label][v][y][m]) != dict:
                                        rangeVar = getRangeVar(
                                            label, v, yr2, TString(m), p2, myconfigfile
                                        )
                                    if rangeVar != None:
                                        boundDown = rangeVar[1]
                                        boundUp = rangeVar[2]

                                    if "scaleSigma" in myconfigfile[label] and (
                                        v == "sigma1" or v == "sigma2"
                                    ):
                                        f1 = myconfigfile[label]["scaleSigma"][
                                            yr2.Data()
                                        ]["frac1"]
                                        f2 = myconfigfile[label]["scaleSigma"][
                                            yr2.Data()
                                        ]["frac2"]

                                        expectedValue2 = None

                                        if v == "sigma1":
                                            expectedValue2 = getExpectedValue(
                                                label,
                                                "sigma2",
                                                yr2,
                                                TString(m),
                                                p2,
                                                myconfigfile,
                                            )
                                        elif v == "sigma2":
                                            expectedValue2 = getExpectedValue(
                                                label,
                                                "sigma1",
                                                yr2,
                                                TString(m),
                                                p2,
                                                myconfigfile,
                                            )

                                        oldvalue = expectedValue
                                        if expectedValue2 != None:
                                            if expectedValue > expectedValue2:
                                                f = f1
                                            else:
                                                f = f2
                                        else:
                                            return Exception(
                                                "v is not equal "
                                                "to either sigma1 nor sigma2"
                                            )
                                        expectedValue = expectedValue * f
                                        print(
                                            "[INFO] Change value "
                                            "%s to %s = %s * %s"
                                            % (
                                                str(oldvalue),
                                                str(expectedValue),
                                                str(oldvalue),
                                                str(f),
                                            )
                                        )

                                    variables.append(
                                        WS(
                                            workInt,
                                            RooRealVar(
                                                nameVar.Data(),
                                                nameVar.Data(),
                                                expectedValue,
                                                boundDown,
                                                boundUp,
                                            ),
                                        )
                                    )
                                    setConstantIfSoConfigured(
                                        variables[variables.__len__() - 1],
                                        label,
                                        v,
                                        m,
                                        p,
                                        myconfigfile,
                                    )
        # else:
        #    print "[ERROR] Cannot find the label: %s.
        # Please specify in config file"%(label)
        #    exit(0)

    return workInt


def collectGaussianConstraints(
    myconfigfile, label, prefix, workInt, merge, bound, obsList, sample_list
):
    constraintsList = []
    names = []
    for i in range(0, bound):
        if label not in myconfigfile:
            continue
        for obs in obsList:
            YieldVar = False
            if obs not in myconfigfile[label]:
                if prefix == "n":
                    YieldVar = True
                else:
                    continue
            if YieldVar:
                var = myconfigfile[label]
            else:
                var = myconfigfile[label][obs]
            for v in var:
                if (
                    v == "type"
                    or v == "scaleSigma"
                    or v == "components"
                    or v == "name"
                    or v == "decay"
                ):
                    continue
                year = var[v]
                for y in year:
                    if y == "Fixed" or y == "Constrained" or y == "Rel":
                        continue
                    mode = year[y]
                    pol = []
                    for m in mode:
                        if "up" in sample_list and "down" in sample_list:
                            pol = ["both"]
                        elif "up" in sample_list:
                            pol = ["up"]
                        elif "down" in sample_list:
                            pol = ["down"]

                        for p in pol:
                            pp = GeneralUtils.CheckPolarity(TString(p), False)
                            yy = GeneralUtils.CheckDataYear(TString(y), False)
                            mm = GeneralUtils.CheckDMode(TString(m), False)
                            if mm == "":
                                mm = GeneralUtils.CheckKKPiMode(TString(m), False)
                            if m == "All":
                                mm = "all"
                            else:
                                TString(p)
                            t = TString("_")
                            ssmm = t + pp + t + mm + t + yy

                            nameVar = TString(prefix) + t + v + ssmm
                            if YieldVar:
                                nameVar = TString(prefix) + v + ssmm + "_Evts"
                            if nameVar in names:
                                continue
                            else:
                                names.append(nameVar)

                            if "Constrained" in year:
                                constrainedVar = workInt.var(nameVar.Data())
                                if constrainedVar == None:
                                    print(
                                        "{} not found. Continue...".format(
                                            nameVar.Data()
                                        )
                                    )
                                    continue
                                constrainedOrNot = getConstrainedIfSoConfigured(
                                    constrainedVar,
                                    label,
                                    obs,
                                    v,
                                    YieldVar,
                                    myconfigfile,
                                )
                                if constrainedOrNot:
                                    constrainedError = getErrorConstrained(
                                        constrainedVar, mode[m], myconfigfile
                                    )
                                    if constrainedError != None:
                                        constrainedVar.setError(constrainedError)
                                        constraintsList.append(constrainedVar)
                                    else:
                                        print(
                                            "No error found. "
                                            "Gaussian Constraint cancelled."
                                        )
    return constraintsList


def getPDFNameFromConfig(myconfigfile, key, var, pdfN, pdfK):
    t = TString("_")
    names = []
    if key in myconfigfile:
        if "name" in myconfigfile[key]:
            year = myconfigfile[key]["name"]
            print(year)
            for y in year:
                mode = myconfigfile[key]["name"][y]
                print(mode)
                for m in mode:
                    pol = myconfigfile[key]["name"][y][m]
                    print(pol)
                    if type(pol) == float:
                        pol = ["both"]
                    for p in pol:
                        pp = GeneralUtils.CheckPolarity(TString(p), False)
                        yy = GeneralUtils.CheckDataYear(TString(y), False)
                        mm = GeneralUtils.CheckDMode(TString(m), False)
                        if mm == "":
                            mm = GeneralUtils.CheckKKPiMode(TString(m), False)

                        if (
                            myconfigfile[key]["name"][y][m] == "Both"
                            or myconfigfile[key]["name"][y][m] == "Up"
                            or myconfigfile[key]["name"][y][m] == "Down"
                        ):
                            namepdf = myconfigfile[key]["name"][y][m][p]
                        else:
                            namepdf = myconfigfile[key]["name"][y][m]
                        name = var + t + pp + t + mm + t + yy
                        if name in names:
                            continue
                        else:
                            names.append(name)
                        pdfN = GeneralUtils.AddToList(pdfN, namepdf)
                        pdfK = GeneralUtils.AddToList(pdfK, name)

    return pdfN, pdfK


def getType(myconfigfile, key):
    if key in myconfigfile:
        if "type" in myconfigfile[key]:
            return TString(myconfigfile[key]["type"])
    return TString("None")


def getPIDKComponents(myconfigfile, key, component):
    if key in myconfigfile:
        if "components" in myconfigfile[key]:
            if myconfigfile[key]["components"][component] == True:
                return "true"
            else:
                return "false"
    return "none"


def getSigOrCombPDF(
    myconfigfile, keys, typemode, work, workInt, sm, merge, bound, dim, obs, debug
):
    beautyMass = obs[0]
    charmMass = obs[1]
    bacPIDK = obs[2]
    if dim > 0:
        prefix1 = typemode + "_" + beautyMass.GetName()
        workInt = readVariables(
            myconfigfile,
            myconfigfile["Binning"],
            keys[0],
            prefix1,
            workInt,
            debug
            # myconfigfile, keys[0], prefix1, workInt,
            # sm, merge, bound, debug
        )
    if dim > 1:
        charmMass = obs[1]
        prefix2 = typemode + "_" + charmMass.GetName()
        workInt = readVariables(
            myconfigfile,
            myconfigfile["Binning"],
            keys[1],
            prefix2,
            workInt,
            debug
            # myconfigfile, keys[1], prefix2, workInt,
            # sm, merge, bound, debug
        )
    if dim > 2:
        bacPIDK = obs[2]
        prefix3 = typemode + "_" + bacPIDK.GetName()
        workInt = readVariables(
            myconfigfile,
            myconfigfile["Binning"],
            keys[2],
            prefix3,
            workInt,
            debug
            # myconfigfile, keys[2], prefix3, workInt,
            # sm, merge, bound, debug
        )

    typeBs = getType(myconfigfile, keys[0])
    typeDs = getType(myconfigfile, keys[1])
    typePIDK = getType(myconfigfile, keys[2])

    types = GeneralUtils.GetList(typeBs, typeDs, typePIDK)

    if dim > 2:
        nameKaon = getPIDKComponents(myconfigfile, keys[2], "Kaon")
        namePion = getPIDKComponents(myconfigfile, keys[2], "Pion")
        nameProton = getPIDKComponents(myconfigfile, keys[2], "Proton")
        pidkNames = GeneralUtils.GetList(nameKaon, namePion, nameProton)
    else:
        pidkNames = GeneralUtils.GetList(
            TString("None"), TString("None"), TString("None")
        )
    decay = TString(myconfigfile["Decay"])

    combEPDF = []
    for i in range(0, bound):
        pdfN = GeneralUtils.GetList(TString("pdf_names"))
        pdfK = GeneralUtils.GetList(TString("pdf_keys"))

        if dim > 0:
            pdfN, pdfK = getPDFNameFromConfig(
                myconfigfile, keys[0], prefix1, pdfN, pdfK
            )
        if dim > 1:
            pdfN, pdfK = getPDFNameFromConfig(
                myconfigfile, keys[1], prefix2, pdfN, pdfK
            )
        if dim > 2:
            pdfN, pdfK = getPDFNameFromConfig(
                myconfigfile, keys[2], prefix3, pdfN, pdfK
            )

        if debug:
            GeneralUtils.printList(pdfN)
            GeneralUtils.printList(pdfK)

        EPDF = Bs2Dsh2011TDAnaModels.build_SigOrCombo(
            beautyMass,
            charmMass,
            bacPIDK,
            work,
            workInt,
            sm[i],
            TString(typemode),
            merge,
            decay,
            types,
            pdfN,
            pdfK,
            pidkNames,
            dim,
            debug,
        )

        t = TString(keys[0])
        if t.Contains("Signal"):
            combEPDF.append(WS(workInt, EPDF))
        else:
            combEPDF.append(EPDF)

    return combEPDF, workInt


# ------------------------------------------------------------------------------
def getBkgTypes(myconfigfile, decay, obsName, debug):
    t = "_"
    types = GeneralUtils.GetList(TString("pdf_types"))
    bkgs = GeneralUtils.GetList(TString("bkgs"))

    if "Backgrounds" in myconfigfile:
        modes = myconfigfile["Backgrounds"]
        for mode in modes:
            bkgs = GeneralUtils.AddToList(bkgs, TString(mode))
            if mode + "Shape" in myconfigfile:
                for name in obsName:
                    if name in myconfigfile[mode + "Shape"]:
                        if "type" in myconfigfile[mode + "Shape"][name]:
                            namepdf = mode
                            namepdf = namepdf + t + name + t
                            namepdf = (
                                namepdf + myconfigfile[mode + "Shape"][name]["type"]
                            )
                            types = GeneralUtils.AddToList(types, namepdf)
                    else:
                        modeTS = TString(mode)
                        decayTS = TString(decay)
                        if name == "BeautyMass":
                            namepdf = modeTS + t + name + t + "RooKeysPdf"
                        elif name == "BacPIDK":
                            if (decayTS.Contains("DsK") and modeTS.Contains("DsK")) or (
                                decayTS.Contains("DsPi") and modeTS.Contains("DsPi")
                            ):
                                namepdf = modeTS + t + name + t + "Signal"
                            else:
                                namepdf = modeTS + t + name + t + "Fixed"
                        elif name == "CharmMass":
                            if decayTS.Contains("Dsst") and modeTS.Contains("Ds"):
                                namepdf = modeTS + t + name + t + "RooKeysPdf"
                            elif decayTS.Contains("Ds") and modeTS.Contains("Ds"):
                                namepdf = modeTS + t + name + t + "Signal"
                            elif decayTS.Contains("Lc") and modeTS.Contains("Lc"):
                                namepdf = modeTS + t + name + t + "Signal"
                            elif (
                                decayTS.Contains("D") and not decayTS.Contains("Ds")
                            ) and (modeTS.Contains("Dst")):
                                namepdf = modeTS + t + name + t + "Signal"
                            elif (
                                decayTS.Contains("D") and not decayTS.Contains("Ds")
                            ) and (modeTS.Contains("D") and not modeTS.Contains("Dst")):
                                namepdf = modeTS + t + name + t + "Signal"
                            else:
                                namepdf = modeTS + t + name + t + "RooKeysPdf"
                        types = GeneralUtils.AddToList(types, namepdf)

            else:
                mode = TString(mode)
                decay = TString(decay)
                for name in obsName:
                    if name == "BeautyMass":
                        namepdf = mode + t + name + t + "RooKeysPdf"
                    elif name == "BacPIDK":
                        if (decay.Contains("DsK") and mode.Contains("DsK")) or (
                            decay.Contains("DsPi") and mode.Contains("DsPi")
                        ):
                            namepdf = mode + t + name + t + "Signal"
                        else:
                            namepdf = mode + t + name + t + "Fixed"
                    elif name == "CharmMass":
                        if decay.Contains("Dsst") and mode.Contains("Ds"):
                            namepdf = mode + t + name + t + "RooKeysPdf"
                        elif decay.Contains("Ds") and mode.Contains("Ds"):
                            namepdf = mode + t + name + t + "Signal"
                        elif decay.Contains("Lc") and mode.Contains("Lc"):
                            namepdf = mode + t + name + t + "Signal"
                        elif (
                            (decay.Contains("D")) and not (decay.Contains("Ds"))
                        ) and (mode.Contains("Dst")):
                            namepdf = mode + t + name + t + "Signal"
                        elif (
                            (decay.Contains("D")) and not (decay.Contains("Ds"))
                        ) and (mode.Contains("D") and not mode.Contains("Dst")):
                            namepdf = mode + t + name + t + "Signal"
                        else:
                            namepdf = mode + t + name + t + "RooKeysPdf"

                types = GeneralUtils.AddToList(types, namepdf)

    else:
        print(
            "-----------------------------------------------"
            "------------------------------------------------"
        )
        print("[ERROR] New MDFit requires defintion of basic backgrounds, " "example:")
        print('configdict["Backgrounds"] = ["Bs2DsPi","Bd2DPi"]')
        exit(0)

    if debug:
        print("-------------------------------------")
        print("Background PDFs")
        print("-------------------------------------")
        GeneralUtils.printList(types)

    return types, bkgs


# ------------------------------------------------------------------------------
def getShapeTypes(myconfigfile, obsName, debug):
    t = "_"

    modes = myconfigfile["Yields"]
    types = GeneralUtils.GetList(TString("pdf_types"))

    check = []

    for mode in modes:
        if mode == "Combinatorial":
            mode = "CombBkg"

        if mode + "Shape" in myconfigfile:
            for name in obsName:
                if name in myconfigfile[mode + "Shape"]:
                    if "type" in myconfigfile[mode + "Shape"][name]:
                        namepdf = mode
                        namepdf = namepdf + t + name + t
                        namepdf = namepdf + myconfigfile[mode + "Shape"][name]["type"]
                        types = GeneralUtils.AddToList(types, namepdf)
                        check.append(namepdf)

    ### Backwards compatibiliy, old fashion config file
    keysSig = ["BsSignalShape", "DsSignalShape", "PIDKSignalShape"]
    keysComb = [
        "BsCombinatorialShape",
        "DsCombinatorialShape",
        "PIDKCombinatorialShape",
    ]
    if (
        "BsSignalShape" in myconfigfile
        or "DsSignalShape" in myconfigfile
        or "PIDKSignalShape" in myconfigfile
    ):
        print(
            "--------------------------------------------"
            "---------------------------------------------------"
        )
        print(
            "[WARNING] You are using old fashion shape declaration. "
            "It is not as flexible as the new."
        )
        print(
            "[WARNING] Please consider using configdict"
            '["SignalShape"]["BeautyMass"]["type"] instead'
        )
        print(
            "-------------------------------------------"
            "----------------------------------------------------"
        )
        j = 0
        for name in obsName:
            namepdf = "Signal_" + name
            if keysSig[j] in myconfigfile:
                if "type" in myconfigfile[keysSig[j]]:
                    namepdf = namepdf + "_" + myconfigfile[keysSig[j]]["type"]
                    if namepdf not in check:
                        types = GeneralUtils.AddToList(types, namepdf)
                        check.append(namepdf)
                    else:
                        print(
                            "[ERROR] type of PDF already added. "
                            "Unify your config file!"
                        )
                        exit(0)
            j = j + 1
    if (
        "BsCombinatorialShape" in myconfigfile
        or "DsCombinatorialShape" in myconfigfile
        or "PIDKCombinatorialShape" in myconfigfile
    ):
        print(
            "-----------------------------------------------------------"
            "------------------------------------"
        )
        print(
            "[WARNING] You are using old fashion shape declaration. "
            "It is not as flexible as the new."
        )
        print(
            "[WARNING] Please consider using configdict"
            '["CombinatorialShape"]["BeautyMass"]["type"] instead'
        )
        print(
            "-------------------------------------------------------------"
            "----------------------------------"
        )
        j = 0
        for name in obsName:
            namepdf = "CombBkg_" + name
            if keysComb[j] in myconfigfile:
                if "type" in myconfigfile[keysComb[j]]:
                    namepdf = namepdf + "_" + myconfigfile[keysComb[j]]["type"]
                    if namepdf not in check:
                        print(namepdf)
                        types = GeneralUtils.AddToList(types, namepdf)
                        check.append(namepdf)
                    else:
                        print(
                            "[ERROR] type of PDF already added. "
                            "Unify your config file!"
                        )
                        exit(0)
            j = j + 1
    if debug:
        print("-------------------------------------")
        print("Analytical PDFs")
        print("-------------------------------------")
        GeneralUtils.printList(types)

    return types


# ------------------------------------------------------------------------------
def readVariablesForShapes(myconfigfile, workInt, obsName, debug):
    newSignal = True
    newCombo = True
    keysSig = ["BsSignalShape", "DsSignalShape", "PIDKSignalShape"]
    keysComb = [
        "BsCombinatorialShape",
        "DsCombinatorialShape",
        "PIDKCombinatorialShape",
    ]

    if "Binning" not in myconfigfile:
        myconfigfile["Binning"] = {}

    if "Backgrounds" not in myconfigfile:
        print(
            "-------------------------------------------------------"
            "----------------------------------------"
        )
        print("[ERROR] New MDFit requires defintion of basic backgrounds, " "example:")
        print('configdict["Backgrounds"] = ["Bs2DsPi","Bd2DPi"]')
        exit(0)

    backgrounds = myconfigfile["Backgrounds"] + ["Signal", "CombBkg"]
    for bkg in backgrounds:
        if bkg == "Signal":
            ### Backwards compatibiliy, old fashion config file
            if (
                "BsSignalShape" in myconfigfile
                or "DsSignalShape" in myconfigfile
                or "PIDKSignalShape" in myconfigfile
            ):
                newSignal = False
                print(
                    "--------------------------------------------------------"
                    "---------------------------------------"
                )
                print(
                    "[WARNING] You are using old fashion shape declaration. "
                    "It is not as flexible as the new."
                )
                print(
                    '[WARNING] Please consider using configdict["SignalShape"]'
                    '["BeautyMass"]["type"] instead'
                )
                print(
                    "--------------------------------------------------"
                    "---------------------------------------------"
                )
        if bkg == "CombBkg" or bkg == "Combinatorial":
            ### Backwards compatibiliy, old fashion config file
            if (
                "BsCombinatorialShape" in myconfigfile
                or "DsCombinatorialShape" in myconfigfile
                or "PIDKCombinatorialShape" in myconfigfile
            ):
                newCombo = False
                print(
                    "---------------------------------------------------"
                    "-------------------------------------------------"
                )
                print(
                    "[WARNING] You are using old fashion shape declaration. "
                    "It is not as flexible as the new."
                )
                print(
                    "[WARNING] Please consider using configdict"
                    '["CombinatorialShape"]["BeautyMass"]["type"] instead'
                )
                print(
                    "-------------------------------------------------"
                    "----------------------------------------------------"
                )

        ### Backwards compatibiliy, old fashion config file
        if bkg == "Signal" and newSignal == False:
            j = 0
            for name in obsName:
                prefix = "Signal_" + name
            workInt = readVariables(
                myconfigfile,
                myconfigfile["Binning"],
                keysSig[j],
                prefix,
                workInt,
                debug,
            )
            j = j + 1
        ### Backwards compatibiliy, old fashion config file
        if (bkg == "CombBkg" or bkg == "Combinatorial") and newCombo == False:
            j = 0
            for name in obsName:
                prefix = "CombBkg_" + name
                workInt = readVariables(
                    myconfigfile,
                    myconfigfile["Binning"],
                    keysComb[j],
                    prefix,
                    workInt,
                    debug,
                )
                j = j + 1
        if bkg + "Shape" in myconfigfile:
            bkg + "Shape"
            for name in obsName:
                if name in myconfigfile[bkg + "Shape"]:
                    prefix = bkg + "_" + name
                    workInt = readVariables(
                        myconfigfile[bkg + "Shape"],
                        myconfigfile["Binning"],
                        name,
                        prefix,
                        workInt,
                        debug,
                    )

    return workInt


def checkMerge(mode, pol, year, merge):
    m = list(mode)
    s = list(pol)
    y = list(year)

    if "pol" in merge:
        s = ["both"]

    if ("all" or "mode") in merge:
        modes = ["nonres", "phipi", "kstk", "kpipi", "kkpi", "pipipi"]
        for mode in modes:
            if mode in m:
                m.remove(mode)
        m.append("all")

    if "kkpi" in merge:
        if "nonres" and "phipi" and "kstk" in mode:
            m.remove("nonres")
            m.remove("phipi")
            m.remove("kstk")
            m.append("kkpi")
        else:
            print(
                "[ERROR] merge constains kkpi but nonres, phipi and kstk "
                "are not specified in mode. Make sure you know what you do."
            )
            exit(-1)

    if "run1" in merge:
        run1_years = ["2011", "2012"]
        for run1_year in run1_years:
            if run1_year in year:
                y.remove(run1_year)
        y.append("run1")

    if "run2" in merge:
        run2_years = ["2015", "2016", "2017", "2018"]
        for run2_year in run2_years:
            if run2_year in year:
                y.remove(run2_year)
        y.append("run2")

    for me in merge:
        if "2015" in me:
            y.remove("2015")
            if (me.find("20152016") > -1) or (me.find("20162015") > -1):
                y.remove("2016")
                y.append("20152016")
            elif (me.find("20152017") > -1) or (me.find("20172015") > -1):
                y.remove("2017")
                y.append("20152017")
            elif (me.find("20152018") > -1) or (me.find("20182015") > -1):
                y.remove("2018")
                y.append("20152018")
        elif "2016" in me:
            y.remove("2016")
            if (me.find("20162017") > -1) or (me.find("20172016") > -1):
                y.remove("2017")
                y.append("20162017")
            elif (me.find("20162018") > -1) or (me.find("20182016") > -1):
                y.remove("2018")
                y.append("20162018")
        elif "2017" in me:
            y.remove("2017")
            if (me.find("20172018") > -1) or (me.find("20182017") > -1):
                y.remove("2018")
                y.append("20172018")

    merge_map = {
        "20152016": {"wrong": "20162015"},
        "20152017": {"wrong": "20172015"},
        "20152018": {"wrong": "20182015"},
        "20162017": {"wrong": "20172016"},
        "20162018": {"wrong": "20182016"},
        "20172018": {"wrong": "20182017"},
    }

    for mm in merge_map:
        if merge_map[mm]["wrong"] in merge:
            merge.remove(merge_map[mm]["wrong"])
            merge.append(mm)

    if "runs" in merge:
        y = ["runs"]

    print(y)
    return s, m, y


def getSampleModeYear(mode, pol, year, debug):
    sm = []
    t = "_"
    print(mode)
    print(pol)
    print(year)
    for p in pol:
        for m in mode:
            for y in year:
                sm.append(p + t + m + t + y)

    stdvector = getStdVector(sm, debug)

    return stdvector


def getSampleYear(pol, year, debug):
    sy = []
    t = "_"
    for p in pol:
        for y in year:
            sy.append(p + t + y)

    stdvector = getStdVector(sy, debug)

    return stdvector


def getStdVector(python_list, debug):
    if len(python_list) != 0:
        stdlist = GeneralUtils.GetList(python_list[0])
    else:
        print("[ERROR] Python list empty")
        exit(-1)

    for i in range(1, len(python_list)):
        stdlist = GeneralUtils.AddToList(stdlist, python_list[i])

    print("[INFO] Std::vector: ")
    for i in range(0, len(python_list)):
        print("[INFO] ", stdlist[i])

    return stdlist


def getTopLevelSMY(mode, pol, year, merge):
    t = "_"
    if (mode[0] == "all") or ("all" in merge) or (len(mode) == 5):
        m = "all"
    elif (
        ("nonres" in merge)
        and ("phipi" in merge)
        and ("kstk" in merge)
        and (len(mode) == 3)
    ):
        m = "3modeskkpi"
    elif (mode[0] == "kkpi") or ("kkpi" in merge):
        m = "kkpi"
    elif len(mode) > 1:
        m = ""
        for mm in mode:
            m += mm + t
        m = m[:-1]
    else:
        m = mode[0]

    if (pol[0] == "both") or ("pol" in merge):
        p = "both"
    else:
        p = pol[0]

    if (
        (year[0] == "all")
        or (year[0] == "runs")
        or ("runs" in merge)
        or (("run1" in merge) and ("run2" in merge))
    ):
        y = "runs"
    elif (year[0] == "run1") or ("run1" in merge):
        y = "run1"
    elif (year[0] == "run2") or ("run2" in merge):
        y = "run2"
    elif len(year) > 1:
        y = ""
        for yy in year:
            y += yy
    else:
        y = year[0]

    return m, p, y


def checkDuplications(year, merge):
    merge_map = {
        "20152016": {"wrong": "20162015"},
        "20152017": {"wrong": "20172015"},
        "20152018": {"wrong": "20182015"},
        "20162017": {"wrong": "20172016"},
        "20162018": {"wrong": "20182016"},
        "20172018": {"wrong": "20182017"},
    }

    for mm in merge_map:
        if merge_map[mm]["wrong"] in merge:
            merge.remove(merge_map[mm]["wrong"])
            merge.append(mm)

    count = []

    for y in year:
        c = 0
        for mm in merge:
            if mm.find(y) > -1:
                c = c + 1
        count.append(c)

    check = False
    for c in count:
        if c > 1:
            check = True

    return check


def getBinCut(myconfigfile, bins, debug):
    dim = len(myconfigfile["Binning"])
    if len(myconfigfile["Binning"]) != len(bins):
        print(
            "[ERROR] The number of binning variables "
            "not equal to dimension of bins provided"
        )
        print(
            "[ERROR] If you want to reduce number of dimensions, "
            "provide -1 for skipped dimension"
        )

    i = 0
    cuts = ["None"] * dim
    for var in myconfigfile["Binning"]:
        if bins[i] != -1:
            cuts[i] = "{var} > {rd} && {var} < {ru}".format(
                var=var,
                rd=myconfigfile["Binning"][var]["Bins"][bins[i]][0],
                ru=myconfigfile["Binning"][var]["Bins"][bins[i]][1],
            )
        i = i + 1

    final_cut = ""
    first = False
    for c in cuts:
        if c == "None":
            continue
        else:
            if first == False:
                final_cut += c
                first = True
            else:
                final_cut += " && " + c

    if debug:
        print("[INFO] The binning cut is: ", final_cut)

    return final_cut
